<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "/third_party/PHPExcel/Classes/PHPExcel.php";
require_once APPPATH . "/third_party/PHPExcel/Classes/PHPExcel/IOFactory.php";

class Excel extends PHPExcel {

    public function __construct() {
        parent::__construct();
    }

    public function readExcel($filePath) {

        try {

            PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($filePath);
            $objWorksheet = $objPHPExcel->getActiveSheet();
            $highestRow = $objWorksheet->getHighestRow();
            $highestColumn = $objWorksheet->getHighestColumn();
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

            $rows = array();
            $head = array();

            for ($row = 0; $row <= 200; $row++) {

                $flag = false;

                if ($row == 1) {

                    for ($col = 0; $col <= 20; $col++) {

                        $field_name = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();

                        if ($field_name) {
                            $head[] = $field_name;
                        }
                    }
                } else {

                    foreach ($head as $col => $field) {

                        $value = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                        $rows[$row][$field] = $value;

                        if ($value) {
                            $flag = true;
                        }
                    }
                }

                if ($flag == false) {
                    unset($rows[$row]);
                }
            }
            
            return $rows;
            
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

}
