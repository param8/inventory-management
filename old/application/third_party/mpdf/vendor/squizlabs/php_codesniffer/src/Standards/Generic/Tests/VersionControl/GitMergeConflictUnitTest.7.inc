<!-- Test detecting merge conflicts in inline HTML. -->
<div class="abc">
	<p id="test-that">Another text string.</p>
</div>

<!-- Test detecting merge conflicts in inline HTML. -->
<div class="abc">
	<p id="test-that"><?= 'Another text string.'; ?></p>
</div>

<?= $text; ?>
