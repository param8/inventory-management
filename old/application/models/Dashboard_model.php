<?php 

class dashboard_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}


   function vendors_count($condition){
	  $this->db->select('*');
	  $this->db->from('users');
	  $this->db->where($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }

  function total_expense($condition){
    $this->db->select_sum('expence');
    $this->db->from('expences');
    $this->db->where($condition);
    return $this->db->get()->row();    
  }

  function total_product($condition){
      $this->db->select('*');
	  $this->db->from('products');
	  $this->db->where($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
  }

  function total_project($condition){
    $this->db->select('*');
    $this->db->from('projects');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->num_rows();
  }

  function total_enquiry($condition){
    $this->db->select('*');
    $this->db->from('quotations_inquiry');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->num_rows();
  }

  function total_order($condition){
    $this->db->select('*');
    $this->db->from('quotation');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->num_rows();
  }

  

}