<?php 
class Dashboard extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
		$this->load->model('auth_model');
		$this->load->model('dashboard_model');
	}

	public function index()
	{	 
		$data['title'] = 'Dashboard';
		$data['total_vendor'] = $this->dashboard_model->vendors_count(array('user_type'=>4));
		$data['total_expense'] = $this->dashboard_model->total_expense(array('status'=>1));
		$data['total_product'] = $this->dashboard_model->total_product(array('status'=>1));
		$data['total_project'] = $this->dashboard_model->total_project(array('status<>'=>0));
		//print_r($_SESSION['id']);die;
		$data['total_enquiry'] = $this->dashboard_model->total_enquiry(array('assigned_to'=>$_SESSION['id']));
		$data['total_orders'] = $this->dashboard_model->total_order(array('quote_by'=>$_SESSION['id']));
		// $condition = $this->session->userdata('user_type') == 'Admin' ? array('customers.status'=>1) :  array('customers.created_by'=>$this->session->userdata('unique_id'),'customers.status'=>1);
		// if($this->session->userdata('booked_date')){
		// }else{
		// 	$data['customers'] = array();
		// }
		//print_r($data);die;
	  $this->template('dashboard',$data);
		
	}


  public function customer_bar_chart(){
   $months = array('01'=>'Jan', '02'=>'Feb', '03'=>'Mar', '04'=>'Apr', '05'=>'May', '06'=>'Jun', '07'=>'Jul', '08'=>'Aug', '09'=>'Sep', '10'=>'Oct', '11'=>'Nov', '12'=>'Dec');
   

  }


}