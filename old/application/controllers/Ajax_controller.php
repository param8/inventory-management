<?php 
class Ajax_controller extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
      $this->load->model('user_model');
      $this->load->model('setting_model');

	}
   public function get_state(){
      $countryID = $this->input->post('countryID');
      $states = $this->Common_model->get_state(array('country_id'=>$countryID));
      if(count($states) > 0){ ?>
       <option value="">Select State</option>
       <?php foreach( $states as  $state){?>
       <option value="<?=$state->id?>"><?=$state->name?></option>
       <?php }
       }else{?> 
       <option value="">No State found</option>
       <?php
       }
   }
   

	public function get_city()
    {
      $cityID = "";
      if($this->input->post('cityID')){
        echo $cityID =  $this->input->post('cityID');
      }
       $stateID =  $this->input->post('stateID');
       $cities = $this->Common_model->get_city(array('state_id'=>$stateID));
       if(count($cities) > 0)
       {
        ?>
        <option value="">Select City</option>
        <?php foreach($cities as $city){ ?>
           <option value="<?=$city->id?>" <?=$cityID==$city->id ? 'selected' : ''?>><?=$city->name?></option>
        <?php
        }
       }else
       {
        ?>
        <option value="">No City found</option>
        <?php
       }
    }

  }
      

   
