<?php
class Setting extends MY_Controller 
{
    public function __construct()
    {
      parent::__construct();
     $this->load->model('Project_model');
     $this->load->library('csvimport');
    }


public function Upload_Client($neg=0){
  if(null !== $this->input->post('user_name') && $this->input->post('user_name') != ''){
    $user_id= $this->input->post('user_name');  
  }else{
    $user_id= $this->input->post('user_id');
  }
  
  if (isset($_FILES["fileURL"])) {
    $config['upload_path']   = "uploads/clientimport/";
    $config['allowed_types'] = 'text/plain|text/csv|csv';
    $config['max_size']      = '2048';
    $config['file_name']     = $_FILES["fileURL"]['name'];
    $config['overwrite']     = TRUE;
    $this->load->library('upload', $config);
    $this->upload->initialize($config);
    if (!$this->upload->do_upload("fileURL")) {
      echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
     exit();
    } else {
      $file_data = $this->upload->data();
      $file_path = 'uploads/clientimport/' . $file_data['file_name'];
      if ($this->csvimport->get_array($file_path)) {
        $csv_array = $this->csvimport->get_array($file_path);
        // check if any duplicate record exist in system
        $duplicate_found = false;
        $err_msg = '';
        // foreach ($csv_array as $key => $row) {
        //   $where = array(
        //     'company_contact' => $row['company_contact']
        //   );

        //   $is_already_exist =  $this->Model_Registration->IsClientAlreadyExist($where);
        //   if($is_already_exist)
        //   {
        //     $duplicate_found = true;
        //     $err_msg .= '<p>'.$is_already_exist['contact_person_name'].' - '.$is_already_exist['company_contact'].'</p>';
        //   }
        // }

        // if($duplicate_found)
        // {
        //   $err_msg .= '<p> Above leads already exist in system. Please remove them from sheet.</p>';
        //   $this->session->set_flashdata('error', $err_msg);
        //   if($neg=='1')
        //   {
        //     redirect('Controller_Enquiry/New_Enq_Generated');
        //   }else{
        //     redirect('Controller_Registration/addclient');
        //   } 
        // }
        // end check for duplicate
        foreach ($csv_array as $key => $row) {
            $where = array(
            'company_contact' => $row['company_contact']
          );
            $is_already_exist =  $this->Model_Registration->IsClientAlreadyExist($where);
          if($is_already_exist)
          {
            continue;
          }
          if(!empty($row['company_email']) || !empty($row['company_contact'])){
         $date = date('Y-m-d');
         $lead_status = array('HOT'=>'1','COLD'=>'2','WARM'=>'3','CONFIRM'=>'4','CNP'=>'5','LEAP'=>'6','NEW'=>'7');
         $lead_statusValue= !empty($row['lead_status'])?strtoupper($row['lead_status']):'NEW';
         $lead_source= !empty($row['lead_source'])?$row['lead_source']:'Facebook';

         $contact_person = preg_replace('/[^A-Za-z0-9\- ]/', '', $row['contact_person_name']);
         
         if($contact_person == '')
         {
            if(null != $row['company_email'])
            {
              $arr = explode("@", $row['company_email'], 2);
              $contact_person = $arr[0];   
            }else{
              $contact_person = 'need_to_update';  
            } 
         }
            $insert_data = array(
            'date' => $date,
            'company_name' => $row['company_name'],
            'contact_person_name' => $contact_person,
            'company_email' =>  $row['company_email'],
            'company_contact' => $row['company_contact'],
            'company_alt_contact' => $row['alternate_contact'],
            'requirement' => $row['requirement'],
            'user_id' => $user_id,
            'status' => !empty($row['status'])?$row['status']:'1',
            'lead_type' => $row['lead_type'],
            'lead_source' => $lead_source,
            "lead_status" => $lead_status[$lead_statusValue],
            'assigned_by' => $this->session->userdata('user_id'),
            'assigned_date_time' => date('Y-m-d H:i:s')
          );
          $insert_id = $this->Model_Registration->Insertclient($insert_data);
          $counter = (count($row)-9)/2;
          //echo $row['Followupdate1'];
          for($i=1;$i<=$counter;$i++){
            if(null !== $row['Followupdate'.$i] && $row['Followupdate'.$i] !='')
            {
               $followupDate = $row['Followupdate'.$i];
               $currdate = date('Y-m-d');
               $statusone;
               if($currdate>$followupDate){
                $statusone=1;
               }else{
                $statusone=0;
               }
               $statusone;

                //$followupDate = date('Y-m-d',strtotime($row['Followupdate'.$i]));
                $insert_followup = array(
                  'enquiry_id' => $insert_id,
                  'remark' => $row['Followup'.$i],
                  'date' => !empty($followupDate)?date('Y-m-d',strtotime($followupDate)):$date,
                  'status' => $statusone,
                  'statusone' =>  $lead_status[$lead_statusValue],
                  // 'date' => $date,
                   'user_id' => $user_id,
                  // 'quote' => $row['quotation'.$i],
                  // 'PO' => $row['advance'.$i],
                );
                $this->db->insert('table_followup',$insert_followup);
            }

          }
        }
      }

      // send mail to both assign_to and assigned_by user
      // send mail to user
        $user_info = $this->Model_Registration->getUserInfo($user_id);
        $user_name = $user_info['name'];
        $to = $user_info['email'];
        $subject = 'New Lead Assigned';
        
        $bodyMessage  = 'Hi '.$user_name.',<br><br>';
        $bodyMessage .= 'A new lead has been assigned to you. Please take required action.';
        $bodyMessage .= '<br><br><br>';
        $bodyMessage .= 'Thanks,';

        $mail = sendMail($to, $subject, $bodyMessage);

        // send mail to assign by user
        if($user_id != $this->session->userdata('user_id'))
        {
          $assign_by_info = $this->Model_Registration->getUserInfo($this->session->userdata('user_id'));
          $assign_by_name = $assign_by_info['name'];
          $assign_by_email = $assign_by_info['email'];
          $assign_by_subject = 'Lead Successfully Assigned';
          
          $bodyMessage1  =  'Hi '.$assign_by_name.',<br><br>';
          $bodyMessage1 .= 'Lead has been assigned successfully to '.$user_name.'.';
          $bodyMessage1 .= '<br><br><br>';
          $bodyMessage1 .= 'Thanks,';

          $mail = sendMail($assign_by_email, $assign_by_subject, $bodyMessage1);
        }
        
         if($duplicate_found)
        {
          $err_msg .= '<p> Above leads already exist in Database.</p>';
          $this->session->set_flashdata('error', $err_msg);
          
        }
        else{
            $this->session->set_flashdata('success', 'Client Added Successfully');
        }
        if($neg=='1')
          {
            redirect('Controller_Enquiry/New_Enq_Generated');
          }else{
            redirect('Controller_Registration/addclient');
          } 
        //echo json_encode(['status'=>200, 'message'=>'Application Uploaded Successfully']);
      } 
    }
  } 
}

}

?>