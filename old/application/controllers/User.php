<?php 
class User extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
    $this->load->library('encryption');
		$this->load->model('auth_model');
		$this->load->model('user_model');
	}

	public function index()
	{	 
    $menuID = 3;
		$permission = $this->permissions()->$menuID;
    if($permission[0]=='view'){
      $role = base64_decode($this->uri->segment(2));    
      $data['permission'] = $permission; 
      $data['title'] = $role; 
      $this->template('user/user',$data);
    }else{
      redirect(base_url('dashboard'));
     }
    
	}

  public function profile()
	{	 
    //$role = base64_decode($this->uri->segment(2));  
    $id = $this->session->userdata('id');
		$data['title'] = "Profile"; 
    $data['states'] = $this->Common_model->get_state(array('country_id'=>101));
    $data['user_data'] = $this->user_model->get_user(array('id'=>$id));
    $data['vendor_data'] = $this->user_model->get_vendor(array('vendor.vendor_id'=>$id));
	  $this->template('user/profile',$data);
	}
	
	 public function password(){
    $data['title'] = "Change Password"; 
    $this->template('user/change-password',$data);
  }

  public function changePassword(){
    //print_r($_POST);die;
    $old_pas = base64_encode($this->input->post('opassword'));
    $new_pass = base64_encode($this->input->post('cpassword'));
    $user_id = $this->session->userdata('id');
    $user = $this->user_model->get_user(array('id'=>$user_id));
  
  if($user->password != $old_pas){
    echo json_encode(['status'=>403, 'message'=>'Current Password is wrong please Enter Correct Password']); 
        exit();
  }
  $update_user = $this->user_model->update_user(array('password'=>$new_pass),$user_id); 
      if($update_user){
      echo json_encode(['status'=>200, 'message'=>'Password Changed successfully!']);
      }else{
      echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
    }
  
  }

  public function createUser(){
    $menuID = 3;
		$permission = $this->permissions()->$menuID;
    if($permission[1]=='add'){
    $data['title'] = 'Create User';
    $data['locations'] = $this->Common_model->get_create_locations(array('location.status'=>1));
   // print_r($data['locations']);die;
    $data['user_roles'] = $this->user_model->get_condition_roles();
    $this->template('user/create_user',$data);
    }else{
    redirect(base_url('dashboard'));
   }
  }

  

  
  
public function ajaxUserTable(){
  $usermenuID = 3;
  $vendormenuID = 2;
  $userpermission = $this->permissions()->$usermenuID;
  $vendorpermission = $this->permissions()->$vendormenuID;
  
   $role = $this->input->post('role');
    $condition = array('users.status'=>1,'roles.role'=>$role);
    $users = $this->user_model->make_datatables($condition); // this will call modal function for fetching data
    //print_r($users); die;
    $data = array();
    foreach($users as $key=>$user) // Loop over the data fetched and store them in array
    {
        $button = '';
        $sub_array = array();
        $profile_pic = $user['profile_pic'];
        if($user['user_type']!=4 && $userpermission[2]=='edit'){
          $button .='<a href="'.base_url('edit-user/'.base64_encode($user['id'])).'"  title="Edit User" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
        }
        if($user['user_type']==4 && $userpermission[2]=='edit'){
          $button .='<a onclick="editUserModal('.$user['id'].')"  title="Edit User" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
        }
        //$button .= ()?'<a href="'.base_url('edit-user/'.base64_encode($user['id'])).'"  title="Edit User" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </a>':'<a onclick="editUserModal('.$user['id'].')"  title="Edit User" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
        if($user['user_type']!=4 && $userpermission[3]=='delete'){
          $button .='<a href="javascript:void(0)" onclick="delete_user('.$user['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete User" class="btn  btn-sm  text-danger"><i class="fa fa-trash-o"></i> </a>';
        }
        if($user['user_type']==4 && $userpermission[3]=='delete'){
          $button .='<a href="javascript:void(0)" onclick="delete_user('.$user['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete User" class="btn  btn-sm  text-danger"><i class="fa fa-trash-o"></i> </a>';
        }
        //$button .= '<a href="javascript:void(0)" onclick="delete_user('.$user['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete User" class="btn  btn-sm  text-danger"><i class="fa fa-trash-o"></i> </a>';
        $sub_array[] = $key+1;
        $sub_array[] = '<img src="'.base_url($profile_pic).'" height="50" width="50">';
        $sub_array[] = $user['name']; 
        $sub_array[] = $user['email'];
        $sub_array[] = $user['phone'];
        if($this->session->userdata('role_id')==1){
        $sub_array[] = base64_decode($user['password']);
        }
        $sub_array[] = date('d-F-Y', strtotime($user['created_at']));
        $sub_array[] = $button;
        $data[] = $sub_array;  
    }
        $output = array(
        "draw"                    =>     intval($_POST["draw"]),
        "recordsTotal"            =>     $this->user_model->get_all_data($condition),
        "recordsFiltered"         =>     $this->user_model->get_filtered_data($condition),
        "data"                    =>     $data
        );
       echo json_encode($output);

  }



public function create_user(){
  $view_permission =array();
  $edit_permission =array();
  $delete_permission =array();
  $approve_permission =array();
  $name = $this->input->post('name'); 
  $email = $this->input->post('email');
  $password = $this->input->post('password');
  $user_type = $this->input->post('user_type');
  $location = implode(',',$this->input->post('location'));
  $menus = $this->input->post('menu');
  $view_permission = $this->input->post('view');
  $edit_permission = $this->input->post('edit');
  $delete_permission = $this->input->post('delete');
  $approve_permission = $this->input->post('approve');
  $json_permission = array();
  $view = '';
  $edit = '';
  $delete = '';
  $approve = '';
  $excel = '';
 foreach($menus as $key=>$menu){
  //echo "<pre>";
  // echo $key;
  $view = !empty($view_permission) && array_key_exists($key,array_filter($view_permission)) ? $view_permission[$key] :'' ;
  $edit = !empty($edit_permission) && array_key_exists($key,array_filter($edit_permission)) ? $edit_permission[$key] : '';
  $delete = !empty($delete_permission) && array_key_exists($key,array_filter($delete_permission)) ? $delete_permission[$key] : '';
  $approve = !empty($approve_permission) && array_key_exists($key,array_filter($approve_permission)) ? $approve_permission[$key] : '';
  $excel = !empty($approve_permission) && array_key_exists($key,array_filter($excel_permission)) ? $excel_permission[$key] : '';
  $json_permission[$key] = array($view,$edit,$delete,$approve,$excel);
 }
  $data_permission =array();
 foreach($json_permission as $key_array=>$row){
  $data_permission[$key_array] = $row;
    //print_r(array_filter($row));
 }
 //print_r($data_permission);
   
 $menu_json = json_encode($json_permission);
 //print_r($menu_json);
  if(empty($name)){
    echo json_encode(['status'=>403, 'message'=>'Please enter your name']); 
    exit();
  }

  if(empty($email)){
    echo json_encode(['status'=>403, 'message'=>'Please enter your email']); 
    exit();
  }
   $checkEmail = $this->user_model->get_user(array('email'=>$email));
		if($checkEmail){
			echo json_encode(['status'=>403,'message'=>'This email is already in use']);
			exit();
		}
    if(empty($password)){
      echo json_encode(['status'=>403, 'message'=>'Please enter Password']); 
      exit();
    }
    if(empty($user_type)){
      echo json_encode(['status'=>403, 'message'=>'Please Select Usertype']); 
      exit();
    }
    if(empty($location)){
			echo json_encode(['status'=>403, 'message'=>'Please select location']); 	
			exit();
		}
    $adminId = $this->session->userdata('id');
    
    $data = array(
    'adminID' => $adminId,
    'name' =>  $name,
    'email' => $email,
    'profile_pic' => 'public/dummy_image.jpg',
    'password' => base64_encode($password),
    'user_type' => $user_type,
    'permission' => $menu_json,
    'location' => $location,
    );
   //print_r($data); die;
    $add_user = $this->user_model->store_user($data); 
    if($add_user){
    echo json_encode(['status'=>200, 'message'=>'User Create successfully!']);
    }else{
    echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
    }

}


  public function edit_user(){
    $id = base64_decode($this->uri->segment(2)); 
    $data['title'] = 'Edit User';
    $data['user'] = $this->user_model->get_user(array('users.id' => $id));
    $data['locations'] = $this->Common_model->get_locations(array('location.status'=>1));
    $this->template('user/edit-user',$data);
  }

public function update_user(){
  $userID = $this->input->post('userID');
  $name = $this->input->post('name'); 
  $password = $this->input->post('password'); 

  if(empty($name)){
    echo json_encode(['status'=>403, 'message'=>'Please Enter your name']); 
    exit();
  }

  if(empty($password)){
    echo json_encode(['status'=>403, 'message'=>'Please Enter Password']); 
    exit();
  }

    $menus = $this->menus();
    $permission = array();
		foreach($menus as $menu){
		
			$view = !empty($this->input->post('view')[$menu['id']]) ? $this->input->post('view')[$menu['id']] : '';
			$add = !empty($this->input->post('add')[$menu['id']]) ? $this->input->post('add')[$menu['id']] : '';
			$edit = !empty($this->input->post('edit')[$menu['id']]) ? $this->input->post('edit')[$menu['id']] : '';
			$delete = !empty($this->input->post('delete')[$menu['id']]) ? $this->input->post('delete')[$menu['id']] : '';
			$approve = !empty($this->input->post('approve')[$menu['id']]) ? $this->input->post('approve')[$menu['id']] : '';
			$excel = !empty($this->input->post('excel')[$menu['id']]) ? $this->input->post('excel')[$menu['id']] : '';
			$excel = !empty($this->input->post('excel')[$menu['id']]) ? $this->input->post('excel')[$menu['id']] : '';
			$permission[$menu['id']] = array($view,$add,$edit,$delete,$approve,$excel);
// 			$permission[$menu['id']] = array($view,$add,$edit,$delete,$approve,$excel);
			
		}
		$permission_json = json_encode($permission);
   
    $data = array(
    'name' =>  $name,
    'password' => base64_encode($password),
    'permission' => $permission_json
    );
   //print_r($data); die;
    $update_user = $this->user_model->update_user($data,$userID); 
    if($update_user){
    echo json_encode(['status'=>200, 'message'=>'User Update successfully!']);
    }else{
    echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
  }

}


public function update_profile(){
  $name = $this->input->post('name');
  $email = $this->input->post('email');
  $phone = $this->input->post('phone');
  $state = $this->input->post('state');
  $city = $this->input->post('city');
  $pincode = $this->input->post('pincode');
  $Id =  $this->session->userdata('id');
  $this->load->library('upload');
		if($_FILES['profile_pic']['name']!= ''){
      //echo "hello";die;
			//echo $_FILES['profile_pic']['name'] ; die;
			$config = array(
				'upload_path' => 'uploads/user',
				'file_name' => str_replace(' ','',$name).uniqid(),
				'allowed_types' => 'jpg|png|jpeg|gif',
				'max_size' => '10000000',
			);
			$this->upload->initialize($config);
			if(!$this->upload->do_upload('profile_pic'))
			{
        $error = $this->upload->display_errors();
        echo json_encode(['status'=>403, 'message'=>$error]);
			  exit();
			}
			else{
				$type = explode('.',$_FILES['profile_pic']['name']);
				$type = $type[count($type)-1];
				$image = 'uploads/user/'.$config['file_name'].'.'.$type;
			}
      //print_r($image);die;
		}else{
      //echo "hi";die;
      $user_data = $this->user_model->get_user(array('id'=>$id));
      //print_r($vendor_data->profile_pic);die;
      $image = $user_data->profile_pic ;
		}
  $data = array(
    'name' => $name,
    'email' =>  $email,
    'phone' => $phone,
    'profile_pic' => $image,
    'state' => $state,
    'city' => $city,
    'pincode' => $pincode,
    );
    //print_r($_SESSION);die;
      $update_profile = $this->user_model->update_user($data,$Id); 
      $this->session->set_userdata('profile_pic',$image);
      if($update_profile){
        echo json_encode(['status'=>200, 'message'=>'Profile Update successfully!']);
      }else{
      echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
      }

}



public function get_user(){
  $userID  = $this->input->post('userID');
  $user = $this->user_model->get_user(array('id'=>$userID));
  echo json_encode($user);
}


 
public function delete_user(){
  $userID =  $this->input->post('userID');
	$user = $this->user_model->delete_user(array('id' => $userID));
	if($user){
		echo json_encode(['status'=>200, 'message'=>'user successfully deleted']);
	   }else{
		  echo json_encode(['status'=>302, 'message'=>'Something went wrong']);   
	   }
 }

//  Vendor



public function get_vendor(){
  $userID  = $this->input->post('userID');
  $vendor = $this->user_model->get_vendor(array('users.id'=>$userID));
  //print_r($vendor);
  echo json_encode($vendor);
}

public function ajaxVendorTable(){
  $condition = array('status'=>1,'user_type' =>4);
  $users = $this->user_model->make_datatables($condition); // this will call modal function for fetching data
  //print_r($users); die;
  $data = array();
  foreach($users as $key=>$user) // Loop over the data fetched and store them in array
  {
      $button = '';
      $sub_array = array();
      $profile_pic = $user['profile_pic'];
      $button .= '<button type="button" onclick="editUserModal('.$user['id'].')" title="Edit User" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </button>';
      $button .= '<a href="javascript:void(0)" onclick="delete_user('.$user['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete User" class="btn  btn-sm  text-danger"><i class="fa fa-trash-o"></i> </a>';
      $sub_array[] = $key+1;
      $sub_array[] = '<img src="'.base_url($profile_pic).'" height="50" width="50">';
      $sub_array[] = $user['name']; 
      $sub_array[] = $user['email'];
      $sub_array[] = $user['phone'];
      $sub_array[] = date('d-F-Y', strtotime($user['created_at']));
      $sub_array[] = $button;
      $data[] = $sub_array;  
  }
      $output = array(
      "draw"                    =>     intval($_POST["draw"]),
      "recordsTotal"            =>     $this->user_model->get_all_data($condition),
      "recordsFiltered"         =>     $this->user_model->get_filtered_data($condition),
      "data"                    =>     $data
      );
     echo json_encode($output);

}

public function createVendor(){
  $menuID = 2;
  $permission = $this->permissions()->$menuID;
  if($permission[0]=='view'){
    $role = base64_decode($this->uri->segment(2));    
    $data['permission'] = $permission; 
    $data['title'] = 'Vendor';
    $data['roles'] = $this->user_model->get_condition_roles();
    $data['states'] = $this->Common_model->get_state(array('country_id'=>101));
    //print_r($data);die;
    $this->template('user/create-vendor',$data);
  }else{
    redirect(base_url('dashboard'));
   }
 
}

public function update_vendor(){
  //print_r($_POST);die;
  $gst = $this->input->post('gst');
  $adhar = $this->input->post('adhar'); 
  $pan = $this->input->post('pan');
  $id = $this->session->userdata('id');
  if(empty($gst)){
    echo json_encode(['status'=>403, 'message'=>'Please Enter GST']); 
    exit();
  }
  if(empty($adhar)){
    echo json_encode(['status'=>403, 'message'=>'Please Enter Adhar']); 
    exit();
  }
  if(empty($pan)){
    echo json_encode(['status'=>403, 'message'=>'Please Enter PAN']); 
    exit();
  }

  $data = array(
    'vendor_id' => $id,
    'gst' =>  $gst,
    'adhar' => $adhar,
    'pancard' => $pan,
    );
    $user = $this->user_model->get_vendor(array('vendor_id'=>$id));
    if(empty($user)){
      //echo "hi";die;
      $update_vendor = $this->user_model->insert_vendor($data); 
    }else{
      $update_vendor = $this->user_model->update_vendor($data,$userID); 

    }

    if($update_vendor){
      echo json_encode(['status'=>200, 'message'=>'Vendor Data Update successfully!']);
      }else{
      echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
    }


}


public function edit_vendor(){
	//print_r($_POST);die;
  $id = $this->input->post('edit_userId');
	$vendor_code = $this->input->post('edit_vendor_code');
	$name = $this->input->post('edit_name');
	$email = $this->input->post('edit_email'); 
	$password = $this->input->post('edit_password');
	$phone = $this->input->post('edit_phone');
	$address = $this->input->post('edit_address');
	$state = $this->input->post('edit_state');
	$city = $this->input->post('edit_city');
	$user_type = $this->input->post('edit_user_type');
	$gst = $this->input->post('edit_gst');
	$adhar = $this->input->post('edit_adhar');
  $pancard = $this->input->post('edit_pancard');

	if(empty($name)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your name']); 	
		exit();
	}
	if(empty($email)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
		exit();
	}
		// if(empty($password)){
		// 	echo json_encode(['status'=>403, 'message'=>'Please enter password']); 	
		// 	exit();
		// }
		if(empty($phone)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your mobile']); 	
			exit();
		}
		if(empty($address)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your address']); 	
			exit();
		}
		if(empty($state)){
			echo json_encode(['status'=>403, 'message'=>'Please select your State']); 	
			exit();
		}
		if(empty($city)){
			echo json_encode(['status'=>403, 'message'=>'Please select your City']); 	
			exit();
		}
		// if($user_type == 'vendor'){
		// 	if(empty($gst)){
		// 		echo json_encode(['status'=>403, 'message'=>'Please enter your GST number']); 	
		// 		exit();
		// 	}
		// 	if(empty($adhar)){
		// 		echo json_encode(['status'=>403, 'message'=>'Please enter your Adhar number']); 	
		// 		exit();
		// 	}
		// 	if(empty($pancard)){
		// 		echo json_encode(['status'=>403, 'message'=>'Please enter your PANARD number']);  	
		// 		exit();
		// 	}
		// }

		$this->load->library('upload');
		if($_FILES['profile_pic']['name']!= ''){
			//echo $_FILES['profile_pic']['name'] ; 
			$config = array(
				'upload_path' => 'uploads/user',
				'file_name' => str_replace(' ','',$name).uniqid(),
				'allowed_types' => 'jpg|png|jpeg|gif',
				'max_size' => '10000000',
			);
			$this->upload->initialize($config);
			if(!$this->upload->do_upload('profile_pic'))
			{
        $error = $this->upload->display_errors();
        echo json_encode(['status'=>403, 'message'=>$error]);
			  exit();
			}
			else{
				$type = explode('.',$_FILES['profile_pic']['name']);
				$type = $type[count($type)-1];
				$image = 'uploads/user/'.$config['file_name'].'.'.$type;
			}
		}else{
			//$image = 'public/dummy_image.jpg';
      $vendor_data = $this->user_model->get_user(array('id'=>$id));
      //print_r($vendor_data->profile_pic);die;
      $image = $vendor_data->profile_pic ;
		}
		//$permission = '{"6":["view","edit","delete",""],"7":["view","edit","delete",""],"8":["view","edit","delete","approve"]}';
		 $data = array(
			 'name' => $name,
			 'email' => $email,
			 'phone' => $phone,
			 'address' => $address,
			 'state' => $state,
			 'city' => $city,
			 'profile_pic' => $image,
			 //'password' => md5($password),
			 //'permission' => $permission,
		 );
//print_r($data);die;
		 $update_user = $this->user_model->update_user($data,$id);
		 if($update_user){
        $vendorData = array(
          'vendor_code'    => $vendor_code,
		      'gst'            => $gst,
          'adhar'          => $adhar ,
          'pancard'        => $pancard ,
        );
        //print_r($vendorData);die;
      $vendor_update =  $this->user_model->update_vendor($vendorData,$id);
      if($vendor_update){
        echo json_encode(['status'=>200, 'message'=>'Vendor Updated successfully!']);
      }else{
        echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
      }
    }
	}


// Roles

public function role()
	{	 $menuID = 1;
		$permission = $this->permissions()->$menuID;
		if($permission[0]=='view'){     
      $data['permission'] = $permission;
      $data['title'] = 'User Role'; 
      $this->template('user/role',$data);
   }else{
		redirect(base_url('dashboard'));
	 }
		
	}

  public function export_roles(){

      $this->load->library('excel');
  
      $object = new PHPExcel();
      $object->setActiveSheetIndex(0);
      $table_columns = array("S.No","Role", "Date");
  
      $column = 0;
  
      foreach($table_columns as $field) {
          $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
          $column++;
      }
      
      $roles_data = $this->user_model->get_roles(array('roles.status'=>1));
      //$expence_data = $this->Project_model->get_expences(array('status'=>1));
      //print_r($roles_data);die;
      $excel_row = 2;
      $count = 1;
      foreach($roles_data as $row) {
          $month_year = date('dd mm yy',strtotime($row['created_at']));
          $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $count++);
          $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['role']);
          $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $month_year);
         
          $excel_row++;
      }
      $this_date = "Roles".date("YmdHis");
      $filename= $this_date.'.csv'; //save our workbook as this file name
      header('Content-Type: application/vnd.ms-excel; charset=UTF-8'); //mime type
      header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
      header('Cache-Control: max-age=0'); //no cache
  
      $objWriter = PHPExcel_IOFactory::createWriter($object, 'CSV');
      //ob_end_clean();
      $objWriter->save('php://output');
  
  }

  public function vendor(){
    $data['title'] = 'Vendor'; 
	  $this->template('user/vendor',$data);
  }



public function ajaxRoleTable(){
  //print_r($_POST);die;
  
  $condition = $this->session->userdata('role_filter')==2 ? array() : array('status'=>$this->session->userdata('role_filter'));
  $roles = $this->user_model->make_role_datatables($condition); // this will call modal function for fetching data
  //print_r($users); die;
  $data = array();
  foreach($roles as $key=>$role) // Loop over the data fetched and store them in array
  {
      $button = '';
      $sub_array = array();
      $button .= '<button type="button" onclick="editRoleModal('.$role['id'].')" title="Edit User" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </button>';
      $button .= '<a href="javascript:void(0)" onclick="delete_role('.$role['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete User" class="btn  btn-sm  text-danger"><i class="fa fa-trash-o"></i> </a>';
      $sub_array[] = $key+1;
      $sub_array[] = $role['role']; 
      $active = "<span class='text-success'>Active</span>";
      $Deactive = "<span class='text-danger'>Deactive</span>";
      $sub_array[] = ($role['status']== 1)?$active:$Deactive;
      $sub_array[] = date('d-F-Y', strtotime($role['updated_at']));
      $sub_array[] = $button;
      $data[] = $sub_array;  
  }
      $output = array(
      "draw"                    =>     intval($_POST["draw"]),
       "recordsTotal"            =>     $this->user_model->get_all_role_data($condition),
       "recordsFiltered"         =>     $this->user_model->get_role_filtered_data($condition),
      "data"                    =>     $data
      );
     echo json_encode($output);

}

public function get_role(){
  $roleID  = $this->input->post('roleID');
  $role = $this->user_model->get_role(array('id'=>$roleID));
  echo json_encode($role);
}

public function add_role(){
  //print_r($_POST);
  $role = $this->input->post('role'); 
  $status = $this->input->post('status');
  if(empty($role)){
    echo json_encode(['status'=>403, 'message'=>'Please Enter Role']); 
    exit();
  }

  $checkRole = $this->user_model->get_role(array('role'=>$role)); 
  if($checkRole){
    echo json_encode(['status'=>403,'message'=>'This Role is already exist']);
    exit();
  }

  
  if($status==" "){
    echo json_encode(['status'=>403, 'message'=>'Please Select Status']); 
    exit();
  }
  $data = array(
    'role' => $role,
    'status' =>  $status,
    );
    //print_r($data);die;
    $add_role = $this->user_model->store_role($data); 
    if($add_role){
    echo json_encode(['status'=>200, 'message'=>'Role Added successfully!']);
    }else{
    echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
    }
}

 public function edit_role(){
  $roleID = $this->input->post('edit_roleId');
  $role = $this->input->post('edit_role'); 
  $status = $this->input->post('edit_status');

  if(empty($role)){
    echo json_encode(['status'=>403, 'message'=>'Please Enter Role']); 
    exit();
  }


  $checkRole = $this->user_model->get_role(array('role'=>$role,'id<>'=>$roleID));
  if($checkRole){
    echo  json_encode(['status'=>403, 'message'=>'This Role Already Exist']);
    exit();
  }

  if($status==''){
    echo  json_encode(['status'=>403, 'message'=>'Please Select Status']);
    exit();
  }
    $data = array(
    'role' =>  $role,
    'status' => $status,
    );
   //print_r($data); die;
    $update_role = $this->user_model->update_role($data,$roleID); 
    if($update_role){
    echo json_encode(['status'=>200, 'message'=>'Role Update successfully!']);
    }else{
    echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
  }

}


public function delete_role(){
  $roleID =  $this->input->post('roleID');
	$role = $this->user_model->delete_role(array('id' => $roleID));
	if($role){
		echo json_encode(['status'=>200, 'message'=>'Role successfully deleted']);
	   }else{
		  echo json_encode(['status'=>302, 'message'=>'Something went wrong']);   
	   }
 }


 function setRoleFilterSession(){
  $type = $this->input->post('type');

    $this->session->set_userdata('role_filter',$type);
 
 }
 
}