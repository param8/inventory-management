<?php 
class MY_Controller extends CI_Controller 
{
	public function __construct() 
	{
	    
	   parent::__construct();
	   $this->load->model('user_model');
	   $this->load->library('csvimport');
     	$this->load->model('Common_model');
		 $this->load->helper('mail_helper');
		 if(empty($this->session->userdata('logged_in'))) {
			$session_data = array('logged_in' => FALSE);
			$this->session->set_userdata($session_data);
		}
	}

	public function logged_in()
	{
		
		$session_data = $this->session->userdata();
		//print_r($session_data);die;
		if($session_data['logged_in'] == TRUE) {
			if($session_data['user_type'] == 'Admin' || $session_data['user_type']=='Staff')
			{
				redirect('dashboard', 'refresh');	
			}else{
				redirect('customer', 'refresh');
			}	
		}
	}

	public function not_logged_in()
	{	
		$session_data = $this->session->userdata();
		if($session_data['logged_in'] == FALSE) {
			redirect('Authantication', 'refresh');
		}
	}

	public function not_admin_logged_in()
	{
		
		$session_data = $this->session->userdata();
		//print_r($session_data);die;
		if($session_data['logged_in'] == FALSE) {
			redirect('Authantication', 'refresh');
		}
	}
	public function siteinfo(){
		$siteinfo = $this->Common_model->get_site_info();
	  return $siteinfo;
    
	 }

	 public function menus(){
		$menus = $this->Common_model->get_menues(array('status'=>1));
	  return $menus;
    
	 }

	 public function roles(){
		$roles = $this->user_model->get_roles(array('status'=>1,'id <>'=>1));
	  return $roles;
    
	 }

	//  public function permissions(){
	// 	$permissions_key = !empty($this->session->userdata('permission')) ? json_decode($this->session->userdata('permission')) : array();
	// 	$permisions = array();
	// 	$menu_data = $this->Common_model->get_menues(array('status'=>1));
	// 	$menuID = array();
	// 	foreach($menu_data as $menu){
	// 		$menuID[$menu['id']] = $menu['id'];
	// 	}
	// 	//print_r($menuID);
	// 	//foreach
	// 	$permissions = array();
	
	// 	foreach($permissions_key as $key=>$permission){
	// 		if(array_key_exists($key,$menuID)){
	// 			$permisions[$key] = $permission;
	// 		}
							
		
	// }
		
	// 	 return $permisions;
	//  }

	public function permissions(){
		$session_data = $this->session->userdata();
		$permissions = array();
	 if($session_data['logged_in'] == TRUE){
		$user = $this->user_model->get_user(array('users.id'=>$session_data['id']));
		$permissions = json_decode($user->permission);
	 }
	  return $permissions; 
	 }

	 public function login_template($page = null, $data = array())
	 {
		$data['siteinfo'] = $this->siteinfo();
		$this->load->view('components/head',$data);
		$this->load->view($page);
		$this->load->view('components/footer');
	 }

	//  public function permissions(){
	// 	$userID = $this->session->userdata('id');
		
	// 	$permissions = $this->Common_model->get_permission(array('userID' => $userID));
	// 	return $permissions;
	//  }

	 public function template($page = null, $data = array())
	 {
		$data['siteinfo'] = $this->siteinfo();
		$data['menus'] = $this->menus();
		$data['permissions'] = $this->permissions();
		$data['roles'] = $this->roles();
		$data['menues'] = $this->Common_model->get_menues(array('status'=>1));

		//$data['permission'] = $this->permissions();
		 $this->load->view('components/head',$data);
		 $this->load->view('components/header');
		 $this->load->view('components/sidebar');
		 $this->load->view($page);
		 $this->load->view('components/footer');
	 }


}