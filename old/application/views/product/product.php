
        <div class="page-body">
          <div class="container-fluid">
            <div class="page-title">
              <div class="row">
                <div class="col-6">
                  <h3><?=$title?></h3>
                </div>
                <div class="col-6">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?=base_url('dashboard')?>">                                       
                        <svg class="stroke-icon">
                          <use href="<?=base_url('public/assets/svg/icon-sprite.svg#stroke-home')?>"></use>
                        </svg></a></li>
                    <li class="breadcrumb-item">Dashboard</li>
                    <li class="breadcrumb-item active"><?=$title?></li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid starts-->
          <div class="container-fluid">
            
              <!-- Individual column searching (text inputs) Starts-->
              <div class="col-sm-12">
                <div class="card">
                  <div class="row container">
                  <div class="card-header text-right">
                    <?php if($permission[1]=='add'){?>
                        <button type="button" class="btn btn-primary btn-sm" title="Add Product" data-toggle="modal" data-target="#addProductModal" data-whatever="@fat">Add Product</button>
                        <button type="button" class="btn btn-outline-primary btn-sm " title="Bulk Product Upload" data-bs-toggle="modal" data-bs-target="#bulkProductUploadModal">Bulk Product Upload</button>
                      <?php }if($permission[5]=='excel'){?>
                        <button type="button" class="btn btn-success btn-sm " title="Download CSV File" onclick="downloadProducts()">Download Products</button>
                        <?php }?>
                      </div>
                    
                  <div class="card-body">
                    <div class="table-responsive product-table">
                      <table class="display" id="productDataTable" >
                        <thead>
                          <tr>
                            <th>Sno</th>
                            <th nowrap>Part Name</th>
                            <th nowrap>Product Code</th>
                            <th nowrap>Unit Type</th>
                            <th>Units</th>
                            <th nowrap>Specification</th>
                            <th>Make</th>
                            <th>Model</th>
                            <th>Location</th>
                            <th>Remark</th>
                            <th>Price</th>
                            <th nowrap>Added By</th>
                            <th nowrap>Created At</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Individual column searching (text inputs) Ends-->
            </div>
          </div>
          <!-- Container-fluid Ends-->
        </div>


         <!-- Add product Modal -->
<div class="modal fade" id="addProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form methos="POST" action="<?=base_url('Product/add_product')?>" id="createProductForm">
      <div class="form-group">
       <input type="text" class="form-control" id="part_name" name="part_name" placeholder="Enter Part Name">
      </div>

      <div class="form-group">
       <input type="text" class="form-control" id="product_code" name="product_code" placeholder="Enter Product code">
      </div>

       <div class="form-group">
        <select class="form-control" id="unit_type" name="unit_type">
          <option value="">Select Unit Type</option>
          <?php foreach($units as $unit){?>
          <option value="<?=$unit['id']?>"><?=$unit['name']?></option>
          <?php }?>
        </select>
      </div>

      <div class="form-group">
        <input type="text" class="form-control" id="unit" name="unit" placeholder="Enter Unit"  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
      </div>

      <div class="form-group">
        <input type="text" class="form-control" id="spec" name="spec" placeholder="Enter Specification">
      </div>
       <div class="form-group"> 
        <input type="text" class="form-control" id="make" name="make" placeholder="Enter Make">
      </div>
      <div class="form-group"> 
        <input type="text" class="form-control" id="model" name="model" placeholder="Enter Model">
      </div>
      <!-- <div class="form-group"> 
        <input type="text" class="form-control" id="location" name="location" placeholder="Enter Location">
      </div> -->
      <div class="form-group">
        <select class="form-control" id="location" name="location">
          <option value="">Select Location</option>
          <?php foreach($locations as $location){?>
          <option value="<?=$location->id?>"><?=$location->location?></option>
          <?php }?>
        </select>
      </div>
      <div class="form-group"> 
        <input type="text" class="form-control" id="remark" name="remark" placeholder="Enter Remark">
      </div>
      <div class="form-group"> 
        <input type="text" class="form-control" id="price" name="price" placeholder="Enter Price" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
     </form>
    </div>
  </div>
</div>

 <!-- Edit product Modal -->
 <div class="modal fade" id="editProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form class="row" methos="POST" action="<?=base_url('Product/edit_product')?>" id="editProductForm">
      <div class="col-md-6">
        <div class="form-group">
        <label for="edit_price">Edit Part Name <span class="text-danger">*</span></label>
        <input type="hidden" id="edit_productId" name="edit_productId">
        <input type="text" class="form-control" id="edit_partname" name="edit_partname" placeholder="Enter Part Name">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
        <label for="edit_price">Edit Code <span class="text-danger">*</span></label>
          <input type="text" class="form-control" id="edit_code" name="edit_code" placeholder="Enter Product Code" readonly>
        </div>
        </div>
      <div class="col-md-6">
        <div class="form-group">
        <label for="edit_price">Edit Unit Type <span class="text-danger">*</span></label>
        <select class="form-control" id="edit_unit_type" name="edit_unit_type">
            <option value="">Select Unit Type</option>
            <?php foreach($units as $unit){?>
            <option value="<?=$unit['id']?>"><?=$unit['name']?></option>
            <?php }?>
          </select>
          </div>
        </div>
      <div class="col-md-6">
        <div class="form-group">
        <label for="edit_price">Edit Unit <span class="text-danger">*</span></label>
          <input type="text" class="form-control" id="edit_unit" name="edit_unit" placeholder="Enter Unit" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
        <label for="edit_price">Edit Specification <span class="text-danger">*</span></label>
          <input type="text" class="form-control" id="edit_spec" name="edit_spec" placeholder="Enter Specification">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group"> 
        <label for="edit_price">Edit Make <span class="text-danger">*</span></label>
          <input type="text" class="form-control" id="edit_make" name="edit_make" placeholder="Enter Make">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group"> 
        <label for="edit_price">Edit Model  <span class="text-danger">*</span></label>
          <input type="text" class="form-control" id="edit_model" name="edit_model" placeholder="Enter Model">
        </div>
      </div>
      <!-- <div class="col-md-6">
        <div class="form-group"> 
        <label for="edit_price">Edit Location <span class="text-danger">*</span></label>
          <input type="text" class="form-control" id="edit_location" name="edit_location" placeholder="Enter Location">
        </div>
      </div> -->
      <div class="col-md-6">
        <div class="form-group">
        <label for="edit_price">Location <span class="text-danger">*</span></label>
        <select class="form-control" id="edit_location" name="edit_location">
            <option value="">Select Location</option>
            <?php foreach($locations as $location){?>
            <option value="<?=$location->id?>"><?=$location->location?></option>
            <?php }?>
          </select>
          </div>
        </div>
      <div class="col-md-6">
        <div class="form-group"> 
        <label for="edit_price">Edit Remark</label>
          <input type="text" class="form-control" id="edit_remark" name="edit_remark" placeholder="Enter Remark">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group"> 
          <label for="edit_price">Edit Price <span class="text-danger">*</span></label>
          <input type="text" class="form-control" id="edit_price" name="edit_price" placeholder="Enter Price">
        </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
     </form>
    </div>
  </div>
</div>

<div class="modal fade" id="bulkProductUploadModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Bulk Upload Products</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?=base_url('Product/bulk_upload_file')?>" method="post" id="bulkFileForm"
                enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Upload Price CSV File</label>
                        <input type="file" name="bulk_products" id="bulk_products" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="<?=base_url("Product/downlodProductList")?>" class="btn btn-warning ">Download CSV Format</a>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
        <script>
  $(document).ready(function() {
      // $('#example').DataTable();
      // } );
      var dataTable = $('#productDataTable').DataTable({
          
          "order": [],
          "ajax": {
              url: "<?=base_url('Product/ajaxproducts')?>",
              type: "POST"
          },
      });
  });

  $('#addProductModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})

$("form#createProductForm").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  toastr.success(data.message);
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){

     location.href="<?=base_url('Product')?>"; 	
    
  }, 1000) 
  
  }else if(data.status==403) {
  toastr.error(data.message);

  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastr.error(data.message);
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });

  function editProductModal(productID){
    $.ajax({
       url: '<?=base_url('Product/get_product')?>',
       type: 'POST',
       data: {productID},
       success: function(data) {
       $('#editProductModal').modal('show');
       var product = $.parseJSON(data);
       
              $('#edit_productId').val(productID);
              $('#edit_partname').val(product.part_name);
              $('#edit_code').val(product.product_code);
              $('#edit_unit_type').val(product.unit_type);
              $('#edit_unit').val(product.unit);
              $('#edit_spec').val(product.specification);
              $('#edit_make').val(product.make);
              $('#edit_model').val(product.model);
              $('#edit_location').val(product.location);
              $('#edit_remark').val(product.remark);
              $('#edit_price').val(product.price);
              
       }
   });
  }

  $("form#bulkFileForm").submit(function(e){
    $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  toastr.success(data.message);
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){

     location.href="<?=base_url('Product')?>"; 	
    
  }, 1000) 
  
  }else if(data.status==403) {
  toastr.error(data.message);

  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastr.error(data.message);
    location.href="<?=base_url('Product/download_error_report/')?>"+data.file_data;
     $(':input[type="submit"]').prop('disabled', false);
     setTimeout(function(){
     location.href="<?=base_url('Product')?>"; 	
     }, 1000) 
  }
  },
  error: function(){} 
  });
  });

  $("form#editProductForm").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  toastr.success(data.message);
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){

     location.href="<?=base_url('Product')?>"; 	
    
  }, 1000) 
  
  }else if(data.status==403) {
  toastr.error(data.message);

  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastr.error(data.message);
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });

  function delete_product(productId){
    Swal.fire({
          title: 'Are you sure?',
          text: "You won't to delet it!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
          if (result.value) {
              $.ajax({
                  url: '<?=base_url('Product/delete_product')?>',
                  type: 'POST',
                  data: {
                    productId
                  },
                  dataType: 'json',
                  success: function(data) {
                      if (data.status == 200) {
                          toastr.success(data.message);
                          location.href = "<?=base_url('Product')?>"
  
                      } else if (data.status == 302) {
                          toastr.error(data.message);
                      }
                  }
              })
          }
      })
  }

  function downloadProducts(){
    var messageText  = "You want download  All Products!";
      var confirmText =  'Yes, download it!';
      var message  ="Product downloaded Successfully!";
   
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {

          location.href="<?=base_url('Product/export_products')?>";  
        }
        })
  }
  </script>