<style>
    
</style>
<div class="page-body">
   <div class="container-fluid">
      <div class="page-title">
         <div class="row">
            <div class="col-6">
               <h3><?=$title?></h3>
            </div>
            <div class="col-6">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                     <a href="index.html">
                        <svg class="stroke-icon">
                           <use href="<?=base_url('public/assets/svg/icon-sprite.svg#stroke-home')?>"></use>
                        </svg>
                     </a>
                  </li>
                  <li class="breadcrumb-item active"><?=$title?></li>
               </ol>
            </div>
         </div>
      </div>
   </div>
   <!-- Container-fluid starts-->
   <div class="container-fluid">
      <!-- Individual column searching (text inputs) Starts-->
         <div class="card">
            <div class="row container">
               <div class="card-header ">
                  <div class="row">
                  <div class="col-md-12">
                     <?php if($permission[1]=='add'){    ?>
                        <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#addExpenceModal" data-whatever="@fat">Add Expence</button>
                     <?php }if($permission[5]=='excel'){ ?>
                        <button type="button" class="btn btn-success float-right mr-2" onclick="downloadExpence()"><i class="fa fa-download"></i> All Expence(CSV)</button>
                     <?php }?>   
                  </div>
                     <div class="col-md-12 d-flex">
                     
                        <div class="form-group m-4">
                           <label for="">Select Project :</label>                      
                           <select name="project" id="" class="js-example-basic-single" onchange="projectFilter(this.value)">
                           <option value="" disabled selected>Select Project</option>
                           <?php foreach($projects as $project){?>
                                 <option value="<?=$project['project_id']?>" <?=$this->session->userdata('projectID') == $project['project_id'] ? 'selected' : ''?>><?=$project['name']?></option>
                                 <?php }?>
                           </select>
                           <?php if(!empty($this->session->userdata('projectID'))){?>
                           <a href="javacript:void(0);" class="text-danger" onclick="resetFilter(this.value)">Reset</a>
                           <?php }?>
                        </div>
                        <div class="form-group m-4">
                           <label for="">From Date :</label>                      
                           <input name="from_date" id="from_date" type="date" value="<?=$this->session->userdata('from_date')?>" class="form-control" onchange="projectFilter2(this.value)">
                           
                           <?php if(!empty($this->session->userdata('from_date'))){?>
                              <a href="javacript:void(0);" class="text-danger" onclick="resetFilter2(this.value)">Reset</a>
                           <?php }?>
                        </div>
                        <div class="form-group m-4">
                           <label for="">To Date :</label>                      
                           <input name="to_date" id="to_date" class="form-control" type="date" value="<?=$this->session->userdata('to_date')?>" onchange="projectFilter3(this.value)">
                          
                           <?php if(!empty($this->session->userdata('to_date'))){?>
                              <a href="javacript:void(0);" class="text-danger" onclick="resetFilter3(this.value)">Reset</a>
                           <?php }?>
                        </div>
                
                     </div>
                   
                     
                     <?php if(!empty($this->session->userdata('projectID'))){if(!empty($this->session->userdata('projectBudget'))){?>
                        <div class="col-md-12">
                        <strong>Project Budget :</strong> <i class="fa fa-inr"></i> <?=$this->session->userdata('projectBudget')?> <br>
                        </div>
                     <?php } if(!empty($this->session->userdata('projectExpence'))){?>
                        <div class="col-md-12">
                        <strong>Remaining Balance : </strong> <i class="fa fa-inr"></i> <?=$this->session->userdata('projectBudget')-$this->session->userdata('projectExpence')?> <br>
                        </div>
                        <?php }if($permission[5]=='excel'){?>
                        <button type="button" class="btn btn-success float-right ml-2 col-md-2 j-content-flex-end" onclick="downloadPdf()"><i class="fa fa-download"></i> PDF REPORT</button>

                     <?php }}  ini_set('display_errors',1);?>
                    
                  </div>
                  
                  <!-- <button type="button" class="btn btn-outline-primary float-right mr-2" data-toggle="modal" data-target="#bulkProductUploadModal" data-whatever="@fat">Bulk Upload Project</button> -->
               </div>
               <div class="card-body">
                  <div class="table-responsive product-table">
                     <table class="display" id="expenceDataTable" >
                        <thead>
                           <tr>
                              <th>Sno</th>
                              <th nowrap>Project Name</th>
                              <th>Remark</th>
                              <th>Expence</th>
                              <th>Date</th>
                              <?php 
      if($permission[3]=='delete'){   ?>
                              <th>Action</th>
                              <?php }?>
                           </tr>
                        </thead>
                        <tbody>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <!-- Individual column searching (text inputs) Ends-->
      </div>
   </div>
   <!-- Container-fluid Ends-->
</div>
<!-- Add product Modal -->
<div class="modal fade" id="addExpenceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add Expence</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form methos="POST" action="<?=base_url('Project/add_expence')?>" id="add_expence">
                <div class="form-group">
                    <label for="">Select Project :</label>                      
                    <select class="js-example-basic-single" name="project1" >
                    <!--<option value="" disabled selected>Select Project</option>-->
                    <?php foreach($all_projects as $project){?>
                        <option value="<?=$project['id']?>"><?=$project['name']?></option>
                        <?php }?>
                    </select>
                </div>
                <div id="multiple_form" class="row"> 
                    <table>
                        <tr>
                            <td><input type="date" class="form-control digits" id="edate" name="edate[]"></td>
                            <td><input type="text" class="form-control" id="particular" name="particular[]" placeholder="Enter particular Remark"></td>
                            <td><input type="number" class="form-control" id="expence" name="expence[]" placeholder="Enter Expence Amount"></td>
                            <td><span class="customLook"></span><button type="button" id="addmore">+</button></td>
                        </tr>
                    </table>                
                </div>  
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
         </form>
      </div>
   </div>
</div>
</div>
<!-- Edit product Modal -->
<div class="modal fade" id="editProjectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit Project</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form methos="POST" action="<?=base_url('Project/edit_project')?>" id="editProjectForm">
               <div class="form-group">
                  <input type="hidden" id="edit_projectId" name="edit_projectId">
                  <input type="text" class="form-control" id="edit_name" name="edit_name" placeholder="Enter Project Name">
               </div>
               <div class="form-group">
                  <input type="text" class="form-control" id="edit_client" name="edit_client" placeholder="Enter client Name">
               </div>
               <div class="form-group"> 
                  <input type="text" class="form-control" id="edit_budget" name="edit_budget" placeholder="Enter Budget">
               </div>
               <div class="form-group"> 
                  <textarea class="form-control" id="edit_remark" name="edit_remark" placeholder="Enter Remark"></textarea>
               </div>
         </div>
         <div class="modal-footer">
         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
         <button type="submit" class="btn btn-primary">Update</button>
         </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="bulkProductUploadModal" tabindex="-1" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Modal title</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
         </div>
         <form action="<?=base_url('setting/bulk_upload_file')?>" method="post" id="bulkFileForm"
            enctype="multipart/form-data">
            <div class="modal-body">
               <div>
                  <label>Upload Price CSV File</label>
                  <input type="file" name="bulk_price" id="bulk_price" class="form-control">
               </div>
            </div>
            <div class="modal-footer">
               <a href="<?=base_url("setting/downlodPriceList")?>" class="btn btn-warning ">Download Price List</a>
               <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
         </form>
      </div>
   </div>
</div>
<script>
   $(document).ready(function() {
    $("#addmore").click(function() {
        $("#multiple_form").append('<div class="required_inp d-flex" style="margin:5px;"><input name="edate[]" class="form-control digits" type="date"><input type="text" class="form-control" name="particular[]" placeholder="Enter particular Remark"><input type="number" class="form-control" name="expence[]" placeholder="Enter Expence">' + '<span class="customLook"></span><button type="button" class="inputRemove">x</button></div>');
    });
    $('body').on('click','button.inputRemove',function() {
        $(this).parent('div.required_inp').remove()
    });
    });
   $(document).ready(function() {
       // $('#example').DataTable();
       // } );
       var dataTable = $('#expenceDataTable').DataTable({
         //   "processing": true,
         //   "serverSide": true,
         //   buttons: [{
         //       extend: 'excelHtml5',
         //       text: 'Download Excel'
         //   }],
           "order": [],
           "ajax": {
               url: "<?=base_url('Project/ajaxExpence')?>",
               type: "POST"
           },
         //   "columnDefs": [{
         //       "targets": [1],
         //       "orderable": true,
         //   }, ],
       });
   });
   
   $('#addProjectModal').on('shown.bs.modal', function () {
   $('#myInput').trigger('focus')
   })
   
   $("form#add_expence").submit(function(e) {
   //alert('fgdfgfd');
   $(':input[type="submit"]').prop('disabled', true);
   e.preventDefault();    
   var formData = new FormData(this);
   $.ajax({
   url: $(this).attr('action'),
   type: 'POST',
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   dataType: 'json',
   success: function (data) {
   if(data.status==200) {
   toastr.success(data.message);
   $(':input[type="submit"]').prop('disabled', false);
   setTimeout(function(){
   
      location.href="<?=base_url('Expence')?>"; 	
     
   }, 1000) 
   
   }else if(data.status==403) {
   toastr.error(data.message);
   
   $(':input[type="submit"]').prop('disabled', false);
   }else{
     toastr.error(data.message);
      $(':input[type="submit"]').prop('disabled', false);
   }
   },
   error: function(){} 
   });
   });
   
   function editProjectModal(projectID){
      //alert(projectID);
     $.ajax({
        url: '<?=base_url('Project/get_project')?>',
        type: 'POST',
        data: {projectID},
        //dataType: 'json',
        success: function(data) {
         
        $('#editProjectModal').modal('show');
        var project = $.parseJSON(data);
       // console.log(data);
               $('#edit_projectId').val(projectID);
               $('#edit_name').val(project.name);
               $('#edit_client').val(project.client);
               $('#edit_budget').val(project.budget);
               $('#edit_remark').val(project.remark);
               
        }
    });
   }
   
   $("form#editProjectForm").submit(function(e) {
   //alert('fgdfgfd');
   $(':input[type="submit"]').prop('disabled', true);
   e.preventDefault();    
   var formData = new FormData(this);
   $.ajax({
   url: $(this).attr('action'),
   type: 'POST',
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   dataType: 'json',
   success: function (data) {
   if(data.status==200) {
   toastr.success(data.message);
   $(':input[type="submit"]').prop('disabled', false);
   setTimeout(function(){
   
      location.href="<?=base_url('project')?>"; 	
     
   }, 1000) 
   
   }else if(data.status==403) {
   toastr.error(data.message);
   
   $(':input[type="submit"]').prop('disabled', false);
   }else{
     toastr.error(data.message);
      $(':input[type="submit"]').prop('disabled', false);
   }
   },
   error: function(){} 
   });
   });

   function projectFilter(id) {
     $.ajax({
       url: "<?=base_url('Project/projectFilter')?>",
       type: 'POST',
       data: {id},
       success: function (data) {
            location.reload();
       },
       error: function(){} 
     });
   }

   function projectFilter2(from_date) {
     $.ajax({
       url: "<?=base_url('Project/projectFilter')?>",
       type: 'POST',
       data: {from_date},
       success: function (data) {
            location.reload();
       },
       error: function(){} 
     });
   }

   function projectFilter3(to_date) {
     $.ajax({
       url: "<?=base_url('Project/projectFilter')?>",
       type: 'POST',
       data: {to_date},
       success: function (data) {
            location.reload();
       },
       error: function(){} 
     });
   }

   function resetFilter(){
      var data = 'project';
      $.ajax({
       url: "<?=base_url('Project/resetFilter')?>",
       type: 'POST',
       data: {data},
       success: function (data) {
            location.reload();
       },
       error: function(){} 
     });
   }
   function resetFilter2(){
      var data = 'fromdate';
      $.ajax({
       url: "<?=base_url('Project/resetFilter')?>",
       type: 'POST',
       data: {data},
       success: function (data) {
            location.reload();
       },
       error: function(){} 
     });
   }
   function resetFilter3(){
      var data = 'todate'
      $.ajax({
       url: "<?=base_url('Project/resetFilter')?>",
       type: 'POST',
       data: {data},
       success: function (data) {
            location.reload();
       },
       error: function(){} 
     });
   }
   
   function delete_project(projectId){
     Swal.fire({
           title: 'Are you sure?',
           text: "You won't to delet it!",
           type: 'warning',
           showCancelButton: true,
           confirmButtonColor: '#3085d6',
           cancelButtonColor: '#d33',
           confirmButtonText: 'Yes, delete it!'
       }).then((result) => {
           if (result.value) {
               $.ajax({
                   url: '<?=base_url('Project/delete_project')?>',
                   type: 'POST',
                   data: {
                     projectId
                   },
                   dataType: 'json',
                   success: function(data) {
                       if (data.status == 200) {
                           toastr.success(data.message);
                           location.href = "<?=base_url('project')?>"
   
                       } else if (data.status == 302) {
                           toastr.error(data.message);
                       }
                   }
               })
           }
       })
   }

   function delete_exp(Id){
     Swal.fire({
           title: 'Are you sure?',
           text: "You won't to delete it!",
           type: 'warning',
           showCancelButton: true,
           confirmButtonColor: '#3085d6',
           cancelButtonColor: '#d33',
           confirmButtonText: 'Yes, delete it!'
       }).then((result) => {
           if (result.value) {
               $.ajax({
                   url: '<?=base_url('Project/delete_expence')?>',
                   type: 'POST',
                   data: {
                     Id
                   },
                   dataType: 'json',
                   success: function(data) {
                       if (data.status == 200) {
                           toastr.success(data.message);
                           location.href = "<?=base_url('Expence')?>"
   
                       } else if (data.status == 302) {
                           toastr.error(data.message);
                       }
                   }
               })
           }
       })
   }
   function downloadPdf(){
    
    var messageText  = "You want download  Expense in PDF!";
      var confirmText =  'Yes, download it!';
      var message  ="Expense download Successfully!";
   
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {

          location.href="<?=base_url('Project/expense_pdf')?>";  
        }
        })
  }
   function downloadExpence(){
    
    var messageText  = "You want download  Expense!";
      var confirmText =  'Yes, download it!';
      var message  ="Expense download Successfully!";
   
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {

          location.href="<?=base_url('Project/export_expence')?>";  
        }
        })
  }
</script>