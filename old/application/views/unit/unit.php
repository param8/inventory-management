<div class="page-body">
<div class="container-fluid">
  <div class="page-title">
    <div class="row">
      <div class="col-6">
        <h3>Unit list</h3>
      </div>
      <div class="col-6">
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="index.html">
              <svg class="stroke-icon">
                <use href="<?=base_url('public/assets/svg/icon-sprite.svg#stroke-home')?>"></use>
              </svg>
            </a>
          </li>
          <li class="breadcrumb-item">ECommerce</li>
          <li class="breadcrumb-item active">Unit list</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Container-fluid starts-->
<div class="container-fluid">
  <div class="row">
    <!-- Individual column searching (text inputs) Starts-->
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addUnitModal" data-whatever="@fat">Add Unit</button>
        </div>
        <div class="card-body">
          <div class="table-responsive product-table" id="">
            <table class="display" id="unitDataTable">
              <thead>
                <tr>
                  <th>SNo</th>
                  <th>Unit Name</th>
                  <th>Created Date</th>
                  <th>Action</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- Individual column searching (text inputs) Ends-->
  </div>
</div>
<!-- Container-fluid Ends-->
<!-- Modal -->
<div class="modal fade" id="addUnitModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Add Unit</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form methos="POST" action="<?=base_url('Setting/add_unit')?>" id="addUnitForm">
        <div class="form-group">
          <input type="text" class="form-control" id="name" name="name" placeholder="Enter Unit Name">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
<!--Edit Modal -->
<div class="modal fade" id="editUnitModal" role="dialog" aria-labelledby="exampleModalLabel" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="<?=base_url('Setting/edit_unit')?>" id="editUnitForm">
          <div class="form-group">
            <input type="hidden" id="edit_unitId" name="edit_unitId">
            <input type="text" class="form-control" id="edit_name" name="edit_name" placeholder="Enter Unit Name">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
    //alert('dfgfgf');
      // $('#example').DataTable();
      // } );
      var dataTable = $('#unitDataTable').DataTable({
          "processing": true,
          "serverSide": true,
          buttons: [{
              extend: 'excelHtml5',
              text: 'Download Excel'
          }],
          "order": [],
          "ajax": {
              url: "<?=base_url('Setting/ajaxunitTable')?>",
              type: "POST"
          },
          "columnDefs": [{
              "targets": [0],
              "orderable": false,
          }, ],
      });
  });
  
  
  $("form#addUnitForm").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  toastr.success(data.message);
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){
  
     location.href="<?=base_url('Setting/unit_setting')?>"; 	
    
  }, 1000) 
  
  }else if(data.status==403) {
  toastr.error(data.message);
  
  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastr.error(data.message);
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });
  
  function editUnitModal(unitID) {
    //alert(unitID);
   $.ajax({
       url: '<?=base_url('Setting/get_unit')?>',
       type: 'POST',
       data: {unitID},
       success: function(data) {
       $('#editUnitModal').modal('show');
       var unit = $.parseJSON(data);
              $('#edit_unitId').val(unitID);
              $('#edit_name').val(unit.name);
  
       }
   });
  }

  $("form#editUnitForm").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  toastr.success(data.message);
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){

     location.href="<?=base_url('Setting/unit_setting')?>"; 	
    
  }, 1000) 
  
  }else if(data.status==403) {
  toastr.error(data.message);

  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastr.error(data.message);
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });
  
  function delete_unit(unitID){
    Swal.fire({
          title: 'Are you sure?',
          text: "You won't to delet it!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
          if (result.value) {
              $.ajax({
                  url: '<?=base_url('Setting/delete_unit')?>',
                  type: 'POST',
                  data: {
                    unitID
                  },
                  dataType: 'json',
                  success: function(data) {
                      if (data.status == 200) {
                          toastr.success(data.message);
                          location.href = "<?=base_url('Setting/unit_setting')?>"
  
                      } else if (data.status == 302) {
                          toastr.error(data.message);
                      }
                  }
              })
          }
      })
  }
</script>