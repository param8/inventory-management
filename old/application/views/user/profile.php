<div class="page-body">
          <div class="container-fluid">
            <div class="page-title">
              <div class="row">
                <div class="col-6">
                  <h3>Edit Profile</h3>
                </div>
                <div class="col-6">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">                                       
                        <svg class="stroke-icon">
                          <use href="../assets/svg/icon-sprite.svg#stroke-home"></use>
                        </svg></a></li>
                    <li class="breadcrumb-item"><?=$title?></li>
                    <li class="breadcrumb-item active"> Edit Profile</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
          <?php //print_r($vendor_data);die;?>
          <!-- Container-fluid starts-->
          <div class="container-fluid">
            <div class="edit-profile">
              <div class="row">
                <div class="col-xl-4">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title mb-0">My Profile</h4>
                      <div class="card-options"><a class="card-options-collapse" href="#" data-bs-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-bs-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                    </div>
                    <div class="card-body">
                      <form id="updatevendorForm" action="<?=base_url('User/update_vendor')?>" method="post">
                        <div class="row mb-2">
                          <div class="profile-title">
                            <div class="media"> 
                                <img class="img-70 rounded-circle" alt="" height="70" src="<?=base_url($user_data->profile_pic)?>">
                              <div class="media-body">
                                <h5 class="mb-1"><?=$this->session->userdata('name');?></h5>
                                <p><?=$this->session->userdata('user_type')?></p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <?php if($this->session->userdata('role_id') == 4){?>
                        <div class="mb-3">
                          <label class="form-label">GST</label>
                          <input class="form-control" type="text" name="gst" placeholder="Enter Gst" value="<?=(!empty($vendor_data->gst))?$vendor_data->gst:''?>">
                        </div>
                        <div class="mb-3">
                          <label class="form-label">Adhar</label>
                          <input class="form-control" type="text" type="password" name="adhar" placeholder="Enter Gst" value="<?=(!empty($vendor_data->adhar))?$vendor_data->adhar:''?>">
                        </div>
                        <div class="mb-3">
                          <label class="form-label">Pan</label>
                          <input class="form-control" type="text" name="pan" placeholder="Enter Pan" value="<?=(!empty($vendor_data->pancard))?$vendor_data->pancard:''?>">
                        </div>
                        <div class="form-footer">
                          <button type="submit" class="btn btn-primary btn-block">Save</button>
                        </div>
                        <?php }?>
                      </form>
                    </div>
                  </div>
                </div>
                <div class="col-xl-8">
                  <form class="card" id="editProfileForm" action="<?=base_url('User/update_profile')?>">
                    <div class="card-header">
                      <h4 class="card-title mb-0">Edit Profile</h4>
                      <div class="card-options"><a class="card-options-collapse" href="#" data-bs-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-bs-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-sm-6 col-md-6">
                          <div class="mb-3">
                            <label class="form-label">Name</label>
                            <input class="form-control" type="text" name="name" placeholder="Enter Your Name" value="<?=$user_data->name?>">
                          </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                          <div class="mb-3">
                            <label class="form-label">Email address</label>
                            <input class="form-control" type="email" name="email" placeholder="Email" value="<?=$user_data->email?>">
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                          <div class="mb-3">
                            <label class="form-label">Mobile</label>
                            <input class="form-control" type="text" placeholder="Enter Mobile" name="phone" value="<?=$user_data->phone?>">
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                          <div class="mb-3">
                            <label class="form-label">Profile Image</label>
                            <input class="form-control" type="file" name="profile_pic"  value="<?=$user_data->phone?>">
                          </div>
                        </div>
                        
                        
                        <div class="col-md-5">
                          <div class="mb-3">
                            <label class="form-label">State</label>
                            <select class="form-control btn-square" id="state" name="state" onchange="selectCity(this.value)">
                              <option value="0">--Select--</option>
                              <?php foreach($states as $state){?>
                                <option value="<?=$state->id?>" <?=($user_data->state ==$state->id)?'selected':'' ?>><?=$state->name?></option>
                              <?php }?>
                            </select>
                          </div>
                        </div>
                        
                        <div class="col-sm-6 col-md-4">
                          <div class="mb-3">
                            <label class="form-label">City</label>
                            <select class="form-control" id="city" name="city">                        
                            </select>
                          </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                          <div class="mb-3">
                            <label class="form-label">Postal Code</label>
                            <input class="form-control" type="number" name="pincode" placeholder="ZIP Code"  value="<?=($user_data->pincode==0)?'':$user_data->pincode?>">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card-footer text-end">
                      <button class="btn btn-primary" type="submit">Update Profile</button>
                    </div>
                  </form>
                </div>                
              </div>
            </div>
          </div>
          <!-- Container-fluid Ends-->
        </div>

<script>
    function selectState(countryID){
        $.ajax({
          url: '<?=base_url('Ajax_controller/get_state')?>',
          type: 'POST',
          data: {countryID},
          success: function (data) {
            $('#state').html(data);
          }
         });
      }

      function selectCity(stateID){
        var cityID = <?=$user_data->city?>;
        $.ajax({
          url: '<?=base_url('Ajax_controller/get_city')?>',
          type: 'POST',
          data: {stateID,cityID},
          success: function (data) {
            $('#city').html(data);
          }
          });
      }

      $( document ).ready(function() {
        selectCity(<?=$user_data->state?>);
      });

      $("form#editProfileForm").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  toastr.success(data.message);
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){

     location.href="<?=base_url('Profile')?>"; 	
    
  }, 1000) 
  
  }else if(data.status==403) {
  toastr.error(data.message);

  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastr.error(data.message);
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });


  $("form#updatevendorForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  toastr.success(data.message);
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){

     location.href="<?=base_url('Profile')?>"; 	
    
  }, 1000) 
  
  }else if(data.status==403) {
  toastr.error(data.message);

  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastr.error(data.message);
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });

</script>