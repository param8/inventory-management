<div class="page-body">
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6">
                    <h3>User list</h3>
                </div>
                <div class="col-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=base_url('Dashboard')?>">
                                <svg class="stroke-icon">
                                    <use href="<?=base_url('public/assets/svg/icon-sprite.svg#stroke-home')?>"></use>
                                </svg></a></li>
                        <li class="breadcrumb-item">User</li>
                        <li class="breadcrumb-item active">User list</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- Individual column searching (text inputs) Starts-->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#creatVendorModal" data-whatever="@fat">Create Vendor</button>
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive product-table">
                            <table class="display" id="vendorDataTable">
                                <thead>
                                    <tr>
                                        <th>SNo</th>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Contact</th>
                                        <th>created Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Individual column searching (text inputs) Ends-->
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>

<!-- Modal -->
<div class="modal fade" id="creatUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form methos="POST" action="<?=base_url('User/create_user')?>" id="createUserForm">
                    <div class="form-group">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="password" name="password"
                            placeholder="Enter Password">
                    </div>
                    <div class="form-group">
                        <select name="user_type" id="user_type" class="form-control">
                            <option value=""><i class="arrow down">Select User Type</i></option>
                            <option value="vendor">Vendor</option>
                            <option value="user">User</option>
                        </select>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="editUserModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel1">Edit Vendor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form methos="POST" action="<?=base_url('User/edit_user')?>" id="editUserForm">
                    <div class="form-group">
                        <input type="hidden" id="edit_userId" name="edit_userId">
                        <input type="text" class="form-control" id="edit_name" name="edit_name"
                            placeholder="Enter Name">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="edit_email" name="edit_email"
                            placeholder="Enter Email" readonly>
                    </div>
                    <div class="form-group">
                        <input type="file" class="form-control" id="edit_profile_pic" name="edit_profile_pic">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<script>
$(document).ready(function() {
    // $('#example').DataTable();
    // } );
    var dataTable = $('#vendorDataTable').DataTable({
        "processing": true,
        "serverSide": true,
        buttons: [{
            extend: 'excelHtml5',
            text: 'Download Excel'
        }],
        "order": [],
        "ajax": {
            url: "<?=base_url('User/ajaxVendorTable')?>",
            type: "POST"
        },
        "columnDefs": [{
            "targets": [0],
            "orderable": false,
        }, ],
    });
});


$('#creatUserModal').on('shown.bs.modal', function() {
    $('#myInput').trigger('focus')
})

$("form#createUserForm").submit(function(e) {
    //alert('fgdfgfd');
    $(':input[type="submit"]').prop('disabled', true);
    e.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(data) {
            if (data.status == 200) {
                toastr.success(data.message);
                $(':input[type="submit"]').prop('disabled', false);
                setTimeout(function() {

                    location.href = "<?=base_url('User')?>";

                }, 1000)

            } else if (data.status == 403) {
                toastr.error(data.message);

                $(':input[type="submit"]').prop('disabled', false);
            } else {
                toastr.error(data.message);
                $(':input[type="submit"]').prop('disabled', false);
            }
        },
        error: function() {}
    });
});


$("form#editUserForm").submit(function(e) {
    //alert('fgdfgfd');
    $(':input[type="submit"]').prop('disabled', true);
    e.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(data) {
            if (data.status == 200) {
                toastr.success(data.message);
                $(':input[type="submit"]').prop('disabled', false);
                setTimeout(function() {

                    location.href = "<?=base_url('User')?>";

                }, 1000)

            } else if (data.status == 403) {
                toastr.error(data.message);

                $(':input[type="submit"]').prop('disabled', false);
            } else {
                toastr.error(data.message);
                $(':input[type="submit"]').prop('disabled', false);
            }
        },
        error: function() {}
    });
});

function editUserModal(userID) {
    $.ajax({
        url: '<?=base_url('User/get_user')?>',
        type: 'POST',
        data: {
            userID
        },
        success: function(data) {
            $('#editUserModal').modal('show');
            var user = $.parseJSON(data);
            $('#edit_userId').val(userID);
            $('#edit_name').val(user.name);
            $('#edit_email').val(user.email);
            $('#edit_image').val(user.profile_pic);

        }
    });
}

function delete_user(userID) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't to delet it!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: '<?=base_url('User/delete_user')?>',
                type: 'POST',
                data: {
                    userID
                },
                dataType: 'json',
                success: function(data) {
                    if (data.status == 200) {
                        toastr.success(data.message);
                        location.href = "<?=base_url('User')?>"

                    } else if (data.status == 302) {
                        toastr.error(data.message);
                    }
                }
            })
        }
    })
}
</script>