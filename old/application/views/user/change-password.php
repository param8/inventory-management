<div class="page-body">
          <div class="container-fluid">
            <div class="page-title">
              <div class="row">
                <div class="col-6">
                  <h3>Change Password</h3>
                </div>
                <div class="col-6">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">                                       
                        <svg class="stroke-icon">
                          <use href="../assets/svg/icon-sprite.svg#stroke-home"></use>
                        </svg></a></li>
                    <li class="breadcrumb-item">Dashboard</li>
                    <li class="breadcrumb-item active"> <?=$title?></li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
          <?php //print_r($vendor_data);die;?>
          <!-- Container-fluid starts-->
          <div class="container-fluid">
            <div class="edit-profile">
              <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                  <form class="card" id="changepassForm" action="<?=base_url('User/changePassword')?>">
                    <div class="card-header">
                      <h4 class="card-title mb-0">Change Password</h4>
                      <div class="card-options"><a class="card-options-collapse" href="#" data-bs-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-bs-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                    </div>
                    <div class="card-body">
                      <div class="row">
                          <div class="mb-3">
                            <label class="form-label" for="opassword">Current Password <span class="text-danger">*</span></label>
                            <input class="form-control" type="password" id="opassword" name="opassword" placeholder="Enter Your Current Password">
                          </div>
                          <div class="mb-3">
                            <label class="form-label">New Password <span class="text-danger">*</span></label>
                            <input class="form-control" type="password" id="npassword" name="npassword" onkeyup="matchPassword()" placeholder="Enter New Password">
                          </div>
                          <div class="mb-3">
                            <label class="form-label">Confirm Password <span class="text-danger">*</span></label>
                            <input class="form-control" type="password" id="cpassword" name="cpassword" onkeyup="matchPassword()" placeholder="Enter New Password">
                            <p id="error"></p>  
                        </div>
                      </div>
                    </div>
                    <div class="card-footer text-end">
                      <button class="btn btn-primary" type="submit">Update Password</button>
                    </div>
                  </form>
                </div>                
              </div>
            </div>
          </div>
          <!-- Container-fluid Ends-->
        </div>
<script>
     function matchPassword(){
       var cnp = document.getElementById('cpassword').value;
       var np = document.getElementById('npassword').value;
       if(cnp != np){
        $('#error').html('<span class="text-danger">Password Not Matched</span>');
       }else{
        $('#error').html('<span class="text-success">Password Matched</span>');
       }
      }
</script>
<script>
  
      $("form#changepassForm").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  toastr.success(data.message);
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){

     location.href="<?=base_url('change-password')?>"; 	
    
  }, 1000) 
  
  }else if(data.status==403) {
  toastr.error(data.message);

  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastr.error(data.message);
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });



</script>