<div class="page-body">
  <div class="container-fluid">
    <div class="page-title">
      <div class="row">
        <div class="col-6">
          <h3>Role list</h3>
        </div>
        <div class="col-6">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?=base_url('Dashboard')?>">
                <svg class="stroke-icon">
                  <use href="<?=base_url('public/assets/svg/icon-sprite.svg#stroke-home')?>"></use>
                </svg></a></li>
            <li class="breadcrumb-item">Role</li>
            <li class="breadcrumb-item active">Role list</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <!-- Individual column searching (text inputs) Starts-->
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header row text-left ">

            <div class="col-md-6">
              
              <a href="javascript:void(0)" onclick="setRoleFilterSession(2)" class="btn btn-primary btn-sm <?=$this->session->userdata('role_filter')== 2 ? 'active' : 'badge badge-light-primary' ?>" >All</a>
             
              <a href="javascript:void(0)" onclick="setRoleFilterSession(1)" class="btn btn-success btn-sm <?=$this->session->userdata('role_filter')== 1 ? 'active' : 'badge badge-light-success' ?>" >Active</a>

              <a href="javascript:void(0)" onclick="setRoleFilterSession(0)" class="btn btn-danger btn-sm <?=$this->session->userdata('role_filter')== 0 ? 'active' : 'badge badge-light-danger' ?>" >Deactive</a>
             
              </div>
                <div class="col-md-6 text-right">
                  <?php if($permission[1]=='add'){?>
                  <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addroleModal"
                    data-whatever="@fat">Add Role</button>
                  <?php }?>
                  <?php if($permission[5]=='excel'){?>
                  <button type="button" class="btn btn-success btn-sm " title="Download CSV File"
                    onclick="downloadRoles()">Download Roles</button>
                  <?php }?>
                </div>
             
            </div>
            <div class="card-body">
              <div class="table-responsive product-table">
                <table class="display" id="roleDataTable">
                  <thead>
                    <tr>
                      <th>SNo</th>
                      <th>Role</th>
                      <th>Status</th>
                      <th>Create Date</th>
                      <th>Action</th>
                    </tr>
                  </thead>

                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- Individual column searching (text inputs) Ends-->
      </div>
    </div>
    <!-- Container-fluid Ends-->
  </div>

  <!-- Modal -->
  <div class="modal fade" id="addroleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Role for user</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form methos="POST" action="<?=base_url('User/add_role')?>" id="createRoleForm">
            <div class="form-group">
              <input type="text" class="form-control" id="role" name="role" placeholder="Enter Role Type">
            </div>

            <div class="form-group">
              <select name="status" id="status" class="form-control">
                <option value=""><i class="arrow down">Select Status</i></option>
                <option value="1">Active</option>
                <option value="0">Deactive</option>
              </select>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="editRoleModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel1">Edit Role</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form methos="POST" action="<?=base_url('User/edit_role')?>" id="editRoleForm">
            <div class="form-group">
              <input type="hidden" id="edit_roleId" name="edit_roleId">
              <input type="text" class="form-control" id="edit_role" name="edit_role" placeholder="Enter Role Type">
            </div>

            <div class="form-group">
              <select name="edit_status" id="edit_status" class="form-control">
                <option value=""><i class="arrow down">Select Status</i></option>
                <option value="1">Active</option>
                <option value="0">Deactive</option>
              </select>
            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
  <script>
  $(document).ready(function() {
    // $('#example').DataTable();
    // } );
    var dataTable = $('#roleDataTable').DataTable({
      "processing": true,
      "serverSide": true,
      buttons: [{
        extend: 'excelHtml5',
        text: 'Download Excel'
      }],
      "order": [],
      "ajax": {
        url: "<?=base_url('User/ajaxRoleTable')?>",
        type: "POST"
      },
      "columnDefs": [{
        "targets": [0],
        "orderable": false,
      }, ],
    });
  });


  //   $('#creatUserModal').on('shown.bs.modal', function () {
  //   $('#myInput').trigger('focus')
  // })

  $("form#createRoleForm").submit(function(e) {
    //alert('fgdfgfd');
    $(':input[type="submit"]').prop('disabled', true);
    e.preventDefault();
    var formData = new FormData(this);
    $.ajax({
      url: $(this).attr('action'),
      type: 'POST',
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      dataType: 'json',
      success: function(data) {
        if (data.status == 200) {
          toastr.success(data.message);
          $(':input[type="submit"]').prop('disabled', false);
          setTimeout(function() {

            location.href = "<?=base_url('role')?>";

          }, 1000)

        } else if (data.status == 403) {
          toastr.error(data.message);

          $(':input[type="submit"]').prop('disabled', false);
        } else {
          toastr.error(data.message);
          $(':input[type="submit"]').prop('disabled', false);
        }
      },
      error: function() {}
    });
  });


  $("form#editRoleForm").submit(function(e) {
    //alert('fgdfgfd');
    $(':input[type="submit"]').prop('disabled', true);
    e.preventDefault();
    var formData = new FormData(this);
    $.ajax({
      url: $(this).attr('action'),
      type: 'POST',
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      dataType: 'json',
      success: function(data) {
        if (data.status == 200) {
          toastr.success(data.message);
          $(':input[type="submit"]').prop('disabled', false);
          setTimeout(function() {

            location.href = "<?=base_url('role')?>";

          }, 1000)

        } else if (data.status == 403) {
          toastr.error(data.message);

          $(':input[type="submit"]').prop('disabled', false);
        } else {
          toastr.error(data.message);
          $(':input[type="submit"]').prop('disabled', false);
        }
      },
      error: function() {}
    });
  });

  function editRoleModal(roleID) {
    $.ajax({
      url: '<?=base_url('User/get_role')?>',
      type: 'POST',
      data: {
        roleID
      },
      success: function(data) {
        $('#editRoleModal').modal('show');
        var role = $.parseJSON(data);
        $('#edit_roleId').val(roleID);
        $('#edit_role').val(role.role);
        $('#edit_status').val(role.status);

      }
    });
  }

  function delete_role(roleID) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't to delet it!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: '<?=base_url('User/delete_role')?>',
          type: 'POST',
          data: {
            roleID
          },
          dataType: 'json',
          success: function(data) {
            if (data.status == 200) {
              toastr.success(data.message);
              location.href = "<?=base_url('role')?>"

            } else if (data.status == 302) {
              toastr.error(data.message);
            }
          }
        })
      }
    })
  }

  function downloadRoles() {
    var messageText = "You want download  All Roles!";
    var confirmText = 'Yes, download it!';
    var message = "Roles downloaded Successfully!";

    Swal.fire({
      title: 'Are you sure?',
      text: messageText,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: confirmText
    }).then((result) => {
      if (result.isConfirmed) {

        location.href = "<?=base_url('User/export_roles')?>";
      }
    })
  }

  function setRoleFilterSession(type){
    $.ajax({
      url: '<?=base_url('User/setRoleFilterSession')?>',
      type: 'POST',
      data: {
        type
      },
      success: function(data) {
       location.reload();  
      }
    });
  }
  </script>