<style>
.no-border {
  border: none;
}
@media (min-width: 992px)
.modal-lg {
    max-width: 1100px!important;
}
</style>
<div class="page-body">
  <div class="container-fluid">
    <div class="page-title">
      <div class="row">
        <div class="col-6">
          <h3><?=$title?></h3>
        </div>
        <div class="col-6">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="javascript:void(0);">
                <svg class="stroke-icon">
                  <use href="<?=base_url('public/assets/svg/icon-sprite.svg#stroke-home')?>"></use>
                </svg>
              </a>
            </li>
            <li class="breadcrumb-item">Dashboard</li>
            <li class="breadcrumb-item active"><?=$title?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>

  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <!-- Individual column searching (text inputs) Starts-->
    <div class="card container">
      <div class="row  ">
        <div class="card-header">
          <div class="col-md-6">
            <div class="form-group ">
              <label for="">Select Projects :</label>
              <select name="project" id="" class="form-control  js-example-basic-single" onchange="projectFilter(this.value)">
                <option value="" disabled selected>Select Project</option>
                <?php foreach($bom_projects as $project){?>
                <option value="<?=$project['project_id']?>"
                  <?=$this->session->userdata('BomprojectID') == $project['project_id'] ? 'selected' : ''?>>
                  <?=$project['name']?></option>
                <?php }?>
              </select>
              <?php if(!empty($this->session->userdata('BomprojectID'))){?>
              <a href="javacript:void(0);" class="text-danger" onclick="resetFilter(this.value)">Reset</a>
              <?php }?>
            </div>
          </div>

          <?php 
                  if($this->session->userdata('id')==1){?>
          <a href="<?=base_url('bom-edit-history')?>" class="btn btn-success float-left"> Bom Edit History</a>
          <a href="<?=base_url('bom-discard-history')?>" class="btn btn-outline-success float-left ml-2">Bom
            Discard History</a>
          <?php }else{?>
          <a href="<?=base_url('bom-discard-history')?>" class="btn btn-outline-success float-left ml-2">Bom
            Reassign History</a>

          <?php }?>
          <?php if($permission[1]=='add'){?>
          <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#uploadBom"
            data-whatever="@fat">Upload Bom</button>
            
          <button type="button" class="btn btn-outline-primary float-right mr-2" data-toggle="modal"
            data-target="#addBom" data-whatever="@fat">Add Bom</button>
            <?php } ?>
        </div>
        <div class="card-body">
          <div class="table-responsive product-table nowrap">
            <table class="display" id="projectDataTable">
              <thead>
                <tr>
                  <th>SNO</th>
                  <th nowrap>Bom Title</th>
                  <th nowrap>Bom Product</th>
                  <th nowrap>Requested To</th>
                  <th nowrap>Added By</th>
                  <th>PDF</th>
                  <th nowrap>Material Status</th>
                  <th nowrap>Created On</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- Individual column searching (text inputs) Ends-->
  </div>
</div>
<!-- Container-fluid Ends-->
</div>
<!-- Add product Modal -->
<div class="modal fade" id="addBom" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Bom</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form methos="POST" class="row" action="<?=base_url('Bom/store_bom')?>" id="createBomForm">
          <div class="form-group col-md-5">
            <label for="">Select Project :</label>
            <select name="project" id="project" class="js-example-basic-single" >
              <?php foreach($projects as $project){ ?>
              <option value="<?=$project['id']?>"><?=$project['name']?></option>
              <?php  }?>
            </select>
          </div>
          <div class="form-group col-md-6 row">
            <!-- <div class="col-md-3" style="padding: 0; display: flex; align-items: center; justify-content: flex-end;"><label for="title">Enter Title :</label></div> -->
            <div class="" style="padding: 28px 26px 0 16px;"><input type="text" name="title" id="title"
                class="form-control" placeholder="Enter Bom Title"></div>
             </div>
             <div class="form-group col-md-8">
            <label for="">Select Products :</label>
            <select name="products[]" id="productsData" class="js-example-basic-multiple" multiple onchange="getProductRows()">
              <?php foreach($products as $product){ ?>
              <option value="<?=$product['product_code']?>"><?=$product['product_code']?></option>
              <?php  }?>
            </select>
          </div>
          <div id="getProductRows" class="row"></div>
            <!-- <div id="multiple_form" class="row">

              <div class="required_inp ad_row " style="margin:2px;">
                <div class="w40"><input type="text" class="form-control w100" id="product_code" name="product_code[]"
                    placeholder="Enter Product Code"></div>
                <div class="w40"><input type="text" class="form-control w100" id="qty" name="qty[]"
                    placeholder="Enter Quantity"></div>
                <span class="customLook"></span><button type="button" id="addmore" onclick="addField()"><i
                    class="fa fa-plus"></i></button>
            </div> -->


          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- product detail model--->
<div class="modal fade" id="productDetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Product Detail</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid basic_table">
          <div class="card">
            <form action="<?=base_url('Bom/assignToVendor')?>" id="vendorAsign" method="post">
              <div class="table-responsive"  style="max-height: 450px;">
                <table class="table">
                  <thead>
                    <tr class="border-bottom-primary">
                      <?php $Id =  $this->session->userdata('role_id');
                                    if($Id== 4){ ?>
                      <th scope="col">S.No.</th>
                      <?php }?>
                      <th scope="col" nowrap>Product Code</th>
                      <th scope="col" nowrap>Part Name</th>
                      <th scope="col" nowrap>Specification</th>
                      <th scope="col" nowrap>Make</th>
                      <th scope="col" nowrap>Qty</th>
                       <th scope="col" nowrap>Unit Type</th>
                      <th scope="col">Stock</th>
                      <th scope="col" nowrap>Req Qty</th>
                    </tr>
                  </thead>
                  <tbody id="products">
                  </tbody>
                </table>
              </div>
            </form>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<!-- Assign to vendor Modal -->
<div class="modal fade" id="vendorAssignModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Assign Bom To Vendor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid basic_table">
          <div class="card">
            <form action="<?=base_url('Bom/assignToVendor')?>" id="vendorAsign" method="post">
              <div class="table-responsive"  style="max-height: 450px;">
                <table class="table">
                  <thead>
                    <tr class="border-bottom-primary">
                         <?php if($Id==7 || $Id == 1){?>
                      <th scope="col" nowrap><input type="checkbox" id="select_all" /><label for="select_all"></label></th>
                      <?php } $Id =  $this->session->userdata('role_id');
                                    if($Id==3){?>
                      <th scope="col">S.No.</th>
                      <?php }?>
                      <th scope="col" nowrap>Assigned Vendor</th>
                      <th scope="col" nowrap>Product Code</th>
                      <th scope="col" nowrap>Part Name</th>
                      <th scope="col" nowrap>Specification</th>
                      <th scope="col" nowrap>Make</th>
                      <th scope="col" nowrap>Qty</th>
                      <th scope="col" nowrap>Stock</th>
                      <th scope="col" nowrap>Req Qty</th>
                     
                    </tr>
                  </thead>
                  <tbody id="products_vendor">
                  </tbody>
                </table>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="uploadBom" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Upload BOM</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
      </div>

      <form action="<?=base_url('Bom/bom_upload_excel')?>" method="post" id="bulkFileForm"
        enctype="multipart/form-data">
        <div class="modal-body">
          <div clas="form-group">
            <label>Select Project*</label>
            <select name="project" id="project" class="form-control">
              <option value="" disabled selected>Select Project</option>
              <?php foreach($projects as $project){ ?>
              <option value="<?=$project['id']?>"><?=$project['name']?></option>
              <?php  }?>
            </select>
          </div>
          <div class="form-group">
            <label>Enter Title*</label>
            <input type="text" name="title" class="form-control" placeholder="Enter Bom Title">
          </div>
          <div>
            <label>Upload Bom Excel File*</label>
            <input type="file" name="bom_excel" id="bom_excel" class="form-control">
          </div>
        </div>
        <div class="modal-body">
          <div>
            <label>Upload Bom Pdf File(Optional)</label>
            <input type="file" name="bom_pdf" id="bom_pdf" class="form-control">
          </div>
        </div>
        <div class="modal-footer">
           <a href="<?=base_url("Bom/downlodBomFormat")?>" class="btn btn-warning ">Download Bom Format</a> 
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="editBomModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit BOM</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
      </div>

      <form action="<?=base_url('Bom/update_bom')?>" method="post" id="update_bom" enctype="multipart/form-data">
        <div class="modal-body" id="edit_bom">

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Update Bom</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="requestedPModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Requested Products For Project</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="container-fluid basic_table">
          <div class="card">
            <div class="table-responsive">
              <table class="table" id="requestedProducts">
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
<div class="modal fade" id="requestModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Send Request For Products</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <form action="<?=base_url('Bom/send_request')?>" id="requestForm">
          <div class="table table-responsive" style="max-height:400px">
              <table class="datatable">
                <thead>
                  <tr>
                    <th nowrap>S. No.</th>
                    <th nowrap>Product Code</th>
                    <th nowrap>Part Name</th>
                    <th nowrap>Specification</th>
                    <th nowrap>Required Qty</th>
                  </tr>
                </thead>
                <tbody id="bomProducts">

                </tbody>
              </table>
          </div>
          <!-- <div class="form-group" style="padding-right: 59px">
                    
                        <textarea name="req_comment" id="req_comment" class="form-control" placeholder="Enter any comment regards Request"></textarea>
                    </div> -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Send Request</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  $("#addmoreR").click(function() {
    $("#multiple_Rform").append(
      '<div class="required_inp d-flex" style="margin:5px;"><input type="text" class="form-control" name="product_code[]" placeholder="Enter Product Code"><input type="number" class="form-control" name="qty[]" placeholder="Enter Quantity">' +
      '<span class="customLook"></span><button type="button" class="inputRemove">x</button></div>'
    );
  });
  $('body').on('click', 'button.inputRemove', function() {
    $(this).parent('div.required_inp').remove()
  });
});


function addEditField() {
  $("#multipleEdit_form").append(
    '<div class="required_inp ad_row " style="margin:5px -10px 0 20px;"><div class="w40"><input name="product_code[]" class="form-control" type="text" placeholder="Enter Product Code"></div><div class="w40"><input type="number" class="form-control" name="qty[]" placeholder="Enter Quantity"></div>' +
    '<span class="customLook"></span><button type="button" class="inputRemove text-danger"><i class="fa fa-times"></i></button></div>'
  );
}
 
function addField() {
  $("#multiple_form").append(
    '<div class="required_inp  ad_row mb-2" style="margin:2px;"><div class="w40"><input name="product_code[]" class="form-control w100" type="text" placeholder="Enter Product Code"></div><div class="w40"><input type="number" class="form-control w100" name="qty[]" placeholder="Enter Quantity"></div>' +
    '<span class="customLook"></span><button type="button" class="inputRemove text-danger"><i class="fa fa-times"></i></button></div>'
  );
}
$(document).ready(function() {
  //$("#addmore").click(function() {
  // alert('hi');die;
  //$("#multiple_form").append('<div class="required_inp d-flex" style="margin:5px;"><input name="edate[]" class="form-control digits" type="date"><input type="text" class="form-control" name="particular[]" placeholder="Enter particular Remark"><input type="number" class="form-control" name="expence[]" placeholder="Enter Expence">' + '<span class="customLook"></span><button type="button" class="inputRemove">x</button></div>');
  ///});
  $('body').on('click', 'button.inputRemove', function() {
    $(this).parent('div.required_inp').remove()
  });
});
</script>
<script>
function getBom(projectId) {
  //alert('hi');die;
  $.ajax({
    url: '<?=base_url('Bom/check_bom')?>',
    type: 'POST',
    data: {
      projectId
    },
    dataType: 'json',
    success: function(data) {
      if (data.status == 403) {
        toastr.error(data.message);
      } else {
        toastr.error(data.message);
      }
    },
    error: function() {}
  });
}
// $(document).ready(function getBomData(projectID) {
$(document).ready(function() {
  var dataTable = $('#projectDataTable').DataTable({
    order: [],
    // "processing": true,
    // "serverSide": true,
    // buttons: [{
    //     extend: 'excelHtml5',
    //     text: 'Download Excel'
    // }],
    
    "ajax": {
      url: "<?=base_url('Bom/ajaxboms')?>",
      type: "POST",
    },
    // "columnDefs": [{
    //           "targets": [0],
    //           "orderable": true,
    //       }, ],
  });
});


$('#addProjectModal').on('shown.bs.modal', function() {
  $('#myInput').trigger('focus')
})

$("form#bulkFileForm").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('bom')?>";

        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});


$("form#update_bom").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('bom')?>";

        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

function view_product(BomID, vendorID) {
  $('#productDetailModal').modal('show');
  $.ajax({
    url: '<?=base_url('Bom/get_viewproducts')?>',
    type: 'POST',
    data: {
      BomID,
      vendorID
    },
    success: function(data) {
      $('#products').html(data);

    }
  });

}


function vendorAssignModal(BomID) {
  
  $.ajax({
    url: '<?=base_url('Bom/get_products')?>',
    type: 'POST',
    data: {
      BomID
    },
    success: function(data) {
      $('#vendorAssignModal').modal('show');
      $('#products_vendor').html(data);

    }
  });
}

$("form#createBomForm").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('bom')?>";

        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

function delete_project(productId) {
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't to delet it!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url: '<?=base_url('Project/delete_project')?>',
        type: 'POST',
        data: {
          productId
        },
        dataType: 'json',
        success: function(data) {
          if (data.status == 200) {
            toastr.success(data.message);
            location.href = "<?=base_url('project')?>"

          } else if (data.status == 302) {
            toastr.error(data.message);
          }
        }
      })
    }
  })
}
</script>
<script>
function discard_bom(bomId, projectId, addedBy) {

  (async () => {

    const {
      value: reason
    } = await Swal.fire({
      title: 'Enter Reason for Discard',
      input: 'text',
      inputLabel: 'Your Reason',
      inputPlaceholder: 'Enter Reason',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Discard it!',
      inputValidator: (value) => {
        if (!value) {
          return 'Please Enter Reason!'
        }
      }
    }).then((reason) => {
      if (reason) {
        var reas = reason.value;
        $.ajax({
          url: '<?=base_url('Bom/discard_bom')?>',
          type: 'POST',
          data: {
            bomId,
            projectId,
            addedBy,
            reas
          },
          dataType: 'json',
          success: function(data) {
            if (data.status == 200) {
              toastr.success(data.message);
              location.href = "<?=base_url('bom')?>"
            } else if (data.status == 302) {
              toastr.error(data.message);
            }
          }
        })
      }
    })
  })()

}

function delete_bom(bomId) {
  Swal.fire({
    title: 'Are you sure?',
    text: "You want to delete it!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url: '<?=base_url('Bom/delete_bom')?>',
        type: 'POST',
        data: {
          bomId
        },
        dataType: 'json',
        success: function(data) {
          if (data.status == 200) {
            toastr.success(data.message);
            location.href = "<?=base_url('bom')?>"
          } else if (data.status == 302) {
            toastr.error(data.message);
          }
        }
      })
    }
  })
}


$("form#vendorAsign").submit(function(e) {

  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('bom')?>";

        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});


function editBomModal(bomID) {
  //alert(bomID);
  $.ajax({
    url: '<?=base_url('Bom/get_bom')?>',
    type: 'POST',
    data: {
      bomID
    },
    //dataType: 'json',
    success: function(data) {

      $('#editBomModal').modal('show');
      var project = $.parseJSON(data);
      $('#edit_bom').html(project);
    }
  });
}

function requestModal(bomID) {
  //alert(bomID);die;
  $('#requestModal').modal('show');
  $.ajax({
    url: '<?=base_url('Bom/requestBomProducts')?>',
    type: 'POST',
    data: {
      bomID
    },
    success: function(data) {
      $('#bomProducts').html(data);
    }
  });
}

function requestedPModal(id) {
  $('#requestedPModal').modal('show');
  $.ajax({
    url: '<?=base_url('Bom/get_requestedProducts')?>',
    type: 'POST',
    data: {
      id
    },
    success: function(data) {
      $('#requestedProducts').html(data);
    }
  });
}

$("form#requestForm").submit(function(e) {

  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('bom')?>";

        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

function projectFilter(id) {
  $.ajax({
    url: "<?=base_url('Bom/projectFilter')?>",
    type: 'POST',
    data: {
      id
    },
    success: function(data) {
      location.reload();
    },
    error: function() {}
  });
}

function resetFilter() {
  $.ajax({
    url: "<?=base_url('Bom/resetProjectFilter')?>",
    type: "POST",
    data: {},

    success: function(data) {
      location.reload();
    },
    error: function() {}
  });
}

function onoffswitchspublish(svalue, qid) {
  //alert(svalue);die;
  if (svalue == 2) {

    Swal.fire({
      title: 'Do you want to Diss Approve Quotation',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Continue'
    }).then((result) => {

      if (result.isConfirmed) {

        $.ajax({
          type: 'POST',
          url: '<?php echo base_url();?>Bom/ApproveProductRequest',
          data: "id=" + qid + "&status=" + svalue,
          dataType: "html",
          success: function(data) {
            Swal.fire(
              'Status Change!',
              'Diss Approved sucessfully.',
              'success'
            )
            location.reload();
          }

        })

      } else {

        location.reload();
      }

    })



  } else {

    Swal.fire({
      title: 'Do you want to Approve Quotation',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Continue'
    }).then((result) => {

      if (result.isConfirmed) {

        $.ajax({
          type: 'POST',
          url: '<?php echo base_url();?>Bom/ApproveProductRequest',
          data: "id=" + qid + "&status=" + svalue,
          dataType: "html",
          success: function(data) {
            Swal.fire(
              'Status Change!',
              'Approved sucessfully.',
              'success'
            )
            location.reload();
          }

        })

      } else {
        location.reload();
      }

    })

  }
}

function downloadBom(id){
    var messageText  = "You want download  Bom!";
      var confirmText =  'Yes, download it!';
      var message  ="Bom downloaded Successfully!";
   
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {

          location.href="<?=base_url('Bom/export_bom/')?>"+id;  
        }
        })
  }

  function getProductRows(){
    var products = $("#productsData").val();
    //alert(products);
    $.ajax({
    url: '<?=base_url('Bom/getProductRows')?>',
    type: 'POST',
    data: {
      products
    },
    success: function(data) {
      $('#getProductRows').html(data);

    }
  });
}

function checkbomqty(qty,code){
  //alert(code);
   var orgid = '#bomqty_'+code;
   var qtyid = '#qty_'+code;
   var origQty = $(orgid).val();
   if(parseFloat(qty) > parseFloat(origQty)){
    toastr.error("Requested Qty must be Less than or Equal to bom's product qty");
    $(qtyid).val(origQty);
   }
   //alert(origQty);
}
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#select_all').on('click',function(){
        if(this.checked){
            $('.form-check-input').each(function(){
                this.checked = true;
            });
        }else{
             $('.form-check-input').each(function(){
                this.checked = false;
            });
        }
    });
    
    $('.form-check-input').on('click',function(){
      
        if($('.checkbox:checked').length == $('.checkbox').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
    });
});
</script>