<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title>Delivery Challan</title>
</head>

<body>
  <center>
    <div style="width:100%;border: 2px solid #55714c;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style=" font-size:14px;">
        <tbody style="background-color: #fff;  padding-bottom: 20px">

          <tr>
            <td align="cenetr" colspan="5">
              <h1 style="text-align: center;    margin: 0;;">Delivery Challan</h1>
              <h3 style="text-align: center;    margin: 0;">Returnable / Non Returnable</h3>
            </td>
          </tr>

          <tr>
            <td colspan="5">
              <table style="width: 100%;">
                <tbody>
                  <tr>
                    <td valign="top" style="padding:5px; width: 70%; ">
                      <table style="width: 100%;">
                        <tr>
                          <td colspan="4">
                            <div class="" style="text-align: center;">
                              <h3 style="margin: 5px; font-weight: bold;">HINDIKO SOLUTION INDIA PVT LTD</h3>
                              <p style="margin: 5px; font-weight: bold;">484, ECOTECH 3, UDYOG KENDRA 2</p>
                              <p style="margin: 5px; font-weight: bold;">UTTAR PRADESH, Code : 09</p>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td style="width: 24%;font-weight: 600;">Phone :</td>
                          <td style="width: 24%;font-weight: 600;"><?=$siteinfo->site_contact?></td>
                          <td style="width: 24%;font-weight: 600;">Email :</td>
                          <td style="width: 24%;font-weight: 600;"><?=$siteinfo->site_email?></td>
                        </tr>
                      </table>
                    </td>
                    <td valign="top" style="padding:5px; width: 30%;text-align: right;">
                      <img src="<?=base_url($siteinfo->site_logo)?>" alt="logo" style="width: 100%;height: 66px;">
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>

        </tbody>
      </table>
      <table width="100% " border="0" cellspacing="0" cellpadding="0"
        style="    border-top: 3px solid #000;       font-weight: 500; padding-top: 20px;margin-top:0px; font-size:14px">
        <tbody>
          <tr>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:15%;"><b>Bill To:</b></td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:35%;"><?=$user->vendorName?></td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:35%;"><b>Delivery Challan No.</b>
            </td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:15%;"><?=$user->challan_no?></td>
          </tr>
          <tr>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:15%;">- </td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:35%;">- </td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:35%;">DC Date and Time</td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:15%;">
              <?=date('d-m-Y H:i:s',strtotime($user->challan_date_time))?></td>
          </tr>
          <tr>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:15%;"> Address:</td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:35%;"><?=$user->vendorAddress?>
            </td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:35%;">Mode of Delivery <br><br>PO
              number</td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:15%;">
              <?=$user->mode_of_delivery?><br><br> <span><?=$user->po_no?></span></td>
          </tr>
          <tr>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:15%;">State :</td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:35%;"> <?=$user->vendorState?>
            </td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:35%;">Vehicle Number</td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:15%;"><?=$user->vehicle_no?></td>
          </tr>
          <tr>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:15%;">GSTN Number :</td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:35%;"><?=$user->vendorGst?></td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:35%;">Place of Supply</td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:15%;"><?=$user->place_of_supply?>
            </td>
          </tr>
          <tr>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:15%;">Phone :
              <?=$user->vendorPhone?></td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:35%;">Email :
              <?=$user->vendorEmail?></td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:35%;">FAX : <?=$user->fax?></td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:15%;"></td>
          </tr>

        </tbody>
      </table>
      <br>
      <table style="width: 100%;padding-bottom: 10px; text-align: center;border-top: 3px solid #000;border-spacing: 0;">
        <thead style="    background-color: #c0d0c2;">
          <tr>
            <th style="width: 5%;">No</th>
            <th style="width: 17%;">Product Description</th>
            <th style="width: 12%;">HSN Codes</th>
            <th style="width: 9%;">Qty</th>
            <th style="width: 9%;">Unit</th>
            <th style="width: 12%;">Unit Rate</th>
            <th style="width: 12%;">GST Taxes</th>
            <th style="width: 18%;">Sub total (Without Tax)</th>
          </tr>
        </thead>
        <tbody>

          <?php $sno=1; foreach($products as $key=>$data){?>
          <tr>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"><?=$sno?></td>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"><?=$key?></td>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"></td>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"><?=$data->qty?></td>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"></td>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"><?=$data->iqty?></td>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"></td>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"></td>
          </tr>
          <?php $sno++;}?>
          <tr>
            <td colspan="8" style="    background-color: #c0d0c2;"></td>

          </tr>
          <tr>
            <td colspan="6" style=" text-align: right;">Delivery Challan Subtotal</td>
            <td style=""></td>
            <td style="border-bottom: 2px solid #c0d0c2;text-align: right;">Rs. </td>
          </tr>
          <tr>
            <td colspan="7" style=""></td>
            <td style="border-bottom: 2px solid #c0d0c2; text-align: right;">Rs.</td>
          </tr>
          <tr>
            <td colspan="6" style=" text-align: right;">Total GST Amount</td>
            <td style=""></td>
            <td style="border-bottom: 2px solid #c0d0c2;text-align: right;">Rs. </td>
          </tr>
          <tr>
            <td colspan="3"></td>
            <td style="">SAC Code</td>
            <td style="background-color: #c0d0c2;"></td>
            <td colspan="" style=" text-align: right;">Shipping charges</td>
            <td style=""></td>
            <td style="border-bottom: 2px solid #c0d0c2;text-align: right;">Rs. </td>
          </tr>

          <tr>

            <td colspan="8" style=" margin-top: 5px;   background-color: #c0d0c2;"></td>

          </tr>
          <tr style=" background-color: #c0d0c2;">
            <td colspan="6" style=" text-align: right;">Total</td>
            <td style=""></td>
            <td style="border-bottom: 2px solid #c0d0c2;text-align: right;">Rs.</td>
          </tr>
          <tr>
            <td colspan="3" style="text-align: left; padding-left: 20px; color: red;">Total Amount in Words</td>
            <td colspan="1">-</td>
            <td colspan="4"></td>
          </tr>
          <tr>
            <td colspan="4 " style="text-align: left; padding-left: 20px;">
              <p style="margin-bottom: 0;"><b>Terms and Conditions</b></p>
              <p style="margin-bottom: 0;">Only for delivery record</p>
            </td>
            <td colspan="4"></td>
          </tr>
          <tr>
            <td colspan="8" style="text-align: right; padding-right:10px;">
              <div style="display: inline-block;
border: 1px solid;
padding: 6px;
text-align: center;">
                for <b>Hindiko Solution India Pvt LtdAuthorized</b>
                <br>
                <br>
                <br>
                Signatory
              </div>
            </td>
          </tr>

        </tbody>
      </table>
    </div>
  </center>

</body>

</html>