
<div class="page-body">
          <div class="container-fluid">
            <div class="page-title">
              <div class="row">
                <div class="col-6">
                  <h3><?=$title?></h3>
                </div>
                <div class="col-6">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?=base_url('Dashboard')?>">                                       
                        <svg class="stroke-icon">
                          <use href="<?=base_url('public/assets/svg/icon-sprite.svg#stroke-home')?>"></use>
                        </svg></a></li>
                    <li class="breadcrumb-item">Users</li>
                    <li class="breadcrumb-item active"><?=$title?></li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid starts-->
          <div class="container-fluid">
            <div class="row">
              <!-- Individual column searching (text inputs) Starts-->
              <div class="col-sm-12">
                <div class="card">
                  <div class="card-header">
                      <?php if($this->session->userdata('role_id')!=4){?>
                 
                   <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#pendingEnq" data-whatever="@fat">Pending Requests By vendors</button>
                    </button>
                    <?php }?>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive product-table">
                      <table class="display" id="qoutationTable">
                        <thead>
                          <tr>
                            <th>SNo</th>
                            <?php if($this->session->userdata('role_id')== 4){?>
                            <th>Name</th>                           
                            <th nowrap> Qoutation Remark</th>
                            <th nowrap>Delivery Date</th>
                            <th nowrap> Date</th>
                            <th>Status</th>
                            <?php }if($this->session->userdata('role_id')!= 4){?>
                            <th nowrap>Project Name</th>
                            <th nowrap>Bom  Title</th>
                            <!-- <th nowrap>Qoutation By</th> -->
                            <th>Date</th>
                            <?php }?>
                           
                            <th>Action</th>
                          </tr>
                        </thead>
                    
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Individual column searching (text inputs) Ends-->
            </div>
          </div>
          <!-- Container-fluid Ends-->
        </div>

        <div class="modal fade" id="pendingEnq" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Pending Request List</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
              </div>
         
         <div class="modal-body">
              <div id="multiple_form" class="row"> 
                <div class="table-responsive">
                  <table class="table">
                      <thead>
                        <tr class="border-bottom-primary" >
                          <th>S.No.</th>
                          <th>Bom </th>
                          <th>Vendor Name</th>
                          <th nowrap class="text-danger">Assigned Date</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?=!empty($enq)?$enq:'<td colspan="5" class="text-center">No data Found</td>'?>
                      </tbody>
                  </table>    
                </div>            
              </div>  
      </div>
      </div>
   </div>
</div>

<div class="modal fade" id="viewQuoteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog  modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">View Quotation</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
         <div class="container-fluid basic_table">
                <div class="card">
                    <form action="<?=base_url('Bom/sendQuotation')?>" id="sendQuote" method="post">
                        <div class="table-responsive">
                            <table class="table">
                            <thead>
                                <tr class="border-bottom-primary">
                                <?php $Id =  $this->session->userdata('role_id');
                                    ?>
                                   
                                <!-- <th scope="col">Product Code</th> -->
                                <th scope="col" nowrap>Part Name</th>
                                <th scope="col">Specification</th>
                                <th scope="col">Make</th>
                                <th scope="col">Qty</th>
                                <th scope="col">Price/Unit</th>                               
                                <th scope="col" nowrap>Total Price</th>
                                
                                </tr>
                            </thead>
                                <tbody id="quote_products">
                                </tbody>
                            </table>
                        </div>
                    </form>
            </div>
            </div>
            </div>
      </div>
   </div>
</div>

</div>
        <script>
function onoffswitchspublish(svalue,qid)
  {
          if(svalue == 'Disapprove'){   
            
            (async () => {
            const { value: reason } = await Swal.fire({
            title: 'Enter Reason for Disapprove Qoutation',
            input: 'text',
            inputLabel: 'Your Reason',
            inputPlaceholder: 'Enter Reason',
            type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Disapprove it!',
              inputValidator: (value) => {
              if (!value) {
                  return 'Please Enter Reason!'
              }
            }
            }).then((reason) => {
              
                if (reason) {
                  var reas = reason.value;
                    $.ajax({
                        url: '<?=base_url('Bom/quotation_approval')?>',
                        type: 'POST',
                        data: "id="+ qid+"&status="+svalue+"&reason="+reas,
                        dataType: 'json',
                        success: function(data) {
                            if (data.status == 200) {
                                toastr.success(data.message);
                                location.reload();
                            } else if (data.status == 302) {
                                toastr.error(data.message);
                            }
                        }
                    })
                }
            })
            })()
            
  }else{
  
      Swal.fire({
          title: 'Do you want to Approve Quotation',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Continue'
          }).then((result) => {
              
      if (result.isConfirmed) {
  
      $.ajax({
      type:'POST',
      url:'<?php echo base_url();?>Bom/quotation_approval',
      data:"id="+ qid+"&status="+svalue,
      dataType: "html",
      success:function(data)
      {    
          Swal.fire(
              'Status Change!',
             'Approved sucessfully.',
              'success'
                  )
              location.reload();
       }
  
       })
  
      }else{
  
          location.reload();
      }
  
      })
  
  }}
function get_tprice(price,productCode){
  
    var qty = document.getElementById('qty_'+productCode).value;
    var totalprice = price * qty;
    document.getElementById('tprice_'+productCode).value = totalprice;
  }
function viewQuoteModal(ID){
    $('#viewQuoteModal').modal('show');
    var page_name = '<?=$title?>';
    //alert(page_name);die;
    $.ajax({
        url: '<?=base_url('Bom/get_quotations')?>',
        type: 'POST',
        data: {ID,page_name},
        success: function(data) {
            $('#quote_products').html(data);        
               
        }
    });
   }

   $(document).ready(function() {
    //alert('hi');die;
    $.ajax({
      url: '<?=base_url('Bom/disappr_quotations')?>',
      type: 'POST',
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      dataType: 'json',
      success: function (data) {
        $('#diss_qoutes').html(data);
      },
      error: function(){} 
      });
   });
  $(document).ready(function() {
      // $('#example').DataTable();
      // } );
      var role = '<?=$title?>';
      var dataTable = $('#qoutationTable').DataTable({
          // "processing": true,
          // "serverSide": true,
          // buttons: [{
          //     extend: 'excelHtml5',
          //     text: 'Download Excel'
          // }],
          "order": [],
          "ajax": {
              url: "<?=base_url('Bom/ajaxQuotations')?>",
              type: "POST",
              data:{role}
          },
          // "columnDefs": [{
          //     "targets": [0],
          //     "orderable": false,
          // }, ],
      });
  });


  $('#creatUserModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})

$("form#sendQuote").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  toastr.success(data.message);
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){

     location.href="<?=base_url('Enquiry')?>"; 	
    
  }, 1000) 
  
  }else if(data.status==403) {
  toastr.error(data.message);

  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastr.error(data.message);
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });


  $("form#editUserForm").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  toastr.success(data.message);
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){

     location.href="<?=base_url('User')?>"; 	
    
  }, 1000) 
  
  }else if(data.status==403) {
  toastr.error(data.message);

  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastr.error(data.message);
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });

 
</script>