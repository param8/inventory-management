<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Authantication';
$route['project'] = 'Project/index';
$route['expence'] = 'Project/expence';
$route['bom'] = 'Bom/index';
$route['requested-bom/(:any)'] = 'Bom/vendor_assigned';
$route['bom-discard-history'] = 'Bom/bom_discard_history';
$route['bom-edit-history'] = 'Bom/bom_edit_history';
$route['enquiry'] = 'Bom/inquiry';
$route['quotation'] = 'Bom/quotation';
$route['view-quotations/(:any)'] = 'Bom/viewQoutations/$1';
$route['PO'] = 'Bom/po';
$route['orders'] = 'Bom/po';
$route['manage-store'] = 'Bom/store';
$route['dc'] = 'Bom/dc';
$route['add-dc'] = 'Bom/dcForm';
$route['returnable-products'] = 'Bom/returnableProducts';
$route['in-material-history'] = 'Bom/in_materialHistory';
$route['out-material-history'] = 'Bom/out_materialHistory';
$route['product'] = 'Product/index';
$route['site_setting'] = 'Setting/site_setting';
$route['404_override'] = '';
$route['create-user'] = 'User/createUser';
$route['role'] = 'User/role';
$route['users/(:any)'] = 'User/index/$1';
$route['vendor'] = 'User/createVendor';
$route['Profile'] = 'User/profile';
$route['change-password'] = 'User/password';
$route['unit'] = 'Setting/unit_setting';
$route['edit-user/(:any)'] = 'User/edit_user';
$route['location'] = 'Setting/location';

?>