<style>
.no-border {
  border: none;
}

.show-hide {
  position: absolute;
  top: 18px;
  right: 20px;
  transform: translateY(-50%);
}
</style>
<div class="page-body">
  <div class="container-fluid">
    <div class="page-title">
      <div class="row">
        <div class="col-6">
          <h3><?=$title?></h3>
        </div>
        <div class="col-6">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?=base_url('dashboard')?>">
                <svg class="stroke-icon">
                  <use href="<?=base_url('public/assets/svg/icon-sprite.svg#stroke-home')?>"></use>
                </svg>
              </a>
            </li>
            <li class="breadcrumb-item">Dashboard</li>
            <li class="breadcrumb-item active"><?=$title?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>

  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <!-- Individual column searching (text inputs) Starts-->
    <div class="card container">
      <div class="row ">
        <div class="card-header">

          <?php if($permission[1]=='add'){?>
          <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#addVendor"
            data-whatever="@fat">Add Vendor</button>
          <!-- <button type="button" class="btn btn-outline-primary float-right mr-2" data-toggle="modal" data-target="#bulkProductUploadModal" data-whatever="@fat">Bulk Upload Project</button> -->
          <?php }?>
        </div>
        <div class="card-body">
          <div class="table-responsive product-table">
            <table class="display" id="userDataTable">
              <thead>
                <tr>
                  <th>S. No</th>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Contact</th>
                  <?php if($this->session->userdata('role_id')==1){?>
                  <th>Password</th>
                  <?php }?>
                  <th>Created Date</th>
                  <?php if($this->session->userdata('role_id')==1){?>
                  <th>Action</th>
                  <?php }?>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- Individual column searching (text inputs) Ends-->
  </div>
</div>
<!-- Container-fluid Ends-->
</div>
<!-- Add product Modal -->
<div class="modal fade" id="editUserModal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel1">Edit Vendor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form methos="POST" action="<?=base_url('User/edit_vendor')?>" id="editUserForm">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <input type="hidden" name="edit_userId" id="edit_userId">
                <label class="col-form-label pt-0"><i class="fa fa-user"> Vendor Code</i></label>
                <input class="form-control" type="text" name="edit_vendor_code" id="edit_vendor_code"
                  placeholder="Enter Vendor Code">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="col-form-label pt-0"><i class="fa fa-user"> Name</i></label>
                <input class="form-control" type="text" name="edit_name" id="edit_name" placeholder="Enter name">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="col-form-label"><i class="fa fa-envelope"> Email Address</i></label>
                <input class="form-control" type="email" name="edit_email" id="edit_email"
                  placeholder="Enter email Address" autocomplete="off">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="col-form-label"><i class="fa fa-mobile"> Phone</i></label>
                <input class="form-control" type="text" name="edit_phone" id="edit_phone" placeholder="phone"
                  maxlength="10" minlength="10"
                  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"></i>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="col-form-label"><i class="fa fa-picture-o"> Profile Pic</i></label>
                <input class="form-control" type="file" name="profile_pic" id="profile_pic" accept="image/*">
              </div>
            </div>
            <div class="col-md-6">
              <input type="hidden" id="user_type" name="user_type" value="4">
              <div class="form-group">
                <label class="col-form-label"><i class="fa fa-address-card"> GST</i></label>
                <input class="form-control" type="text" id="edit_gst" name="edit_gst" placeholder="Enter GST number">
              </div>
            </div>
            <div class="col-md-6">
              <label class="col-form-label"><i class="fa fa-address-card"> Adhar-No</i></label>
              <input class="form-control" type="text" id="edit_adhar" name="edit_adhar"
                placeholder="Enter Adhar number">
            </div>
            <div class="form-group col-md-6" id="vendor_pan">
              <label class="col-form-label"><i class="fa fa-address-card"> PAN-No.</i></label>
              <input class="form-control" type="text" id="edit_pancard" name="edit_pancard"
                placeholder="Enter PAN number">
            </div>
            <div class="col-md-6">
              <label class="col-form-label"><i class="fa fa-map-marker"> State</i></label>
              <select class="form-control js-example-basic-single" id="edit_state" name="edit_state" onchange="selectCity2(this.value)">
                <option value="">Select Country</option>
                <?php foreach($states as $state ){?>
                <option value="<?=$state->id?>"><?=$state->name?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-6">
              <label class="col-form-label"><i class="fa fa-map-marker"> City</i></label>
              <select class="form-control js-example-basic-single" id="edit_city" name="edit_city">

              </select>
            </div>
            <div class="form-group col-md-6">
              <label class="col-form-label"><i class="fa fa-address-card"> Address</i></label>
              <textarea class="form-control" name="edit_address" id="edit_address" placeholder="address"></textarea>
            </div>
          </div>
          <div class="form-group mb-0">
            <button class="btn btn-primary btn-block w-100" type="submit">Update Vendor</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="addVendor" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">ADD Vendor</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="container-fluid basic_table">
          <div class="">
            <form id="Frontregister" method="POST" action="<?=base_url('Authantication/register')?>"
              enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-form-label pt-0"><i class="fa fa-user"> Vendor Code</i></label>
                    <input class="form-control" type="text" name="vendor_code" id="vendor_code"
                      placeholder="Enter Vendor Code">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-form-label pt-0"><i class="fa fa-user"> Name</i></label>
                    <input class="form-control" type="text" name="name" id="name" placeholder="Enter name">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-form-label"><i class="fa fa-envelope"> Email Address</i></label>
                    <input class="form-control" type="email" name="email" id="email" placeholder="Enter email Address"
                      autocomplete="off">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-form-label"><i class="fa fa-lock"> Password</i></label>
                    <div class="form-input position-relative">
                      <input class="form-control" type="password" name="password" id="password" placeholder="*********"
                        autocomplete="off">
                      <div class="show-hide"><span class="show"></span></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-form-label"><i class="fa fa-mobile"> Phone</i></label>
                    <input class="form-control" type="text" name="phone" id="phone" placeholder="phone" maxlength="10"
                      minlength="10"
                      oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"></i>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-form-label"><i class="fa fa-picture-o"> Profile Pic</i></label>
                    <input class="form-control" type="file" name="profile_pic" id="profile_pic" accept="image/*">
                  </div>
                </div>
                <div class="col-md-6">
                  <input type="hidden" id="user_type" name="user_type" value="4">
                  <div class="form-group">
                    <label class="col-form-label"><i class="fa fa-address-card"> GST</i></label>
                    <input class="form-control" type="text" id="gst" name="gst" placeholder="Enter GST number">
                  </div>
                </div>
                <div class="col-md-6">
                  <label class="col-form-label"><i class="fa fa-address-card"> Adhar-No</i></label>
                  <input class="form-control" type="text" id="adhar" name="adhar" placeholder="Enter Adhar number">
                </div>
                <div class="form-group col-md-6" id="vendor_pan">
                  <label class="col-form-label"><i class="fa fa-address-card"> PAN-No.</i></label>
                  <input class="form-control" type="text" id="pancard" name="pancard" placeholder="Enter PAN number">
                </div>
                <div class="col-md-6">
                  <label class="col-form-label"><i class="fa fa-map-marker"> State</i></label>
                  <select class="form-control js-example-basic-single" id="state" name="state" onchange="selectCity(this.value)">
                    <option value="">Select State</option>
                    <?php foreach($states as $state ){?>
                    <option value="<?=$state->id?>"><?=$state->name?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-6">
                  <label class="col-form-label"><i class="fa fa-map-marker"> City</i></label>
                  <select class="form-control js-example-basic-single" id="city" name="city">

                  </select>
                </div>
                <div class="form-group col-md-6">
                  <label class="col-form-label"><i class="fa fa-address-card"> Address</i></label>
                  <textarea class="form-control" name="address" id="address" placeholder="address"></textarea>
                </div>
                
              </div>
              
              <div class="form-group mb-0">
                <button class="btn btn-primary btn-block w-100" type="submit">Create Vendor</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  // $('#example').DataTable();
  // } );
  var role = '<?=$title?>';
  var dataTable = $('#userDataTable').DataTable({
    "processing": true,
    "serverSide": true,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Download Excel'
    }],
    "order": [],
    "ajax": {
      url: "<?=base_url('User/ajaxUserTable')?>",
      type: "POST",
      data: {
        role
      }
    },
    "columnDefs": [{
      "targets": [0],
      "orderable": false,
    }, ],
  });
});

$("form#Frontregister").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('vendor')?>";

        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});


function selectState(countryID) {
  $.ajax({
    url: '<?=base_url('Ajax_controller/get_state')?>',
    type: 'POST',
    data: {
      countryID
    },
    success: function(data) {
      $('#state').html(data);
    }
  });
}

function selectCity(stateID) {
  $.ajax({
    url: '<?=base_url('Ajax_controller/get_city')?>',
    type: 'POST',
    data: {
      stateID
    },
    success: function(data) {
      $('#city').html(data);
    }
  });
}

function selectCity2(stateID, cityID) {
  $.ajax({
    url: '<?=base_url('Ajax_controller/get_city')?>',
    type: 'POST',
    data: {
      stateID,
      cityID
    },
    success: function(data) {
      $('#edit_city').html(data);
    }
  });
}

$(document).ready(function() {

});

function editUserModal(userID) {
  $.ajax({
    url: '<?=base_url('User/get_vendor')?>',
    type: 'POST',
    data: {
      userID
    },
    success: function(data) {
      $('#editUserModal').modal('show');
      var vendor = $.parseJSON(data);
      $('#edit_userId').val(userID);
      $('#edit_vendor_code').val(vendor.vendor_code);
      $('#edit_name').val(vendor.name);
      $('#edit_email').val(vendor.email);
      $('#edit_image').val(vendor.profile_pic);
      $('#edit_phone').val(vendor.phone);
      $('#edit_gst').val(vendor.gst);
      $('#edit_adhar').val(vendor.adhar);
      $('#edit_pancard').val(vendor.pancard);
      $('#edit_state').val(vendor.state);
      $('#edit_state').val(vendor.state);
      $('#edit_city').val(vendor.city);
      $('#edit_address').val(vendor.address);
      var stateId = vendor.state;
      var cityId = vendor.city;
      selectCity2(stateId, cityId);

    }
  });
}

function delete_user(userID) {
  Swal.fire({
    title: 'Are you sure?',
    text: "You want to delete it!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url: '<?=base_url('User/delete_user')?>',
        type: 'POST',
        data: {
          userID
        },
        dataType: 'json',
        success: function(data) {
          if (data.status == 200) {
            toastr.success(data.message);
            location.href = "<?=base_url('User')?>"

          } else if (data.status == 302) {
            toastr.error(data.message);
          }
        }
      })
    }
  })
}
</script>
<script>
$("form#editUserForm").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {
          location.href = "<?=base_url('vendor')?>";

        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});
</script>