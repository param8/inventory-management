<section class="lo_ginn login-card">
    <div class="container-fluid p-0">
      <div class="row m-0">
        <div class="col-12 col-md-4 offset-md-4  p-0">    
          <div class=" login-dark">
            <div>
             
              <div class="login-main"> 
               <div><a class="logo" href="javscript:void(0);"><img class="img-fluid for-light" width="250" src="<?=base_url($siteinfo->site_logo)?>" alt="looginpage"><img class="img-fluid for-dark" src="../assets/images/logo/logo_dark.png" alt="looginpage"></a></div>
                <form class="theme-form" method="POST" action="<?=base_url('Authantication/login')?>" id="Adminlogin">
                  <h4>Sign in to account</h4>
                  <p>Enter your email & password to login</p>
                  <div class="form-group">
                    <label class="col-form-label">Email Address</label>
                    <input class="form-control" type="email" id="email" name="email" placeholder="Enter your email">
                  </div>
                  <div class="form-group">
                    <label class="col-form-label">Password</label>
                    <div class="form-input position-relative">
                      <input class="form-control" type="password" id="password" name="password" placeholder="*********">
                      <div class="show-hide"><span class="show"></span></div>
                    </div>
                  </div>
                  <div class="form-group mb-0">
                    <!--<div class="checkbox p-0">-->
                    <!--  <input id="checkbox1" type="checkbox">-->
                    <!--  <label class="text-muted" for="checkbox1">Remember password</label>-->
                    <!--</div><a class="link" href="forget-password.html">Forgot password?</a>-->
                    <div class="text-end mt-3">
                      <button class="btn btn-primary btn-block w-100" type="submit">Sign in</button>
                    </div>
                  </div>
                  <!-- <h6 class="text-muted mt-4 or">Or Sign in with</h6>
                  <div class="social mt-4">
                    <div class="btn-showcase"><a class="btn btn-light" href="https://www.linkedin.com/login" target="_blank"><i class="txt-linkedin" data-feather="linkedin"></i> LinkedIn </a><a class="btn btn-light" href="https://twitter.com/login?lang=en" target="_blank"><i class="txt-twitter" data-feather="twitter"></i>twitter</a><a class="btn btn-light" href="https://www.facebook.com/" target="_blank"><i class="txt-fb" data-feather="facebook"></i>facebook</a></div>
                  </div> -->
                  <!--<p class="mt-4 mb-0 text-center">Don't have account?<a class="ms-2" href="<?//=base_url('Authantication/signup')?>">Create Account</a></p>-->
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
</section>

    <script>
  $("form#Adminlogin").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  toastr.success(data.message);
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){

     location.href="<?=base_url('dashboard')?>"; 	
    
  }, 1000) 
  
  }else if(data.status==403) {
  toastr.error(data.message);

  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastr.error(data.message);
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });
  </script>