<div class="page-body">
  <div class="container-fluid">
    <div class="page-title">
      <div class="row">
        <div class="col-6">
          <h3><?=$title?></h3>
        </div>
        <div class="col-6">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?=base_url('Dashboard')?>">
                <svg class="stroke-icon">
                  <use href="<?=base_url('public/assets/svg/icon-sprite.svg#stroke-home')?>"></use>
                </svg></a></li>
            <li class="breadcrumb-item">Dashboard</li>
            <li class="breadcrumb-item active"><?=$title?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <!-- Individual column searching (text inputs) Starts-->
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header ">
            <div class="row">
              <div class="col-md-4">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#inMaterial"
                  data-whatever="@fat" data-bs-original-title="In Material">In Material</button>
                <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#outMaterial"
                  data-whatever="@fat" data-bs-original-title="Out Material">Out Material</button>
              </div>
              <div class="col-md-8 text-right">
                <a href="<?=base_url('returnable-products')?>" class="btn btn-warning text-light" data-whatever="@fat"
                  data-bs-original-title="In Material History">Returnable Products</a>
                <a href="<?=base_url('in-material-history')?>" class="btn btn-outline-success " data-whatever="@fat"
                  data-bs-original-title="In Material History">In Material
                  History</a>
                <a href="<?=base_url('out-material-history')?>" class="btn btn-success" data-whatever="@fat"
                  data-bs-original-title="Out Material History">Out Material History</a>
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive product-table">
              <table class="display" id="productDataTable">
                <thead>
                  <tr>
                    <th>Sno</th>
                    <th nowrap>Part Name</th>
                    <th nowrap>Product Code</th>
                    <th nowrap>Unit Type</th>
                    <th>Units</th>
                    <th nowrap>Specification</th>
                    <th>Make</th>
                    <th>Model</th>
                    <th>Location</th>
                    <th>Remark</th>
                    <th>Price</th>
                    <th nowrap>Added By</th>
                    <th nowrap>Created At</th>
                    <th nowrap>Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- Individual column searching (text inputs) Ends-->
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>
<!-- Edit product Modal -->
<div class="modal fade" id="editProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form class="row" methos="POST" action="<?=base_url('Product/edit_product')?>" id="editProductForm">
      <div class="col-md-6">
        <div class="form-group">
        <label for="edit_price">Edit Part Name <span class="text-danger">*</span></label>
        <input type="hidden" id="edit_productId" name="edit_productId">
        <input type="text" class="form-control" id="edit_partname" name="edit_partname" placeholder="Enter Part Name">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
        <label for="edit_price">Edit Code <span class="text-danger">*</span></label>
          <input type="text" class="form-control" id="edit_code" name="edit_code" placeholder="Enter Product Code" readonly>
        </div>
        </div>
      <div class="col-md-6">
        <div class="form-group">
        <label for="edit_price">Edit Unit Type <span class="text-danger">*</span></label>
        <select class="form-control" id="edit_unit_type" name="edit_unit_type">
            <option value="">Select Unit Type</option>
            <?php foreach($units as $unit){?>
            <option value="<?=$unit['id']?>"><?=$unit['name']?></option>
            <?php }?>
          </select>
          </div>
        </div>
      <div class="col-md-6">
        <div class="form-group">
        <label for="edit_price">Edit Unit <span class="text-danger">*</span></label>
          <input type="text" class="form-control" id="edit_unit" name="edit_unit" placeholder="Enter Unit" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
        <label for="edit_price">Edit Specification <span class="text-danger">*</span></label>
          <input type="text" class="form-control" id="edit_spec" name="edit_spec" placeholder="Enter Specification">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group"> 
        <label for="edit_price">Edit Make <span class="text-danger">*</span></label>
          <input type="text" class="form-control" id="edit_make" name="edit_make" placeholder="Enter Make">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group"> 
        <label for="edit_price">Edit Model  <span class="text-danger">*</span></label>
          <input type="text" class="form-control" id="edit_model" name="edit_model" placeholder="Enter Model">
        </div>
      </div>
      <!-- <div class="col-md-6">
        <div class="form-group"> 
        <label for="edit_price">Edit Location <span class="text-danger">*</span></label>
          <input type="text" class="form-control" id="edit_location" name="edit_location" placeholder="Enter Location">
        </div>
      </div> -->
      <div class="col-md-6">
        <div class="form-group">
        <label for="edit_price">Location <span class="text-danger">*</span></label>
        <select class="form-control" id="edit_location" name="edit_location">
            <option value="">Select Location</option>
            <?php foreach($locations as $location){?>
            <option value="<?=$location->id?>"><?=$location->location?></option>
            <?php }?>
          </select>
          </div>
        </div>
      <div class="col-md-6">
        <div class="form-group"> 
        <label for="edit_price">Edit Remark</label>
          <input type="text" class="form-control" id="edit_remark" name="edit_remark" placeholder="Enter Remark">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group"> 
          <label for="edit_price">Edit Price <span class="text-danger">*</span></label>
          <input type="text" class="form-control" id="edit_price" name="edit_price" placeholder="Enter Price">
        </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
     </form>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="outMaterial" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Out Material</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid basic_table">
          <div class="card">
            <form action="<?=base_url('Bom/stock_out_form')?>" id="stock_out_form" method="post">
              <div class="form-group">
                <div class="row container mt-2 mb-2">
                  <div class="col-md-5">
                    <label for="">Project :</label>
                    <select name="project" id="" class="js-example-basic-single" onchange="getBoms2(this.value)">
                      <option value="" disabled selected>Select Project</option>
                      <?php foreach($projects as $project){?>
                      <option value="<?=$project['project_id']?>"><?=$project['name']?></option>
                      <?php }?>
                    </select>
                  </div>
                  <div class="col-md-4">
                    <label for=""> Select User :</label>
                    <select name="bom_users2" id="bom_users2" class="js-example-basic-single"
                      onchange="getBomDetail2(this.value)">
                      <option value="" disabled selected>Select User</option>
                    </select>
                  </div>

                  <div class="col-md-3">
                  <label for=""> Returnable
                        Products :</label>
                    <div class="btn btn-warning" style="padding: 4px 5px 0px 5px;"><input type="checkbox"
                        name="returnableProducts" id="returnableProducts"> <label for="returnableProducts"> Returnable
                        Products</label></div>
                  </div>

                </div>
              </div>
              <div class="table-responsive" style="max-height: 400px;">
                <table class="table">
                  <thead>
                  <tr><td colspan="7"><h5>Requested Products</h5></td></tr>
                    <tr class="border-bottom-primary">
                      <th scope="col">Product_Code</th>
                      <th scope="col">Part Name</th>
                      <th scope="col">Qty</th>                      
                      <th scope="col">Stock Qty</th>                      
                      <th scope="col">Issue Qty</th>
                      <th scope="col">GST(%)</th>
                      <th scope="col">Updated Stock Qty</th>
                    </tr>
                  </thead>
                  <tbody id="products">
                  </tbody>
                </table>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="inMaterial" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">In Material</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid basic_table">
          <div class="card">
            <form action="<?=base_url('Bom/stock_in_form')?>" id="stock_in_form" method="post">
              <div class="form-group ">
                <div class="row container mt-2 mb-2">
                  <div class="col-md-6">
                    <label for="">Select Project :</label>
                    <select name="project" id="" class="js-example-basic-single" onchange="getBoms(this.value)">
                      <option value="" disabled selected>Select Project</option>
                      <?php foreach($projects as $project){?>
                      <option value="<?=$project['project_id']?>"><?=$project['name']?></option>
                      <?php }?>
                    </select>
                  </div>
                  <div class="col-md-6">
                    <label for=""> Select Bom :</label>
                    <select name="po_id" id="po_id" class="js-example-basic-single" onchange="getBomDetail(this.value)">
                      <option value="" disabled selected>Select</option>
                    </select>
                  </div>
                </div>
                <div class="table-responsive" style="max-height:400px">
                  <table class="table">
                    <thead>
                      <tr class="border-bottom-primary">
                        <th scope="col" nowrap>Product</th>
                        <th scope="col">Qty</th>
                        <th scope="col" nowrap>Recieved Qty</th>
                        <th scope="col" nowrap>Rejected Qty</th>
                        <th scope="col" nowrap>Not RecievedQty</th>
                      </tr>
                    </thead>
                    <tbody id="bom_products">
                    </tbody>
                  </table>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="viewQuoteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">View Quotation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid basic_table">
          <div class="card">
            <form action="<?=base_url('Bom/sendQuotation')?>" id="sendQuote" method="post">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr class="border-bottom-primary">
                      <?php $Id =  $this->session->userdata('role_id');
                                    ?>

                      <!-- <th scope="col">Product Code</th> -->
                      <th scope="col">Part Name</th>
                      <th scope="col">Qty</th>
                      <th scope="col">Price/Unit</th>
                      <th scope="col">Total Price</th>

                    </tr>
                  </thead>
                  <tbody id="quote_products">
                  </tbody>
                </table>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
<script>
$(document).ready(function() {
  // $('#example').DataTable();
  // } );
  var dataTable = $('#productDataTable').DataTable({
    // "processing": true,
    // "serverSide": true,
    // buttons: [{
    //     extend: 'excelHtml5',
    //     text: 'Download Excel'
    // }],
    "order": [],
    "ajax": {
      url: "<?=base_url('Product/ajaxproducts')?>",
      type: "POST"
    },
    // "columnDefs": [{
    //     "targets": [0],
    //     "orderable": false,
    // }, ],
  });
});

function CompleteOrder(id) {
  var order_status = 1;
  Swal.fire({
    title: 'Do you want to Complete Order',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Create'
  }).then((result) => {

    if (result.isConfirmed) {
      $.ajax({
        type: 'POST',
        url: '<?php echo base_url();?>Bom/update_order',
        data: "id=" + id + "&order_status=" + order_status,
        dataType: "html",
        success: function(data) {
          Swal.fire(
            'Status Change!',
            'Order Completed successfully.',
            'success'
          )
          //location.reload();
        }

      })

    } else {
      //location.reload();
    }
  });
}

function get_tprice(price, productCode) {

  var qty = document.getElementById('qty_' + productCode).value;
  var totalprice = price * qty;
  document.getElementById('tprice_' + productCode).value = totalprice;
}

function getBoms(id) {
  //alert(id);
  $.ajax({
    url: '<?=base_url('Bom/get_pos')?>',
    type: 'POST',
    data: {
      id
    },
    success: function(data) {
      console.log(data);
      $('#po_id').html(data);

    }
  });
}

function getBoms2(id) {
  //alert(id);
  $.ajax({
    url: '<?=base_url('Bom/get_bomsUsers')?>',
    type: 'POST',
    data: {
      id
    },
    success: function(data) {
      console.log(data);
      $('#bom_users2').html(data);

    }
  });
}

function getBomDetail(ID) {
  $.ajax({
    url: '<?=base_url('Bom/get_bomProducts')?>',
    type: 'POST',
    data: {
      ID
    },
    success: function(data) {
      $('#bom_products').html(data);

    }
  });
}

function getBomDetail2(ID) {
  $.ajax({
    url: '<?=base_url('Bom/get_bomProducts2')?>',
    type: 'POST',
    data: {
      ID
    },
    success: function(data) {
      $('#products').html(data);

    }
  });
}

function getRecivedItems(productCode) {
  var qty = document.getElementById('qty_' + productCode).value;
  var rcqty = document.getElementById('rcqty_' + productCode).value;
  var rjqty = document.getElementById('rjqty_' + productCode).value;
  var nrqty = document.getElementById('nrqty_' + productCode).value;
  var ncqty = qty - rcqty;
  if (parseInt(qty) < parseInt(rcqty)) {
    toastr.error('Recieved Quantity must be less or Equal to Qty');
    //document.getElementById('rcqty_'+productCode).value = 0;
    //var ncqty = qty - 0;
  }
  var rejectqty = rcqty - rjqty;
  document.getElementById('nrqty_' + productCode).value = ncqty;
  document.getElementById('rjqty_' + productCode).value = 0;
  document.getElementById('finalqty_' + productCode).value = rejectqty;
}

function getfinalQty(productCode) {
  var qty = document.getElementById('qty_' + productCode).value;
  var rcqty = document.getElementById('rcqty_' + productCode).value;
  var rjqty = document.getElementById('rjqty_' + productCode).value;
  var nrqty = document.getElementById('nrqty_' + productCode).value;
  var totalrcv = parseInt(rcqty) + parseInt(rjqty);

  if (parseInt(totalrcv) > parseInt(qty)) {
    toastr.error('Rejected Quantity missmatch');
    // document.getElementById('rjqty_'+productCode).value = 0;
    // var rejectqty = rcqty - 0;
  }
  var missqty = parseInt(qty) - (parseInt(rcqty) + parseInt(rjqty));
  // var ncqty = qty - rejectqty;
  document.getElementById('nrqty_' + productCode).value = missqty;
  //document.getElementById('finalqty_'+productCode).value = missqty;
}

function getBalanceqty(productCode) {
  var iqty = document.getElementById('iqty_' + productCode).value;
  var totalqty = document.getElementById('totalqty_' + productCode).value;
  var bqty = totalqty - iqty;
  console.log(iqty);
  console.log(totalqty);
  console.log(bqty);
  document.getElementById('bqty_' + productCode).value = bqty;
}


$("form#stock_in_form").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {
          window.open("<?=base_url('Bom/in_stock_pdf/')?>" + data.id, '_blank');
          // location.href="<?=base_url('Bom/in_stock_pdf/')?>"+data.id; 	
          location.reload();

        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});


$("form#stock_out_form").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {
          // location.reload();
          window.open("<?=base_url('Bom/out_stock_pdf/')?>"+data.id, '_blank');
          // location.href = "<?=base_url('Bom/out_stock_pdf/')?>" + data.id;
           location.reload();

        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

$("#returnForm").hide();
$("#returnableProducts").click(function() {
  if ($(this).is(":checked")) {
    //alert('hi');die;
    $("#returnForm").show();
  } else {
    $("#returnForm").hide();
  }
});
</script>
<script>
function addmore() {
  // alert('hi');die;
  $("#multiple_Rform").append(
    '<div class="required_inp d-flex" style="margin:5px;"><input type="text" class="form-control" name="product_name[]" placeholder="Enter Product Name"><input type="number" class="form-control" name="product_qty[]" placeholder="Enter Quantity"><input type="text" class="form-control" name="person_name[]" placeholder="Enter Person Name"><input type="date" class="form-control" name="return_date[]">' +
    '<span class="customLook"></span><button type="button" class="inputRemove">x</button></div>'
  );
}
$('body').on('click', 'button.inputRemove', function() {
  $(this).parent('div.required_inp').remove()
});

function editProductModal(productID){
    $.ajax({
       url: '<?=base_url('Product/get_product')?>',
       type: 'POST',
       data: {productID},
       success: function(data) {
       $('#editProductModal').modal('show');
       var product = $.parseJSON(data);
       
              $('#edit_productId').val(productID);
              $('#edit_partname').val(product.part_name);
              $('#edit_code').val(product.product_code);
              $('#edit_unit_type').val(product.unit_type);
              $('#edit_unit').val(product.unit);
              $('#edit_spec').val(product.specification);
              $('#edit_make').val(product.make);
              $('#edit_model').val(product.model);
              $('#edit_location').val(product.location);
              $('#edit_remark').val(product.remark);
              $('#edit_price').val(product.price);
              
       }
   });
  }

  $("form#editProductForm").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  toastr.success(data.message);
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){

     	
     $(".modal").modal('hide');
    
  }, 1000) 
  
  }else if(data.status==403) {
  toastr.error(data.message);

  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastr.error(data.message);
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });

  function delete_product(productId){
    Swal.fire({
          title: 'Are you sure?',
          text: "You won't to delet it!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
          if (result.value) {
              $.ajax({
                  url: '<?=base_url('Product/delete_product')?>',
                  type: 'POST',
                  data: {
                    productId
                  },
                  dataType: 'json',
                  success: function(data) {
                      if (data.status == 200) {
                          toastr.success(data.message);
                          location.reload();
  
                      } else if (data.status == 302) {
                          toastr.error(data.message);
                      }
                  }
              })
          }
      })
  }
</script>