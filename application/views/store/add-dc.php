<div class="page-body">
  <div class="container-fluid">
    <div class="page-title">
      <div class="row">
        <div class="col-6">
          <h3><?=$title?></h3>
        </div>
        <div class="col-6">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?=base_url('Dashboard')?>">
                <svg class="stroke-icon">
                  <use href="<?=base_url('public/assets/svg/icon-sprite.svg#stroke-home')?>"></use>
                </svg></a></li>
            <li class="breadcrumb-item">Dashboard</li>
            <li class="breadcrumb-item active"><?=$title?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <!-- Individual column searching (text inputs) Starts-->
      <div class="col-sm-12">
        <div class="card">
          <div class="card-body">
          <form action="<?=base_url('Bom/storeDeliveryChallan')?>" id="dc_form" method="post">
              <div class="form-group ">
                <div class="row container mt-2 mb-2">
                  <div class="col-md-6 form-group">
                    <label for="">Select Project <span class="text-danger">*</span></label>
                    <select name="project" id="project" class="js-example-basic-single" onchange="getProducts(this.value)">
                      <option value="" disabled selected>Select Project</option>
                      <?php foreach($projects as $project){?>
                      <option value="<?=$project['id']?>"><?=$project['name']?></option>
                      <?php }?>
                    </select>
                  </div>
                  <div class="col-md-6 form-group">
                    <label for="chllan_type">Challan Type <span class="text-danger">*</span></label>
                    <select name="chllan_type" id="chllan_type" class="form-control">
                      <option value="" selected>Select</option>
                      <option value="Returnable">Returnable</option>
                      <option value="Non-Returnable" >Non Returnable</option>
                    </select>
                  </div>
                  <!-- <div class="col-md-6 form-group">
                    <label for="bill_to">Bill From <span class="text-danger">*</span></label>
                    <textarea type="text" name="bill_from" id="bill_from" class="form-control" Placeholder="Enter Bill From"></textarea>               
                  </div> -->
                  <div class="col-md-6 form-group">
                    <label for="bill_to">Bill To <span class="text-danger">*</span></label>
                    <select name="bill_to" id="bill_to" class="js-example-basic-single">
                    <option value="">Select</option>
                      <?php foreach($vendors as $vendor){?>
                        <option value="<?=$vendor->id?>"><?=$vendor->name?></option>
                        <?php }?>
                    </select>                    
                  </div>
                  <div class="col-md-6 form-group">
                    <label for="delivery_mode">Mode Of Delivery </label>
                    <input type="text" name="delivery_mode" id="delivery_mode" class="form-control" Placeholder="Enter Mode Of Delivery">                     
                  </div>
                  <!-- <div class="col-md-6 form-group">
                    <label for="gstn">Gstn Number </label>
                    <input type="text" name="gstn" id="gstn" class="form-control" Placeholder="Enter GSTN Number">                     
                  </div> -->
                  <div class="col-md-6 form-group">
                    <label for="gstn">Po Number </label>
                    <input type="text" name="po_number" id="po_number" class="form-control" Placeholder="Enter PO Number">                     
                  </div>
                  <!-- <div class="col-md-6 form-group">
                    <label for="mobile">Phone Number <span class="text-danger">*</span></label>
                    <input type="text" maxlength="10"  name="mobile" id="mobile" class="form-control" Placeholder="Enter Phone Number" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">                     
                  </div> -->
                  <!-- <div class="col-md-6 form-group">
                    <label for="email">Email </label>
                    <input type="email" name="email" id="email" class="form-control" Placeholder="Enter Email">                     
                  </div> -->
                 
                  <div class="col-md-6 form-group">
                    <label for="vehicle_number">Vehicle Number <span class="text-danger">*</span></label>
                    <input type="text" name="vehicle_number" id="vehicle_number" class="form-control" Placeholder="Enter Vehicle Number">                     
                  </div>
                  <div class="col-md-6 form-group">
                    <label for="supply_place">Place Of supply <span class="text-danger">*</span></label>
                    <input type="text" name="supply_place" id="supply_place" class="form-control" Placeholder="Enter Place Of Supply">                     
                  </div>
                  <!-- <div class="col-md-6 form-group">
                    <label for="state">State <span class="text-danger">*</span></label>
                    <input type="text" name="state" id="state" class="form-control" Placeholder="Enter State">                     
                  </div> -->
                  <!-- <div class="col-md-6 form-group">
                    <label for="address">Address <span class="text-danger">*</span></label>
                    <textarea name="address" id="address" class="form-control" Placeholder="Enter Address"></textarea>                     
                  </div> -->
                  <div class="col-md-6 form-group">
                    <label for="terms">Terms & Conditions <span class="text-danger">*</span></label>
                    <textarea name="terms" id="terms" class="form-control" Placeholder="Enter Terms & Conditions"></textarea>                     
                  </div>
                  <div class="col-md-6 form-group">
                    <label for="sac">SAC Code </label>
                    <input type="text" name="sac" id="sac" class="form-control" Placeholder="Enter SAC Code">                   
                  </div>
                  <div class="col-md-6 form-group">
                    <label for="ship_charge">Shipping Charges </label>
                    <input type="text" name="ship_charge" id="ship_charge" class="form-control" Placeholder="Enter Shipping Charge" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">                 
                  </div>
                  
                  <div class="form-group col-md-12 form-group">
                    <label for="">Select Products <span class="text-danger">*</span></label>
                    <select name="products[]" id="productsData" class="js-example-basic-multiple" multiple onchange="getProductRows()">
                   
                    </select>
                </div>
                <div class="table-responsive" style="max-height: 400px;">
                <table class="table">
                  <thead>
                    <tr class="border-bottom-primary">
                      <th scope="col">Product_Code</th>
                      <th scope="col">HSN Code</th>
                      <th scope="col" nowrap>Stock Qty</th>
                      <th scope="col">Qty</th>
                      <th scope="col">Unit Type</th>
                      <th scope="col">Rate/Unit</th>
                      <th scope="col">GST(%)</th>
                    </tr>
                  </thead>
                  <tbody id="getProductRows">
                  </tbody>
                </table>
              </div>
                  <div class="form-group"><br>
                    <input type="submit" class="btn btn-primary" value="Submit">
                  </div>
                </div>
                
                
            </form>
          </div>
        </div>
      </div>
      </div>
      <!-- Individual column searching (text inputs) Ends-->
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<div class="modal fade" id="inMaterial" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">

</div>
<script>
    function getProductRows(){
    var products = $("#productsData").val();
    //alert(products);
    $.ajax({
    url: '<?=base_url('Bom/getDcProductRows')?>',
    type: 'POST',
    data: {
      products
    },
    success: function(data) {
      $('#getProductRows').html(data);

    }
  });
}

function getProducts(id){
    
    //alert(products);
    $.ajax({
    url: '<?=base_url('Bom/get_dc_products')?>',
    type: 'POST',
    data: {
      id
    },
    success: function(data) {
      $('#productsData').html(data);

    }
  });
}



$("form#dc_form").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {
          //location.href = "<?//=base_url('Bom/download_dc/')?>" + data.id;
          //window.open("<?//=base_url('Bom/download_dc/')?>"+data.id, '_blank');
          location.href = "<?=base_url('dc')?>";
        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

</script>