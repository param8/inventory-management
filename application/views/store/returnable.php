<style>
.no-border {
    border: none;
}
</style>
<div class="page-body">
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6">
                    <h3><?=$title?></h3>
                </div>
                <div class="col-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0);">
                                <svg class="stroke-icon">
                                    <use href="<?=base_url('public/assets/svg/icon-sprite.svg#stroke-home')?>"></use>
                                </svg>
                            </a>
                        </li>
                        <li class="breadcrumb-item">Dashboard</li>
                        <li class="breadcrumb-item active"><?=$title?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <!-- Individual column searching (text inputs) Starts-->
        <div class="card">
            <div class="row container">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="display" id="basic-1">
                            <thead>
                                <tr>
                                    <th>S. No.</th>
                                    <th nowrap>Project Name</th>
                                    <th nowrap>Product Name</th>
                                    <th>Qty</th>
                                    <th nowrap>Issue To</th>
                                    <th nowrap>Issue By</th>
                                    <th nowrap>Return Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $sno = 1; foreach($return_data as $data){?>
                                <tr>
                                    <td><?=$sno++?></td>
                                    <td><?=$data['name']?></td>
                                    <td><?=$data['products_name']?></td>                                    
                                    <td><?=$data['qty']?></td>
                                    <td><?=$data['person']?></td>
                                    <td><?=$data['issue_name']?></td>
                                    <td><?=date('d/m/Y',strtotime($data['return_date']))?></td>
                                    <td><?=($data['return_status']==1)?'<span class="text-danger">Not Recieved</span>':'<span class="text-success">Recieved</span>'?></td>
                                    <td><?php if($data['return_status']==1){?><a href="javascript:void(0)" onclick="onoffswitchspublish(<?=$data['id']?>)" class="btn  btn-success">Recieved<?php }?></a>
                                    <a href="<?=base_url('Bom/returnable_download/').base64_encode($data['id'])?>" class="btn  btn-warning" target="_blank"><i class="fa fa-download"></i> Challan</a></td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!-- Individual column searching (text inputs) Ends-->
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>
<!-- Add product Modal -->
<div class="modal fade" id="addBom" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Bom</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form methos="POST" action="<?=base_url('Bom/store_bom')?>" id="createBomForm">
                    <div class="form-group">
                        <label for="">Select Project :</label>
                        <select name="project" id="project" class="btn btn-primary">
                            <option value="" disabled selected>Select Project</option>
                            <?php foreach($projects as $project){ ?>
                            <option value="<?=$project['id']?>"><?=$project['name']?></option>
                            <?php  }?>
                        </select>
                    </div>
                    <div id="multiple_form" class="row">
                        <table>
                            <tr>
                                <td><input type="text" class="form-control" id="product_code" name="product_code[]"
                                        placeholder="Enter Product Code"></td>
                                <td><input type="text" class="form-control" id="qty" name="qty[]"
                                        placeholder="Enter Quantity"></td>
                                <td><span class="customLook"></span><button type="button" id="addmore"
                                        onclick="addField()"><i class="fa fa-plus"></i></button></td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- product detail model--->
<div class="modal fade" id="productDetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Product Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid basic_table">
                    <div class="card">
                        <form action="<?=base_url('Bom/assignToVendor')?>" id="vendorAsign" method="post">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr class="border-bottom-primary">
                                            <?php $Id =  $this->session->userdata('role_id');
                                    if($Id== 4){ ?>
                                            <th scope="col">S.No.</th>
                                            <?php }?>
                                            <th scope="col">Product_Code</th>
                                            <th scope="col">Part Name</th>
                                            <th scope="col">Make</th>
                                            <th scope="col">Qty</th>
                                            <th scope="col">Recieved Qty</th>
                                            <th scope="col">Rejected Qty</th>
                                            <th scope="col">Not Recieved Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody id="products">
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Assign to vendor Modal -->

<script>
    function view_product(id) {
    $('#productDetailModal').modal('show');
    $.ajax({
        url: '<?=base_url('Bom/get_inmaterialproducts')?>',
        type: 'POST',
        data: {
            id
        },
        success: function(data) {
            $('#products').html(data);

        }
    });

}

function onoffswitchspublish(id)
  {
    
      Swal.fire({
          title: 'Is Product Recieved',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText : 'Not Recieved',
          confirmButtonText: 'Yes'
          }).then((result) => {
              
      if (result.isConfirmed) {
  
      $.ajax({
      type:'POST',
      url:'<?php echo base_url();?>Bom/updateReturnable',
      data:"id="+ id+"&status="+2,
      dataType: "html",
      success:function(data)
      {    
          Swal.fire(
              'Status Change!',
             'Recieved sucessfully.',
              'success'
                  )
              location.reload();
       }
  
       })
  
      }else{  
          location.reload();
      }
  
      })
  
  }

</script>