<div class="page-body">
  <div class="container-fluid">
    <div class="page-title">
      <div class="row">
        <div class="col-6">
          <h3><?=$title?></h3>
        </div>
        <div class="col-6">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?=base_url('Dashboard')?>">
                <svg class="stroke-icon">
                  <use href="<?=base_url('public/assets/svg/icon-sprite.svg#stroke-home')?>"></use>
                </svg></a></li>
            <li class="breadcrumb-item">Dashboard</li>
            <li class="breadcrumb-item active"><?=$title?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <!-- Individual column searching (text inputs) Starts-->
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header ">
            <div class="row">
              <div class="col-md-12 text-right">
              <a href="<?=base_url('add-dc')?>" class="btn btn-primary"  data-bs-original-title="Add DC">Add DC</a>
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive product-table">
              <table class="display" id="productDataTable">
                <thead>
                  <tr>
                    <th>Sno</th>
                    <th nowrap>Action</th>
                    <th nowrap>Approval Status</th>
                    <th nowrap>Material Status</th>
                    <th nowrap>Challan Type</th>
                    <th nowrap>Project</th>
                    <th nowrap>Location</th>
                    <th nowrap>Challan no</th>
                    <th nowrap>Po No</th>
                    <th nowrap>Bill To</th>
                    <th nowrap>Mobile</th>
                    <th nowrap>Added By</th>
                    <th nowrap>Created At</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- Individual column searching (text inputs) Ends-->
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<div class="modal fade" id="requestedDCModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">DC For Project</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="basic_table">
          <div class="card">
            <div class="table-responsive">
              <table class="table" id="requestedProducts">
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


</div>
<script>
$(document).ready(function() {
  // $('#example').DataTable();
  // } );
  var dataTable = $('#productDataTable').DataTable({
    // "processing": true,
    // "serverSide": true,
    // buttons: [{
    //     extend: 'excelHtml5',
    //     text: 'Download Excel'
    // }],
    "order": [],
    "ajax": {
      url: "<?=base_url('Bom/ajaxdc')?>",
      type: "POST"
    },
    // "columnDefs": [{
    //     "targets": [0],
    //     "orderable": false,
    // }, ],
  });
});

function requestedDC(id) {
  $('#requestedDCModal').modal('show');
  $.ajax({
    url: '<?=base_url('Bom/get_requestedDc')?>',
    type: 'POST',
    data: {
      id
    },
    success: function(data) {
      $('#requestedProducts').html(data);
    }
  });
}  

function onoffswitchspublish(svalue, qid) {
  //alert(svalue);die;
  if (svalue == 2) {

    Swal.fire({
      title: 'Do you want to Diss Approve DC',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Continue'
    }).then((result) => {

      if (result.isConfirmed) {

        $.ajax({
          type: 'POST',
          url: '<?php echo base_url();?>Bom/ApproveDCRequest',
          data: "id=" + qid + "&status=" + svalue,
          dataType: "html",
          success: function(data) {
            Swal.fire(
              'Status Change!',
              'Diss Approved sucessfully.',
              'success'
            )
            location.reload();
          }

        })

      } else {

        location.reload();
      }

    })



  } else {

    Swal.fire({
      title: 'Do you want to Approve DC',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Continue'
    }).then((result) => {

      if (result.isConfirmed) {

        $.ajax({
          type: 'POST',
          url: '<?php echo base_url();?>Bom/ApproveDCRequest',
          data: "id=" + qid + "&status=" + svalue,
          dataType: "json",
          success: function(data) {
            Swal.fire(
              'Status Change!',
              'Approved sucessfully.',
              'success'
            )
            location.reload();
          }

        })

      } else {
        location.reload();
      }

    })

  }
}

$("form#stock_out_form").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {
          // location.reload();
          //window.open("<?//=base_url('Bom/out_stock_pdf/')?>"+data.id, '_blank');
          // location.href = "<?=base_url('Bom/out_stock_pdf/')?>" + data.id;
           location.reload();

        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});


function returnMaterial(id){
  
  Swal.fire({
      title: 'Do you want to Return DC Material',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {

      if (result.isConfirmed) {

        $.ajax({
          type: 'POST',
          url: '<?php echo base_url();?>Bom/returnDcMaterial',
          data: "id=" + id,
          dataType: "html",
          success: function(data) {
            Swal.fire(
              'Status Change!',
              'Matarial return sucessfully.',
              'success'
            )
            location.reload();
          }

        })

      } else {

        location.reload();
      }

    })


}

</script>