
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title><?=$title?></title>
</head>

<body>
  <center>
    <div style="width:100%;border: 3px solid #575378;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0"
      style=" font-size:14px;">
      <tbody  >
        <tr>
          <!-- <td style="  padding:20px 25px;" colspan="5" valign="top" align="right"><img
              src="https://swamatics.com/images/logo.png"></td>
        </tr>
        <tr> -->
          <!-- <td align="cenetr" colspan="5">
            <h1 style="text-align: center;    margin-top: 0;color: #fff;">Puchage Order</h1>
          </td>
        </tr> -->

        <!-- <tr>
          <td colspan="5" height="20">&nbsp;</td>
        </tr> -->

        <tr>
          <td colspan="5">
            <table style="width: 100%;">
              <tbody>
                <tr>
                  <td valign="top" style="padding:5px; width: 30%; vertical-align: bottom;    font-weight: 500;">
                    <p>S.No : <?=date('Ym', strtotime($date)).base64_decode($this->uri->segment(3));  ?></p>
                    <p>Project : <?=$project->name?></p>
                    <!--<p>Department :</p>-->
                  </td>
                  <td align="cenetr" colspan="5" style="padding:5px; width: 40%; text-align: center;     font-weight: 500;">
                    <img src="<?=base_url($siteinfo->site_logo)?>" alt="" style="width: 55%;padding-top: 20px;">
                    <p>Plot no-484,Udyog 2,Ecotech 3,Greater noida 201306 (GB nager)</p>
                    <h2 style="text-align: center;    margin-top: 0; ">Material Issue slip</h2>
                  </td>
                  <td valign="top" style="padding:5px; width: 30%;text-align: right;vertical-align: bottom;">
                    <div class="po_detial">
                      <div style=" margin-bottom: 10px; ">  <span style="    font-weight: 500; ">Date: <u><?=date('d F Y', strtotime($date));  ?></u></span></div>
                  
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>

      </tbody>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0"
      style=" margin-top:0px; font-size:14px">
      <tbody>
       
      
          <?php if(!empty($returnables)){?>
            <tr><td colspan="6" style="text-align:center"><h2>Returnable Products</h2></td></tr>
            <tr style="background:#575378;color:white;border: 2px solid #666666;">
          <td valign="middle"  style="padding:5px;height:25px;text-align:center;font-weight:bold">S.No</td>
          <td valign="middle" align="cenetr" style="padding:5px;height:25px;text-align:center;font-weight:bold">Product</td>
          <td valign="middle" align="cenetr" style="padding:5px;height:25px;text-align:center;font-weight:bold">Qty</td>
          <td valign="middle" align="cenetr" style="padding:5px;height:25px;text-align:center;font-weight:bold">Person</td>
          <td valign="middle" align="cenetr" colspan="2" style="padding:5px;height:25px;text-align:center;font-weight:bold">Return Date</td>
        </tr>
        <?php $no=1; //foreach($returnables as $key=>$row){ ?>
            <tr style="border: 2px solid #666666; ">
                <td valign="top" align="left" style="padding:5px;text-align:center;"><?=$no?></td>
                <td valign="top" align="left" style="padding:5px;text-align:center;"><?=$returnables->products_name?></td>
                <td valign="top" align="right" style="padding:5px;text-align:center;"><?=$returnables->qty?></td>
                <td valign="top" align="right" style="padding:5px;text-align:center;"><?=$returnables->person?></td>
                <td style="padding:5px;text-align:center;" colspan="3"><?=date('d/m/Y',strtotime($returnables->return_date))?></td>
            </tr>
            <?php $no++;} //}?>
        
        <tr style="">
          <td colspan="2" style="padding:5px;font-weight: bold;height: 25px;" align="left">Indent by : <?=$User?></td>
          <td colspan="1" style="padding:5px;font-weight: bold;height: 25px;" align="left"><img src="<?=base_url('public/signature.jpg')?>" style="height:90px"><br>Approved by :  <?=$approve_by?></td>
          <td colspan="2" style="padding:5px;font-weight: bold;height: 25px;padding-right: 8%;" align="right">Issue by : <?=$Issue_by?></td>
        </tr>
        
      </tbody>
    </table>
    </div>
  </center>

</body>

</html>