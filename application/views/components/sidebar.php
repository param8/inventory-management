<div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
        <div class="sidebar-wrapper" sidebar-layout="stroke-svg">
          <div>
            <div class="logo-wrapper"><a href="<?=base_url('Dashboard')?>"><img class="img-fluid for-light" src="<?=base_url($siteinfo->site_logo)?>" alt="" width="185"><img class="img-fluid for-dark" src="<?=base_url($siteinfo->site_logo)?>" alt="" width="185"></a>
              <div class="back-btn"><i class="fa fa-angle-left"></i></div>
              <div class="toggle-sidebar"><i class="status_toggle middle sidebar-toggle" data-feather="grid"> </i></div>
            </div>
            <div class="logo-icon-wrapper"><a href="index.html"><img class="img-fluid" src="../assets/images/logo/logo-icon.png" alt=""></a></div>
            <nav class="sidebar-main">
              <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
              <div id="sidebar-menu">
                <ul class="sidebar-links" id="simple-bar">
                  <li class="back-btn"><a href="<?=base_url()?>"><img class="img-fluid" src="../assets/images/logo/logo-icon.png" alt=""></a>
                    <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
                  </li>
                  <li class="pin-title sidebar-main-title">
                    <div> 
                      <h6>Pinned</h6>
                    </div>
                  </li>
                  <li class="sidebar-main-title">
                    <div>
                      <h6 class="lan-1">General</h6>
                    </div>
                  </li>
                  <li class="sidebar-list"><i class="fa fa-thumb-tack"></i>
                    <label class="badge badge-light-primary"></label><a class="sidebar-link sidebar-title" href="<?=base_url('Dashboard');?>">
                      <svg class="stroke-icon">
                        <use href="../assets/svg/icon-sprite.svg#stroke-home"></use>
                      </svg>
                      <svg class="fill-icon">
                        <use href="../assets/svg/icon-sprite.svg#fill-home"></use>
                      </svg><span class="lan-3">Dashboard</span></a>
                  </li>
                  
                  <li class="sidebar-main-title">
                    <div>
                      <h6 class="lan-8">Applications</h6>
                    </div>
                  </li>
                  <?php 

                    foreach($menues as $menu){

                       $menuID = $menu['id'];
                       $permission = $permissions->$menuID;
                        if($permission[0]=='view'){
                      ?>
                  <li class="sidebar-list"><i class="fa fa-thumb-tack"></i>
                    <a class="sidebar-link sidebar-title <?=($menu['name']!='Users')?'link-nav':''?>" href="<?=($menu['name']=='Users')?'javascript:void(0)':base_url($menu['url'])?>">
                      <svg class="stroke-icon">
                        <use href="../assets/svg/icon-sprite.svg#stroke-board"></use>
                      </svg>
                      <svg class="fill-icon">
                        <use href="../assets/svg/icon-sprite.svg#fill-board"></use>
                      </svg><span><?=$menu['name']?></span>
                      <?php if($menu['name']=='Users'){?>
                        <div class="according-menu"><i class="fa fa-angle-right"></i></div>
                      <?php }?>
                      </a>
                      <?php if($menu['name']=='Users'){?>                        
                      <ul class="sidebar-submenu" style="display: none;">
                      <?php  if($permission[1]=='add'){?>
                    <li><a href="<?=base_url('create-user');?>">Create New User</a></li>
                    <?php }foreach($roles as $role){
                      if($role['role'] != 'Vendor'){ ?>                      
                      <li><a href="<?=base_url('users/'.base64_encode($role['role']));?>"><?=$role['role']?></a></li>
                      <?php }}?>
                    </ul>
                    <?php }?>
                  </li>
                  <?php } } ?>


                  
                  <li class="sidebar-list"><i class="fa fa-thumb-tack"></i>
                    <a class="sidebar-link sidebar-title link-nav" href="<?=base_url('Authantication/logout')?>">
                      <svg class="stroke-icon">
                        <use href="../assets/svg/icon-sprite.svg#stroke-board"></use>
                      </svg>
                      <svg class="fill-icon">
                        <use href="../assets/svg/icon-sprite.svg#fill-board"></use>
                      </svg><span>Log out</span></a>
                  </li>
                 
                 
                </ul>
              </div>
              <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
            </nav>
          </div>
        </div>