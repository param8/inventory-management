 <!-- footer start-->
 <footer class="footer">
     <div class="container-fluid">
         <div class="row">
             <div class="col-md-12 footer-copyright text-center">
                 <p class="mb-0">Copyright 2023 © Webcadance india</p>
             </div>
         </div>
     </div>
 </footer>
 </div>
 </div>
 <!-- latest jquery-->
 <script src="<?=base_url('public/assets/js/jquery.min.js')?>"></script>
 <!-- Bootstrap js-->
 <script src="<?=base_url('public/assets/js/bootstrap/bootstrap.bundle.min.js')?>"></script>
 <!-- feather icon js-->
 <script src="<?=base_url('public/assets/js/icons/feather-icon/feather.min.js')?>"></script>
 <script src="<?=base_url('public/assets/js/icons/feather-icon/feather-icon.js')?>"></script>
 <!-- scrollbar js-->
 <script src="<?=base_url('public/assets/js/scrollbar/simplebar.js')?>"></script>
 <script src="<?=base_url('public/assets/js/scrollbar/custom.js')?>"></script>
 <!-- Sidebar jquery-->
 <script src="<?=base_url('public/assets/js/config.js')?>"></script>
 <!-- Plugins JS start-->
 <script src="<?=base_url('public/assets/js/sidebar-menu.js')?>"></script>
 <!-- <script src="<?=base_url('public/assets/js/clock.js')?>"></script> -->
 <script src="<?=base_url('public/assets/js/slick/slick.min.js')?>"></script>
 <script src="<?=base_url('public/assets/js/slick/slick.js')?>"></script>
 <script src="<?=base_url('public/assets/js/header-slick.js')?>"></script>
 <!-- <script src="<?=base_url('public/assets/js/chart/apex-chart/apex-chart.js')?>"></script>
 <script src="<?=base_url('public/assets/js/chart/apex-chart/stock-prices.js')?>"></script>
 <script src="<?=base_url('public/assets/js/chart/apex-chart/moment.min.js')?>"></script> -->
 <script src="<?=base_url('public/assets/js/notify/bootstrap-notify.min.js')?>"></script>
 <script src="<?=base_url('public/assets/js/dashboard/default.js')?>"></script>
 <!-- <script src="<?=base_url('public/assets/js/notify/index.js')?>"></script> -->
 <script src="<?=base_url('public/assets/js/typeahead/handlebars.js')?>"></script>
 <script src="<?=base_url('public/assets/js/typeahead/typeahead.bundle.js')?>"></script>
 <script src="<?=base_url('public/assets/js/typeahead/typeahead.custom.js')?>"></script>
 <script src="<?=base_url('public/assets/js/typeahead-search/handlebars.js')?>"></script>
 <script src="<?=base_url('public/assets/js/typeahead-search/typeahead-custom.js')?>"></script>
 <script src="<?=base_url('public/assets/js/height-equal.js')?>"></script>
 <script src="<?=base_url('public/assets/js/animation/wow/wow.min.js')?>"></script>
 <script src="<?=base_url('public/assets/js/datatable/datatables/jquery.dataTables.min.js')?>"></script>
 <script src="<?=base_url('public/assets/js/datatable/datatables/datatable.custom.js')?>"></script>
 <script src="<?=base_url('public/assets/js/rating/jquery.barrating.js')?>"></script>
 <script src="<?=base_url('public/assets/js/rating/rating-script.js')?>"></script>
 <script src="<?=base_url('public/assets/js/owlcarousel/owl.carousel.js')?>"></script>
 <script src="<?=base_url('public/assets/js/ecommerce.js')?>"></script>
 <script src="<?=base_url('public/assets/js/product-list-custom.js')?>"></script>
 <script src="<?=base_url('public/assets/js/tooltip-init.js')?>"></script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
 <!-- Plugins JS Ends-->
 <script>
  $(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});

$(document).ready(function() {
    $('.js-example-basic-single').select2();
});

// $(".close").click(function(e) {
//     location.reload();
// });
$("button[data-dismiss=modal]").click(function()
{
  $(".modal").modal('hide');
});
 </script>
 <!-- Theme js-->
 <script src="<?=base_url('public/assets/js/script.js')?>"></script>
 <!-- <script src="<?//=base_url('public/assets/js/theme-customizer/customizer.js')?>"></script> -->
 <!-- Plugin used-->
 <script>
new WOW().init();
 </script>
 <script>
$(function() {
    $('[data-toggle="tooltip"]').tooltip()
})

function nofication_read(id){
    $.ajax({
            url: '<?=base_url('Setting/update_notification')?>',
            type: 'POST',
            data: {
                id
            },
            dataType: 'json',
        })
    }
     
 </script>
 </body>

 </html>