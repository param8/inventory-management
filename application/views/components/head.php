<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?=base_url('public/favicon.jpg')?>" type="image/x-icon">
    <link rel="shortcut icon" href="<?=base_url('public/favicon.jpg')?>" type="image/x-icon">
    <title><?=$title?></title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&family=Noto+Serif:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/font-awesome.css')?>">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/vendors/icofont.css')?>">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/vendors/themify.css')?>">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/vendors/flag-icon.css')?>">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/vendors/feather-icon.css')?>">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/vendors/slick.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/vendors/slick-theme.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/vendors/scrollbar.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/vendors/animate.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/vendors/datatables.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/vendors/owlcarousel.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/vendors/rating.css')?>">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/vendors/bootstrap.css')?>">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/style.css')?>">
      <link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/custom.css')?>">
    <link id="color" rel="stylesheet" href="<?=base_url('public/assets/css/color-1.css" media="screen')?>">
    <!-- Responsive css-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/responsive.css')?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
      <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
      <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
   <!-- include summernote css-->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<!-- include summernote js-->
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

<style>
body{
    font-family :"Noto Serif", serif!important;
}
::-webkit-scrollbar {
  width: 15px;
}
.h4, h4 {
    font-size: 1.4em;
}
td{
  text-align: center;
}

/* Track */
::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px grey; 
  border-radius: 10px;
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: gray; 
  border-radius: 10px;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #b30000; 
}
  .toast-message{
    color:#fff!important;
  }

  .form-control{
        margin:3px;
    }
    .customLook + button.inputRemove{
        color:#f73164;
        box-shadow: 0 0 0 2px inset #f73164;
    }
    .customLook + button.inputRemove:hover {
        box-shadow: 0 0 0 5px inset #f73164;
    }
    .inputRemove{
        padding: 0 13px;
    }
    #multiple_form td{
        padding: 0 0 0 15px;
    }

    #edit_bom #addmore{
      margin-top: 0px;
    float: right;
    }
    #edit_bom td{
      width: 360px;
    }
    #edit_bom .form-control {
    margin: 1px;
    }
    #edit_bom .customLook + button.inputRemove, #addBom .customLook + button.inputRemove{
    padding: 0 15px 0 8px;
   }

   tr.table-pt td input.form-control {
    width: 230px;
    margin: 2px 2px 0 5px;
  }
  .required_inp .form-control {
    width: 231px;
    margin: 0 30px 0 0px;
  }
  table.dataTable.stripe tbody tr.odd, table.dataTable.display tbody tr.odd {
    white-space: nowrap;
    background-color: #f9f9f9;
}
td{
    text-wrap: nowrap;
}
</style>
</head>