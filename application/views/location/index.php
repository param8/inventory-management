<div class="page-body">
<div class="container-fluid">
  <div class="page-title">
    <div class="row">
      <div class="col-6">
        <h3><?=$page_title?></h3>
      </div>
      <div class="col-6">
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="index.html">
              <svg class="stroke-icon">
                <use href="<?=base_url('public/assets/svg/icon-sprite.svg#stroke-home')?>"></use>
              </svg>
            </a>
          </li>
          <li class="breadcrumb-item">Location</li>
          <li class="breadcrumb-item active"><?=$page_title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Container-fluid starts-->
<div class="container-fluid">
  <div class="row">
    <!-- Individual column searching (text inputs) Starts-->
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addLocationModal" data-whatever="@fat">Add <?=$page_title?></button>
        </div>
        <div class="card-body">
          <div class="table-responsive product-table" id="">
            <table class="table " >
              <thead>
                <tr>
                  <th>SNo</th>
                  <th nowrap>Location Name</th>
                  <th>Gst</th>
                  <th>Address</th>
                  <th nowrap>Created Date</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($locations as $key=>$location){?>
                <tr>
                  <td><?=$key+1?></td>
                  <td><?=$location->location?></td>
                  <td><?=$location->gstn?></td>
                  <td><textarea readonly><?=$location->address?></textarea></td>
                  <td><?=date('d-m-Y',strtotime($location->created_at))?></td>
                  <td><a href="javascript:void(0)" onclick="editLocationModal(<?=$location->id?>)"><i class="fa fa-edit"></i></a></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- Individual column searching (text inputs) Ends-->
  </div>
</div>
<!-- Container-fluid Ends-->
<!-- Modal -->
<div class="modal fade" id="addLocationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Add <?=$page_title?></h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form methos="POST" action="<?=base_url('Setting/add_location')?>" id="addLocationForm">
        <div class="form-group">
          <label for="">Location*</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Enter Location Name">
        </div>
        <div class="form-group">
          <label for="">Gst</label>
          <input type="text" class="form-control" id="gst" name="gst" placeholder="Enter Gst">
          
        </div>
        <div class="form-group">
        <label for="">Address*</label>
          <textarea class="form-control" id="address" name="address" placeholder="Enter Address"></textarea>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
<!--Edit Modal -->
<div class="modal fade" id="editLocationModal" role="dialog" aria-labelledby="exampleModalLabel" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="<?=base_url('Setting/update_location')?>" id="editLocationForm">
          <div id="show_location"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
<script>

  
  
  $("form#addLocationForm").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  toastr.success(data.message);
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){
  
     location.href="<?=base_url('location')?>"; 	
    
  }, 1000) 
  
  }else if(data.status==403) {
  toastr.error(data.message);
  
  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastr.error(data.message);
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });
  
  function editLocationModal(locationID) {
    //alert(unitID);
   $.ajax({
       url: '<?=base_url('Setting/edit_location_form')?>',
       type: 'POST',
       data: {locationID},
       success: function(data) {
       $('#editLocationModal').modal('show');
       $('#show_location').html(data);
       }
   });
  }

  $("form#editLocationForm").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  toastr.success(data.message);
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){

     location.href="<?=base_url('location')?>"; 	
    
  }, 1000) 
  
  }else if(data.status==403) {
  toastr.error(data.message);

  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastr.error(data.message);
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });
  
  function delete_unit(unitID){
    Swal.fire({
          title: 'Are you sure?',
          text: "You won't to delet it!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
          if (result.value) {
              $.ajax({
                  url: '<?=base_url('Setting/delete_unit')?>',
                  type: 'POST',
                  data: {
                    unitID
                  },
                  dataType: 'json',
                  success: function(data) {
                      if (data.status == 200) {
                          toastr.success(data.message);
                          location.href = "<?=base_url('Setting/unit_setting')?>"
  
                      } else if (data.status == 302) {
                          toastr.error(data.message);
                      }
                  }
              })
          }
      })
  }
</script>