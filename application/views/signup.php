<!--<div class="container-fluid p-0">-->
<!--  <div class="row m-0">-->
<!--    <div class="col-12 p-0">-->
<!--      <div class="login-card login-dark">-->
          
          
          <section class="lo_ginn login-card">
    <div class="container-fluid p-0">
      <div class="row m-0">
        <div class="col-12 col-md-6 offset-md-3  p-0">    
          <div class=" login-dark">
           
        <div>
         
          <div class="login-main">
               <div><a class="logo" href="javascript:void(0);"><img class="img-fluid for-light" src="<?=base_url($siteinfo->site_logo)?>" alt="looginpage" width="280"><img class="img-fluid for-dark" src="<?=base_url('public/assets/images/logo/logo_dark.png')?>" alt="looginpage"></a></div>
            <form class="theme-form row" id="Frontregister" method="POST" action="<?=base_url('Authantication/register')?>" enctype="multipart/form-data">
              <h4>Create your account</h4>
              <p>Enter your personal details to create account</p>
              <div class="form-group col-md-6">
                <label class="col-form-label pt-0"><i class="fa fa-user"> Name</i></label>
                <input class="form-control" type="text" name="name" id="name" placeholder="Enter name">
              </div>
              <div class="form-group col-md-6">
                <label class="col-form-label"><i class="fa fa-envelope"> Email Address</i></label>
                <input class="form-control" type="email" name="email" id="email" placeholder="Enter email Address" autocomplete="off">
              </div>
              <div class="form-group col-md-6">
                <label class="col-form-label"><i class="fa fa-lock"> Password</i></label>
                <div class="form-input position-relative">
                  <input class="form-control" type="password" name="password" id="password" placeholder="*********" autocomplete="off">
                  <div class="show-hide"><span class="show"></span></div>
                </div>
              </div>
              <div class="form-group col-md-6">
                <label class="col-form-label"><i class="fa fa-mobile"> Phone</i></label>
                <input class="form-control" type="text" name="phone" id="phone" placeholder="phone" maxlength="10" minlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"></i>
              </div>
              <div class="form-group col-md-6">
                <label class="col-form-label"><i class="fa fa-address-card">Address</i></label>
                <input class="form-control" type="text" name="address" id="address" placeholder="address">
              </div>
              <div class="form-group col-md-6">
                <label class="col-form-label"><i class="fa fa-picture-o"> Profile Pic</i></label>
                <input class="form-control" type="file" name="profile_pic" id="profile_pic" accept="image/*">
              </div>
             
              <div class="form-group ">
                <div class="row g-2">
                  <div class="col-6">
                    <label class="col-form-label">State</label>
                    <select class="form-control" id="state" name="state" onchange="selectCity(this.value)">
                    <option value="">Select Country</option>
                      <?php foreach($states as $state ){?>
                        <option value="<?=$state->id?>"><?=$state->name?></option>
                        <?php } ?>
                    </select>
                  </div>
                  <div class="col-6">
                    <label class="col-form-label">City</label>
                    <select class="form-control" id="city" name="city">
                  
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group col-md-6">
                      <label  class="col-form-label">Vendor</label>                  
                        <select name="user_type" id="user_type" class="form-control">
                        <option value=""><i class="arrow down">Select User Type</i></option>
                        <?php foreach($roles as $role){?>
                        <option value="<?=$role['id']?>"><?=$role['role']?></option>
                        <?php }?>
                        </select>
                     
              </div>
              <div class="form-group col-md-6" id="vendor_gst">
                <label class="col-form-label">GST</label>
                <input class="form-control" type="text" id="gst" name="gst" placeholder="Enter GST number">
              </div>
              <div class="form-group" id="vendor_adhar">
                <div class="row g-2">
                  <div class="col-6">
                    <label class="col-form-label">Adhar-No</label>
                    <input class="form-control" type="text" id="adhar" name="adhar" placeholder="Enter Adhar number">
                  </div>
                  <div class="form-group col-6" id="vendor_pan">
                    <label class="col-form-label">PAN-No.</label>
                    <input class="form-control" type="text" id="pancard" name="pancard" placeholder="Enter PAN number">
                  </div>
                </div>
              </div>
              <div class="form-group mb-0">
                <div class="checkbox p-0">
                  <input id="checkbox1" type="checkbox">
                  <label class="text-muted" for="checkbox1">Agree with<a class="ms-2" href="#">Privacy Policy</a></label>
                </div>
                <button class="btn btn-primary btn-block w-100" type="submit">Create Account</button>
              </div>
              <!-- <h6 class="text-muted mt-4 or">Or signup with</h6>
                <div class="social mt-4">
                  <div class="btn-showcase"><a class="btn btn-light" href="https://www.linkedin.com/login" target="_blank"><i class="txt-linkedin" data-feather="linkedin"></i> LinkedIn </a><a class="btn btn-light" href="https://twitter.com/login?lang=en" target="_blank"><i class="txt-twitter" data-feather="twitter"></i>twitter</a><a class="btn btn-light" href="https://www.facebook.com/" target="_blank"><i class="txt-fb" data-feather="facebook"></i>facebook</a></div>
                </div> -->
              <p class="mt-4 mb-0">Already have an account?<a class="ms-2" href="<?=base_url('authantication')?>">Sign in</a></p>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


</section>
<script>
 $("form#Frontregister").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  toastr.success(data.message);
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){

     location.href="<?=base_url('dashboard')?>"; 	
    
  }, 1000) 
  
  }else if(data.status==403) {
  toastr.error(data.message);

  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastr.error(data.message);
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });
  
  function userTypeFields(user_type){
      //alert(user_type);
        // if(user_type=='vendor'){
        //   $('#vendor_gst').show();
        //   $('#vendor_adhar').show();
        //   $('#vendor_pan').show();
        // }else{
        //   $('#vendor_gst').hide();
        //   $('#vendor_adhar').hide();
        //   $('#vendor_pan').hide();
        // }
      }

      function selectState(countryID){
        $.ajax({
          url: '<?=base_url('Ajax_controller/get_state')?>',
          type: 'POST',
          data: {countryID},
          success: function (data) {
            $('#state').html(data);
          }
         });
      }

      function selectCity(stateID){
        $.ajax({
          url: '<?=base_url('Ajax_controller/get_city')?>',
          type: 'POST',
          data: {stateID},
          success: function (data) {
            $('#city').html(data);
          }
          });
      }
</script>