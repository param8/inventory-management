<!-- Content Wrapper. Contains page content -->
<div class="page-body">
    <div class="container-fluid">
        <!-- Content Header (Page header) -->
        <div class="page-title">
            <div class="row">
                <div class="col-6">
                    <h3><?=$page_title?></h3>
                </div>
                <div class="col-6">

                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                        <li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
                        <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
                    </ol>

                </div>
            </div>
        </div>
    </div>
    <!-- Main content -->
    <section class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <!-- <div class="card-header">
              <h3 class="box-title"><?=$page_title?></h3>
            </div> -->

                    <!-- /.box-header -->
                    <div class="card-body">
                        <form action="<?=base_url('Setting/store_siteInfo')?>" id="addSiteInfo" method="POST"
                            enctype="multipart/form-data">
                            <div class="modal-body row">
                                <div class="form-group col-md-6">
                                    <label for="site_name" class="col-form-label">Site Name:</label>
                                    <input type="text" class="form-control" name="site_name" id="site_name"
                                        value="<?=$siteinfo->site_name?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="site_contact" class="col-form-label">Mobile No.:</label>
                                    <input type="text" class="form-control" maxlength="10" minlength="10"
                                        name="site_contact" id="site_contact" value="<?=$siteinfo->site_contact?>"
                                        oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="whatsapp_no" class="col-form-label">Whatsapp No.:</label>
                                    <input type="text" class="form-control" maxlength="10" minlength="10"
                                        name="whatsapp_no" id="whatsapp_no" value="<?=$siteinfo->whatsapp_no?>"
                                        oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="site_email" class="col-form-label">Email:</label>
                                    <input type="email" class="form-control" name="site_email" id="site_email"
                                        value="<?=$siteinfo->site_email?>">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="facebook_url" class="col-form-label">Facebook URL:</label>
                                    <input type="text" class="form-control" name="facebook_url" id="facebook_url"
                                        value="<?=$siteinfo->facebook_url?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="youtube_url" class="col-form-label">Youtube URL:</label>
                                    <input type="text" class="form-control" name="youtube_url" id="youtube_url"
                                        value="<?=$siteinfo->youtube_url?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="linkedin_url" class="col-form-label">Linkedin URL:</label>
                                    <input type="text" class="form-control" name="linkedin_url" id="linkedin_url"
                                        value="<?=$siteinfo->linkedin_url?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="twitter_url" class="col-form-label">Twitter URL:</label>
                                    <input type="text" class="form-control" name="twitter_url" id="twitter_url"
                                        value="<?=$siteinfo->twitter_url?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="insta_url" class="col-form-label">Instagram URL:</label>
                                    <input type="text" class="form-control" name="insta_url" id="insta_url"
                                        value="<?=$siteinfo->insta_url?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="footer_contant" class="col-form-label">Footer Data:</label>
                                    <input type="text" class="form-control" name="footer_contant" id="footer_contant"
                                        value="<?=$siteinfo->footer_contant?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="discription" class="col-form-label">Discription:</label>
                                    <textarea class="form-control" name="discription"
                                        id="discription"><?=$siteinfo->discription?></textarea>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="site_address" class="col-form-label">Address:</label>
                                    <textarea class="form-control" name="site_address"
                                        id="site_address"><?=$siteinfo->site_address?></textarea>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="image" class="col-form-label">Class Image:</label>
                                    <input type="file" class="form-control" name="site_logo" id="site_logo"
                                        accept="image/png, image/jpeg">
                                </div>
                                <div>
                                    <image src="<?=base_url($siteinfo->site_logo)?>" width="200" height="100">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
</div>
</div>
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
$("form#addSiteInfo").submit(function(e) {
    $(':input[type="submit"]').prop('disabled', true);
    e.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(data) {
            if (data.status == 200) {
                toastr.success(data.message);
                setTimeout(function() {
                    location.reload();
                }, 1000)

            } else if (data.status == 403) {
                toastr.error(data.message);
                $(':input[type="submit"]').prop('disabled', false);
            } else {
                toastr.error('Unable to add site info');
                $(':input[type="submit"]').prop('disabled', false);
            }
        },
        error: function() {}
    });
});
</script>