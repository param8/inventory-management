<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />
    <title>Hindiko Solution || Expenses</title>
    
     <style>
      .tbodystyle{
        font-size: 16px;
        font-weight: bolder;
      }
      table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: center;
  padding: 5px;
}
td{
  text-align:center;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
     </style>
  </head>
  <body style="background-color:#fff;">
    <div class="right_col" role="main" >
      <!-- top tiles -->
      <div class="row" >
        <div class="" style="border:1px solid;">
          <div class="">
            <div class="x_content">
              <!-- start form for validation -->
              <table class="table table-striped table-bordered" style="width:100%">
              <thead><tr><th></th><th colspan="2" style="text-align:right"><a style="" href="<?=base_url('expence');?>" class="btn btn-primary"> Back To Expenses</a></th></tr></thead>
                <thead>
                   
                  <tr>
                    <th colspan="3" class="thListHeading" style="text-align:center;background-color:#cccc00 !important; "><p><strong style="font-size:22px;">EXPENSE REPORT</strong><p></th>
                 
                  </tr>
                  <tr>
                        <th>Project Name : <?=$project_data->name?></th>
                        <th>Project Budget : <?=$project_data->budget?></th>
                        <?php $texpense=0; foreach($expence_data as $key=>$data){$texpense+=$data['expence'];}?>
                 
                        <th>Remaining Balance : <?=$project_data->budget-$texpense?></th>
                    </tr>
                  <tr style="border-top:1px solid black">
                    <th>Expense Amount</th>
                    <th>Perticular Remark</th>
                    <th>Date</th>
                  </tr>
                </thead>
                <tbody>
                <?php $expense=0; foreach($expence_data as $key=>$data){$expense+=$data['expence'];?>
                  <tr>
                    <td><?=$data['expence']?></td>
                    <td ><?=$data['particular']?></td>
                    <td ><?=date('d F Y',strtotime($data['created_at']))?></td>
                  </tr>
                  <?php }?>
                </tbody>
                <tfoot>
                  <tr>
                  <th colspan="3">Total Expense : <?=$expense?></th>
                </tr>
                </tfoot>
                <tfoot>
                   
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>