<div class="page-body">
   <div class="container-fluid">
      <div class="page-title">
         <div class="row">
            <div class="col-6">
               <h3><?=$title?></h3>
            </div>
            <div class="col-6">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                     <a href="#">
                        <svg class="stroke-icon">
                           <use href="<?=base_url('public/assets/svg/icon-sprite.svg#stroke-home')?>"></use>
                        </svg>
                     </a>
                  </li>
                  <li class="breadcrumb-item">ECommerce</li>
                  <li class="breadcrumb-item active"><?=$title?></li>
               </ol>
            </div>
         </div>
      </div>
   </div>
   <!-- Container-fluid starts-->
   <div class="container-fluid">
      <!-- Individual column searching (text inputs) Starts-->
         <div class="card container">
            <div class="card-header">
               <div class="row">
                  <div class="text-left col-md-3">
                        <label for="">Location</label>
                        <select class="form-control" onchange="locationFilter(this.value)">
                        <option value="">Select Location</option>
                        <?php $user_location = explode(',',$this->session->userdata('location'));
                        foreach($locations as $location){
                          if(in_array($location->id,$user_location)){?>
                        <option value="<?=$location->id?>" <?=($this->session->userdata('ProjectlocationID') == $location->id)?'selected':''?>><?=$location->location?></option>
                        <?php }}?>
                        </select>
                    <?php if(!empty($this->session->userdata('ProjectlocationID'))){?>
                      <a href="javacript:void(0);" class="text-danger" onclick="resetFilter(this.value)">Reset</a>
                      <?php }?>
                    </div>
                  <?php if($permission[1]=='add'){?>
                     <div class="col-md-9">
                  <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#addProjectModal" data-whatever="@fat">Add Project</button>
                  <!-- <button type="button" class="btn btn-outline-primary float-right mr-2" data-toggle="modal" data-target="#bulkProductUploadModal" data-whatever="@fat">Bulk Upload Project</button> -->
                  <button type="button" class="btn btn-success float-right mr-2" onclick="downloadExpence()"><i class="fa fa-download"></i> All Projects(CSV)</button>
                  </div>
                  
               <?php }?>
               </div>
               <div class="card-body">
                  <div class="table-responsive product-table">
                     <table class="display nowrap" id="projectDataTable" >
                        <thead>
                           <tr>
                              <th>SNO</th>
                              <th nowrap>Project Added By</th>
                              <th nowrap>Project Location</th>
                              <th nowrap>Project Name</th>
                              <th nowrap>Client Name</th>
                              <th>Budget</th>
                              <th>Remark</th>
                              <th nowrap>Start Date</th>
                              <th nowrap>Finish Date</th>
                              <th nowrap>Created Date</th>
                              <!-- <th nowrap>Added By</th> -->
                              <th nowrap>Project Status</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <!-- Individual column searching (text inputs) Ends-->
      </div>
   </div>
   <!-- Container-fluid Ends-->
</div>
<!-- Add product Modal -->
<div class="modal fade" id="addProjectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add Project</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form methos="POST" class="row" action="<?=base_url('Project/add_project')?>" id="createProjectForm">
            <div class="col-md-6">
               <div class="form-group">
                  <label for="project_name">Project Name :</label>
                  <input type="text" class="form-control" id="project_name" name="project_name" placeholder="Enter Project Name">
               </div>
            </div>   
            <div class="col-md-6">
               <div class="form-group"> 
               <label for="budget">Budget :</label>
                  <input type="text" class="form-control" id="budget" name="budget" placeholder="Enter Budget" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
               </div>
            </div>   
            <div class="col-md-6">
               <div class="form-group"> 
                  <label for="client">Client Name :</label>
                  <input type="text" class="form-control" id="client" name="client" placeholder="Enter Client Name">
               </div>
            </div>   
              
            <div class="col-md-6">
               <div class="form-group"> 
                  <label for="sdate">Start Date :</label>
                  <input type="date" class="form-control" id="sdate" name="sdate">
               </div>
            </div>   
            <div class="col-md-6">
               <div class="form-group"> 
                  <label for="sdate">Finish Date :</label>
                  <input type="date" class="form-control" id="fdate" name="fdate">
               </div>               
            </div>   
            <div class="col-md-6">
               <div class="form-group"> 
                  <label for="remark">Remark :</label>
                  <textarea class="form-control" id="remark" name="remark" placeholder="Enter Remark" ></textarea>
               </div>
            </div>  
            <div class="col-md-6">
               <div class="form-group"> 
                  <label for="remark">Location :</label>
                  <select class="form-control" id="location" name="location">
                     <option value="">Select Location</option>
                     <?php foreach($locations as $location){?>
                     <option value="<?=$location->id?>"><?=$location->location?></option>
                     <?php }?>
                  </select>
               </div>
            </div>                 
         </div>
         <div class="modal-footer">
         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
         <button type="submit" class="btn btn-primary">Save</button>
         </div>
         </form>
      </div>
   </div>
</div>
<!-- Edit product Modal -->
<div class="modal fade" id="editProjectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit Project</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form methos="POST" class="row" action="<?=base_url('Project/edit_project')?>" id="editProjectForm">
            <div class="col-md-6">
               <div class="form-group">
                  <label for="edit_name">Project Name :</label>
                  <input type="hidden" id="edit_projectId" name="edit_projectId">
                  <input type="text" class="form-control" id="edit_name" name="edit_name" placeholder="Enter Project Name">
               </div>
            </div>   
            <div class="col-md-6">
               <div class="form-group">
                  <label for="edit_name">Client Name :</label>
                  <input type="text" class="form-control" id="edit_client" name="edit_client" placeholder="Enter client Name">
               </div>
            </div>
            <div class="col-md-6">
                <div class="form-group"> 
                 <label for="edit_name">Budget :</label>
                  <input type="text" class="form-control" id="edit_budget" name="edit_budget" placeholder="Enter Budget">
                </div>
            </div>               
            <div class="col-md-6">
               <div class="form-group"> 
                  <label for="sdate">Start Date :</label>
                  <input type="date" class="form-control" id="edit_sdate" name="edit_sdate">
               </div>
            </div>   
            <div class="col-md-6">
               <div class="form-group"> 
                  <label for="sdate">Finish Date :</label>
                  <input type="date" class="form-control" id="edit_fdate" name="edit_fdate">
               </div>               
            </div>  
            <div class="col-md-6">
               <div class="form-group"> 
                  <label for="edit_name">Edit Remark :</label>
                  <textarea class="form-control" id="edit_remark" name="edit_remark" placeholder="Enter Remark"></textarea>
               </div>
            </div> 
            <div class="col-md-6">
            <div class="form-group">
            <label for="edit_price">Location <span class="text-danger">*</span></label>
            <select class="form-control" id="edit_location" name="edit_location">
                  <option value="">Select Location</option>
                  <?php foreach($locations as $location){?>
                  <option value="<?=$location->id?>"><?=$location->location?></option>
                  <?php }?>
               </select>
               </div>
            </div>  
               </div>
         <div class="modal-footer">
         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
         <button type="submit" class="btn btn-primary">Update</button>
         </div>
         </form>
      </div>
   </div>
</div>
<script>
   $(document).ready(function() {
       // $('#example').DataTable();
       // } );
       var dataTable = $('#projectDataTable').DataTable({
          "processing": true,
          "serverSide": true,
        dom: 'lBfrtip',
        buttons: [ 
           { extend: 'excelHtml5', text: 'Download Excel'}
        ],
        aLengthMenu: [
        [10,25, 50, 100, 200, -1],
        [10,25, 50, 100, 200, "All"]
        ],
           "order": [],
           "ajax": {
               url: "<?=base_url('Project/ajaxprojects')?>",
               type: "POST"
           },
        //   "columnDefs": [{
        //       "targets": [0],
        //       "orderable": false,
        //   }, ],
       });
   });
   
   $('#addProjectModal').on('shown.bs.modal', function () {
   $('#myInput').trigger('focus')
   })
   
   $("form#createProjectForm").submit(function(e) {
   //alert('fgdfgfd');
   $(':input[type="submit"]').prop('disabled', true);
   e.preventDefault();    
   var formData = new FormData(this);
   $.ajax({
   url: $(this).attr('action'),
   type: 'POST',
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   dataType: 'json',
   success: function (data) {
   if(data.status==200) {
   toastr.success(data.message);
   $(':input[type="submit"]').prop('disabled', false);
   setTimeout(function(){
   
      location.href="<?=base_url('Project')?>"; 	
     
   }, 1000) 
   
   }else if(data.status==403) {
   toastr.error(data.message);
   
   $(':input[type="submit"]').prop('disabled', false);
   }else{
     toastr.error(data.message);
      $(':input[type="submit"]').prop('disabled', false);
   }
   },
   error: function(){} 
   });
   });
   
   function editProjectModal(projectID){
      //alert(projectID);
     $.ajax({
        url: '<?=base_url('Project/get_project')?>',
        type: 'POST',
        data: {projectID},
        //dataType: 'json',
        success: function(data) {
         
        $('#editProjectModal').modal('show');
        var project = $.parseJSON(data);
       // console.log(data);
               $('#edit_projectId').val(projectID);
               $('#edit_name').val(project.name);
               $('#edit_client').val(project.client);
               $('#edit_budget').val(project.budget);
               $('#edit_remark').val(project.remark);
               $('#edit_sdate').val(project.start_date);
               $('#edit_fdate').val(project.end_date);
               $('#edit_location').val(project.location);
               
        }
    });
   }
   
   $("form#editProjectForm").submit(function(e) {
   //alert('fgdfgfd');
   $(':input[type="submit"]').prop('disabled', true);
   e.preventDefault();    
   var formData = new FormData(this);
   $.ajax({
   url: $(this).attr('action'),
   type: 'POST',
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   dataType: 'json',
   success: function (data) {
   if(data.status==200) {
   toastr.success(data.message);
   $(':input[type="submit"]').prop('disabled', false);
   setTimeout(function(){
   
      location.href="<?=base_url('project')?>"; 	
     
   }, 1000) 
   
   }else if(data.status==403) {
   toastr.error(data.message);
   
   $(':input[type="submit"]').prop('disabled', false);
   }else{
     toastr.error(data.message);
      $(':input[type="submit"]').prop('disabled', false);
   }
   },
   error: function(){} 
   });
   });
   
   function delete_project(projectId){
     Swal.fire({
           title: 'Are you sure?',
           text: "You won't to delet it!",
           type: 'warning',
           showCancelButton: true,
           confirmButtonColor: '#3085d6',
           cancelButtonColor: '#d33',
           confirmButtonText: 'Yes, delete it!'
       }).then((result) => {
           if (result.value) {
               $.ajax({
                   url: '<?=base_url('Project/delete_project')?>',
                   type: 'POST',
                   data: {
                     projectId
                   },
                   dataType: 'json',
                   success: function(data) {
                       if (data.status == 200) {
                           toastr.success(data.message);
                           location.href = "<?=base_url('project')?>"
   
                       } else if (data.status == 302) {
                           toastr.error(data.message);
                       }
                   }
               })
           }
       })
   }
   function close_project(projectId){
     Swal.fire({
           title: 'Are you sure?',
           text: "You want to Close it!",
           type: 'warning',
           showCancelButton: true,
           confirmButtonColor: '#3085d6',
           cancelButtonColor: '#d33',
           confirmButtonText: 'Yes, Close it!'
       }).then((result) => {
           if (result.value) {
               $.ajax({
                   url: '<?=base_url('Project/close_project')?>',
                   type: 'POST',
                   data: {
                     projectId
                   },
                   dataType: 'json',
                   success: function(data) {
                       if (data.status == 200) {
                           toastr.success(data.message);
                           location.href = "<?=base_url('project')?>"
   
                       } else if (data.status == 302) {
                           toastr.error(data.message);
                       }
                   }
               })
           }
       })
   }
   

   function locationFilter(id) {
    
    $.ajax({
      url: "<?=base_url('Project/locationFilter')?>",
      type: 'POST',
      data: {
        id
      },
      success: function(data) {
        location.reload();
      },
      error: function() {}
    });
  }
  
  function resetFilter() {
    $.ajax({
      url: "<?=base_url('Project/resetLocationFilter')?>",
      type: "POST",
      data: {},
  
      success: function(data) {
        location.reload();
      },
      error: function() {}
    });
  }

  function downloadExpence(){
    
    var messageText  = "You want download  Projects!";
      var confirmText =  'Yes, download it!';
      var message  ="Projects downloaded Successfully!";
   
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {

          location.href="<?=base_url('Project/export_projects')?>";  
        }
        })
  }
</script>