<div class="page-body">
  <div class="container-fluid">
    <div class="page-title">
      <div class="row">
        <div class="col-6">
          <h3><?=$title?></h3>
        </div>
        <div class="col-6">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?=base_url('Dashboard')?>">
                <svg class="stroke-icon">
                  <use href="<?=base_url('public/assets/svg/icon-sprite.svg#stroke-home')?>"></use>
                </svg></a></li>
            <li class="breadcrumb-item">Users</li>
            <li class="breadcrumb-item active"><?=$title?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <!-- Individual column searching (text inputs) Starts-->
      <div class="col-sm-12">
        <div class="card">
          <!-- <div class="card-header">
                   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#creatUserModal" data-whatever="@fat">Create User</button>
                    </button>
                  </div> -->
          <div class="card-body">
            <div class="table-responsive product-table">
              <table class="display" id="qoutationTable">
                <thead>
                  <tr>
                    <th>SNo</th>
                    <th>Name</th>
                    <th> Date</th>
                    <th>Action</th>
                  </tr>
                </thead>

              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- Individual column searching (text inputs) Ends-->
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- Modal -->

<div class="modal fade bd-example-modal-lg" id="sendQuoteModal" tabindex="-1" role="dialog"
  aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog   modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Send Quotation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid basic_table">
          <div class="card">
            <form action="<?=base_url('Bom/sendQuotation')?>" id="sendQuote" method="post">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr class="border-bottom-primary">
                      <?php $Id = $this->session->userdata('role_id');
                       if($Id==3){?>
                      <th scope="col">S.No.</th>
                      <?php }?>

                      <!-- <th scope="col">Product Code</th> -->
                      <th scope="col">Part Name</th>
                      <th scope="col">Specification</th>
                      <th scope="col">Make</th>
                      <th scope="col">Qty</th>
                      <th scope="col">Price/Unit</th>
                      <th scope="col" nowrap>Tax (%)</th>
                      <th nowrap scope="col">Total Price</th>

                    </tr>
                  </thead>
                  <tbody id="enquery_products">
                  </tbody>
                </table>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<div class="modal fade bd-example-modal-lg" id="editqutation" tabindex="-1" role="dialog"
  aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog   modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Send ReQuotation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid basic_table">
          <div class="card">
            <form action="<?=base_url('Bom/sendReQuotation')?>" id="sendQuote" method="post">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr class="border-bottom-primary">
                      <?php $Id = $this->session->userdata('role_id');
                       if($Id==3){?>
                      <th scope="col">S.No.</th>
                      <?php }?>

                      <!-- <th scope="col">Product Code</th> -->
                      <th scope="col">Part Name</th>
                      <th scope="col">Specification</th>
                      <th scope="col">Make</th>
                      <th scope="col">Qty</th>
                      <th scope="col">Price/Unit</th>
                      <th scope="col" nowrap>Tax (%)</th>
                      <th nowrap scope="col">Total Price</th>

                    </tr>
                  </thead>
                  <tbody id="quotation_products">
                  </tbody>
                </table>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
<script>
function get_tprice(price, productCode) {
  var qty = document.getElementById('qty_' + productCode).value;
  var totalprice = parseFloat(price) * parseFloat(qty);
  document.getElementById('tprice_' + productCode).value = isNaN(totalprice) ? 0 : totalprice;
}

function sendQuoteModal(EnID) {
  $('#sendQuoteModal').modal('show');
  $.ajax({
    url: '<?=base_url('Bom/get_inquiries')?>',
    type: 'POST',
    data: {
      EnID
    },
    success: function(data) {
      $('#enquery_products').html(data);

    }
  });
}

function editqutation(quID) {
  $('#editqutation').modal('show');
  $.ajax({
    url: '<?=base_url('Bom/requotation_form')?>',
    type: 'POST',
    data: {
      quID
    },
    success: function(data) {
      $('#quotation_products').html(data);

    }
  });
}
$(document).ready(function() {
  // $('#example').DataTable();
  // } );
  var role = '<?=$title?>';
  var dataTable = $('#qoutationTable').DataTable({
    "processing": true,
    "serverSide": true,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Download Excel'
    }],
    "order": [],
    "ajax": {
      url: "<?=base_url('Bom/ajaxEnquiries')?>",
      type: "POST",
      data: {
        role
      }
    },
    "columnDefs": [{
      "targets": [0],
      "orderable": false,
    }, ],
  });
});


$('#creatUserModal').on('shown.bs.modal', function() {
  $('#myInput').trigger('focus')
})

$("form#sendQuote").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('enquiry')?>";

        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});


$("form#editUserForm").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('User')?>";

        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});
</script>