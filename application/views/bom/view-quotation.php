<?php
$compare = $compares->row();
// echo "<pre>";
// print_r($bom_detail);die;sds
?>
<div class="page-body">
  <div class="container-fluid">
    <div class="page-title">
      <div class="row">
        <div class="col-6">
          <h3><?=$title?></h3>
        </div>
        <div class="col-6">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?=base_url('Dashboard')?>">
                <svg class="stroke-icon">
                  <use href="<?=base_url('public/assets/svg/icon-sprite.svg#stroke-home')?>"></use>
                </svg></a></li>
            <li class="breadcrumb-item">Users</li>
            <li class="breadcrumb-item active"><?=$title?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <style>
  .checked_class {
    box-sizing: border-box;
    padding: 0;
    border-radius: 8px;
    height: 10px;
    width: 10px;
    -webkit-appearance: none;
    background-color: #0075ff;
  }
  </style>
  <div class="container basic_table">
    <div class="card">
      <div class="card-header ">
        <div class="text-left">
          <h4>Project : <?=$project_detail->name?>(<?=$bom_detail['title']?>)</h4>

        </div>
        <div class="text-right">
          <?php if($permission[5]=='excel'){?>
          <button type="button" class="btn btn-success" data-toggle="modal" onclick="downloadExcel()">Download
            Excel
          </button>
          <?php }?>
        </div>

      </div>

      <div class="card-body">
        <form action="<?=base_url('Bom/compare_quotations')?>" id="sendCompareQuote" method="post">
          <div class="table-responsive" style="max-height:450px;">
            <table class="table table-bordered">
              <thead>
                <th>Products</th>
                <!-- <th>QTY</th> -->
                <?php 
              //print_r($bom_detail);die;
               $total_vendor =  count($bom_detail['object']);
               foreach($bom_detail['object'] as $vendor){?>
                <th nowrap><?=$vendor['userName']?> <?php if($compare->status!=1){?><a href="javascript:void()" class="btn btn-danger btn-sm"
                    title="Requot Quotation" onclick="requotation(<?=$vendor['quote_by']?>,<?=$vendor['id']?>)"><i
                      class="fa fa-refresh" aria-hidden="true"></i></a><?php } ?><br>(<b>Delivery Date : </b><?= date('d/m/Y', strtotime($vendor['delivery_date']))?>)</th>

                <?php } ?>

              </thead>
              <tbody style="max-height: 450px;">
                <?php $products = json_decode($bom_detail['product_detail']);
                $total_price = array();
                
                  foreach($products as $product_key=>$product){
                    $price_product = 0;
                     
                     $product_data = $this->Product_model->get_product(array('products.product_code'=>$product->product_code));
                    $product_code = $product_data->product_code;
                    // print_r($product_code);die;
                    $this->db->where('product_code',$product_code);
                   $product_name =  $this->db->get('products')->row();
                  
                  $compareProduct = json_decode($compare->product_detail);
                 $compare_venoder = json_decode($compareProduct->$product_code);

                    ?>
                <tr>
                  <td nowrap><?=$product_name->part_name.' ('.$product_code.')'?></td>

                  <?php
                    // ini_set('display_errors',1);
                    foreach($bom_detail['object'] as $price){

                         $decode_detail = json_decode($price['product_detail']);
                         $product_price = $decode_detail->$product_code;
                      ?>

                  <td class="text-center" nowrap><?php 
                     if(!empty($product_price->price)){
                      
                     echo  'QTY ' .$product_price->qty.' <b>--</b> ₹ '. $product_price->price;
                     $pro_val = 'productID@'.$price['quote_by'];
                     ?>

                    <span>
                      <input type="radio"
                        class="radio <?=$compare_venoder->vendorID==$price['quote_by'] && $compare_venoder->$pro_val == $product_code && $compares->num_rows() > 0 && $compare->status != 0 ? 'checked_class' : ''?>"
                        name="comapre[<?=$product_code?>]"
                        value="<?=base64_encode(json_encode(array('vendorID'=>$price['quote_by'],'productID@'.$price['quote_by']=>$product_code,'price@'.$price['quote_by']=>$product_price->price,'qty@'.$price['quote_by']=>$product_price->qty,'tax@'.$price['quote_by']=>$product_price->tax,'quotID@'.$price['quote_by']=>$price['id'],'totalPrice'=>($product_price->price*$product_price->qty))))?>"
                        <?=$compares->num_rows() > 0 && $compare->status != 0 ? 'disabled' : '' ?>
                        <?=$compare_venoder->vendorID==$price['quote_by'] && $compare_venoder->$pro_val == $product_code ? 'checked' : ''?>
                        onclick="get_total_price()">
                    </span>
                    <?php
                     }else{
                      echo '<b style="text-align: center;font-weight:bold">-</b>';
                     }
                   ?>
                  </td>
                  <?php } ?>
                </tr>

                <?php
                  
                  }
                ?>


              </tbody>
              <tfoot>
                <tr class="bg-gray">
                  <td class="text-center text-light"><b>Grand Total</b></td>
                  <td colspan="<?=$total_vendor?>" class="text-center text-light"><b id="grand_total"></b></td>
                </tr>
              </tfoot>

            </table>

          </div>
          <hr>
          <div class="row">
            <div class="col-md-4 text-center">
              <!-- <input type="submit" class="btn btn-primary" value="send"> -->

              <p class="font-weight-bold text-right">
                <?=(empty($compare->approval_by) ) ? '<span><a href="javascript:void(0)" class="btn btn-danger">Pending From Manager Side</a></span>' : (json_decode($compare->approval_by)->role_id == 1 ? '<span><a href="javascript:void(0)" class="btn btn-danger">Pending From General Manager Side </a></span>' : (json_decode($compare->approval_by)->role_id == 5 ? '<span><a href="javascript:void(0)" class="btn btn-danger">Pending From Director Side </a></span>' : '<span><a href="javascript:void(0)" class="btn btn-success">Approved</a></span>'))?>
              </p>
            </div>
            <div class="col-md-3 text-center">
              <?php if((empty($compare->approval_by) && $this->session->userdata('role_id') == 1) || (!empty($compare->approval_by)  && $this->session->userdata('role_id') == 5 && json_decode($compare->approval_by)->statusint == 2) || (!empty($compare->approval_by) && $this->session->userdata('role_id') == 11 && json_decode($compare->approval_by)->statusint == 3)){?>
              <input type="submit" class="btn btn-primary" value="Submit for Aprove">
              <?php }?>
            </div>
            <div class="col-md-2 text-center">
              <?php if(($this->session->userdata('role_id') == 5 && json_decode($compare->approval_by)->statusint == 2) || ($this->session->userdata('role_id') == 11 && json_decode($compare->approval_by)->statusint == 3)){?>
              <a href="javascript:void(0)" class="btn btn-danger" onclick="recompareModal()">Re-comapre</a>
              <?php } ?>
            </div>
            <div class="col-md-3 text-center">
              <?php if(!empty($compare->disapprove_reason)){?>
              <a href="javascript:void(0)" class="btn btn-warning" onclick="viewMessageModal(<?=$compare->id?>)">View
                Message</a>
              <?php } ?>
            </div>

          </div>
          <hr>
        </form>
      </div>
    </div>
  </div>

</div>
<div class="modal fade" id="recompareModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Recompare Reason</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <form action="<?=base_url('Bom/send_recompare')?>" id="recompareForm" method="post">
          <div class="form-group">
            <textarea class="form-control" id="recompare" name="recompare" placeholder="Enter Recompare"></textarea>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="compareReasonModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Recompare Reason</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <div id="multiple_form" class="row">
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>S.No.</th>
                  <th nowrap>Recomapre By</th>
                  <th nowrap class="text-danger">Recompare Reason</th>
                </tr>
              </thead>
              <tbody id="disappreason">

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- /Requotetation -->

<div class="modal fade" id="requotetationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Requotetation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <div id="multiple_form" class="row">
          <form action="<?=base_url('Bom/send_requotetation')?>" id="requotetationForm" method="post">
            <div class="form-group">
              <textarea class="form-control" id="requotetation" name="requotetation" placeholder="Enter Requotetion"></textarea>
            </div>
            <input type="hidden" name="requot_vendorID" id="requot_vendorID">
            <input type="hidden" name="requot_quotetationID" id="requot_quotetationID">
            <input type="hidden" name="requot_projectID" id="requot_projectID" value="<?=$bom_detail['project_id']?>">
            <input type="hidden" name="requot_bomID" id="requot_bomID" value="<?=$bom_detail['id']?>">
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$("form#sendCompareQuote").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("project_id", '<?=$bom_detail['project_id']?>');
  formData.append("bom_id", '<?=$bom_detail['id']?>');
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('quotation')?>";

        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

$("form#recompareForm").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("project_id", '<?=$bom_detail['project_id']?>');
  formData.append("bom_id", '<?=$bom_detail['id']?>');
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('quotation')?>";

        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

function recompareModal() {
  $('#recompareModal').modal('show');
}

function viewMessageModal(id) {
  $.ajax({
    url: '<?=base_url('Bom/disappreason/')?>',
    type: 'POST',
    data: {
      id: id
    },
    dataType: 'html',
    success: function(data) {
      $('#compareReasonModal').modal('show');
      $('#disappreason').html(data);
    },
  });
}

function get_total_price() {
  var totalprice = 0;
  $(".radio:checked").each(function() {

    var val = $(this).val();
    var decoded_val = atob(val);
    var json_decode_val = jQuery.parseJSON(decoded_val);
    var vendorID = json_decode_val.vendorID;
    var price = json_decode_val.totalPrice;
    vendor_array = [vendorID];
    if (vendorID == vendorID) {
      totalprice += +price;
    }

  });
  $('#grand_total').html(
    '<span>₹ ' + totalprice +
    '</span>');

}

$(document).ready(function() {
  get_total_price();
});

$("input:radio").on('click', function() {
  // in the handler, 'this' refers to the box clicked on
  var $box = $(this);
  if ($box.is(":checked")) {
    // the name of the box is retrieved using the .attr() method
    // as it is assumed and expected to be immutable
    var group = "input:radio[name='" + $box.attr("name") + "']";
    // the checked state of the group/box on the other hand will change
    // and the current value is retrieved using .prop() method
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});

function requotation(vendorID, quotetionID) {
  $('#requotetationModal').modal('show');
  $('#requot_vendorID').val(vendorID);
  $('#requot_quotetationID').val(quotetionID);
}

$("form#requotetationForm").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("project_id", '<?=$bom_detail['project_id']?>');
  formData.append("bom_id", '<?=$bom_detail['id']?>');
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('view-quotations/'.base64_encode($bom_detail['id']))?>";

        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});
</script>