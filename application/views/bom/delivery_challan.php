<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title>Delivery Challan</title>
</head>

<body style="margin:0!important;padding:0!important">
  <center>
    <div style="width:100%;border: 2px solid #55714c;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style=" font-size:14px;">
        <tbody style="background-color: #fff;  padding-bottom: 20px">

          <tr>
            <td align="cenetr" colspan="5">
              <h1 style="text-align: center;    margin: 0;;">Delivery Challan</h1>
              <h3 style="text-align: center;    margin: 0;">Returnable / Non Returnable</h3>
            </td>
          </tr>

          <tr>
            <td colspan="5">
              <table style="width: 100%;">
                <tbody>
                  <tr>
                    <td valign="top" style="padding:5px; width: 70%; ">
                      <table style="width: 100%;">
                        <tr>
                          <td colspan="4">
                            <div class="" style="text-align: center;">
                              <h3 style="margin: 5px; font-weight: bold;">HINDIKO SOLUTION INDIA PVT LTD</h3>
                              <p style="margin: 5px; font-weight: bold;">484, ECOTECH 3, UDYOG KENDRA 2</p>
                              <p style="margin: 5px; font-weight: bold;">UTTAR PRADESH, Code : 09</p>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td style="width: 24%;font-weight: 600;">Phone :</td>
                          <td style="width: 24%;font-weight: 600;"><?=$siteinfo->site_contact?></td>
                          <td style="width: 24%;font-weight: 600;">Email :</td>
                          <td style="width: 24%;font-weight: 600;"><?=$siteinfo->site_email?></td>
                        </tr>
                      </table>
                    </td>
                    <td valign="top" style="padding:5px; width: 30%;text-align: right;">
                      <img src="<?=base_url($siteinfo->site_logo)?>" alt="logo" style="width: 100%;height: 66px;">
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>

        </tbody>
      </table>
      <table width="100% " border="0" cellspacing="0" cellpadding="0"
        style="    border-top: 3px solid #000;       font-weight: 500; padding-top: 20px;margin-top:0px; font-size:14px">
        <tbody>
          <tr>
            <td colspan=""  style="padding:5px;text-align:center;width:15%;"><b>Bill To:</b></td>
            <td colspan=""  style="padding:5px;text-align:center;width:35%;"><?=$user->vendorName?></td>
            <td colspan=""  style="padding:5px;text-align:center;width:35%;"><b>Delivery Challan No.</b>
            </td>
            <td colspan="" style="padding:5px;text-align:center;width:15%;"><?=$user->challan_no?></td>
          </tr>
          <tr>
            <td colspan=""  style="padding:5px;text-align:center;width:35%;">DC Date and Time</td>
            <td colspan=""  style="padding:5px;text-align:center;width:15%;"> <?=date('d-m-Y H:i:s',strtotime($user->challan_date_time))?></td>
            <td>PO Number</td>
            <td><?=$user->po_no?></td>
          </tr>
          <tr>
            <td colspan="" style="padding:5px;text-align:center;width:15%;"> Address:</td>
            <td colspan="" style="padding:5px;text-align:center;width:35%;"><?=$user->vendorAddress?>
            </td>
            <td colspan="" style="padding:5px;text-align:center;width:35%;">Mode of Delivery </td>
            <td colspan="" style="padding:5px;text-align:center;width:15%;">
              <?=$user->mode_of_delivery?></td>
          </tr>
          <tr>
            <td colspan="" style="padding:5px;text-align:center;width:15%;">State :</td>
            <td colspan="" style="padding:5px;text-align:center;width:35%;"> <?=$user->vendorState?>
            </td>
            <td colspan="" style="padding:5px;text-align:center;width:35%;">Vehicle Number</td>
            <td colspan="" style="padding:5px;text-align:center;width:15%;"><?=$user->vehicle_no?></td>
          </tr>
          <tr>
            <td colspan="" style="padding:5px;text-align:center;width:15%;">GSTN Number :</td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:35%;"><?=empty($user->vendorGst)?'-':$user->vendorGst?></td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:35%;">Place of Supply</td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:15%;"><?=$user->place_of_supply?>
            </td>
          </tr>
          <tr>
            <td colspan=""  style="padding:5px;text-align:center;width:15%;">Phone : <?=$user->vendorPhone?></td>
            <td colspan="" style="padding:5px;text-align:center;width:35%;">Email :  <?=$user->vendorEmail?></td>
            <td colspan=""  style="padding:5px;text-align:center;width:35%;">FAX : <?=$user->fax?></td>
            <td colspan="" valign="middle" style="padding:5px;text-align:center;width:15%;"></td>
          </tr>

        </tbody>
      </table>
      <br>
      <table style="width: 100%;padding-bottom: 10px; text-align: center;border-top: 3px solid #000;border-spacing: 0;">
        <thead style="    background-color: #c0d0c2;">
          <tr>
            <th style="width: 10%;">No</th>
            <th style="width: 26%;">Product Description</th>
            <th style="width: 12%;">HSN Codes</th>
            <th style="width: 9%;">Qty</th>
            <th style="width: 12%;">Unit Rate</th>
            <th style="width: 12%;">GST Taxes</th>
            <th style="width: 20%;">Sub total (Without Tax)</th>
          </tr>
        </thead>
        <tbody>

          <?php $dc_subtotal = 0; $tax_total = 0; $grand_total = 0; $sno=1; 
          foreach(json_decode($user->product_detail) as $key=>$data){
            //print_r(json_decode($data));die;
            $row = json_decode($data);
            //print_r($row);die;
            $vendorID = $row->vendorID;

            //print_r($vendorID);die;
            if($vendorID == $row->vendorID){
              $product_pro = 'productID@'.$vendorID; 
              $price_pro = 'price@'.$vendorID;
              $price[$row->$product_pro] = $row->$price_pro;
              $price_pro = 'price@'.$vendorID;
              $qty_pro = 'qty@'.$vendorID;
              $tax_pro = 'tax@'.$vendorID;
              //   print_r($row->$price_pro);  die;
              
              $dc_subtotal+= $row->$qty_pro*$row->$price_pro;
              $tax_total+= empty($row->$tax_pro)?0:$row->$tax_pro;
             
              $product = $this->Product_model->get_product(array('product_code'=>$key))     ;  
                  // print_r($product);die;   
            ?>
          <tr>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"><?=$sno?></td>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"><?=$product->part_name?></td>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"></td>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"><?=$row->$qty_pro?></td>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"><?=$row->$price_pro?></td>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"><?=empty($row->$tax_pro)?0:$row->$tax_pro?></td>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"><?=$row->$qty_pro*$row->$price_pro?></td>
          </tr>
          <?php $sno++;}}
          $grand_total= $dc_subtotal + $tax_total;
          $number = $grand_total;
          $no = floor($number);
          $point = round($number - $no, 2) * 100;
          $hundred = null;
          $digits_1 = strlen($no);
          $i = 0;
          $str = array();
          $words = array('0' => '', '1' => 'one', '2' => 'two',
           '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
           '7' => 'seven', '8' => 'eight', '9' => 'nine',
           '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
           '13' => 'thirteen', '14' => 'fourteen',
           '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
           '18' => 'eighteen', '19' =>'nineteen', '20' => 'twenty',
           '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
           '60' => 'sixty', '70' => 'seventy',
           '80' => 'eighty', '90' => 'ninety');
          $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
          while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;
            if ($number) {
               $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
               $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
               $str [] = ($number < 21) ? $words[$number] .
                   " " . $digits[$counter] . $plural . " " . $hundred
                   :
                   $words[floor($number / 10) * 10]
                   . " " . $words[$number % 10] . " "
                   . $digits[$counter] . $plural . " " . $hundred;
            } else $str[] = null;
          }
          $str = array_reverse($str);
          $result = implode('', $str);
          $points = ($point) ?
           "." . $words[$point / 10] . " " . 
                 $words[$point = $point % 10] : '';
          // echo $result ;?>
        

        </tbody>
      </table>
      <table  style="width: 100%;padding-bottom: 10px; text-align: center;border-top: 3px solid #000;border-spacing: 0;">
        <tbody>
        <tr>
            <td colspan="8" style="    background-color: #c0d0c2;"></td>

          </tr>
          <tr style="border-bottom: 2px solid black;">
            <td colspan="6" style=" text-align: right;">Delivery Challan Subtotal</td>
            <td colspan="2" style=" text-align: right;">Rs. <?=$dc_subtotal?></td>
          </tr>
          <tr>
            <td colspan="6" style=" text-align: right;">Total GST Amount</td>
            <td colspan="2" style=" text-align: right;">Rs. <?=$tax_total?></td>
          </tr>
          <!-- <tr>
            <td colspan="3"></td>
            <td style="">SAC Code</td>
            <td style="background-color: #c0d0c2;"></td>
            <td colspan="" style=" text-align: right;">Shipping charges</td>
            <td style=""></td>
            <td style="border-bottom: 2px solid #c0d0c2;text-align: right;">Rs. </td>
          </tr> -->

          <tr>

            <td colspan="8" style=" margin-top: 5px;   background-color: #c0d0c2;"></td>

          </tr>
          <tr style=" background-color: #c0d0c2;">
            <td colspan="6" style=" text-align: right;">Total</td>
            <td colspan="2"style="border-bottom: 2px solid #c0d0c2;text-align: right;">Rs.<?=$grand_total?></td>
          </tr>
          <tr>
            <td colspan="2" style="text-align: left; padding-left: 20px; color: red;">Total Amount in Words -</td>
            <td colspan="6"><p style="text-transform:capitalize;"><?=$result?></p></td>
          </tr>
          <tr>
            <td colspan="4 " style="text-align: left; padding-left: 20px;">
              <p style="margin-bottom: 0;"><b>Terms and Conditions</b></p>
              <p style="margin-bottom: 0;">Only for delivery record</p>
            </td>
            <td colspan="4"></td>
          </tr>
          <tr>
            <td colspan="8" style="text-align: right; padding-right:10px;">
              <div style="display: inline-block;border: 1px solid;padding: 6px;text-align: center;">
                for <b>Hindiko Solution India Pvt Ltd</b>
                <br>(Authorized)
                <br>
                <br>
                Signatory
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </center>

</body>

</html>