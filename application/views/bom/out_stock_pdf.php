
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title><?=$title?></title>
</head>

<body>
  <center>
    <div style="width:100%;border: 3px solid #575378;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0"
      style=" font-size:14px;">
      <tbody  >
        <tr>
          <!-- <td style="  padding:20px 25px;" colspan="5" valign="top" align="right"><img
              src="https://swamatics.com/images/logo.png"></td>
        </tr>
        <tr> -->
          <!-- <td align="cenetr" colspan="5">
            <h1 style="text-align: center;    margin-top: 0;color: #fff;">Puchage Order</h1>
          </td>
        </tr> -->

        <!-- <tr>
          <td colspan="5" height="20">&nbsp;</td>
        </tr> -->

        <tr>
          <td colspan="5">
            <table style="width: 100%;">
              <tbody>
                <tr>
                 
                  <td align="cenetr" colspan="4" style="padding:5px; width: 40%; text-align: center;     font-weight: 500;">
                    <img src="<?=base_url($siteinfo->site_logo)?>" alt="" style="width: 30%;padding-top: 20px;">
                    <h3><?=$siteinfo->site_name?></h3>
                    <p style="margin-bottom:0"><?=$location->address?></p>
                    <h2 style="text-align: center; margin-top: 0; ">Material Issue slip</h2>
                  </td>
                  
                </tr>
                <tr>
                    
                     <td valign="top" colspan="2" style="padding:5px; vertical-align: bottom;    font-weight: 500;">
                    <p>S.No : <?=date('Ym', strtotime($date)).base64_decode($this->uri->segment(3));  ?></p>
                    <h3>Project : <?=$project->project_name?></h3>
                    <!--<p>Department :</p>-->
                  
                    </td>
                    <td valign="top" colspan="2" style="padding:5px; text-align: right;vertical-align: bottom;">
                   
                    <p>Date : <?=date('d F Y', strtotime($date))?></p>
                    <h3>Bom : <?=$project->title?></h3>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>

      </tbody>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0"
      style=" margin-top:0px; font-size:14px">
      <tbody>
       
      
        <tr style="background:#575378;color:white;border-bottom: 2px solid #666666!important;">
          <td valign="middle"  style="padding:5px;height:25px;text-align:center;font-weight:bold;border-bottom: 2px solid #666666!important;">S.No</td>
          <td valign="middle" align="cenetr" style="padding:5px;height:25px;text-align:center;font-weight:bold;border-bottom: 2px solid #666666!important;">Description</td>
          <td valign="middle" align="cenetr" style="padding:5px;height:25px;text-align:center;font-weight:bold;border-bottom: 2px solid #666666!important;">Req. Qty</td>
          <td valign="middle" colspan="2"align="cenetr" style="padding:5px;height:25px;text-align:center;font-weight:bold;border-bottom: 2px solid #666666!important;">Issue Qty</td>
          <!--<td valign="middle" align="cenetr" style="padding:5px;height:25px;text-align:center;font-weight:bold">Remarks</td>-->
        </tr>
        <?php $sno=1; foreach($products as $key=>$data){
        $product_data = $this->Product_model->get_product(array('product_code'=>$key));
        if($data->qty != 0){
        ?>
            <tr style="border-bottom: 2px solid #666666!important;">
                <td valign="top" align="left" style="padding:5px;height: 25px;text-align:center;"><?=$sno?></td>
                <td valign="top" align="left" style="padding:5px;height: 25px;text-align:center;border-left: 2px solid #666666;"><?=$product_data->name?>(<?=$key?>)</td>
                <td valign="top" align="right" style="padding:5px;height: 25px;text-align:center;border-left: 2px solid #666666;border-right: 2px solid #666666;"><?=$data->qty?> (<?=$product_data->name?>)</td>
                <td valign="top" align="right" colspan="2" style="padding:5px;height: 25px;text-align:center;border-right: 2px solid #666666;"><?=empty($data->iqty)?0:$data->iqty?> (<?=$product_data->name?>)</td>
                <!--<td valign="top" align="left" style="padding:5px;height: 25px;"></td>-->
            </tr>
            <?php $sno++;}}?>
          <?php if(!empty($returnables)){?>
            <tr><td colspan="6" style="text-align:center"><h2>Returnable Products</h2></td></tr>
            <tr style="background:#575378;color:white;">
          <td valign="middle"  style="padding:5px;height:25px;text-align:center;font-weight:bold">S.No</td>
          <td valign="middle" align="cenetr" style="padding:5px;height:25px;text-align:center;font-weight:bold">Product</td>
          <td valign="middle" align="cenetr" style="padding:5px;height:25px;text-align:center;font-weight:bold">Qty</td>
          <td valign="middle" align="cenetr" style="padding:5px;height:25px;text-align:center;font-weight:bold">Person</td>
          <td valign="middle" align="cenetr" style="padding:5px;height:25px;text-align:center;font-weight:bold">Return Date</td>
        </tr>
        <?php $no=1; foreach($returnables as $key=>$row){ ?>
            <tr>
                <td valign="top" align="left" style="padding:5px;height: 25px;text-align:center;"><?=$no?></td>
                <td valign="top" align="left" style="padding:5px;height: 25px;text-align:center;border-left: 2px solid #666666;"><?=$row['products_name']?></td>
                <td valign="top" align="right" style="padding:5px;height: 25px;text-align:center;border-left: 2px solid #666666;border-right: 2px solid #666666;"><?=$row['qty']?></td>
                <td valign="top" align="right" style="padding:5px;height: 25px;text-align:center;border-right: 2px solid #666666;"><?=$row['person']?></td>
                <td valign="top" align="right" style="padding:5px;text-align:center;height: 25px;"><?=date('d/m/Y',strtotime($row['return_date']))?></td>
            </tr>
            <?php $no++;} }?>
        
        
<!--         
        
        
        <tr>
          <td colspan="5" valign="middle" style="padding:5px;background: #d1d8dd;">Totals</td>
        </tr> -->
        <tr style="">
          <td colspan="2" style="padding:5px;border-top: 2px solid #666666;font-weight: bold;height: 25px;" align="left">Indent by : <?=$User?></td>
          <td colspan="1" style="padding:5px;border-top: 2px solid #666666;font-weight: bold;height: 25px;" align="left"><img src="<?=base_url('public/signature.jpg')?>" style="height:90px"><br>Approved by :  <?=$approve_by?></td>
          <td colspan="2" style="padding:5px;border-top: 2px solid #666666;font-weight: bold;height: 25px;padding-right: 8%;" align="right">Issue by : <?=$Issue_by?></td>
        </tr>
        
      </tbody>
    </table>
    </div>
  </center>

</body>

</html>