<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />
    <title>Hindiko Solution </title>

    <style>
    .tbodystyle {
        font-size: 16px;
        font-weight: bolder;
    }

    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td,
    th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
    </style>
</head>

<body style="background-color:#fff;">
    <div class="right_col" role="main">
        <!-- top tiles -->
        <div class="row">
            <div class="" style="border:1px solid;">
                <div class="">
                    <div class="x_content">
                        <!-- start form for validation -->
                        <table class="table table-striped table-bordered" style="width:100%">
                            <!-- <thead>
                                <tr>
                                    <th colspan="5" style="text-align:right"><a style=""
                                            href="<?//=base_url('Manage-store');?>" class="btn btn-primary"> Back To
                                            Store</a></th>
                                </tr>
                            </thead> -->
                            <thead>
                                <tr>
                                    <th colspan="5" class="thListHeading"
                                        style="text-align:center;background-color:#cccc00 !important; ">
                                        <p><strong style="font-size:22px;">Stock In Material Report</strong>
                                        <p>
                                    </th>

                                </tr>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Qty</th>
                                    <th>Recieved Qty</th>
                                    <th>Rejected Qty</th>
                                    <th>Not RecievedQty</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php

                
                            foreach($products as $key=>$data){?>
                                <tr>
                                    <td><?=$key?></td>
                                    <td><?=$data->qty?></td>
                                    <td><?=$data->rcqty?></td>
                                    <td><?=$data->rjqty?></td>
                                    <td><?=$data->nrqty?></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>