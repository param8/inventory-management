<div class="page-body">
  <div class="container-fluid">
    <div class="page-title">
      <div class="row">
        <div class="col-6">
          <h3><?=$title?></h3>
        </div>
        <div class="col-6">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?=base_url('Dashboard')?>">
                <svg class="stroke-icon">
                  <use href="<?=base_url('public/assets/svg/icon-sprite.svg#stroke-home')?>"></use>
                </svg></a></li>
            <li class="breadcrumb-item">Users</li>
            <li class="breadcrumb-item active"><?=$title?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>

  <div class="container basic_table">
    <div class="card">
      <div class="card-header text-right">
        <?php if($permission[5]=='excel'){?>
        <button type="button" class="btn btn-success" data-toggle="modal" onclick="downloadExcel()">Download
          Excel</button>
        </button>
        <?php }?>
      </div>
      <div class="card-body">
        <form action="http://localhost/inventory-management/Bom/sendQuotation" id="sendQuote" method="post">
          <div class="table-responsive">
            <table class="table table-bordered" id="quotations">

            </table>
          </div>
        </form>
      </div>
    </div>
  </div>

</div>

<!-- Modal -->
<div class="modal fade" id="qoutationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Dis Approve Reason</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <div id="multiple_form" class="row">
          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr class="border-bottom-primary">
                  <th>S.No.</th>
                  <th nowrap>Dis Approve By</th>
                  <th nowrap class="text-danger">Dissaprove Reason</th>
                </tr>
              </thead>
              <tbody id="disappreason">

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="viewQuoteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">View Quotation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid basic_table">
          <div class="card">
            <form action="<?=base_url('Bom/sendQuotation')?>" id="sendQuote" method="post">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr class="border-bottom-primary">
                      <?php $Id =  $this->session->userdata('role_id');
                                    ?>

                      <!-- <th scope="col">Product Code</th> -->
                      <th scope="col">Part Name</th>
                      <th scope="col">Qty</th>
                      <th scope="col">Price/Unit</th>
                      <th scope="col">Total Price</th>

                    </tr>
                  </thead>
                  <tbody id="quote_products">
                  </tbody>
                </table>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
<script>
function downloadExcel() {
  var messageText = "Do You want download  Data in Excel ?";
  var confirmText = 'Yes, download it!';
  var message = "Excel download Successfully!";

  Swal.fire({
    title: 'Are you sure?',
    text: messageText,
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: confirmText
  }).then((result) => {
    if (result.isConfirmed) {

      location.href = "<?=base_url('Bom/export_qouteExcel')?>";
    }
  })
}

function onoffswitchspublish(svalue, qid) {
  if (svalue == 'Disapprove') {

    (async () => {
      const {
        value: reason
      } = await Swal.fire({
        title: 'Enter Reason for Disapprove Qoutation',
        input: 'text',
        inputLabel: 'Your Reason',
        inputPlaceholder: 'Enter Reason',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Disapprove it!',
        inputValidator: (value) => {
          if (!value) {
            return 'Please Enter Reason!'
          }
        }
      }).then((reason) => {

        if (reason.isConfirmed) {
          var reas = reason.value;
          $.ajax({
            url: '<?=base_url('Bom/quotation_approval')?>',
            type: 'POST',
            data: "id=" + qid + "&status=" + svalue + "&reason=" + reas,
            dataType: 'json',
            success: function(data) {
              if (data.status == 200) {
                toastr.success(data.message);
                location.reload();
              } else if (data.status == 302) {
                toastr.error(data.message);
              }
            }
          })
          location.reload();
        } else {
          location.reload();
        }
      })
    })()

  } else {

    Swal.fire({
      title: 'Do you want to Approve Quotation',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Continue'
    }).then((result) => {

      if (result.isConfirmed) {

        $.ajax({
          type: 'POST',
          url: '<?php echo base_url();?>Bom/quotation_approval',
          data: "id=" + qid + "&status=" + svalue,
          dataType: "html",
          success: function(data) {
            Swal.fire(
              'Status Change!',
              'Approved sucessfully.',
              'success'
            )
            location.reload();
          }

        })

      } else {

        location.reload();
      }

    })

  }
}

function get_tprice(price, productCode) {

  var qty = document.getElementById('qty_' + productCode).value;
  var totalprice = price * qty;
  document.getElementById('tprice_' + productCode).value = totalprice;
}

function viewQuoteModal(ID) {
  $('#viewQuoteModal').modal('show');
  var page_name = '<?=$title?>';
  $.ajax({
    url: '<?=base_url('Bom/get_quotations')?>',
    type: 'POST',
    data: {
      ID,
      page_name
    },
    success: function(data) {
      $('#quote_products').html(data);

    }
  });
}

$(document).ready(function() {
  //alert('hi');die;
  $.ajax({
    url: '<?=base_url('Bom/disappr_quotations')?>',
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      $('#diss_qoutes').html(data);
    },
    error: function() {}
  });
});


$('#creatUserModal').on('shown.bs.modal', function() {
  $('#myInput').trigger('focus')
})

$("form#sendQuote").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('Enquiry')?>";

        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});


$("form#editUserForm").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('User')?>";

        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});
</script>
<script>
$(document).ready(function() {
  //alert('hi');die;
  var bomId = <?=$bom_id?>;
  //alert(bomId);die;
  $.ajax({
    url: '<?=base_url('Bom/ajaxBomQuotations/')?>' + bomId,
    type: 'POST',
    data: bomId,
    success: function(data) {
      //alert(data);
      $('#quotations').html(data);
    },
    error: function() {}
  });
});
</script>
<script>
function viewReason(id) {
  $('#qoutationModal').modal('show');
  $.ajax({
    url: '<?=base_url('Bom/disappreason/')?>' + id,
    type: 'POST',
    data: id,
    success: function(data) {
      //alert(data);
      $('#disappreason').html(data);
    },
    error: function() {}
  });
}
</script>