<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title>Delivery Challan</title>
</head>

<body style="margin:0!important;padding:0!important">
  <center>
    <div style="width:100%;border: 2px solid #55714c;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style=" font-size:14px;">
        <tbody style="background-color: #fff;  padding-bottom: 20px">

          <tr>
            <td align="cenetr" colspan="5">
              <h1 style="text-align: center;    margin: 0;;">Delivery Challan</h1>
              <h3 style="text-align: center;    margin: 0;"><?=$dc_data->chllan_type?></h3>
            </td>
          </tr>

          <tr>
            <td colspan="5">
              <table style="width: 100%;">
                <tbody>
                  <tr>
                    <td valign="top" style="padding:5px; width: 70%; ">
                      <table style="width: 100%;">
                        <tr>
                          <td colspan="4">
                            <div class="" style="text-align: center;">
                              <h3 style="margin: 5px; font-weight: bold;">HINDIKO SOLUTION INDIA PVT LTD</h3>
                              <p style="margin: 5px; font-weight: bold;"><br><?=$location->address?></p>
                            </div>
                          </td>
                        </tr>
                        <tr class="row">
                          <td colspan="2"><b>Phone : <?=$siteinfo->site_contact?></b></td>
                          
                          <td colspan="2"><b>Email : <?=$siteinfo->site_email?></b></td>
                          
                        </tr>
                      </table>
                    </td>
                    <td valign="top" style="padding:5px; width: 30%;text-align: right;">
                      <img src="<?=base_url($siteinfo->site_logo)?>" alt="logo" style="width: 100%;height: 66px;">
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>

        </tbody>
      </table>
      <table width="100% " border="0" cellspacing="0" cellpadding="0"
        style="    border-top: 3px solid #000;       font-weight: 500; padding-top: 20px;margin-top:0px; font-size:14px">
        <tbody>
          <tr>
            <td colspan=""  style="padding:5px;text-align:left;width:15%;"><b>Bill To:</b></td>
            <td colspan=""  style="padding:5px;text-align:left;width:35%;"><?=empty($dc_data->vendor) ? $dc_data->bill_to : $dc_data->vendor?></td>
            <td colspan=""  style="padding:5px;text-align:left;width:35%;"><b>Delivery Challan No. :</b>
            </td>
            <td colspan="" style="padding:5px;text-align:left;width:15%;"><?=$dc_data->challan_no?></td>
          </tr>
          <tr>
            <td colspan=""  style="padding:5px;text-align:left;width:35%;">DC Date and Time :</td>
            <td colspan=""  style="padding:5px;text-align:left;width:15%;"> <?=date('d-m-Y H:i:s',strtotime($dc_data->cretaed_at))?></td>
            <td colspan=""  style="padding:5px;text-align:left;width:15%;">PO Number : </td>
            <td><?=$dc_data->po_number?></td>
          </tr>
          <tr>
            <td colspan="" style="padding:5px;text-align:left;width:15%;"> Address:</td>
            <td colspan="" style="padding:5px;text-align:left;width:35%;"><?=empty($dc_data->vendor_address)? $dc_data->address : $dc_data->vendor_address?>
            </td>
            <td colspan="" style="padding:5px;text-align:left;width:35%;">Mode of Delivery :</td>
            <td colspan="" style="padding:5px;text-align:left;width:15%;">
              <?=$dc_data->delivery_mode?></td>
          </tr>
          <tr>
            <!--<td colspan="" style="padding:5px;text-align:left;width:15%;">State :</td>-->
            <!--<td colspan="" style="padding:5px;text-align:left;width:35%;"> <?=$dc_data->state?>-->
            <!--</td>-->
            <td colspan="" style="padding:5px;text-align:left;width:35%;">Vehicle Number :</td>
            <td colspan="" style="padding:5px;text-align:left;width:15%;"><?=$dc_data->vehicle_number?></td>
          </tr>
          <tr>
            <td colspan="" style="padding:5px;text-align:left;width:15%;">GSTN Number :</td>
            <td colspan="" valign="middle" style="padding:5px;text-align:left;width:35%;"><?=empty($dc_data->gst)?$dc_data->gstn:$dc_data->gst?></td>
            <td colspan="" valign="middle" style="padding:5px;text-align:left;width:35%;">Place of Supply :</td>
            <td colspan="" valign="middle" style="padding:5px;text-align:left;width:15%;"><?=$dc_data->supply_place?>
            </td>
          </tr>
          <tr>
            <td colspan=""  style="padding:5px;text-align:left;width:15%;">Phone : </td>
            <td colspan=""  style="padding:5px;text-align:left;width:15%;"><?=empty($dc_data->vendor_mobile)?$dc_data->mobile :$dc_data->vendor_mobile?></td>
            <td colspan="2" style="padding:5px;text-align:left;width:35%;">Email :  <?=empty($dc_data->vendor_email)?$dc_data->email:$dc_data->vendor_email?></td>
            
            
          </tr>

        </tbody>
      </table>
      <br>
      <table style="width: 100%;padding-bottom: 10px; text-align: center;border-top: 3px solid #000;border-spacing: 0;">
        <thead style="    background-color: #c0d0c2;">
          <tr>
            <th style="width: 10%;">No</th>
            <th style="width: 26%;">Product Description</th>
            <th style="width: 12%;">HSN Codes</th>
            <th style="width: 9%;">Qty</th>
            <th style="width: 9%;">Type </th>
            <th style="width: 12%;">Unit Rate</th>
            <th style="width: 12%;">GST(%)</th>
            <th style="width: 20%;">Sub total (Without Tax)</th>
          </tr>
        </thead>
        <tbody>

          <?php $dc_subtotal = 0; $total = 0; $tax_total = 0; $grand_total = 0; $sno=1; $tax_price = 0;$tax_cal= 0;
          foreach(json_decode($dc_data->products) as $key=>$data){
            $product_detail = $this->Product_model->get_product(array('products.product_code'=>$data->product_code));
            //$this->product_model->get_product(array('products.id'=>$data->product_code));
            //print_r($product_detail);die;
              $dc_subtotal+= floatVal($data->qty)*floatVal($data->price);
              $gst = empty($data->tax)? 0 : $data->tax;
              $tax_price = $dc_subtotal * $gst / 100;
              $tax_total+= $tax_price;
              $tax_cal+= $tax_price;
              $total+= $dc_subtotal;
            ?>
          <tr>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"><?=$sno?></td>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"><?=$product_detail->part_name ?> (<?=$product_detail->specification?>)</td>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"><?=empty($data->hsn)?'-':$data->hsn?></td>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"><?=$data->qty?></td>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"><?=$data->unit_type?></td>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"><?=$data->price?></td>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"><?=$data->tax?></td>
            <td style="border-top: 1px solid #000; border-right: 1px solid #000;"><?=$dc_subtotal?></td>
          </tr>
          <?php $sno++;$dc_subtotal = 0;}
          $grand_total= floatVal($total) + floatVal($tax_total) + floatVal($dc_data->ship_charge);
          $number = $grand_total;
          $no = floor($number);
          $point = round($number - $no, 2) * 100;
          $hundred = null;
          $digits_1 = strlen($no);
          $i = 0;
          $str = array();
          $words = array('0' => '', '1' => 'one', '2' => 'two',
           '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
           '7' => 'seven', '8' => 'eight', '9' => 'nine',
           '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
           '13' => 'thirteen', '14' => 'fourteen',
           '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
           '18' => 'eighteen', '19' =>'nineteen', '20' => 'twenty',
           '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
           '60' => 'sixty', '70' => 'seventy',
           '80' => 'eighty', '90' => 'ninety');
          $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
          while ($i < $digits_1) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += ($divider == 10) ? 1 : 2;
            if ($number) {
               $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
               $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
               $str [] = ($number < 21) ? $words[$number] .
                   " " . $digits[$counter] . $plural . " " . $hundred
                   :
                   $words[floor($number / 10) * 10]
                   . " " . $words[$number % 10] . " "
                   . $digits[$counter] . $plural . " " . $hundred;
            } else $str[] = null;
          }
          $str = array_reverse($str);
          $result = implode('', $str);
          $points = ($point) ?
           "." . $words[$point / 10] . " " . 
                 $words[$point = $point % 10] : '';
          // echo $result ;?>
        

        </tbody>
      </table>
      <table  style="width: 100%;padding-bottom: 10px; text-align: center;border-top: 3px solid #000;border-spacing: 0;">
        <tbody>
        <tr>
            <td colspan="8" style="    background-color: #c0d0c2;"></td>

          </tr>
          <tr style="border-bottom: 2px solid black;">
            <td colspan="6" style=" text-align: right;">Delivery Challan Subtotal</td>
            <td colspan="2" style=" text-align: right;">Rs. <?=$total?></td>
          </tr>
          
          <?php 
              $gstdigit = substr($dc_data->gstn,0,2);              
              $companyGst = substr('09AAFCH5944N1ZY',0,2);
              if($companyGst == $gstdigit){
                ?>
          <tr>
            <td colspan="6" style=" text-align: right;">CGST :</td>
            <td colspan="2" style=" text-align: right;">Rs. <?=$tax_cal/2?></td>
          </tr>
          <tr>
            <td colspan="6" style=" text-align: right;">SGST :</td>
            <td colspan="2" style=" text-align: right;">Rs. <?=$tax_cal/2?></td>
          </tr>
          <?php               
              }else if($gst != $gstdigit && !empty($gst)){?>
                <tr>
                <td colspan="6" style=" text-align: right;">IGST :</td>
                <td colspan="2" style=" text-align: right;">Rs. <?=$tax_cal?></td>
              </tr>
              
                <?php }
            ?>
          <tr>
            
            <td colspan="4" style=" text-align: right;">SAC Code <?=empty($dc_data->sac)?'-':$dc_data->sac?></td>
           
            <td colspan="4" style=" text-align: right;">Shipping charges <?=empty($dc_data->ship_charge)?0:$dc_data->ship_charge ?></td>
            
          </tr>

          <tr>

            <td colspan="8" style=" margin-top: 5px;   background-color: #c0d0c2;"></td>

          </tr>
          <tr style=" background-color: #c0d0c2;">
            <td colspan="6" style=" text-align: right;">Total</td>
            <td colspan="2"style="border-bottom: 2px solid #c0d0c2;text-align: right;">Rs.<?=$grand_total?></td>
          </tr>
          <tr>
            <td  style="text-align: left; text-transform:capitalize" colspan="8"><span style="padding-left: 20px; color: red;">Total Amount in Words </span>- <?=$result?> Rupees Only.</td>
            
          </tr>
          <tr>
            <td colspan="4 " style="text-align: left; padding-left: 20px;">
              <p style="margin-bottom: 0;"><b>Terms and Conditions</b></p>
              <p style="margin-bottom: 0;"><?=$dc_data->terms?></p>
            </td>
            <td colspan="4"><div style="display: inline-block;text-align: center;">
            <?php if($dc_data->admin_approval == 1){?>
                <img src="<?=base_url('public/signature.jpg')?>" style="height:90px"><?php }?><br>
                for <b>Hindiko Solution India Pvt Ltd</b>
                <br>(Authorized Signature)
              </div></td>
          </tr>
          <tr>
            <td colspan="8" style="text-align: right; padding-right:10px;">
              
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </center>

</body>

</html>