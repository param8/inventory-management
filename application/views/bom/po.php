
<div class="page-body">
          <div class="container-fluid">
            <div class="page-title">
              <div class="row">
                <div class="col-6">
                  <h3><?=$title?></h3>
                </div>
                <div class="col-6">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?=base_url('Dashboard')?>">                                       
                        <svg class="stroke-icon">
                          <use href="<?=base_url('public/assets/svg/icon-sprite.svg#stroke-home')?>"></use>
                        </svg></a></li>
                    <li class="breadcrumb-item">Users</li>
                    <li class="breadcrumb-item active"><?=$title?></li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid starts-->
          <div class="container-fluid">
            <div class="row">
              <!-- Individual column searching (text inputs) Starts-->
              <div class="col-sm-12">
                <div class="card">
                  <div class="card-header">
                  <div class="col-md-3">
                        <label for="">Location</label>
                        <select class="form-control" onchange="locationFilter(this.value)">
                        <option value="">Select Location</option>
                        <?php $user_location = explode(',',$this->session->userdata('location'));
                        foreach($locations as $location){
                          if(in_array($location->id,$user_location)){?>
                        <option value="<?=$location->id?>" <?=($this->session->userdata('poLocationID') == $location->id)?'selected':''?>><?=$location->location?></option>
                        <?php }}?>
                        </select>
                        <?php if(!empty($this->session->userdata('poLocationID'))){?>
                          <a href="javacript:void(0);" class="text-danger" onclick="resetFilter(this.value)">Reset</a>
                          <?php }?>
                        </div>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive product-table">
                      <table class="display" id="qoutationTable">
                        <thead>
                          <tr>
                            <th>SNo</th>
                            <!-- <th>Name</th> -->
                            <th>Project Name</th>
                            <?php if($this->session->userdata('role_id')!=4){ ?>
                            <th>Vendor Name</th>
                            <th>Location</th>
                            <?php }?>
                            <th> Date</th>
                            <th> Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                    
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Individual column searching (text inputs) Ends-->
            </div>
          </div>
          <!-- Container-fluid Ends-->
        </div>

       <!-- Modal -->
<div class="modal fade" id="poModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog  modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Send PO</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
         <div class="container-fluid basic_table">
                <div class="card container">
                    <form action="<?=base_url('Bom/update_po')?>" id="sendpo" method="post">
                        <input type="hidden" name="qID" id="qID">
                        <input type="hidden" name="po" id="po" value="1">
                        
                        <label for="">Terms & Conditions <span class="text-danger">*</span></label>
                        <textarea name="terms" id="terms" cols="30" rows="10"></textarea>
                       
                        <button type="submit" class="btn btn-primary m-2 text-right" name="submit">Send</button>
                    </form>
                </div>
            </div>
            </div>
      </div>
   </div>
</div>
<div class="modal fade" id="viewPoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog  modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">View PO</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
         <div class="container-fluid basic_table">
                <div class="card">
                    <form action="<?=base_url('Bom/sendQuotation')?>" id="sendQuote" method="post">
                        <div class="table-responsive" style="max-height:450px">
                            <table class="table">
                            <thead>
                                <tr class="border-bottom-primary">
                                <?php $Id =  $this->session->userdata('role_id');
                                    ?>
                                   
                                <!-- <th scope="col">Product Code</th> -->
                                <th nowrap scope="col">Part Name</th>
                                <th scope="col">Specification</th>
                                <th nowrap scope="col">Make</th>    
                                <th nowrap scope="col">Qty</th>    
                                <th nowrap scope="col">Price/Unit</th>    
                                <th nowrap scope="col">Total Price</th>
                                <th nowrap scope="col">Tax (%)</th>
                                <th nowrap scope="col">Tax Amount</th>
                                <th nowrap scope="col">Grand Total</th>
                                
                                </tr>
                            </thead>
                                <tbody id="po_products">
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
            </div>
      </div>
   </div>
</div>

</div>
<script>
    function CompleteOrder(id){
        var order_status = 1;
        Swal.fire({
            title: 'Do you want to Complete Order',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Create'
            }).then((result) => {
                
            if (result.isConfirmed) {        
            $.ajax({
            type:'POST',
            url:'<?php echo base_url();?>Bom/update_order',
            data:"id="+ id+"&order_status="+order_status,
            dataType: "html",
            success:function(data)
            {    
                Swal.fire(
                    'Status Change!',
                    'Order Completed successfully.',
                    'success'
                        )
                    location.reload();
            }
        
            })
        
            }else{        
                location.reload();
            }    
        });
    }
    function onoffswitchspublish(qid)
    {  var po = 1;
        Swal.fire({
            title: 'Do you want to Create PO',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Create'
            }).then((result) => {
                
        if (result.isConfirmed) {
    
        $.ajax({
        type:'POST',
        url:'<?php echo base_url();?>Bom/update_po',
        data:"id="+ qid+"&po="+po,
        dataType: "html",
        success:function(data)
        {    
            Swal.fire(
                'Status Change!',
                'PO Created successfully.',
                'success'
                    )
                location.reload();
        }
    
        })
    
        }else{
    
            location.reload();
        }
    
        })
    }
    function get_tprice(price,productCode){
    
        var qty = document.getElementById('qty_'+productCode).value;
        var totalprice = price * qty;
        document.getElementById('tprice_'+productCode).value = totalprice;
    }
    function viewQuoteModal(ID){
        $('#viewQuoteModal').modal('show');
        $.ajax({
            url: '<?=base_url('Bom/get_quotations')?>',
            type: 'POST',
            data: {ID},
            success: function(data) {
                $('#quote_products').html(data);        
                
            }
        });
    }

    function viewPoModal(id){
        $('#viewPoModal').modal('show');
        $.ajax({
            url: '<?=base_url('Bom/get_po')?>',
            type: 'POST',
            data: {id},
            success: function(data) {
                $('#po_products').html(data);        
                
            }
        });
    }

    function poModal(id){
        $('#poModal').modal('show');
        $('#qID').val(id);
    }

    $("form#sendpo").submit(function(e) {
    //alert('fgdfgfd');
    $(':input[type="submit"]').prop('disabled', true);
    e.preventDefault();    
    var formData = new FormData(this);
    $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function (data) {
    if(data.status==200) {
    toastr.success(data.message);
    $(':input[type="submit"]').prop('disabled', false);
    setTimeout(function(){

        location.href="<?=base_url('PO')?>"; 	
        
    }, 1000) 
    
    }else if(data.status==403) {
    toastr.error(data.message);

    $(':input[type="submit"]').prop('disabled', false);
    }else{
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
    }
    },
    error: function(){} 
    });
    });


    $(document).ready(function() {
        // $('#example').DataTable();
        // } );
        var role = '<?=$title?>';
        var dataTable = $('#qoutationTable').DataTable({
            // "processing": true,
            // "serverSide": true,
            // buttons: [{
            //     extend: 'excelHtml5',
            //     text: 'Download Excel'
            // }],
            "order": [],
            "ajax": {
                url: "<?=base_url('Bom/ajaxPOQuotations')?>",
                type: "POST",
                data:{role}
            },
            // "length":10,
            // "columnDefs": [{
            //     "targets": [0],
            //     "orderable": true,
            // }, ],
        });
    });


    $('#creatUserModal').on('shown.bs.modal', function () {
    $('#myInput').trigger('focus')
    })

    $("form#sendQuote").submit(function(e) {
    //alert('fgdfgfd');
    $(':input[type="submit"]').prop('disabled', true);
    e.preventDefault();    
    var formData = new FormData(this);
    $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function (data) {
    if(data.status==200) {
    toastr.success(data.message);
    $(':input[type="submit"]').prop('disabled', false);
    setTimeout(function(){

        location.href="<?=base_url('Enquiry')?>"; 	
        
    }, 1000) 
    
    }else if(data.status==403) {
    toastr.error(data.message);

    $(':input[type="submit"]').prop('disabled', false);
    }else{
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
    }
    },
    error: function(){} 
    });
    });


    $("form#editUserForm").submit(function(e) {
    //alert('fgdfgfd');
    $(':input[type="submit"]').prop('disabled', true);
    e.preventDefault();    
    var formData = new FormData(this);
    $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function (data) {
    if(data.status==200) {
    toastr.success(data.message);
    $(':input[type="submit"]').prop('disabled', false);
    setTimeout(function(){

        location.href="<?=base_url('User')?>"; 	
        
    }, 1000) 
    
    }else if(data.status==403) {
    toastr.error(data.message);

    $(':input[type="submit"]').prop('disabled', false);
    }else{
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
    }
    },
    error: function(){} 
    });
    });

    $('#terms').summernote({
            height:200,
    });

    function locationFilter(id) {
    
    $.ajax({
      url: "<?=base_url('Bom/poLocationFilter')?>",
      type: 'POST',
      data: {
        id
      },
      success: function(data) {
        location.reload();
      },
      error: function() {}
    });
  }
  
  function resetFilter() {
    $.ajax({
      url: "<?=base_url('Bom/resetPoLocationFilter')?>",
      type: "POST",
      data: {},
  
      success: function(data) {
        location.reload();
      },
      error: function() {}
    });
  }
</script>