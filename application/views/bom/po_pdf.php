
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title><?=$title?></title>
  <style>
    input:focus{
      outline: none;
    }
  </style>
</head>

<body>
  <center>
    <div style="width:100%; ">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style=" margin-top:0px; font-size:14px">
      <tbody>
        <tr>
          <td colspan="5">
            <table style=" width: 100% ;margin: auto; border-spacing: 0;border: 2px solid #000;">
              <tbody >
                <tr>
                  <td > <img src="<?=base_url($siteinfo->site_logo)?>" alt="" style="width: 200px;"></td>
                  <td style="font-weight: 700;font-size: 22px;" colspan="3">PURCHASE ORDER</td>
                  <td style="margin-top:10px; font-weight: 700;font-size: 16px;" colspan="4"> 
                    <p>P O - <?=$po_id?></p>
                    <p>  Date - <?=$date?></p>
                  </td>

                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                  <td colspan="4" style="border: 1px solid #000; vertical-align: top; padding: 10px;max-width:820px"><h4>Invoice To</h4><?=$siteinfo->site_name?><br><?=$location->address?><br>Contact : <?=$siteinfo->site_contact?><br>Email : <?=$siteinfo->site_email?><br><?php if(!empty($location->gstn)){?> Gstn : <?=$location->gstn ?><?php }?></td>
                  <td colspan="5" style="border: 1px solid #000;">
                    <table style="width: 100%;border-spacing: 0;">
                      <!--<tr>-->
                      <!--  <td style=" line-height: 43px; border-bottom: 1px solid #000;padding: 0 6px;"><h4>Quotation refrance - </h4></td>-->
                      
                      <!--</tr>-->
                      <tr>
                      
                        <td style=""><b>Terms of Payment:-</b><br><?=$terms?></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="4" style="border: 1px solid #000; vertical-align: top; padding: 10px;"><h4>Supplier (Bill From)</h4>ID: <?=$vendors->vendor_code?><br>Name: <?=$vendors->name?><br>Contact : <?=$vendors->phone?><br>Email : <?=$vendors->email?><br>Gstn : <?=$vendors->gst?></td>
                 
                  <td colspan="5" style="border: 1px solid #000;">
                 <table style="width: 100%;border-spacing: 0;">
                 <tr>
                    <td style=" line-height: 43px;border-bottom: 1px solid #000;  padding: 0 6px;"><b>Project - </b><?=$project->name?></td>
                  </tr>
                  <tr>
                    <td><b>Delivery terms:-</b><p><?=$remark?></p></td>
                   
                  </tr>
                 
                  <!--<tr>-->
                  <!--  <td style=" line-height: 43px;  padding: 00 6px;">Contact details:-</td>-->
                  <!--</tr>-->
                 </table>
                  </td>
                </tr>


            
                <tr style="width: 100%;">
                  <td colspan="10"  style="width: 100%;">
                    <table style="width: 100%;border-spacing: 0;">
                      <thead>
                        <tr  style="width: 100%;border-spacing: 0;">
                        <th nowrap ># </th>
                        <th nowrap>ITEM</th>
                        <th nowrap>DESCRIPTION</th>
                        <th >HSN/SAC </th>
                        <th >Qty</th>
                        <th >Unit</th>
                        <th >Rate</th>
                        <th >Amount</th>
                        <th >Tax</th>
                        <th nowrap>Sub Total</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?=$products?>
                      </tbody>
                    </table>
                  </td>
                  
                </tr>
                
                <tr></tr>
               
               
                <tr>
                  <td valign="top" align=" " colspan="3" style="padding:5px;height: 25px;  border: 1px solid #000;text-align: center;padding-right: 20px; font-weight: 700;">Amount Chargeable-</td>
                  <td valign="top" colspan="6" align=" " style="padding:5px;height: 25px; border: 1px solid #000; text-align:right"><?=$total?> (Rupees)</td>
                </tr>
                <tr>
                  <td valign="top" align=" " colspan="3" style="padding:5px 10px;height: 25px;  border: 1px solid #000; padding-right: 20px; font-weight: 700;">
                    T&C:
                    <br>
                    1. Fright Charge (As Per Actual)
                  </td>
                  <td valign="top" colspan="6" align=" " style="padding:5px;height: 25px; border: 1px solid #000;">
                  <p style="margin-bottom: 0px;text-align:center"><img src="<?=base_url('public/signature.jpg')?>" style="height:90px"><br>for <b> HINDIKO SOLUTION INDIA PVT LTD</b></p>
                  <p style="text-align: end; margin: 00; text-align: center;">(Authorised Signatory)</p>
                  </td>
                </tr>

              </tbody>
            </table>
             
          </td>
        </tr>
      </tbody>
    </table>
    </div>
  </center>

</body>

</html>