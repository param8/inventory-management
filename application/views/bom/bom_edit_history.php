
<div class="page-body">
          <div class="container-fluid">
            <div class="page-title">
              <div class="row">
                <div class="col-6">
                  <h3><?=$title?></h3>
                </div>
                <div class="col-6">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?=base_url('Dashboard')?>">                                       
                        <svg class="stroke-icon">
                          <use href="<?=base_url('public/assets/svg/icon-sprite.svg#stroke-home')?>"></use>
                        </svg></a></li>
                    <li class="breadcrumb-item">Users</li>
                    <li class="breadcrumb-item active"><?=$title?></li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid starts-->
          <div class="container-fluid">
            <div class="row">
              <!-- Individual column searching (text inputs) Starts-->
              <div class="col-sm-12">
                <div class="card">
                  <!-- <div class="card-header">
                   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#creatUserModal" data-whatever="@fat">Create User</button>
                    </button>
                  </div> -->
                  <div class="card-body">
                    <div class="table-responsive product-table">
                      <table class="display" id="qoutationTable">
                        <thead>
                          <tr>
                            <th>SNo</th>
                            <th>Project Name</th>
                            <th>Bom Name</th>
                            <th>Products</th>
                            <th>Update By</th>                           
                            <th>Date</th>
                          </tr>
                        </thead>
                    
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Individual column searching (text inputs) Ends-->
            </div>
          </div>
          <!-- Container-fluid Ends-->
        </div>

       <!-- Modal -->
       <div class="modal fade" id="oldproductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog  modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">View Old Products</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
         <div class="container-fluid basic_table">
                <div class="card">
                        <div class="table-responsive">
                            <table class="table">
                            <thead>
                                <tr class="border-bottom-primary">
                                <?php $Id =  $this->session->userdata('role_id');
                                    ?>
                                   
                                <!-- <th scope="col">Product Code</th> -->
                                <th scope="col">Part Name</th>
                                <th scope="col">Product Code</th>
                                <th scope="col">Qty</th>
                                
                                </tr>
                            </thead>
                                <tbody id="old_products">
                                </tbody>
                            </table>
                        </div>
            </div>
            </div>
            </div>
      </div>
   </div>
</div>
<div class="modal fade" id="viewQuoteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog  modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">View Updated Products</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
         <div class="container-fluid basic_table">
                <div class="card">
                    <form action="<?=base_url('Bom/sendQuotation')?>" id="sendQuote" method="post">
                        <div class="table-responsive">
                            <table class="table">
                            <thead>
                                <tr class="border-bottom-primary">
                                <?php $Id =  $this->session->userdata('role_id');
                                    ?>
                                   
                                <!-- <th scope="col">Product Code</th> -->
                                <th scope="col">Part Name</th>
                                <th scope="col">Product Code</th>
                                <th scope="col">Qty</th>
                                
                                </tr>
                            </thead>
                                <tbody id="quote_products">
                                </tbody>
                            </table>
                        </div>
                    </form>
            </div>
            </div>
            </div>
      </div>
   </div>
</div>

</div>
        <script>
function onoffswitchspublish(qid)
  {
    
      //alert(qid);die;
       if($('#primaryswitchStatus').prop('checked'))
      {
        status = 1;
        //id = $('#primaryswitchStatus').val();
      }
      else
      {
        status = 0;
        //id = $('#primaryswitchStatus').val();         
      }
  
          if(status == 0){     
      Swal.fire({
          title: 'Do you want to Disapprove Quotation',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Continue '
          }).then((result) => {
              
      if (result.isConfirmed) {
  
      $.ajax({
      type:'POST',
      url:'<?php echo base_url();?>Bom/quotation_approval',
      data:"id="+ qid+"&status="+status,
      dataType: "html",
      success:function(data)
      {    
          Swal.fire(
              'Status Change!',
             'Dis Approved successfully.',
              'success'
                  )
              location.reload();
       }
  
       })
  
      }else{
  
          location.reload();
      }
  
      })
  }else{
  
      Swal.fire({
          title: 'Do you want to Approve Quotation',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Continue'
          }).then((result) => {
              
      if (result.isConfirmed) {
  
      $.ajax({
      type:'POST',
      url:'<?php echo base_url();?>Bom/quotation_approval',
      data:"id="+ qid+"&status="+status,
      dataType: "html",
      success:function(data)
      {    
          Swal.fire(
              'Status Change!',
             'Approved sucessfully.',
              'success'
                  )
              location.reload();
       }
  
       })
  
      }else{
  
          location.reload();
      }
  
      })
  
  }}
function get_tprice(price,productCode){
  
    var qty = document.getElementById('qty_'+productCode).value;
    var totalprice = price * qty;
    document.getElementById('tprice_'+productCode).value = totalprice;
  }
function viewQuoteModal(ID){
    $('#viewQuoteModal').modal('show');
    $.ajax({
        url: '<?=base_url('Bom/get_editBomproducts')?>',
        type: 'POST',
        data: {ID},
        success: function(data) {
            $('#quote_products').html(data);        
               
        }
    });
   }
   function oldproductModal(ID){
    $('#oldproductModal').modal('show');
    $.ajax({
        url: '<?=base_url('Bom/get_oldBomproducts')?>',
        type: 'POST',
        data: {ID},
        success: function(data) {
            $('#old_products').html(data);        
               
        }
    });
   }
  $(document).ready(function() {
      // $('#example').DataTable();
      // } );
      var role = '<?=$title?>';
      var dataTable = $('#qoutationTable').DataTable({
          "processing": true,
          "serverSide": true,
          buttons: [{
              extend: 'excelHtml5',
              text: 'Download Excel'
          }],
          "order": [],
          "ajax": {
              url: "<?=base_url('Bom/ajaxEditBomHistory')?>",
              type: "POST",
              data:{role}
          },
          "columnDefs": [{
              "targets": [0],
              "orderable": false,
          }, ],
      });
  });


  $('#creatUserModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})

$("form#sendQuote").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  toastr.success(data.message);
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){

     location.href="<?=base_url('Enquiry')?>"; 	
    
  }, 1000) 
  
  }else if(data.status==403) {
  toastr.error(data.message);

  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastr.error(data.message);
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });


  $("form#editUserForm").submit(function(e) {
  //alert('fgdfgfd');
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  toastr.success(data.message);
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){

     location.href="<?=base_url('User')?>"; 	
    
  }, 1000) 
  
  }else if(data.status==403) {
  toastr.error(data.message);

  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastr.error(data.message);
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });

 
</script>