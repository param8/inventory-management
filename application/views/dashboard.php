
 
      <!-- Page Body Start-->
      
        <!-- Page Sidebar Ends-->
        <div class="page-body">
          <div class="container-fluid">        
            <div class="page-title">
              <div class="row">
                <div class="col-6">
                  <h4><?=$this->session->userdata('user_type');?></h4>
                </div>
                <div class="col-6">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?=base_url()?>">                                       
                        <svg class="stroke-icon">
                          <use href="<?=base_url('public/assets/svg/icon-sprite.svg#stroke-home')?>"></use>
                        </svg></a></li>
                    <li class="breadcrumb-item">Dashboard</li>
                    <li class="breadcrumb-item active"><?=$this->session->userdata('user_type');?></li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid starts-->
          <div class="container-fluid">
            <div class="row widget-grid">
              <div class="col-xxl-4 col-sm-6 box-col-6"> 
                <div class="card profile-box">
                  <div class="card-body">
                    <div class="media media-wrapper justify-content-between">
                      <div class="media-body"> 
                        <div class="greeting-user">
                          <h4 class="f-w-600">Welcome to <?=$this->session->userdata('user_type');?> Dashboard</h4>
                          <p>Here whats happing in your account today</p>
                          <div class="whatsnew-btn"><a class="btn btn-outline-white">Whats New !</a></div>
                        </div>
                      </div>
                      <div>  
                        <div class="clockbox">
                          <svg id="clock" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 600 600">
                            <g id="face">
                              <circle class="circle" cx="300" cy="300" r="253.9"></circle>
                              <path class="hour-marks" d="M300.5 94V61M506 300.5h32M300.5 506v33M94 300.5H60M411.3 107.8l7.9-13.8M493 190.2l13-7.4M492.1 411.4l16.5 9.5M411 492.3l8.9 15.3M189 492.3l-9.2 15.9M107.7 411L93 419.5M107.5 189.3l-17.1-9.9M188.1 108.2l-9-15.6"></path>
                              <circle class="mid-circle" cx="300" cy="300" r="16.2"></circle>
                            </g>
                            <g id="hour">
                              <path class="hour-hand" d="M300.5 298V142"></path>
                              <circle class="sizing-box" cx="300" cy="300" r="253.9"></circle>
                            </g>
                            <g id="minute">
                              <path class="minute-hand" d="M300.5 298V67">   </path>
                              <circle class="sizing-box" cx="300" cy="300" r="253.9"></circle>
                            </g>
                            <g id="second">
                              <path class="second-hand" d="M300.5 350V55"></path>
                              <circle class="sizing-box" cx="300" cy="300" r="253.9"></circle>
                            </g>
                          </svg>
                        </div>
                        <div class="badge f-10 p-0" id="txt"></div>
                      </div>
                    </div>
                    <div class="cartoon"><img class="img-fluid" src="<?=base_url('public/assets/images/dashboard/cartoon.svg')?>" alt="vector women with leptop"></div>
                  </div>
                </div>
              </div>
              <div class="col-xxl-auto col-xl-3 col-sm-6 box-col-6"> 
              <?php if($_SESSION['role_id']==1){?>
                <div class="row"> 
                  <div class="col-xl-12"> 
                    <div class="card widget-1">
                      <div class="card-body"> 
                        <div class="widget-content">
                          <div class="widget-round secondary">
                            <div class="bg-round">
                              <svg class="svg-fill">                                
                              <use href="<?=base_url('public/assets/svg/icon-sprite.svg#fill-user')?>"> </use>
                              </svg>
                              <svg class="half-circle svg-fill">
                                <use href="<?=base_url('public/assets/svg/icon-sprite.svg#halfcircle')?>"></use>
                              </svg>
                            </div>
                          </div>
                          <div> 
                            <a href="<?=base_url('vendor');?>" class="text-dark"><h4><?=$total_vendor?></h4><span class="f-light">Total Vendors</span></a>
                          </div>
                        </div>
                        <div class="font-secondary f-w-500"><i class="icon-arrow-up icon-rotate me-1"></i><span>+50%</span></div>
                      </div>
                    </div>
                    </div>
                    <div class="col-xl-12"> 
                      <div class="card widget-1">
                        <div class="card-body"> 
                          <div class="widget-content">
                            <div class="widget-round primary">
                              <div class="bg-round">
                                <svg class="svg-fill">
                                  <use href="<?=base_url('public/assets/svg/icon-sprite.svg#tag')?>"> </use>
                                </svg>
                                <svg class="half-circle svg-fill">
                                  <use href="<?=base_url('public/assets/svg/icon-sprite.svg#halfcircle')?>"></use>
                                </svg>
                              </div>
                            </div>
                            <div> 
                              <a href="<?=base_url('product')?>" class="text-dark"><h4><?=$total_product?></h4><span class="f-light">Total Products</span></a>
                            </div>
                          </div>
                          <div class="font-primary f-w-500"><i class="icon-arrow-up icon-rotate me-1"></i><span>+70%</span></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <div class="col-xxl-auto col-xl-3 col-sm-6 box-col-6"> 
                <div class="row">                   
                  <div class="col-xl-12"> 
                    <div class="card widget-1">
                      <div class="card-body"> 
                        <div class="widget-content">
                          <div class="widget-round warning">
                            <div class="bg-round">
                              <svg class="svg-fill">
                                <use href="<?=base_url('public/assets/svg/icon-sprite.svg#doller-return')?>"> </use>
                              </svg>
                              <svg class="half-circle svg-fill">
                                <use href="<?=base_url('public/assets/svg/icon-sprite.svg#halfcircle')?>"></use>
                              </svg>
                            </div>
                          </div>
                          <div> 
                            <a href="<?=base_url('expence')?>" class="text-dark"><h4><i class="fa fa-inr"></i> <?=empty($total_expense->expence)?0:round($total_expense->expence,2)?></h4><span class="f-light">Total Expense</span></a>
                          </div>
                        </div>
                        <div class="font-warning f-w-500"><i class="icon-arrow-down icon-rotate me-1"></i><span>-20%</span></div>
                      </div>
                    </div>
                   </div>
                    <div class="col-xl-12"> 
                      <div class="card widget-1">
                        <div class="card-body"> 
                          <div class="widget-content">
                            <div class="widget-round success">
                              <div class="bg-round">
                                <svg class="svg-fill">
                                  <use href="<?=base_url('public/assets/svg/icon-sprite.svg#fill-project')?>"> </use>
                                </svg>
                                <svg class="half-circle svg-fill">
                                  <use href="<?=base_url('public/assets/svg/icon-sprite.svg#halfcircle')?>"></use>
                                </svg>
                              </div>
                            </div>
                            <div> 
                              <a href="<?=base_url('project')?>" class="text-dark"><h4><?=$total_project?></h4><span class="f-light">Total Projects</span></a>
                            </div>
                          </div>
                          <div class="font-success f-w-500"><i class="icon-arrow-up icon-rotate me-1"></i><span>+70%</span></div>
                        </div>
                      </div>
                    </div>
                  </div>                  
                </div>
                <div class="container">
                <div class="row">
                  <div class="col-md-6 card">
                    <div id="piechart" style="width: 100%; height: 400px;"></div>
                  </div>
                  <div class="col-md-6 card">
                    <div id="projectchart" style="width: 100%; height: 400px;"></div>
                  </div>
                  <div class="col-md-12 card">
                  <div id="container" style="width: 100%; height: 500px;"></div>
                  </div>
                </div>
                
              
              </div>
              <div class="col-xxl-auto col-xl-12">                
                <div class="card">                  
                  <div class="container mt-2">
                    <div class="table-responsive">
                      <table class="display" id="basic-1">
                        <thead>
                          <tr  class="card-header card-no-border" style="background-color:var(--theme-deafult)">
                          <th>
                          <div>
                            <div class="header-top">
                              <h5 class="m-0">New Activities</h5>
                            </div>
                          </div>
                          </th> 
                          <th> Date (Time)</th>                    
                          </tr>
                        </thead>
                        <tbody>     
                          <?php foreach($notifications as $notification){?>                    
                            <tr>
                              <!-- <td><img class="img-fluid img-40 rounded-circle" src="../assets/images/dashboard/user/1.jpg" alt="user"></td> -->
                              <?php $link = '"'.base_url($notification['url']).'"'.'onclick="nofication_read('.$notification['id'].')"';?>
                              <td ><a class="d-block f-w-500" href=<?=$link?>><?=$notification['msg']?></a><span class="f-light">To :<?=$notification['reciever_name']?></span></td>
                              <td >
                                <p class="m-0 font-success"> <?=date('d-m-Y',strtotime($notification['created_at']))?>(<?=date('h:i A',strtotime($notification['created_at']))?>)</p>
                              </td>
                            </tr>
                          <?php }?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
                <?php } if($_SESSION['role_id']==4){?>
                  <div class="row">                   
                    <div class="col-xl-12"> 
                    <div class="card widget-1">
                      <div class="card-body"> 
                        <div class="widget-content">
                          <div class="widget-round warning">
                            <div class="bg-round">
                              <svg class="svg-fill">
                                <use href="<?=base_url('public/assets/svg/icon-sprite.svg#fill-contact')?>"> </use>
                              </svg>
                              <svg class="half-circle svg-fill">
                                <use href="<?=base_url('public/assets/svg/icon-sprite.svg#halfcircle')?>"></use>
                              </svg>
                            </div>
                          </div>
                          <div> 
                            <a href="<?=base_url('enquiry')?>" class="text-dark"><h4><?=$total_enquiry?></h4><span class="f-light">Total Enquiry</span></a>
                          </div>
                        </div>
                        <div class="font-warning f-w-500"><i class="icon-arrow-down icon-rotate me-1"></i><span>-20%</span></div>
                      </div>
                    </div>
                   </div>
                  </div>
                </div>
              <div class="col-xxl-auto col-xl-3 col-sm-6 box-col-6"> 
                <div class="row">   
                    <div class="col-xl-12"> 
                      <div class="card widget-1">
                        <div class="card-body"> 
                          <div class="widget-content">
                            <div class="widget-round success">
                              <div class="bg-round">
                                <svg class="svg-fill">
                                  <use href="<?=base_url('public/assets/svg/icon-sprite.svg#cart')?>"> </use>
                                </svg>
                                <svg class="half-circle svg-fill">
                                  <use href="<?=base_url('public/assets/svg/icon-sprite.svg#halfcircle')?>"></use>
                                </svg>
                              </div>
                            </div>
                            <div> 
                              <a href="<?=base_url('orders')?>" class="text-dark"><h4><?=$total_orders?></h4><span class="f-light">Total Orders</span></a>
                            </div>
                          </div>
                          <div class="font-success f-w-500"><i class="icon-arrow-up icon-rotate me-1"></i><span>+70%</span></div>
                        </div>
                      </div>
                    </div>
                  </div>                  
                </div>
                <?php }?>
              </div>
              
          </div>
          <!-- Container-fluid Ends-->
        </div>

        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
          // google.charts.load('current', {'packages':['corechart']});
          // google.charts.setOnLoadCallback(drawChart);

          // function drawChart() {

          //   var data = google.visualization.arrayToDataTable([
          //     ['Location', 'Users'],
          //     <?=$location_users?>
          //   ]);

          //   var options = {
          //     title: 'Location Wise Users'
          //   };

          //   var chart = new google.visualization.PieChart(document.getElementById('piechart'));

          //   chart.draw(data, options);
          // }
        </script>
        <script type="text/javascript">
            google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Location', 'Projects'],
          <?=$location_projects?>
           ]);

        var options = {
          //colors: ['#e33244', '#b6e745'],
          title: 'Location Wise Projects',
          pieHole: 0.4,
          legend: {
          position: 'right',
          alignment: 'center',
         },
         
        };

        var chart = new google.visualization.PieChart(document.getElementById('projectchart'));

        chart.draw(data, options);
      }
          // google.charts.load('current', {'packages':['corechart']});
          // google.charts.setOnLoadCallback(drawChart);

          // function drawChart() {

          //   var data = google.visualization.arrayToDataTable([
          //     ['Location', 'Projects'],
          //     <?=$location_projects?>
          //   ]);

          //   var options = {
          //     title: 'Location Wise Projects'
          //   };

          //   var chart = new google.visualization.PieChart(document.getElementById('projectchart'));

          //   chart.draw(data, options);
          // }
        </script>

<script type="text/javascript">
          google.charts.load('current', {'packages':['corechart']});
          google.charts.setOnLoadCallback(drawChart);

          function drawChart() {

            var data = google.visualization.arrayToDataTable([
              ['Project', 'Boms'],
              <?=$bom_projects?>
            ]);

            var options = {
              title: 'Project Wise Boms'
            };

            var chart = new google.visualization.PieChart(document.getElementById('bomchart'));

            chart.draw(data, options);
          }
        </script>

<script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Project', 'Boms'],
          <?=$bom_projects?>
        ]);

        var options = {
          chart: {
            title: 'Project Wise Boms',
            //subtitle: 'Sales, Expenses, and Profit: 2014-2017',
          }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>
    <script>
        google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Location', 'Users'],
          //['Total Target [₹-100000]',100000],['Recieved Amount [₹-10000]', 10000],
          <?=$location_users?>        ]);

        var options = {
          //colors: ['#e33244', '#b6e745'],
          title: 'Location Wise Users',
          pieHole: 0.4,
          legend: {
          position: 'right',
          alignment: 'center',
         },
         
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
    <script src="https://work.webcadenceindia.com/office/application/calling-mangment/assets/js/anychart.js"></script>
  <script>
    anychart.onDocumentReady(function () {
  
  // add data
  var data = [
    <?=$bom_projects?>
    //["Apr", 145000],["Mar", 155000],    
    // ["January", 100],
    // ["February", 90],
    // ["March", 100],
    // ["April", 2000]
  ];
  
  // create a data set
  var dataSet = anychart.data.set(data);

  

  // map the data for all series
  var firstSeriesData = dataSet.mapAs({x: 0, value: 1});
  var secondSeriesData = dataSet.mapAs({x: 0, value: 2});
  var thirdSeriesData = dataSet.mapAs({x: 0, value: 3});

  // create a line chart
  var chart = anychart.line();
  // create the series and name them
  var firstSeries = chart.splineArea(firstSeriesData);
  firstSeries.name("Boms");
  var secondSeries = chart.splineArea(secondSeriesData);
  secondSeries.name("-");
  var thirdSeries = chart.splineArea(thirdSeriesData);
  thirdSeries.name("-");
  
  

thirdSeries.normal().fill();
thirdSeries.normal().stroke();

  // add a legend
  chart.legend().enabled(true);
  
  // add a title
  chart.title("Project Wise Boms");
  
  // specify where to display the chart
  chart.container("container");
  
  // draw the resulting chart
  chart.draw();
  
});
  </script>