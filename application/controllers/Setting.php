<?php
date_default_timezone_set('Asia/Kolkata');
class Setting extends MY_Controller 
{
    public function __construct()
    {
      parent::__construct();
     $this->load->model('setting_model');
     $this->load->model('common_model');
    
    }

    public function site_setting()
    { 
      //die('dfdgdfgdf'); 	
    // if($this->session->userdata('user_type')!='Admin'){
    //       redirect('home');
    
        $this->not_admin_logged_in();
        $data['page_title'] = 'Site Info';
        $data['title'] = 'Site Info';
        $data['siteinfo'] = $this->siteinfo();
        $data['siteinfo'] = $this->Common_model->get_site_info();
        $this->template('site_setting',$data);
    }

    public function update_notification(){
      $id = $this->input->post('id');
      
      $this->common_model->update_notification(array('status'=>0),$id);
    }

    public function store_siteInfo(){
       $site_name = $this->input->post('site_name');
       $site_contact = $this->input->post('site_contact');
       $whatsapp_no = $this->input->post('whatsapp_no');
       $site_email = $this->input->post('site_email');
       $site_address = $this->input->post('site_address');
       $facebook_url = $this->input->post('facebook_url');
       $youtube_url = $this->input->post('youtube_url');
       $linkedin_url = $this->input->post('linkedin_url');
       $twitter_url = $this->input->post('twitter_url');
       $insta_url = $this->input->post('insta_url');
       $footer_contant = $this->input->post('footer_contant');
       $discription = $this->input->post('discription');
       $siteinfo = $this->Common_model->get_site_info();
       if(empty($site_name)){
        echo json_encode(['status'=>403, 'message'=>'Please enter site name']); 	
        exit();
       }
       if(empty($site_contact)){
        echo json_encode(['status'=>403, 'message'=>'Please enter site mobile']); 	
        exit();
       }
       if(empty($site_email)){
        echo json_encode(['status'=>403, 'message'=>'Please enter site email']); 	
        exit();
       }

       if(strlen((string)$site_contact) != 10){
        echo json_encode(['status'=>403, 'message'=>'Please enter mobile number 10 digits']);
        exit();
       }
       if(!empty($whatsapp_no)){
       if(strlen((string)$whatsapp_no) != 10){
        echo json_encode(['status'=>403, 'message'=>'Please enter whatsapp number 10 digits']);
        exit();
       }
      }

       if(empty($discription)){
        echo json_encode(['status'=>403, 'message'=>'Please enter description']); 	
        exit();
       }

       if(empty($footer_contant)){
        echo json_encode(['status'=>403, 'message'=>'Please enter footer contant']); 	
        exit();
       }
     $this->load->library('upload');
    if(!empty($_FILES['site_logo']['name'])){
     $config = array(
      'upload_path' 	=> 'uploads/siteInfo',
      'file_name' 	=> str_replace(' ','',$site_name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif',
      'max_size' 		=> '10000000',
     );
     $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('site_logo'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]); 	
          exit();
      }
      else
      {
        $type = explode('.', $_FILES['site_logo']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/siteInfo/'.$config['file_name'].'.'.$type;
      }
    }elseif(!empty($siteinfo->site_logo)){
      $image = $siteinfo->site_logo;
    }else{
      $image = 'public/images/dummy_image.jpg';
    }
      $data = array(
       'site_name' => $site_name,
       'site_email' => $site_email,
       'site_contact' => $site_contact,
       'whatsapp_no' => $whatsapp_no,
       'site_address' => $site_address,
       'discription' => $discription,
       'footer_contant' => $footer_contant,
       'site_logo' => $image,
       'facebook_url' => $facebook_url,
       'youtube_url' => $youtube_url,
       'linkedin_url' => $linkedin_url,
       'twitter_url' => $twitter_url,
       'insta_url' => $insta_url,
      
      );

      $update = $this->setting_model->update_siteInfo($data);

      if($update){
        echo json_encode(['status'=>200, 'message'=>'Site-info update successfully!']);
    }else{
        echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
    }

    }

  

  
  public function unit_setting()
    { 
        $this->not_admin_logged_in();
        $data['page_title'] = 'Unit';
        $data['title'] = 'Unit';
        $data['unit'] = $this->Common_model->get_unit();
        $this->template('unit/unit',$data);
    }

public function add_unit(){
$name = $this->input->post('name');

if(empty($name)){
  echo json_encode(['status'=>403, 'message'=>'Please enter name']);
  exit();
}
 $data = array(
    'adminID' => $this->session->userdata('id'),
    'name' => $name,
 );

$addunit = $this->setting_model->add_unit($data);
if($addunit){
 echo json_encode(['status'=>200, 'message' => 'Unit added successfully']);
}else{
    echo json_encode(['status'=>403, 'message'=>'something went wrong']);
}
}

public function ajaxunitTable(){
  $condition = array('status'=>1);
  $units = $this->setting_model->make_datatables($condition); // this will call modal function for fetching data
  $data = array();
 
  foreach($units as $key=>$unit) // Loop over the data fetched and store them in array
  {
      $button = '';
      $sub_array = array();
      $button .= '<button type="button" onclick="editUnitModal('.$unit['id'].')" title="Edit Unit" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </button>';
      $button .= '<a href="javascript:void(0)" onclick="delete_unit('.$unit['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete User" class="btn  btn-sm  text-danger"><i class="fa fa-trash-o"></i> </a>';
      $sub_array[] = $key+1;
      $sub_array[] = $unit['name']; 
      $sub_array[] = date('d-F-Y', strtotime($unit['created_at']));
      $sub_array[] = $button;
      $data[] = $sub_array;
      
}
$output = array(
  "draw"                    =>     intval($_POST["draw"]),
  "recordsTotal"            =>     $this->setting_model->get_all_data($condition),
  "recordsFiltered"         =>     $this->setting_model->get_filtered_data($condition),
  "data"                    =>     $data
);

echo json_encode($output);

}

public function get_unit(){
  $unitID = $this->input->post('unitID');
  $unit = $this->setting_model->get_unit(array('id'=>$unitID));
  echo json_encode($unit);
}

public function edit_unit(){
  $unitID = $this->input->post('edit_unitId');
  $name = $this->input->post('edit_name');
  if(empty($name)){
  echo json_encode(['status'=>403, 'message'=>'Please enter unit name']);
  exit();
  }
   $data = array(
    'name' => $name,
   );
  $updateUnit = $this->setting_model->update_unit($data,$unitID);
  if($updateUnit){
   echo json_encode(['status'=>200, 'message' => 'Unit Update successfully']);
   }else{
    echo json_encode(['status'=>403, 'message'=>'something went wrong']);
   }
  }

  public function delete_unit(){
    $unitID =  $this->input->post('unitID');
    $unit = $this->setting_model->delete_unit(array('id' => $unitID));
    if($unit){
      echo json_encode(['status'=>200, 'message'=>'Unit successfully deleted']);
       }else{
        echo json_encode(['status'=>302, 'message'=>'Something went wrong']);   
       }
}

public function location()
{
  $this->not_admin_logged_in();
  $data['page_title'] = 'Location';
  $data['title'] = 'Location';
  $data['locations'] = $this->Common_model->get_create_locations(array('location.status'=>1));
  // print_r($data['locations']);die;
  $this->template('location/index',$data);
}

public function add_location(){
  //print_r($_POST);die;
  $location = $this->input->post('name');
  $location_check = $this->Common_model->get_location(array('location',$location));
  $gstn = $this->input->post('gst');
  $address = $this->input->post('address');
  
  if(!empty($location_check)){
    echo json_encode(['status'=>403, 'message'=>'This location is already exists']);
    exit();
   }

  if(empty($location)){
     echo json_encode(['status'=>403, 'message'=>'Please Enter Location ']);
     exit();
  }

  $data = array(
    'location' => $location,
    'gstn' => $gstn,
    'address' => $address,
  );

  $store = $this->Common_model->store_location($data);
  if($store){
    echo json_encode(['status'=>200, 'message'=>'Location store successfully']);
     }else{
      echo json_encode(['status'=>302, 'message'=>'Something went wrong']);   
     }

}

public function edit_location_form(){
  $locationID = $this->input->post('locationID');
  $location = $this->Common_model->get_location(array('id'=>$locationID));
 ?>
  <div class="form-group">
    <label for="">Location*</label>
    <input type="text" class="form-control" id="name" name="name" value="<?=$location->location?>" placeholder="Enter Location Name">
  </div>
  <div class="form-group">
    <label for="">Gst</label>
    <input type="text" class="form-control" id="gst" name="gst" value="<?=$location->gstn?>" placeholder="Enter Gst">
  </div>
  <div class="form-group">
    <label for="">Address*</label>
    <textarea type="text" class="form-control" id="address" name="address" value="" placeholder="Enter Address"><?=$location->address?></textarea>
  </div>
  <input type="hidden" name="id" value="<?=$location->id?>">
 <?php
}

public function update_location(){
  //ini_set('display_errors',1);
  //print_r($_POST);
  $id = $this->input->post('id');
  $location = $this->input->post('name');
  $gstn = $this->input->post('gst');
  $address = $this->input->post('address');
  $location_check = $this->Common_model->get_location(array('location'=>$location,'id<>'=>$id));
  //print_r($location_check);die;
  if(!empty($location_check)){
    echo json_encode(['status'=>403, 'message'=>'This location is already exists']);
    exit();
   }

  if(empty($location)){
     echo json_encode(['status'=>403, 'message'=>'Please enter location ']);
     exit();
  }

  $data = array(
    'location' => $location,
    'gstn' => $gstn,
    'address' => $address
  );

  $update = $this->Common_model->update_location($data,$id);
  if($update){
    echo json_encode(['status'=>200, 'message'=>'Location update successfully']);
     }else{
      echo json_encode(['status'=>302, 'message'=>'Something went wrong']);   
     }

}


}
?>