<?php 
date_default_timezone_set('Asia/Kolkata');
class Product extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
		$this->load->model('auth_model');
		$this->load->model('product_model');
		$this->load->model('setting_model');
    $this->load->library('csvimport');
	}

	public function index()
	{	 
		
		$menuID = 13;
		$permission = $this->permissions()->$menuID;
		if($permission[0]=='view'){     
      $data['permission'] = $permission;
      $data['title'] = 'Product';
    $data['units'] = $this->product_model->get_units(); 
	  $this->template('product/product',$data);
   }else{
		redirect(base_url('dashboard'));
	 }
	}

	public function ajaxproducts(){
    $menuID = 13;
		$permission = $this->permissions()->$menuID;
		
    $condition = array('products.status'=>1);
    $products = $this->product_model->make_datatables($condition); // this will call modal function for fetching data
    //print_r($products); 
		$data = array();
    foreach($products as $key=>$product) // Loop over the data fetched and store them in array
    {
        $sub_array = array();
        $button = '';
        if($permission[2]=='edit'){  
        $button .= '<button type="button" onclick="editProductModal('.$product['id'].')" title="Edit Product" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </button>';
        }
        if($permission[3]=='delete'){  
        $button .= '<a href="javascript:void(0)" onclick="delete_product('.$product['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Product" class="btn  btn-sm  text-danger"><i class="fa fa-trash-o"></i> </a>';
        }
        $sub_array[] = $key+1;
        
        $sub_array[] = $product['part_name'];
        $sub_array[] = $product['product_code'];
        $sub_array[] = $product['name'];        
        $sub_array[] = $product['unit'];        
        $sub_array[] = $product['specification'];        
        $sub_array[] = $product['make'];
        $sub_array[] = $product['model'];
        $sub_array[] = $product['location'];
        $sub_array[] = $product['remark'];
        $sub_array[] = $product['price'];
        $sub_array[] = $product['user_name'];
        $sub_array[] = date('d-F-Y', strtotime($product['created_at']));
        $sub_array[] = $button;
        $data[] = $sub_array;
  }
  $output = array(
    "draw"                    =>     intval($_POST["draw"]),
    "recordsTotal"            =>     $this->product_model->get_all_data($condition),
    "recordsFiltered"         =>     $this->product_model->get_filtered_data($condition),
    "data"                    =>     $data
);

echo json_encode($output);

  }
  
  public function add_product(){
    //print_r($_POST);die;
    $name = $this->input->post('part_name'); 
    $code = $this->input->post('product_code');
    $unit_type = $this->input->post('unit_type');
    $unit =      $this->input->post('unit');
    $spec = $this->input->post('spec');
    $make = $this->input->post('make');
    $model = $this->input->post('model');
    $location = $this->input->post('location');
    $remark = $this->input->post('remark');
    $price = $this->input->post('price');
    $checkProduct = $this->product_model->get_product(array('products.product_code'=>$code)); 
    //print_r($checkProduct);die;
    if($checkProduct){
      echo json_encode(['status'=>403, 'message'=>'This Product Code Already Exist']);
      exit();
    }
    if(empty($name)){
      echo json_encode(['status'=>403, 'message'=>'Please Enter part name']); 
      exit();
    }
  
    if(empty($unit_type)){
      echo json_encode(['status'=>403, 'message'=>'Please Select unit type']); 
      exit();
    }

    if($unit==''){
      echo json_encode(['status'=>403, 'message'=>'Please Enter unit']); 
      exit();
    }
    if(empty($spec)){
      echo json_encode(['status'=>403, 'message'=>'Please Enter Specification']); 
      exit();
    }
    
      if(empty($make)){
        echo json_encode(['status'=>403, 'message'=>'Please Enter make']); 
        exit();
      }
      if(empty($model)){
        echo json_encode(['status'=>403, 'message'=>'Please Enter model']); 
        exit();
      }
      if(empty($location)){
        echo json_encode(['status'=>403, 'message'=>'Please Enter location']); 
        exit();
      }
      if(empty($price)){
        echo json_encode(['status'=>403, 'message'=>'Please Enter Price']); 
        exit();
      }
      $Id = $this->session->userdata('id');
      $data = array(
      'added_by' => $Id,
      'part_name' =>  $name,
      'product_code' => $code,
      'unit_type'  => $unit_type,
      'unit' => $unit,
      'specification' => $spec,
      'make' => $make,
      'model' => $model,
      'location' => $location,
      'price' => $price,
      'remark' => $remark,

      );
     //print_r($data); die;
      $add_product = $this->product_model->store_product($data); 
      if($add_product){
      echo json_encode(['status'=>200, 'message'=>'Product Add successfully!']);
      }else{
      echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
    }
  
  }



  public function edit_product(){
    $productId = $this->input->post('edit_productId');
    $name = $this->input->post('edit_partname'); 
    $code = $this->input->post('edit_code');
    $unit_type = $this->input->post('edit_unit_type');
    $unit = $this->input->post('edit_unit');
    $spec = $this->input->post('edit_spec');
    $make = $this->input->post('edit_make');
    $model = $this->input->post('edit_model');
    $location = $this->input->post('edit_location');
    $remark = $this->input->post('edit_remark');
    $price = $this->input->post('edit_price');

    if(empty($name)){
      echo json_encode(['status'=>403, 'message'=>'Please enter part name']); 
      exit();
    }
  
    if(empty($unit_type)){
      echo json_encode(['status'=>403, 'message'=>'Please Select Unit Type']); 
      exit();
    }

    if($unit==''){
      echo json_encode(['status'=>403, 'message'=>'Please enter unit']); 
      exit();
    }
    if(empty($spec)){
      echo json_encode(['status'=>403, 'message'=>'Please enter Specification']); 
      exit();
    }
    
      if(empty($make)){
        echo json_encode(['status'=>403, 'message'=>'Please enter make']); 
        exit();
      }
      if(empty($model)){
        echo json_encode(['status'=>403, 'message'=>'Please enter model']); 
        exit();
      }
      if(empty($location)){
        echo json_encode(['status'=>403, 'message'=>'Please enter location']); 
        exit();
      }
      if($price==''){
        echo json_encode(['status'=>403, 'message'=>'Please enter price']); 
        exit();
      }
      $data = array(
      'part_name' =>  $name,
      'unit_type' => $unit_type,
      'unit' => $unit,
      'specification' => $spec,
      'make' => $make,
      'model' => $model,
      'location' => $location,
      'remark' => $remark,
      'price' => $price,

      );
     //print_r($data); die;
      $update_product = $this->product_model->update_product($data,$productId); 
      if($update_product){
      echo json_encode(['status'=>200, 'message'=>'Product Update successfully!']);
      }else{
      echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
    }
  }

  public function get_product(){
    $productID  = $this->input->post('productID');
    $product = $this->product_model->get_product(array('products.id'=>$productID));
    echo json_encode($product);
  }

  public function delete_product(){
    $productId =  $this->input->post('productId');
    $product = $this->product_model->delete_product(array('id' => $productId));
    if($product){
      echo json_encode(['status'=>200, 'message'=>'Product successfully deleted']);
       }else{
        echo json_encode(['status'=>302, 'message'=>'Something went wrong']);   
       }
  }
  public function downlodProductList(){
    //$filename = $this->session->userdata('zoneName') ? $this->session->userdata('zoneName')."_price_list.csv" : "all_zone_price_list.csv";
     $filename = base_url('products.csv');
      header('Content-type: text/csv');
      header('Content-disposition:attachment; filename="'.$filename.'"');
      $output = fopen('php://output', 'w');
      fputcsv($output, array('product_name', 'product_code', 'unit_type','unit','specification','make','model','location','remark','price'));
      $data = $this->product_model->get_products(array('products.id'=>1));
        //print_r($data);die;
        $data = array();
      foreach($data as $row) {
        $data[]= $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $count++);
        $data[]= $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['part_name']);
        $data[]=$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['product_code']);
        $data[]=$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['unit_name']);
        $data[]=$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['unit']);
        $data[]=$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['specification']);
        $data[]=$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row['make']);
        $data[]=$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row['model']);
        $data[]=$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row['location']);
        $data[]=$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row['price']);
        $data[]=$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row['remark']);
        $data[]=$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $month_year);
        
      } 
      fputcsv($output, $data);
      fclose($output);   
   }

  //  public function downlodProductList(){
	// 	$this->load->helper('download');
	// 	$file = 'products.csv';
	// 	$pth  = base_url('uploads/download/');
	// 	$nme   =  $file;
	// 	force_download($nme ,$pth);    
	// }
  public function bulk_upload_file(){
   
  
    $unit_type = array();
    $product_location =array();
    $product = array();
    if (isset($_FILES["bulk_products"])) {
      $config['upload_path']   = "uploads/csv/";
      $config['allowed_types'] = 'text/plain|text/csv|csv';
      $config['max_size']      = '2048';
      $config['file_name']     = $_FILES["bulk_products"]['name'];
      $config['overwrite']     = TRUE;
      $this->load->library('upload', $config);
      $this->upload->initialize($config);
      if (!$this->upload->do_upload("bulk_products")) {
        echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
      } else {
        $file_data = $this->upload->data();
        $file_path = 'uploads/csv/' . $file_data['file_name'];
        //print_r($this->csvimport->get_array($file_path));die;
        if ($this->csvimport->get_array($file_path)) {
          $csv_array = $this->csvimport->get_array($file_path);
          foreach($csv_array as $value){

            $check_product = $this->product_model->get_product(array('product_code'=>$value['product_code']));
            $unit_id = $this->setting_model->get_unit(array('name'=>$value['unit_type']));
            $location = $this->Common_model->get_locations(array('location'=>$value['location']));
           
            if(empty($unit_id)){
              $unit_type['unit-'.$value['product_code']] =  $value['product_code'].' Please Enter valid unit Type';
            }
            if(empty($location)){
              $product_location['location-'.$value['product_code']] =  $value['product_code'].' Please Enter valid Location';
            }
      
            if($check_product){
              $product['product_code-'.$value['product_code']] =  $value['product_code'].' This product Alerady Exits';
            }
      
          }

          if(!empty($unit_type) || !empty($product_location )){
            $first_array =  array_merge($unit_type,$product_location);
            //$second_array = array_merge($first_array,$product);
           $decode_url = base64_encode($file_path);
            echo json_encode(['status'=>303, 'message'=>'File Not upload downlod error report please check..','file_data'=>$decode_url]);
           exit;
            }
      
          foreach ($csv_array as $key => $row) {
            
            $unit_id = $this->setting_model->get_unit(array('name'=>$row['unit_type']));
            //print_r($unit_id->id);die;
            if($check_product){
              continue;
            }
               $Id =  $this->session->userdata('id');
              $data = array(
                'added_by' => $Id,
                'part_name' => $row['product_name'],
                'product_code' => str_replace(' ','_',($row['product_code'])),
                'unit' => 0,//$row['unit'],
                'unit_type' => $unit_id->id,
                'specification' => $row['specification'],
                'make' => $row['make'],
                'model' => $row['model'],
                'location' => $row['location'],
                'remark' => $row['remark'],
                'price' => !empty($row['price'])?$row['price']:0,
            );
            //print_r($data);die;
            //$this->setting_model->update_zone_price($data,$id); 
            $this->product_model->store_product($data); 
            if($check_product){
              echo json_encode(['status'=>403, 'message'=>'Some Products Already Exist...']);
              exit();
            }     
          }
          echo json_encode(['status'=>200, 'message'=>'Product Added Successfully']);
          exit();
        } else {
          echo json_encode(['status'=>403, 'message'=>'Product Add Failure']);
          exit();
        }
      }
    } else {
      echo json_encode(['status'=>403, 'message'=>'Please upload CSV']);
    }
  }

  public function download_error_report(){
    $file_path = base64_decode($this->uri->segment(3)); 
    $unit_type = array();
    $product_location =array();
    $product = array();
    $first_array = array();
    if ($this->csvimport->get_array($file_path)) {
      $csv_array = $this->csvimport->get_array($file_path);
      //print_r($csv_array);
      foreach($csv_array as $value){

        $check_product = $this->product_model->get_product(array('product_code'=>$value['product_code']));
        $unit_id = $this->setting_model->get_unit(array('name'=>$value['unit_type']));
        $location = $this->Common_model->get_locations(array('location'=>$value['location']));
       
        if(empty($unit_id)){
          $unit_type['unit-'.$value['product_code']] =  $value['product_code'].' Please Enter valid unit Type';
        }
        if(empty($location)){
          $product_location['location-'.$value['product_code']] =  $value['product_code'].' Please Enter valid Location';
        }
  
        if($check_product){
          $product['product_code-'.$value['product_code']] =  $value['product_code'].' This product Alerady Exits';
        }
  
      }
      if(!empty($unit_type) || !empty($product_location || !empty($product))){
        $first_array =  array_merge($unit_type,$product_location);
        $second_array =  array_merge($first_array,$product); 
      }
    }
    $this->load->library('excel');
    $object = new PHPExcel();
    $object->setActiveSheetIndex(0);
    $table_columns = array("S.No", "Product Code", "issuse");

  $column = 0;

  foreach($table_columns as $field) {
      $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $column++;
  }
    $excel_row = 2;
    $count = 1;
    foreach($second_array as $key=>$excel){
      //print_r($excel);
      $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $count++);
      $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $key);
      $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $excel);
      $excel_row++; 
    }

    $this_date = "Error_Report".date("YmdHis");
    $filename= $this_date.'.csv'; //save our workbook as this file name
    
    header('Content-Type: application/vnd.ms-excel; charset=UTF-8'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0'); //no cache

    $objWriter = PHPExcel_IOFactory::createWriter($object, 'CSV');
    //ob_end_clean();
    $objWriter->save('php://output');

  }

  public function export_products(){

    $this->load->library('excel');

    $object = new PHPExcel();
    $object->setActiveSheetIndex(0);
    $table_columns = array("S.No","Part Name", "Product Code", "Unit Type", "Units","Specification","Make","Model","Location","Price","Remark","Created At");

    $column = 0;

    foreach($table_columns as $field) {
        $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
        $column++;
    }
    
    $products_data = $this->product_model->get_products(array('products.status'=>1));
    //$expence_data = $this->Project_model->get_expences(array('status'=>1));
    //print_r($products_data);die;
    $excel_row = 2;
    $count = 1;
    foreach($products_data as $row) {
        $month_year = date('d M Y',strtotime($row['created_at']));
        $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $count++);
        $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['part_name']);
        $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['product_code']);
        $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['unit_name']);
        $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['unit']);
        $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['specification']);
        $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row['make']);
        $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row['model']);
        $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row['location']);
        $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row['price']);
        $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row['remark']);
        $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $month_year);
       
        $excel_row++;
    }
    $this_date = "Products".date("Y-m-d-H-i-s");
    $filename= $this_date.'.csv'; //save our workbook as this file name
    header('Content-Type: application/vnd.ms-excel; charset=UTF-8'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0'); //no cache

    $objWriter = PHPExcel_IOFactory::createWriter($object, 'CSV');
    //ob_end_clean();
    $objWriter->save('php://output');
  }

 



}