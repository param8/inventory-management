<?php 
date_default_timezone_set('Asia/Kolkata');
class Project extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
		$this->load->model('auth_model');
		$this->load->model('Project_model');
    $this->load->model('Bom_model');
    
	}

	public function index()
	{	 $menuID = 4;
		$permission = $this->permissions()->$menuID;
		if($permission[0]=='view'){     
      $data['permission'] = $permission;
      $data['title'] = 'Project';
      $data['locations'] = $this->Common_model->get_create_locations(array('location.status'=>1));
      // print_r( $data['locations']);die;
      $this->template('project/project',$data);
   }else{
		redirect(base_url('dashboard'));
	 }
		
		
	}

  public function locationFilter(){
    //print_r($_POST);die;
    $id = $this->input->post('id');
    //print_r($id);die;
    $this->session->set_userdata('ProjectlocationID',$id);
    //print_r($_SESSION);die;
  }

  public function resetLocationFilter(){
    
    $this->session->unset_userdata('ProjectlocationID');
  }

  public function expence(){
    $menuID = 11;
		$permission = $this->permissions()->$menuID;
		if($permission[0]=='view'){     
      $data['permission'] = $permission;
      $data['title'] = 'Expence';  
      $data['all_projects'] = $this->Project_model->get_projects(array('projects.status'=>1));
      $data['projects'] = $this->Bom_model->get_bomProject(array('bom.status'=>1,));
      $data['locations'] = $this->Common_model->get_create_locations(array('location.status'=>1));
	    $this->template('project/expence',$data);
   }else{
		redirect(base_url('dashboard'));
	 }
   
  }

  public function export_expence(){

    $this->load->library('excel');

    $object = new PHPExcel();
    $object->setActiveSheetIndex(0);
    $table_columns = array("S.No","Project Name", "Remark", "Expence", "Date");

    $column = 0;

    foreach($table_columns as $field) {
        $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
        $column++;
    }
    
    $expence_data = $this->Project_model->get_expences(array('expences.status'=>1));
    //$expence_data = $this->Project_model->get_expences(array('status'=>1));
    //print_r($expence_data);die;
    $excel_row = 2;
    $count = 1;
    foreach($expence_data as $row) {
        $month_year = date('d M Y',strtotime($row['created_at']));
        $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $count++);
        $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['name']);
        $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['particular']);
        $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['expence']);
        $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $month_year);
       
        $excel_row++;
    }
    $this_date = "Expense".date("YmdHis");
    $filename= $this_date.'.csv'; //save our workbook as this file name
    header('Content-Type: application/vnd.ms-excel; charset=UTF-8'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0'); //no cache

    $objWriter = PHPExcel_IOFactory::createWriter($object, 'CSV');
    //ob_end_clean();
    $objWriter->save('php://output');
  }

  public function expense_pdf(){
    $id = $this->session->userdata('projectID');
    //print_r($id);
    $data['title'] = 'Print PDF';
    $data['expence_data'] = $this->Project_model->get_expences(array('expences.project'=>$id));
    $data['project_data'] = $this->Project_model->get_project(array('id'=>$id));
   
    $this->load->library('pdf');
    $html = $this->load->view('project/expence_pdf',$data, true);
    $this->pdf->createPDF($html, 'mypdf', false);
  }

  public function export_projects(){

    $this->load->library('excel');

    $object = new PHPExcel();
    $object->setActiveSheetIndex(0);
    $table_columns = array("S.No","Project Name","Client Name","Budget","Remark", "Location", "Added By", "Start Date", "End Date","Created Date");

    $column = 0;

    foreach($table_columns as $field) {
        $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
        $column++;
    }
    
    $projects_data = $this->Project_model->get_excel_projects(array('projects.status'=>1));
    //$expence_data = $this->Project_model->get_expences(array('status'=>1));
    //print_r($expence_data);die;
    $excel_row = 2;
    $count = 1;
    foreach($projects_data as $row) {
        $month_year = date('d/m/Y',strtotime($row['created_at']));
        $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $count++);
        $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['name']);
        $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['client']);
        $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['budget']);
        $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['remark']);
        $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['projectLocation']);
        $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row['userName']);
        $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row['start_date']);
        $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row['end_date']);
        
        $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $month_year);
       
        $excel_row++;
    }
    $this_date = "Projects-data";
    $filename= $this_date.'.csv'; //save our workbook as this file name
    header('Content-Type: application/vnd.ms-excel; charset=UTF-8'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0'); //no cache

    $objWriter = PHPExcel_IOFactory::createWriter($object, 'CSV');
    //ob_end_clean();
    $objWriter->save('php://output');
  }

	public function ajaxprojects(){
    $menuID = 4;
		$permission = $this->permissions()->$menuID;
    $condition = array('projects.status<>'=>0);
    $projects = $this->Project_model->make_datatables($condition); // this will call modal function for fetching data
    //print_r($projects); 
		$data = array();
    foreach($projects as $key=>$project) // Loop over the data fetched and store them in array
    {
        $user = $this->user_model->get_user(array('id'=>$project['added_by']));
       
       if($project['status']==1){
        $status = '<b class="text-success">On Going</b>';
      }else if($project['status']==2){
        $status = '<b class="text-danger">Closed</b>';
      }
        $sub_array = array();
        $button = '';
        if($project['status']!=2){
          if($permission[2]=='edit'){     
            $button .= '<button type="button" onclick="editProjectModal('.$project['id'].')" title="Edit Project" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </button>';
          } if($permission[3]=='delete'){
            $button .= '<a href="javascript:void(0)" onclick="delete_project('.$project['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Project" class="btn  btn-sm  text-danger"><i class="fa fa-trash-o"></i> </a>';
            $button .= '<a href="javascript:void(0)" onclick="close_project('.$project['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Close Project" class="btn  btn-sm  text-warning"><i class="fa fa-times"></i> </a>';
          }
        }
        $budget = ($this->session->userdata('role_id') == 1 || $this->session->userdata('role_id') == 11 || $this->session->userdata('role_id') == 5)? $project['budget'] : '<span class="text-center">-</span>';
        $sub_array[] = $key+1;
        $sub_array[] = $project['userName'];
        $sub_array[] = $project['projectLocation'];
        $sub_array[] = $project['name'];
        $sub_array[] = $project['client'];
        $sub_array[] = $budget;
        $sub_array[] = '<textarea readonly>'.$project['remark'].'</textarea>';
        $sub_array[] = date('d M Y', strtotime($project['start_date']));
        $sub_array[] = date('d M Y', strtotime($project['end_date']));
        $sub_array[] = date('d M Y', strtotime($project['created_at']));
        //$sub_array[] = $user->name;
        $sub_array[] = $status;
        $sub_array[] = $button;
        $data[] = $sub_array;
  }
  $output = array(
    "draw"                    =>     intval($_POST["draw"]),
    "recordsTotal"            =>     $this->Project_model->get_all_data($condition),
    "recordsFiltered"         =>     $this->Project_model->get_filtered_data($condition),
    "data"                    =>     $data
);

echo json_encode($output);

  }

  public function ajaxExpence(){
    $menuID = 11;
		$permission = $this->permissions()->$menuID;
		
    $condition = array('expences.status'=>1);
    $projects = $this->Project_model->make_expdatatables($condition); // this will call modal function for fetching data
    //print_r($projects); 
		$data = array();
    $total_exp = 0;
    foreach($projects as $key=>$project) // Loop over the data fetched and store them in array
    {
        $sub_array = array();
        $button = '';
        //$button .= '<button type="button" onclick="editProjectModal('.$project['id'].')" title="Edit Product" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </button>';
        if($permission[3]=='delete'){     
        $button .= '<a href="javascript:void(0)" onclick="delete_exp('.$project['id'].')"data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Product" class="btn  btn-sm  text-danger"><i class="fa fa-trash-o"></i> </a>';
        }
        $sub_array[] = $key+1;
        
        $sub_array[] = $project['name'];
        $sub_array[] = '<textarea rowspan="5" class="form-control" style="width:400px" readonly>'.$project['particular'].'</textarea>';
        $sub_array[] = $project['expence'];
        $sub_array[] = date('d-F-Y', strtotime($project['date']));
        $sub_array[] = $button;
        $total_exp += $project['expence'];
        $data[] = $sub_array;
  }
  //print_r($total_exp);die;
  //$this->session->set_userdata('projectExpence',$total_exp);
  $value[] = array('','','<b>Total Expence</b>','<b>'.$total_exp.'</b>','','');
  $alldata = array_merge($data,$value);
  $output = array(
    //"draw"                    =>     intval($_POST["draw"]),
    "recordsTotal"            =>     $this->Project_model->get_all_expdata($condition),
    "recordsFiltered"         =>     $this->Project_model->get_expfiltered_data($condition),
    "data"                    =>     $alldata
);

echo json_encode($output);
  }
  
  public function add_project(){
    //print_r($_POST);die;
    $project = $this->input->post('project_name'); 
    $budget = $this->input->post('budget');
    $client = $this->input->post('client');
    $startDate = $this->input->post('sdate');
    $finishDate = $this->input->post('fdate');
    $remark = $this->input->post('remark');
    $location = $this->input->post('location');
    $checkProject = $this->Project_model->get_project(array('name'=>$project)); 
    if($checkProject){
      echo json_encode(['status'=>403, 'message'=>'This Project Already Exist']);
      exit();
    }
    if(empty($project)){
      echo json_encode(['status'=>403, 'message'=>'Please enter project name']); 
      exit();
    }
  
    if(empty($budget)){
      echo json_encode(['status'=>403, 'message'=>'Please enter budget']); 
      exit();
    }
    
      if(empty($client)){
        echo json_encode(['status'=>403, 'message'=>'Please enter client name']); 
        exit();
      }
       if(empty($location)){
        echo json_encode(['status'=>403, 'message'=>'Please select location']); 
        exit();
      }

      $userId = $this->session->userdata('id');
      $data = array(
      'added_by' => $userId,
      'name' =>  $project,
      'budget' => $budget,
      'client' => $client,
      'remark' => $remark,
      'start_date' => $startDate,
      'end_date' => $finishDate,
      'location' => $location,
      );
     //print_r($data); die;
      $add_project = $this->Project_model->store_project($data); 
      if($add_project){
      echo json_encode(['status'=>200, 'message'=>'Project Add successfully!']);
      }else{
      echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
    }
  
  }

  public function edit_project(){
    $projectId = $this->input->post('edit_projectId');
    $name = $this->input->post('edit_name'); 
    $client = $this->input->post('edit_client');
    $budget = $this->input->post('edit_budget');
    $startDate = $this->input->post('edit_sdate');
    $finishDate = $this->input->post('edit_fdate');
    $remark = $this->input->post('edit_remark');
    $location = $this->input->post('edit_location');
    if(empty($name)){
      echo json_encode(['status'=>403, 'message'=>'Please Enter Project name']); 
      exit();
    }
  
    if(empty($client)){
      echo json_encode(['status'=>403, 'message'=>'Please Enter Client Name']); 
      exit();
    }
    
      if(empty($budget)){
        echo json_encode(['status'=>403, 'message'=>'Please Enter Budget']); 
        exit();
      }
      if(empty($remark)){
        echo json_encode(['status'=>403, 'message'=>'Please Enter Remark']); 
        exit();
      }
      if(empty($location)){
        echo json_encode(['status'=>403, 'message'=>'Please select location']); 
        exit();
      }
      $data = array(
      'name' =>  $name,
      'client' => $client,
      'budget' => $budget,
      'remark' => $remark,
      'start_date' => $startDate,
      'end_date' => $finishDate,
      'location' => $location,

      );
     //print_r($data); die;
      $update_project = $this->Project_model->update_project($data,$projectId); 
      if($update_project){
      echo json_encode(['status'=>200, 'message'=>'Project Update successfully!']);
      }else{
      echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
    }
  }

  public function get_project(){
    $projectID  = $this->input->post('projectID');
    //print_r($productId);die;
    $project = $this->Project_model->get_project(array('id'=>$projectID));
    //print_r($project);die;
    echo json_encode($project);
  }

  public function delete_project(){
    $projectID =  $this->input->post('projectId');
    $project = $this->Project_model->update_project(array('status' => 0),$projectID);
    // $update_project = $this->Project_model->update_project($data,$projectId); 
    if($project){
      echo json_encode(['status'=>200, 'message'=>'Project successfully deleted']);
       }else{
        echo json_encode(['status'=>302, 'message'=>'Something went wrong']);   
       }
  }

  public function close_project(){
    $projectID =  $this->input->post('projectId');
    $project = $this->Project_model->update_project(array('status' => 2),$projectID);
    if($project){
      echo json_encode(['status'=>200, 'message'=>'Project successfully Closed']);
       }else{
        echo json_encode(['status'=>302, 'message'=>'Something went wrong']);   
       }
  }

  
  public function delete_expence(){
    $ID =  $this->input->post('Id');
    $expence = $this->Project_model->delete_expence(array('id' => $ID));
    if($expence){
      echo json_encode(['status'=>200, 'message'=>'Expence successfully deleted']);
       }else{
        echo json_encode(['status'=>302, 'message'=>'Something went wrong']);   
       }
  }

  public function add_expence(){
    //print_r($_POST);die;
    $edate = $this->input->post('edate');
    $particular = $this->input->post('particular');
    $expence = $this->input->post('expence');
    $project = $this->input->post('project1') ? $this->input->post('project1') : '';
     $total_value = count(array_filter($edate));
    if(empty($project)){
      echo json_encode(['status'=>403, 'message'=>'Please select atleast one Project to Add Expence']);
      exit();
    } 
    if(empty(array_filter($edate))){
      echo json_encode(['status'=>403, 'message'=>'Please Select Date to Add Expence']);
      exit();
    } 
    if(empty(array_filter($particular))){
      echo json_encode(['status'=>403, 'message'=>'Please Enter Particular to Add Expence']);
      exit();
    } 
    if(empty(array_filter($expence))){
      echo json_encode(['status'=>403, 'message'=>'Please Enter Expence to Add Expence']);
      exit();
    } 
    $expence_json = array();
    //print_r($particular);
    //   foreach($particular as $key=>$perticular){
    //       echo "<pre>";
    //       //echo $perticular;
    //     echo  $expences = $expence[$key];
    //   }
      for($i=0; $i<$total_value; $i++){  
        $date = $edate[$i];
        
         $particulars = $particular[$i];
         $expences = $expence[$i];
        //$expence_json[$edate[$i]] = array('edate'=>$edate[$i],'particular'=>$particular[$i],'expence'=>$expence[$i]);
        $user_id = $this->session->userdata('id');
        $data = array(
          'project' => $project,
          'date' => $date,
          'particular' => $particulars,
          'expence' => $expences,
          'added_by' => $user_id,
        );
        // print_r($data);die;
         $add_expence = $this->Project_model->store_expence($data);
        
      }   
      //print_r($expence_json);
      
      if($add_expence){
        echo json_encode(['status'=>200, 'message'=>'Expence Added Succefully!']);
      }else{
        echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
      }

  }
  public function projectFilter(){
    //print_r($this->input->post('to_date'));die;
    $id = !empty($this->input->post('id')) ? $this->input->post('id') : 0;
    $from_date = !empty($this->input->post('from_date'))? $this->input->post('from_date'): 0;
    $to_date = !empty($this->input->post('to_date'))? $this->input->post('to_date'): 0;
    if(!empty($id)){
    $Project = $this->Project_model->get_projects(array('projects.id'=>$id)); 
    
    foreach($Project as $key=>$data){
      //echo $data['budget'];
      $this->session->set_userdata('projectBudget',$data['budget']);
    }
    //print_r($id);
    $expences = $this->Project_model->get_expences(array('expences.project'=>$id));
    //print_r($expences);die;
    $totalxpence = 0;
    foreach($expences as $key=>$row){
       $totalxpence += $row['expence'];      
    }
    //print_r($totalxpence);
    $this->session->set_userdata('projectExpence',$totalxpence);
    
    $this->session->set_userdata('projectID',$id);
  }
    if(!empty($from_date)){
    $this->session->set_userdata('from_date',$from_date);
    }if(!empty($to_date)){
    $this->session->set_userdata('to_date',$to_date);
    }
    //echo $this->session->userdata('projectID');
  }
  public function resetFilter(){
    //print_r($_POST);die;
    if($this->input->post('data')=='project'){
      $this->session->unset_userdata('projectID');
    }
    if($this->input->post('data')=='fromdate'){
      $this->session->unset_userdata('from_date');
    }
    if($this->input->post('data')=='todate'){
      $this->session->unset_userdata('to_date');
    }
    
    
    
  }



}