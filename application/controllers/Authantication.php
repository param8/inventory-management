<?php 
date_default_timezone_set('Asia/Kolkata');
class Authantication extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		//$this->not_admin_logged_in();
		$this->load->model('Auth_model');
		$this->load->model('User_model');
    	$this->load->model('setting_model');

	}


	public function index()
	{	
		$data['title'] = 'Login';
		$this->login_template('signin',$data);
		// $this->load->view('layout/login_head',$data);


	}
	public function signup(){
		$date['title'] = 'Signup';
		$date['roles'] = $this->User_model->get_roles(array('status'=>1,'id <>'=>1));
		$date['states'] = $this->Common_model->get_state(array('country_id'=>101));
		//print_r($date);die;
		$this->login_template('signup',$date);
	}

  public function register(){
	//print_r($_POST);die;
	$vendor_code = $this->input->post('vendor_code');
	$name = $this->input->post('name');
	$email = $this->input->post('email'); 
	$password = $this->input->post('password');
	$phone = $this->input->post('phone');
	$address = $this->input->post('address');
	$state = $this->input->post('state');
	$city = $this->input->post('city');
	$user_type = $this->input->post('user_type');
	$gst = $this->input->post('gst');
	$adhar = $this->input->post('adhar');
  $pancard = $this->input->post('pancard');
	
// print_r($_POST);
// 	die;
	if(empty($vendor_code)){
		echo json_encode(['status'=>403,'message'=>'Please Enter Vendor code']);
		exit();
	}
	$checkvendorCode = $this->User_model->get_vendor(array('vendor.vendor_code'=>$vendor_code));
		if($checkvendorCode){
			echo json_encode(['status'=>403,'message'=>'This vendor code is already in use']);
			exit();
		}
	if(empty($name)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your name']); 	
		exit();
	}
	if(empty($email)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
		exit();
	}
	 $checkEmail = $this->User_model->get_user(array('email'=>$email));
		if($checkEmail){
			echo json_encode(['status'=>403,'message'=>'This email is already in use']);
			exit();
		}
		if(empty($password)){
			echo json_encode(['status'=>403, 'message'=>'Please enter password']); 	
			exit();
		}
		if(empty($phone)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your mobile']); 	
			exit();
		}
		if(empty($address)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your address']); 	
			exit();
		}
		if(empty($state)){
			echo json_encode(['status'=>403, 'message'=>'Please select your State']); 	
			exit();
		}
		if(empty($city)){
			echo json_encode(['status'=>403, 'message'=>'Please select your City']); 	
			exit();
		}
		
		// if($user_type == 'vendor'){
		// 	if(empty($gst)){
		// 		echo json_encode(['status'=>403, 'message'=>'Please enter your GST number']); 	
		// 		exit();
		// 	}
		// 	if(empty($adhar)){
		// 		echo json_encode(['status'=>403, 'message'=>'Please enter your Adhar number']); 	
		// 		exit();
		// 	}
		// 	if(empty($pancard)){
		// 		echo json_encode(['status'=>403, 'message'=>'Please enter your PANARD number']);  	
		// 		exit();
		// 	}
		// }

		$this->load->library('upload');
		if($_FILES['profile_pic']['name']!= ''){
			//echo $_FILES['profile_pic']['name'] ; 
			$config = array(
				'upload_path' => 'uploads/user',
				'file_name' => str_replace(' ','',$name).uniqid(),
				'allowed_types' => 'jpg|png|jpeg|gif',
				'max_size' => '10000000',
			);
			$this->upload->initialize($config);
			if(!$this->upload->do_upload('profile_pic'))
			{
        $error = $this->upload->display_errors();
        echo json_encode(['status'=>403, 'message'=>$error]);
			  exit();
			}
			else{
				$type = explode('.',$_FILES['profile_pic']['name']);
				$type = $type[count($type)-1];
				$image = 'uploads/user/'.$config['file_name'].'.'.$type;
			}
		}else{
			$image = 'public/dummy_image.jpg';
		}
		$permission = '{"6":["view","edit","delete",""],"7":["view","edit","delete",""],"8":["view","edit","delete","approve"]}';
		 $data = array(
			 'name' => $name,
			 'email' => $email,
			 'phone' => $phone,
			 'address' => $address,
			 'country' => '101',
			 'state' => $state,
			 'city' => $city,
			 'profile_pic' => $image,
			 'password' => base64_encode($password),
			 'user_type' => $user_type,
			 'permission' => $permission,
			 
		 );
		//print_r($data);die;
		 $register = $this->Auth_model->register($data);
		
		 if($register){
        $vendorData = array(
          'vendor_id'      => $register,
		      'vendor_code'    => $vendor_code,
		      'gst'            => $gst,
          'adhar'          => $adhar ,
          'pancard'        => $pancard ,
        );
        $this->Auth_model->vendor_info($vendorData);
		
		 echo json_encode(['status'=>200, 'message'=>'Vendor registered successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
		}
	}

	public function login(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		if(empty($email)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your email']); 	
			exit();
		}
		if(empty($password)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your password']); 	
			exit();
		}
		$login = $this->Auth_model->login($email,$password);

		if($login==403){
			echo json_encode(['status'=>403, 'message'=>'email or password incorrect!']);
		}elseif($login==302){
			echo json_encode(['status'=>403, 'message'=>'Your are not active please contact the administrator']);   
		}else{
      echo json_encode(['status'=>200, 'message'=>'Login Successfully','user_type'=>$this->session->userdata('user_type')]);
    }
	}

	
//   Admin Cradinciales


public function adminLogin(){
	//$this->logged_in();
	 $email = $this->input->post('username');
	 $password = $this->input->post('password');
	 $type = 'Admin';
	if(empty($email)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
		exit();
	}
	if(empty($password)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your password']); 	
		exit();
	}
	$login = $this->Auth_model->login($email,$password);

	if($login){
		echo json_encode(['status'=>200, 'message'=>'Login successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'Invalid username or password!']);   
	}
}
// logout Crandincial
public function logout()
{
	$this->session->sess_destroy();
	redirect(base_url(), 'refresh');
}
	
	
}