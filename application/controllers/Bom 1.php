<?php 
date_default_timezone_set('Asia/Kolkata');
class Bom extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
		$this->load->model('auth_model');
		$this->load->model('Bom_model');
		$this->load->model('project_model');
    $this->load->model('Product_model');
        $this->load->library('csvimport');
	}

	public function index()
	{	 
    $menuID = 5;
		$permission = $this->permissions()->$menuID;
		if($permission[0]=='view'){
      $data['title'] = 'Bom';
      $data['bom_projects'] = $this->Bom_model->get_bomProject(array('bom.status'=>1,));
      $data['projects'] = $this->project_model->get_projects(array('projects.status'=>1));
      $data['products'] = $this->Product_model->get_products(array('products.status'=>1));
      //print_r($data['products']);die;
      $data['permission'] = $permission;
      $this->template('bom/bom',$data);
   }else{
		redirect(base_url('dashboard'));
	 }
	}

  public function viewQoutations(){
    $data['bom_id'] = base64_decode($this->uri->segment(2));
    $data['title'] = "Compare Quotations";
    $data['bom_detail'] = $this->Bom_model->get_boms_enquiry(array('id' => $data['bom_id']));
    $data['project_detail'] = $this->project_model->get_project(array('id'=>$data['bom_detail']['project_id']));
    $data['compares'] = $this->Bom_model->get_compare_quatations(array('project_id' => $data['bom_detail']['project_id'],'bom_id' => $data['bom_id']));
    //$data['pos'] = $this->Bom_model->get_po_quatations(array('comapre_id' => $data['compares']->id));
   // print_r($data['compare']);die;
    $this->template('bom/view-quotation',$data);
   
  }

  public function bom_edit_history()
	{	 
		$data['title'] = 'Bom Edit History';
       // $data['projects'] = $this->project_model->get_projects(array('status'=>1));
        //print_r($data);die;
	  $this->template('bom/bom_edit_history',$data);
		
	}

  public function bom_discard_history()
	{	 
    if($this->session->userdata('id')==1){
      $data['title'] = 'Bom Discard History';
    }else{
      $data['title'] = 'Reassign Bom History';
    }
		
       // $data['projects'] = $this->project_model->get_projects(array('status'=>1));
        //print_r($data);die;
	  $this->template('bom/bom_discard_history',$data);
		
	}

  public function dc(){
    $data['title'] = 'DC';
    //print_r($data);die;
    $this->template('store/dc',$data);
  }

  public function dcForm(){
    $data['title'] = 'Add DC';
    $data['projects'] = $this->Bom_model->get_bomProject(array('bom.status'=>1));
    $data['products'] = $this->Product_model->get_products(array('products.status'=>1));
    $this->template('store/add-dc',$data);
  }

  public function storeDeliveryChallan(){
    //print_r($_POST);die;
    //ini_set('display_errors',1);
    $project = $this->input->post('project');
    $chllan_type = $this->input->post('chllan_type');
    $bill_to = $this->input->post('bill_to');
    $delivery_mode = $this->input->post('delivery_mode');
    $gstn = $this->input->post('gstn');
    $po_number = $this->input->post('po_number');
    $mobile = $this->input->post('mobile');
    $email = $this->input->post('email');
    $fax = $this->input->post('fax');
    $vehicle_number = $this->input->post('vehicle_number');
    $supply_place = $this->input->post('supply_place');
    $state = $this->input->post('state');
    $address = $this->input->post('address');
    $terms = $this->input->post('terms');
    $sac = $this->input->post('sac');
    $ship_charge = $this->input->post('ship_charge');
    /////////////////////////////////////////////////
    $product_code = empty($this->input->post('product_code'))?array():$this->input->post('product_code');
    $product_name = $this->input->post('product_name');
    $hsn = $this->input->post('hsn');
    $qty = $this->input->post('qty');
    $unit_type = $this->input->post('unit_type');
    $price = $this->input->post('price');
    $tax = $this->input->post('tax');
    $total_product = count($product_code);
    $product_json = array();
    $total_expence = 0;
    if(empty($project)){
      echo json_encode(['status'=>403, 'message'=>'Please Select Any Project..']);
      exit();
    }
    if(empty($chllan_type)){
      echo json_encode(['status'=>403, 'message'=>'Please select Challan Type..']);
        exit();
    }
    if(empty($bill_to)){
      echo json_encode(['status'=>403, 'message'=>'Please Enter Bill To Detail..']);
      exit();
    }
    if(empty($po_number)){
      echo json_encode(['status'=>403, 'message'=>'Please Enter Po Number..']);
      exit();
    }

    if(empty($mobile)){
      echo json_encode(['status'=>403, 'message'=>'Please Enter Phone Number..']);
      exit();
    }
    if(empty($vehicle_number)){
      echo json_encode(['status'=>403, 'message'=>'Please Enter Vehicle Number']);
      exit();
    }
    if(empty($supply_place)){
      echo json_encode(['status'=>403, 'message'=>'Please Enter Supply Place']);
      exit();
    }
    if(empty($state)){
      echo json_encode(['status'=>403, 'message'=>'Please Enter State']);
      exit();
    }
    if(empty($address)){
      echo json_encode(['status'=>403, 'message'=>'Please Enter Address']);
      exit();
    }
    if(empty($terms)){
      echo json_encode(['status'=>403, 'message'=>'Please Enter Terms & Conditions']);
      exit();
    }
    if($total_product<=0){
      echo json_encode(['status'=>403, 'message'=>'Please select any one Product To create DC..']);
        exit();
    }
    for($i=0; $i<$total_product; $i++){  
      $product_json[] = array('product_code'=>$product_code[$i],'name'=>$product_name[$i],'hsn'=>$hsn[$i],'qty'=>$qty[$i],'unit_type'=>$unit_type[$i],'price'=>$price[$i],'tax'=>$tax[$i]);
      //print_r($qty[$products[$i]]);
      $get_product = $this->Product_model->get_product(array('products.product_code'=>$product_code[$i]));
      //print_r($get_product);die;
      if($get_product->unit <$qty[$i]){
        echo json_encode(['status'=>403, 'message'=>'Required Quantity must be Less Than or equal to Product Stocks..('.$product_code[$i].')']);
        exit();
      }
      $total_expence+= intVal($price[$i])*intVal($qty[$i]);
      if($qty[$i]<=0){
        echo json_encode(['status'=>403, 'message'=>'Required Quantity must be greater than zero..('.$product_code[$i].')']);
        exit();
      }
      if(empty($price[$i])){
        echo json_encode(['status'=>403, 'message'=>'Please Enter Product Price..('.$product_code[$i].')']);
        exit();
      }
      $updated_qty = intVal($get_product->unit) - intVal($qty[$i]);
      //print_r($updated_qty);die;
      $update_product = $this->Product_model->update_product(array('unit'=>$updated_qty),$get_product->id);
    }
    $check_dc = $this->Bom_model->get_dc_row(array('dc.status' =>1));
    //print_r($check_dc);die;
          if($check_dc){
            $challan_no = date('Ymd').$check_dc->id+1;
           }else{
            $challan_no = date('Ymd').'1';
           }
    $perticular = "DC ".$challan_no."Created For PO ".$po_number ; 

    $data = array(
      'project' => $project,
      'chllan_type' => $chllan_type,
      'challan_no' => $challan_no,
      'bill_to' => $bill_to,
      'delivery_mode' => $delivery_mode,
      'gstn' => $gstn,
      'mobile' => $mobile,
      'email' => $email,
      'fax' => $fax,
      'po_number' => $po_number,
      'vehicle_number' => $vehicle_number,
      'supply_place' => $supply_place,
      'state' => $state,
      'address' => $address,
      'terms' => $terms,
      'sac' => $sac,
      'ship_charge' => $ship_charge,
      'products' => json_encode($product_json),
      'added_by' => $this->session->userdata('id'),
    );
    //print_r($expence_data);die;
    $store_dc = $this->Bom_model->store_dc_data($data);
    
    
     if($store_dc){
      $expence_data = array(
        'project' => $project,
        'date' => date("Y-m-d"),
        'particular' => $perticular,
        'expence' => $total_expence,
        'added_by' => $this->session->userdata('id'),
      );
      //print_r($expence_data);die;
      $add_expence = $this->project_model->store_expence($expence_data);
        echo json_encode(['status'=>200, 'message'=>'DC Added successfully','id'=>base64_encode($store_dc)]);
      }else{
        echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
      }
    
    }

    public function download_dc(){
      $id = base64_decode($this->uri->segment(3));
      $data['title'] = 'Download Delivery Challan';
      $data['siteinfo'] = $this->siteinfo();
      $data['dc_data'] = $this->Bom_model->get_dc_row(array('dc.id'=> $id));
      //print_r($data['dc_data']);die;
      
      $this->load->library('pdf');
      $html = $this->load->view('bom/dc_pdf',$data, true);
      $this->pdf->createPDF($html, 'Delivery-Challan', false);
    }

    public function ajaxdc(){
      //ini_set('display_errors',1);
           $condition = array('dc.status'=>1);
        $dcs = $this->Bom_model->make_dcdatatables($condition); // this will call modal function for fetching data
        //print_r($dcs);die;
           
            
            $data = array();
            $sno = 1;
        foreach($dcs as $dc){
          $sub_array = array();
            $pdf_button= '<a href="'.base_url('Bom/download_dc/'.base64_encode($dc['id'])).'" target="_blank" title="View DC" class="btn  btn-sm  btn-primary"><i class="fa fa-download"></i> DC </button>';
            
            $sub_array[]= $sno++;           
            $sub_array[]= $pdf_button;
          
            $sub_array[]= $dc['challan_no'];
            $sub_array[]= $dc['po_number'];
            $sub_array[]= $dc['bill_to'];           
            $sub_array[]= $dc['mobile'];     
            $sub_array[]= $dc['user_name'];
            $sub_array[]= date('d-m-Y',strtotime($dc['cretaed_at']));
            //print_r($sub_array);die;
            $data[] = $sub_array;
      }
      
      //print_r($data);die;
      $output = array(
        //"draw"                    =>     intval($_POST["draw"]),
         "recordsTotal"            =>     $this->Bom_model->get_all_dcdata($condition),
         "recordsFiltered"         =>     $this->Bom_model->get_filtered_dcdata($condition),
        "data"                    =>     $data,
    );
    
    echo json_encode($output);
    }

  public function returnableProducts(){
    $data['title'] = 'Returnable Products Data';
    $data['return_data'] = $this->Product_model->get_returnables(array('retutnable_products.status'=>1));
    //print_r($data);die;
    $this->template('store/returnable',$data);
  }

  public function updateReturnable(){
    //print_r($_POST);die;
    $id = $this->input->post('id');
    $status = $this->input->post('status');
    $this->Product_model->updateReturnable(array('status'=>$status),$id);

  }

  public function in_materialHistory(){
    $data['title'] = 'In Material History';
    $data['material_data'] = $this->Bom_model->get_in_materials(array('in_material.status'=>1));
    $data['delivery_challan'] = $this->Bom_model->get_delivery_challan(array('status'=>1));
    //print_r($data);
  $this->template('store/in_material_history',$data);
  }

  public function out_materialHistory(){
    $data['title'] = 'Out Material History';
    $data['material_data'] = $this->Bom_model->get_out_materials(array('out_material.status'=>1));
   
    //print_r($data);
  $this->template('store/out_material_history',$data);
  }

  public function inquiry()
	{	 
		$data['title'] = 'Enquiry';
    $data['projects'] = $this->project_model->get_projects(array('projects.status'=>1));
        //print_r($data);die;
	  $this->template('bom/inquiry',$data);
		
	}

  public function disappreason(){
    //ini_set('display_errors',1);
    $id = $this->input->post('id');
    
    $quotation = $this->Bom_model->get_compare_quatations(array('compare_quotation.id'=>$id));
    //print_r($quotation);die;
    $compare = $quotation->row();
    $reason = json_decode($compare->disapprove_reason);
    //print_r($compare);die;
    $reasondata = '';
    $sno = 1;
    foreach($reason as $data){
      foreach($data as $key=>$row){
        $user = $this->user_model->get_user(array('id'=>$key));
        $reasondata .= '<tr><td>'.$sno.'</td><td>'.$user->name.'</td><td>'.$row.'</td></tr>';
        $sno++;
      }
      
    }
    echo $reasondata;
  }

  public function quotation(){
    $datas['title'] = 'Quotations';
    $datas['url'] = $this->uri->segment(1);
    $datas['projects'] = $this->project_model->get_projects(array('projects.status'=>1));   
    
    $datas['enq']='';
    $enq_id = array();
    $enquiries = $this->Bom_model->get_inquiries(array('status'=>1));
    $quotations = $this->Bom_model->get_allquotations(array('status'=>1));
    foreach($quotations as $quotation){
      $enq_id[]= $quotation['enq_id']; 
    }
    //print_r($enquiries);die;
    $sno = 1;
    foreach($enquiries as $enq){
      if(!in_array($enq['id'],$enq_id)){
        $bom = $this->Bom_model->get_bom(array('id'=>$enq['bom_id']));
        $user = $this->Bom_model->get_user(array('id'=>$enq['assigned_to']));
          $datas['enq'] .= "<tr>";
          $datas['enq'] .="<td nowrap>".$sno."</td>";
          $datas['enq'] .="<td nowrap>".$bom->title."</td>";
          $datas['enq'] .= "<td>".$user->name."</td>";
          $datas['enq'] .= "<td>".date('d-m-Y',strtotime($enq['created_at']))."</td>";
          $datas['enq'] .= "</tr>";
          $sno++;
      }   
    }
   
    //print_r($datas);die;
    
	  $this->template('bom/quotation',$datas);
  }

  public function disappr_quotations(){
    $allquotations = $this->Bom_model->get_allquotations(array('quote_by'=>$this->session->userdata('id')));
    //print_r($allquotations);die;
    foreach($allquotations as $key=>$quotation){
      $pcondition = array('product_code'=>$product_code);
      $product_detail = $this->Bom_model->get_partName($pcondition);
    }
  }

  public function po(){
    if($this->session->userdata('role_id')==4){
    $data['title'] = 'Order';
    }else{
      $data['title'] = 'Purchase Order';
    }
    $data['projects'] = $this->project_model->get_projects(array('projects.status'=>1));
        //print_r($data);die;
	  $this->template('bom/po',$data);
  }
  
  public function store(){
    $data['title'] = 'Manage Store';
    $data['projects'] = $this->Bom_model->get_bomProject(array('bom.status'=>1));
    //print_r($data);die;
	  $this->template('store/store',$data);
  }

  public function get_bom(){
    $bomID  = $this->input->post('bomID');
    //print_r($productId);die;
    $bom = $this->Bom_model->get_bom(array('id'=>$bomID));
    //print_r($bom);die;
    $products = json_decode($bom->product_detail);
    //print_r($products);die;
    
    $data = array();
    $data[]= ' <div id="" class="row"> 
    <table><tbody><div class="col-md-10"><tr><td class="row text-center align-center" style="width:100%"><div class="col-md-2 ml-5"><label>Enter Title :</label></div><div class=" col-md-6"><input type="text" id="title" name="title" value="'.$bom->title.'" class="form-control" placeholder="Enter Bom Title"></div> <div id="multipleEdit_form">';
    foreach($products as $product){
    // $data[] = '<tr>
                 
    //                <td><input type="hidden" name="bom_id" value="'.$bomID.'"><input type="text" class="form-control" id="product_code" name="product_code[]" value="'.$product->product_code.'" placeholder="Enter Product  Code"></td>
    //                <td><input type="text" class="form-control" name="qty[]" value="'.$product->qty.'" placeholder="Enter Quantity"></td>
    //             </tr> '; 
    $data[] = '<div class="required_inp ad_row " style="margin:2px;">
                <div class="w40"><input type="hidden" name="bom_id" value="'.$bomID.'"><input type="text" class="form-control w100" id="product_code" name="product_code[]" value="'.$product->product_code.'" placeholder="Enter Product Code"></div>
                <div class="w40"><input type="text" class="form-control w100" id="qty" name="qty[]" value="'.$product->qty.'" placeholder="Enter Quantity"></div>
                    <span class="customLook"></span><button type="button" class="inputRemove text-danger"><i class="fa fa-times"></i></button>
                </div>';
    }

    $data[] = ' <div class="required_inp ad_row " style="margin:2px;">
              <div class="w40"><input type="text" class="form-control w100" id="product_code" name="product_code[]"
                  placeholder="Enter Product Code"></div>
              <div class="w40"><input type="text" class="form-control w100" id="qty" name="qty[]"
                  placeholder="Enter Quantity"></div>
              <span class="customLook"></span><button type="button" id="addmore" onclick="addEditField()"><i
                  class="fa fa-plus"></i></button>
          </div></div>';
    echo json_encode($data);
  }

    public function bom_upload_excel(){
       
        $pdf = empty($_FILES['bom_pdf']['name'])?'':$_FILES['bom_pdf']['name'];
      $project = $this->input->post('project'); 
      $title = $this->input->post('title'); 
      if(empty($project)){
          echo json_encode(['status'=>403, 'message'=>'Please Select Project']);
           exit();
         }

         if(empty($title)){
          echo json_encode(['status'=>403, 'message'=>'Please Enter Bom Title']);
           exit();
         }
        
      $products = $this->Bom_model->get_bom(array('project_id'=>$project));
        // if(!empty($products)){
        //   echo json_encode(['status'=>403, 'message'=>'Bom Already added please choose Another Project']);
        //   exit();
        // }
     $this->load->library('upload');
     if(!empty($_FILES['bom_pdf']['name'])){
      $config = array(
       'upload_path' 	=> 'uploads/bom/pdf',
       'file_name' 	=> str_replace(' ','','bom').uniqid(),
       'allowed_types' => 'pdf',
       'max_size' 		=> '10000000',
      );
      $this->upload->initialize($config);
     if ( ! $this->upload->do_upload('bom_pdf'))
       {
           $error = $this->upload->display_errors();
           echo json_encode(['status'=>403, 'message'=>$error]); 	
           exit();
       }
       else
       {
         $type = explode('.', $_FILES['bom_pdf']['name']);
         $type = $type[count($type) - 1];
         $pdf = 'uploads/bom/pdf/'.$config['file_name'].'.'.$type;
       }
    }
        if (isset($_FILES["bom_excel"])) {
          //  echo "hi";die;
          $config['upload_path']   = "uploads/csv/bom";
          $config['allowed_types'] = 'text/plain|text/csv|csv';
          $config['max_size']      = '2048';
          $config['file_name']     = $_FILES["bom_excel"]['name'];
          $config['overwrite']     = TRUE;
          $this->load->library('upload', $config);
          $this->upload->initialize($config);
          //echo "hi";
          if (!$this->upload->do_upload("bom_excel")) {
            echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
          } else {
            $file_data = $this->upload->data();
            //print_r($file_data);die;
            $file_path = 'uploads/csv/bom/' . $file_data['file_name'];
            //print_r($this->csvimport->get_array($file_path));die;
            if ($this->csvimport->get_array($file_path)) {
              $csv_array = $this->csvimport->get_array($file_path); 
              $productCode = array(); 
              //print_r($csv_array);die;
              foreach($csv_array as $key => $value){
                
                $productCode[] = $value['product_code'];
                 if(! is_numeric(trim($value['qty'])) ||  $value['qty'] <= 0){
                  echo json_encode(['status'=>403, 'message'=>'Please Enter Valid number in Quantity.'.$value['product_code']]);
                  exit();
                }
               
              }
             if(empty(array_filter($productCode))){
              echo json_encode(['status'=>403, 'message'=>'Undefined Products..']);
                  exit();   
             }
               $duplicates = implode(',',array_diff_assoc($productCode,array_unique($productCode)));
               //print_r($duplicates);die;
              $message = "Duplicate products detected, please remove duplicate products</br>".$duplicates; 
              if($duplicates){
                //continue;
                  echo json_encode(['status'=>403, 'message'=>$message]);
                  exit();                
              }
            $bomProducts=array();
            //print_r($csv_array);die;
             foreach($csv_array as $key=>$row){
              $this->db->where('product_code',$row['product_code']);
              $product = $this->db->get('products')->row();
              $bomProducts[] = array('product_code'=>$product->product_code,'qty'=>$row['qty']);
              $check_product = $this->Product_model->get_product(array('product_code'=>$row['product_code']));
              if(empty($check_product)){
                //continue;
                  echo json_encode(['status'=>403, 'message'=>'Product Not Found please Enter correct product Code of Your Location...']);
                  exit();
                
              }

              if(empty($row['qty'])){
                echo json_encode(['status'=>403, 'message'=>'Please Enter correct qty...']);
                  exit();
              }
            //  echo $row['product_code'].$row['qty'];
            } 
              //  die;
              $product_detail = json_encode($bomProducts);
              //print_r($product_detail);die;
              $Id =  $this->session->userdata('id');
              $data = array(
                'added_by' => $Id,
                'project_id' => $project,
                'title' => $title,
                'product_detail' => $product_detail,
                'pdf' => $pdf,
            );
            //print_r($data);die;
            //$this->setting_model->update_zone_price($data,$id); 
            $this->Bom_model->store_bom($data);   
              echo json_encode(['status'=>200, 'message'=>'Bom Added Successfully']);
              exit();
            } else {
              echo json_encode(['status'=>403, 'message'=>'Bom Add Failure']);
              exit();
            }
          }
        } else {
          echo json_encode(['status'=>403, 'message'=>'Please upload CSV']);
          exit();
        }
      }
     
     public function get_boms(){
        $project_id = $this->input->post('id');
        $boms = $this->Bom_model->get_bomsQoutes(array('quotation.project_id'=>$project_id,'po'=>1));
        //print_r($boms);die;
        $data ='';
        $data.=' <option value="" disabled selected>Select</option>';
        foreach($boms as $bom){
         $data.=' <option value="'.$bom['quote_id'].'">Qoute By '.$bom['vendor_name'].'</option>';
        }
        echo $data;
      }

      public function get_pos(){
        //ini_set('display_errors',1);
        $project_id = $this->input->post('id');
        $pos = $this->Bom_model->get_pos(array('po.project_id'=>$project_id,'po.po_sent'=>1));
        //print_r($pos);die;
        $data ='';
        $data.=' <option value="" disabled selected>Select</option>';
        foreach($pos as $po){
         $data.=' <option value="'.$po['id'].'">'.$po['title'].' ('.$po['vendor_name'].')</option>';
        }
        echo $data;
      }

      public function get_bomsUsers(){
        $project_id = $this->input->post('id');
        $boms = $this->Bom_model->get_bomsUsers(array('bom.project_id'=>$project_id,'bom.status'=>1));
        //print_r($boms);die;
        $data ='';
        $data.=' <option value="" disabled selected>Select</option>';
        foreach($boms as $bom){
         $data.=' <option value="'.$bom['id'].'">'.$bom['name'].' ['.$bom['title'].']</option>';
        }
        echo $data;
      }
      public function projectFilter(){
        $id = $this->input->post('id');
        //print_r($id);die;
        $this->session->set_userdata('BomprojectID',$id);
      }

      public function resetProjectFilter(){
        
        $this->session->unset_userdata('BomprojectID');
      }

      public function ajaxassignedboms(){
        $bom_id = $this->uri->segment(3);
      //print_r($bom_id);die;
      
          $condition = array('bom.id'=>$bom_id);
       
        $boms = $this->Bom_model->make_datatables($condition); // this will call modal function for fetching data
        //print_r($boms); 
            $data = array();
        foreach($boms as $key=>$bom) // Loop over the data fetched and store them in array
        {
            $sub_array = array();
            $vendor_name = array();
            $pdf= $bom['pdf'];
            $boms = $this->Bom_model->make_datatables($condition);
            $pdf_button= '<a href='.$pdf.' target="_blank" title="View Pdf" class="btn  btn-sm  text-primary">PDF <i class="fa fa-eye"></i> </button>';
            $button = '';
            if($bom['project_status']!=2){
            $button .= '<button type="button" onclick="vendorAssignModal('.$bom['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Assign BOM" class="btn  btn-sm  btn-success m-2">Assign To Vendor </button>';
            $button .= '<button href="javascript:void(0)" onclick="editBomModal('.$bom['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Bom" class="btn  btn-sm  btn-primary ml-2"><i class="fa fa-edit"></i> </button>';
            //$button .= '<button href="javascript:void(0)" onclick="delete_bom('.$bom['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Bom" class="btn  btn-sm  btn-danger ml-2"><i class="fa fa-trash-o"></i> </button>';
            $button .= '<button href="javascript:void(0)" onclick="requestModal('.$bom['id'].')" class="btn btn-warning ml-2" data-toggle="modal" data-target="#requestModal" data-whatever="@fat"><i class="fa fa-plus"></i></button> ';
            $button .= '<button href="javascript:void(0)" onclick="discard_bom('.$bom['id'].','.$bom['project_id'].','.$bom['added_by'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Discard Bom" class="btn  btn-sm  btn-danger ml-2"><i class="fa fa-trash-o"></i> </button>';
            }else{

            $button .= '<span class="text-danger">Project Closed</span>';
            }
            $sub_array[] = $key+1;           
            $sub_array[] = $bom['title'];
            $sub_array[] = '<a href="javascript:void(0)" onclick="view_product('.$bom['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Product" class="btn  btn-sm  text-danger"><i class="fa fa-eye"></i> Products</a>';
            $sub_array[] = '<a href="'.base_url("requested-bom").'/'.$bom['id'].'" title="View Assigned" class="btn  btn-sm  text-danger"><i class="fa fa-eye"></i> Requested</a>';
           // $sub_array[] = $vendor_name;
            $sub_array[] = $bom['user_name'];
            $sub_array[] = (!empty($pdf))?$pdf_button:'<a href="javascript:void(0);" class="btn btn-sm text-primary">-</a>';     
            $sub_array[] = $button;
            $data[] = $sub_array;
            //print_r($sub_array);die;
      }
      
      $output = array(
        //"draw"                    =>     intval($_POST["draw"]),
        "recordsTotal"            =>     $this->Bom_model->get_all_data($condition),
        "recordsFiltered"         =>     $this->Bom_model->get_filtered_data($condition),
        "data"                    =>     $data,
    );
    
    echo json_encode($output);
      }

    public function downlodBomFormat(){
        //$filename = $this->session->userdata('zoneName') ? $this->session->userdata('zoneName')."_price_list.csv" : "all_zone_price_list.csv";
         $filename = base_url('uploads/csv/bom.csv');
         //print_r($filename);die;
          header('Content-type: text/csv');
          header('Content-disposition:attachment; filename="'.$filename.'"');
          $output = fopen('php://output', 'w');
          fputcsv($output, array('product_code','qty'));
          $data = $this->Product_model->get_products(array('products.id'=>1));
            //print_r($data);die;
            $data = array();
          foreach($data as $row) {
            $data[]= $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row['product_code']);
            $data[]=$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['qty']);
            
          } 
          fputcsv($output, $data);
          fclose($output);   
       }

       public function getProductRows(){
       
        $products = $this->input->post('products');
        foreach($products as $product){?>
          <div class="col-md-6">
              <input type="text" class="form-control" id="product_code" name="product_code[]" value="<?=$product?>" readonly>
          </div>
          <div class="col-md-6">
              <input type="text" class="form-control" id="qty" name="qty[]" placeholder="Enter Quantity"
                  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
          </div>
          <?php }
       }

       public function getDcProductRows(){
       
        $products = $this->input->post('products');
        foreach($products as $product){
          $product_data = $this->Product_model->get_product(array('products.product_code'=>$product));?>
          
        <tr>
          <td><input type="text" class="form-control" id="product_code" name="product_code[]" value="<?=$product?>" readonly><input type="hidden" id="product_name" name="product_name[]" value="<?=$product_data->part_name?>"></td>
          <td><input type="text" class="form-control" id="hsn" name="hsn[]" placeholder="HSN Code" ></td>
          <td><input type="text" class="form-control" id="qty" name="qty[]" placeholder="Enter Quantity"
                  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"></td>
          <td><input type="text" class="form-control" id="unit_type" name="unit_type[]" value="<?=$product_data->name?>" readonly></td>
          <td><input type="text" class="form-control" id="price" name="price[]" value="<?=$product_data->price?>" placeholder="Enter Price"></td>
          <td><input type="text" class="form-control" id="tax" name="tax[]" placeholder="Enter Tax in %"></td>
        </tr>
         
          <?php }
       }
      public function ajaxboms(){
        $project_id = $this->session->userdata('BomprojectID');
        if(!empty($project_id)){
          $condition = array('bom.status'=>1,'bom.project_id'=>$project_id);
        
        
        $boms = $this->Bom_model->make_datatables($condition); // this will call modal function for fetching data
        //print_r($boms); 
            $data = array();
            $menuID = 5;
		       $permission = $this->permissions()->$menuID;
        foreach($boms as $key=>$bom) // Loop over the data fetched and store them in array
        {
          $bom_data = $this->Bom_model->get_bom(array('id'=>$bom['id']));
          $enquiry = $this->Bom_model->get_inquiry(array('bom_id'=>$bom['id']));
         
         // print_r($bom_data->requested_products);die;
            $sub_array = array();
            $vendor_name = array();
            $pdf= $bom['pdf'];
            $inquiry = $this->Bom_model->get_inquiry(array('bom_id'=>$bom['id']));
            //print_r($inquiry);die;
            $boms = $this->Bom_model->make_datatables($condition);
            $pdf_button= '<a href='.$pdf.' target="_blank" title="View Pdf" class="btn  btn-sm  text-primary">PDF <i class="fa fa-eye"></i> </button>';
            $button = '';
            if($bom['project_status']!=2){
            $button .= '<button type="button" onclick="vendorAssignModal('.$bom['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Assign BOM" class="btn  btn-sm  btn-success m-2">Assign To Vendor </button>';
            if($permission[2]=='edit'){
              if(empty($enquiry)){
            $button .= '<button href="javascript:void(0)" onclick="editBomModal('.$bom['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Bom" class="btn  btn-sm  btn-primary ml-2"><i class="fa fa-edit"></i> </button>';
            }}//$button .= '<button href="javascript:void(0)" onclick="delete_bom('.$bom['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Bom" class="btn  btn-sm  btn-danger ml-2"><i class="fa fa-trash-o"></i> </button>';
            if($permission[5]=='excel'){
              $button .= '<button href="javascript:void(0)" onclick="downloadBom('.$bom['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Download Bom" class="btn  btn-sm  btn-success ml-2"><i class="fa fa-download"></i> Bom</button>';
              }
            if($bom_data->request_approve == 1){
              $request_status = '<span class="text-dark"> (Approved)</span>';
            }else if($bom_data->request_approve == 2){
              $request_status ='<span class="text-dark"> (Diss Approved)</span>';
            }else{
              $request_status = '<span class="text-dark"> (Pending)</span>';
            }
            if(empty($bom_data->requested_products)){
              $button .= '<button href="javascript:void(0)" onclick="requestModal('.$bom['id'].')" class="btn btn-warning m-2 btn-sm" data-toggle="modal" data-target="#requestModal" data-whatever="@fat"><i class="fa fa-plus"></i></button> ';
            }else{
            $button .= '<button href="javascript:void(0)" onclick="requestedPModal('.$bom['id'].')" class="btn btn-warning m-2 btn-sm" data-toggle="modal" data-target="#requestedPModal" data-whatever="@fat" title="View Requeted Products"><i class="fa fa-eye"></i> Request Products '.$request_status.'</button> ';
            }
            if(empty($enquiry)){
              $button .= '<button href="javascript:void(0)" onclick="discard_bom('.$bom['id'].','.$bom['project_id'].','.$bom['added_by'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Discard Bom" class="btn  btn-sm  btn-danger ml-2"><i class="fa fa-trash-o"></i> </button>';
            }
            }else{

            $button .= '<span class="text-danger">Project Closed</span>';
            }
            $sub_array[] = $key+1;           
            $sub_array[] = $bom['title'];
            $sub_array[] = '<a href="javascript:void(0)" onclick="view_product('.$bom['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Product" class="btn  btn-sm  text-danger"><i class="fa fa-eye"></i> Products</a>';
            $sub_array[] = !empty($inquiry)?'<a href="'.base_url("requested-bom").'/'.$bom['id'].'" title="View Assigned" class="btn  btn-sm  text-danger"><i class="fa fa-eye"></i> Requested</a>':'None';
           // $sub_array[] = $vendor_name;
            $sub_array[] = $bom['user_name'];
            $sub_array[] = (!empty($pdf))?$pdf_button:'<a href="javascript:void(0);" class="btn btn-sm text-primary">-</a>';     
            $sub_array[] = ($bom['material_status']==1)?'<strong class="text-success"> Issued</strong>':'<strong class="text-danger">Not Issued</strong>';
            $sub_array[] = date('d-F-Y', strtotime($bom['created_at']));  
            $sub_array[] = $button;
            $data[] = $sub_array;
            //print_r($sub_array);die;
      }
      
      $output = array(
        //"draw"                    =>     intval($_POST["draw"]),
        "recordsTotal"            =>     $this->Bom_model->get_all_data($condition),
        "recordsFiltered"         =>     $this->Bom_model->get_filtered_data($condition),
        "data"                    =>     $data,
    );
    
    echo json_encode($output);
    }else{
      echo json_encode(array());
    }
      }

      public function vendor_assigned(){
        $bom_id = $this->uri->segment(2);
        $data['title'] = 'Requests To Vendor';
        //print_r($bom_id);die;
        $data['enquiries'] = $this->Bom_model->get_assigneddata(array('bom_id'=>$bom_id));
        //print_r($enquiries);die;
        $this->template('bom/assigned_vendor',$data);
      }
      public function check_bom(){
        $id = $this->input->post('projectId');
        $products = $this->Bom_model->get_bom(array('project_id'=>$id));
        if(!empty($products)){
          echo json_encode(['status'=>403, 'message'=>'Bom Already added please choose Another Project']);
          exit();
        }
      }
    
    
    public function get_bomProducts(){
        //ini_set('display_errors',1);
        $id =$this->input->post('ID');
        
        $product = $this->Bom_model->get_po(array('po.id'=>$id)); 
        //print_r($product);die;
        $check_po = $this->Bom_model->get_in_material(array('po_id'=>$id));
        if(empty($check_po)){
        $sno = 1;
        $vendorID = $product->vendorID;
        $products = json_decode($product->product_detail);
        foreach($products as $value){
          //print_r($value);die;
          $row = json_decode($value);
          if($vendorID == $row->vendorID){
            $productID = 'productID@'.$vendorID;
            $product_code = $row->$productID;
            $qty_pro = 'qty@'.$vendorID;
            $qty = $row->$qty_pro;
            $pcondition = array('product_code'=>$product_code);
            $product_detail = $this->Bom_model->get_partName($pcondition);
            ?>
<tr>
    <td nowrap><?=$product_detail->part_name.' ('.$product_code.')'?></td>
    <td><input type="hidden" name="product_code[]" value="<?=$product_code?>"><input type="hidden" name="bom_id"
            value="<?=$product->bom_id?>"><input class="form-control" name="qty[<?=$product_code?>]"
            id="qty_<?=$product_code?>" class="form-control" value="<?=$qty?>" readonly></td>
    <td><input type="number" step="0.01" class="form-control" name="rcqty[<?=$product_code?>]"
            onkeyup="getRecivedItems('<?=$product_code?>')" id="rcqty_<?=$product_code?>"></td>
    <td><input type="number" step="0.01" class="form-control" name="rjqty[<?=$product_code?>]"
            onkeyup="getfinalQty('<?=$product_code?>')" id="rjqty_<?=$product_code?>"></td>
    <td><input class="form-control" name="nrqty[<?=$product_code?>]" id="nrqty_<?=$product_code?>" readonly></td>

</tr>
<?php  
      }
        } ?>
<tr>
    <td class="text-right" colspan="2"><input type="submit" class="btn btn-primary" value="Submit"></td>
</tr>
<?php 
      }else{ ?>
<tr>
    <td class="text-center" colspan="5">
        <p>Products Already Stored..</p>
    </td>
</tr>
<?php
      }
    }

      public function ApproveProductRequest(){
        
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        //$reason = (isset($this->input->post('reas')))?$this->input->post('status'):'';

        $update_bom = $this->Bom_model->update_bom(array('request_approve'=>$status),$id);
        //print_r($_POST);die;
        if($update_bom){
          echo json_encode(['status'=>200, 'message'=>'Product Request Approved Succefully!']);
        }else{
          echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
        }
        
      }
      public function get_bomProducts2(){
        
        $id =$this->input->post('ID');
        //print_r($id);die;
        $products = $this->Bom_model->get_products(array('id'=>$id,'request_approve'=>1)); 
        //print_r($products);die;
        $product_details= json_decode($products->product_detail);
        //print_r($product_details);die;
        $data = '';
          $Id =  $this->session->userdata('role_id');
         $requested_products= json_decode($products->requested_products);
         if(!empty($requested_products)){
          $data.='<tr><td colspan="5"><h5>Requested Products</h5></td></tr>';
         foreach($requested_products as $key=>$row){
          $rcondition = array('product_code'=>$row->product_code);
          $rproduct_data = $this->Product_model->get_product($rcondition);
          //print_r($rproduct_data);
          $remaining_qty = intval($row->qty) - intval($rproduct_data->unit) ;
          $sno = $key+1;
           $data.='<tr>';
           $rproductCode = "'".$row->product_code."'";
           $data.='<td><input type="hidden" name="bom_id" value="'.$products->id.'"><input type="hidden" name="project_id" value="'.$products->project_id.'"><input name="product_code[]" id="product_code_'.$row->product_code.'" class="form-control" value="'.$row->product_code.'" readonly></td>';
           $data.='<td><input name="part_name[]" class="form-control" value="'.$rproduct_data->part_name.'" readonly></td>';
           $data.='<td><input class="form-control" name="qty['.$rproduct_data->product_code.']" id="qty_'.$row->product_code.'" class="form-control" value="'.$row->qty.'" readonly></td>';
           $data.='<td><input class="form-control" type="text" value="'.$rproduct_data->unit.'" id="totalqty_'.$row->product_code.'" name="totalqty['.$rproduct_data->product_code.']" readonly></td>';
           $data.='<td><input class="form-control" name="iqty['.$rproduct_data->product_code.']" onkeyup="getBalanceqty('.$rproductCode.')" id="iqty_'.$rproduct_data->product_code.'"></td>';
           $data.='<td><input class="form-control" name="bqty['.$rproduct_data->product_code.']"  id="bqty_'.$rproduct_data->product_code.'" readonly></td>';
           $data.='</tr>';          
            $sno = $sno++;
         }}
          if(!empty($requested_products)){
            $data.="<tr id='returnForm' style='display: none;'><td colspan='6'>";
            $data.="<div id='multiple_Rform' class='row'><table><tr class='table-pt'><td>Product :<input type='text' class='form-control' id='product_name' name='product_name[]' placeholder='Enter Product Name'></td>";
            $data .="<td>Qty :<input type='number' class='form-control' id='product_qty' name='product_qty[]' value='1' placeholder='Enter Quantity'></td>";
            $data .="<td>Person :<input type='text' class='form-control' id='person_name' name='person_name[]' placeholder='Person Name'></td>";
            $data .="<td>Return Date :<input type='date' class='form-control' id='return_date' name='return_date[]'></td>";
            $data .="<td><span class='customLook'></span><button type='button' class='mt-4' id='addmoreR' onclick='addmore()'>+</button></td>";
            $data .="</tr></table></div></td></tr>";
          $data.='<tr><td class="text-right" colspan="2"><input type="submit" class="btn btn-primary" value="Submit"></td></tr>';
          }else{
            $data.='<tr><td class="text-center" colspan="6"><p>No Requests Found To Issue Products..</p></td></tr>';
            }
        
        echo $data;
      }

      
      public function get_requestedProducts(){
        
        $id=$this->input->post('id');
        $products = $this->Bom_model->get_products(array('id'=>$id)); 
        //print_r($products->request_approve);die;
        $request_status = ($products->request_approve==1)?'<span class="text-success">Approved</span>':(($products->request_approve==2)?'<span class="text-danger">Dis Approved</span>':'<span class="text-warning">Pending</span>');
        $requested_products= json_decode($products->requested_products);
        $button = ($products->request_approve ==0)?'<select class="btn btn-success" onchange="onoffswitchspublish(this.value,'.$id.')"><option selected disabled>Select</option><option value="1">Approve</option><option value="2">Disapprove</option></select>':'';

        $data = '';
         if(!empty($requested_products)){
          if($this->session->userdata('role_id')==1){$data.='<tr><th colspan="3">Action '.$button.'</th></tr>';}
         
          $data .='<tr><th nowrap>Product Code</th><th nowrap>Part Name</th><th nowrap>Qty</th></tr>';
          
         foreach($requested_products as $key=>$row){
          $rcondition = array('product_code'=>$row->product_code);
          $rproduct_data = $this->Product_model->get_product($rcondition);
          //print_r($rproduct_data);die;
          $data .='<tr>';
          $data .= '<td>'.$row->product_code.'</td>';
          $data .= '<td>'.$rproduct_data->part_name.'</td>';
          $data .= '<td>'.$row->qty.'</td></tr>';
         }
        }
        echo $data;
      }

      public function get_editBomproducts(){
        //print_r($_POST);die;
        $id =$this->input->post('ID');
        $products = $this->Bom_model->get_editBomproducts(array('id'=>$id)); 
        //print_r($products);die;
        if(!empty($products->updated_products)){
        $product_details= json_decode($products->updated_products);
        //print_r($product_details);die;
        $data = '';
          $Id =  $this->session->userdata('role_id');
          //print_r($Id);die;  
        foreach($product_details as $key=>$product){
          $condition = array('product_code'=>$product->product_code);
          $product_data = $this->Product_model->get_product($condition);
          //print_r($product_data);die;
          $remaining_qty = $product->qty - $product_data->unit ;
          if($remaining_qty<0){
            $remaining_qty = 0;
          }
          $sno = $key+1;
           $data.='<tr>';
           if($Id==2){
           $data.='<td>'.$sno.'</td>';
           }
           $data.='<td>'.$product->product_code.'</td>';
           $data.='<td>'.$product_data->part_name.'</td>';
           //$data.='<td><input name="make[]" class="no-border" value="'.$product_data->make.'" readonly></td>';
           $data.='<td>'.$product->qty.'</td>';
           //$data.='<td><input class="no-border" value="'.$product_data->unit.'" readonly></td>';
           //$data.='<td><input name="qty['.$product_data->product_code.']" id="qty_'.$product->product_code.'" class="no-border" value="'.$remaining_qty.'" readonly></td>';
           
           $data.='</tr>';
          
            $sno = $sno++;
         }
          
         
        echo $data;
        }
    }

    public function get_oldBomproducts(){
      //print_r($_POST);die;
      $id =$this->input->post('ID');
      $products = $this->Bom_model->get_editBomproducts(array('id'=>$id)); 
      //print_r($products);die;
      if(!empty($products->updated_products)){
      $product_details= json_decode($products->product_detail);
      //print_r($product_details);die;
      $data = '';
        $Id =  $this->session->userdata('role_id');
        //print_r($Id);die;  
      foreach($product_details as $key=>$product){
        $condition = array('product_code'=>$product->product_code);
        $product_data = $this->Product_model->get_product($condition);
        //print_r($product_data);die;
        $remaining_qty = $product->qty - $product_data->unit ;
        if($remaining_qty<0){
          $remaining_qty = 0;
        }
        $qty = !empty($product->qty) ? ($product->qty):0;
        $data.='<tr>';
        if($Id==2){
        $data.='<td>'.$sno.'</td>';
        }
        $data.='<td>'.$product->product_code.'</td>';
        $data.='<td>'.$product_data->part_name.'</td>';
        //$data.='<td><input name="make[]" class="no-border" value="'.$product_data->make.'" readonly></td>';
        $data.='<td>'.$qty.'</td>';
         //$data.='<td><input class="no-border" value="'.$product_data->unit.'" readonly></td>';
         //$data.='<td><input name="qty['.$product_data->product_code.']" id="qty_'.$product->product_code.'" class="no-border" value="'.$remaining_qty.'" readonly></td>';
         
         $data.='</tr>';
        
          $sno = $sno++;
       }
        
       
      echo $data;
      }
  }

    public function get_inmaterialproducts()
    {
      $id =$this->input->post('id');
      $products = $this->Bom_model->get_in_material(array('id'=>$id)); 
      //print_r($products);die;
      if(!empty($products->material_detail)){
      $product_details= json_decode($products->material_detail);
      //print_r($product_details);die;
      $data = '';
        $Id =  $this->session->userdata('role_id');
        //print_r($Id);die; 
        $sno=1; 
      foreach($product_details as $key=>$product){
        $condition = array('product_code'=>$product->product_code);
        $product_data = $this->Product_model->get_product($condition);
        //print_r($product_data);die;
        $remaining_qty = $product->qty - $product_data->unit ;
        if($remaining_qty<0){
          $remaining_qty = 0;
        }
        $sno++;
         $data.='<tr>';
         if($Id==2){
         $data.='<td>'.$sno.'</td>';
         }
         $data.='<td>'.$product->product_code.'</td>';
         $data.='<td>'.$product_data->part_name.'</td>';
         $data.='<td>'.$product_data->make.'</td>';
         $data.='<td>'.$product->qty.'</td>';
         $data.='<td>'.$product->rcqty.'</td>';
         $data.='<td>'.$product->rjqty.'</td>';
         $data.='<td>'.$product->nrqty.'</td>';
          
         $data.='</tr>';
        
          $sno = $sno++;
       }
        
       
      echo $data;
    }
    }

    public function get_outmaterialproducts()
    {
      $id =$this->input->post('id');
      $products = $this->Bom_model->get_out_material(array('id'=>$id)); 
      //print_r($products);die;
      if(!empty($products->material_detail)){
      $product_details= json_decode($products->material_detail);
      //print_r($product_details);die;
      $data = '';
        $Id =  $this->session->userdata('role_id');
        //print_r($Id);die; 
        $sno=1; 
      foreach($product_details as $key=>$product){
        $condition = array('product_code'=>$product->product_code);
        $product_data = $this->Product_model->get_product($condition);
        //print_r($product_data);die;
        $remaining_qty = $product->qty - $product_data->unit ;
        if($remaining_qty<0){
          $remaining_qty = 0;
        }
        $sno++;
         $data.='<tr>';
         if($Id==2){
         $data.='<td>'.$sno.'</td>';
         }
         $data.='<td>'.$product->product_code.'</td>';
         $data.='<td>'.$product_data->part_name.'</td>';
         $data.='<td>'.$product_data->make.'</td>';
         $data.='<td>'.$product->totalqty.'</td>';
         $data.='<td>'.$product->qty.'</td>';
         $data.='<td>'.$product->iqty.'</td>';         
         $data.='<td>'.$product->bqty.'</td>';
         
         $data.='</tr>';
        
          $sno = $sno++;
       }
        
       
      echo $data;
    }
    }
    
    public function export_bom(){
      //ini_set('display_errors',1);
      $id= $this->uri->segment(3);
      //print_r($id);
      
      $this->load->library('excel');
  
      $object = new PHPExcel();
      $object->setActiveSheetIndex(0);
      $table_columns = array("S.No", "Product Code","Part Name","Specification","Make","Qty", "Unit Type");
  
      $column = 0;
  
      foreach($table_columns as $field) {
          $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
          $column++;
      }
      $products = $this->Bom_model->get_products(array('id'=>$id)); 
        $vendors = $this->Bom_model->get_vendors(); 
        //print_r($products);die;
        $product_details= json_decode($products->product_detail);
        //print_r($product_details);die;
        $data = '';
          $Id =  $this->session->userdata('role_id');
          //print_r($Id);die;  
        foreach($product_details as $key=>$product){
          $condition = array('product_code'=>$product->product_code);
      $products_data = $this->Product_model->get_products(array('products.status'=>1));
      //$expence_data = $this->Project_model->get_expences(array('status'=>1));
      //print_r($products_data);die;
      $excel_row = 2;
      $count = 1;
      $products = $this->Bom_model->get_products(array('id'=>$id)); 
      
      $product_details= json_decode($products->product_detail);
      
      foreach($product_details as $key=>$product){
        $condition = array('product_code'=>$product->product_code);
        $product_data = $this->Product_model->get_product($condition);
       // print_r($product_data);die;
        
          $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $count++);
          $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $product->product_code);
          $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $product_data->part_name);
          $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $product_data->specification);
          $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $product_data->make);
          $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $product->qty);
          $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $product_data->name);
         
          $excel_row++;

      }
    }
      $this_date = "Bom".date("Y-m-d-H-i-s");
      $filename= $this_date.'.csv'; //save our workbook as this file name
      header('Content-Type: application/vnd.ms-excel; charset=UTF-8'); //mime type
      header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
      header('Cache-Control: max-age=0'); //no cache
  
      $objWriter = PHPExcel_IOFactory::createWriter($object, 'CSV');
      //ob_end_clean();
      $objWriter->save('php://output');
    
  }

    public function get_viewproducts()
      {
        $id =$this->input->post('BomID');
        $products = $this->Bom_model->get_products(array('id'=>$id)); 
        $vendors = $this->Bom_model->get_vendors(); 
        //print_r($products);die;
        if(!empty($products->product_detail)){
        $product_details= json_decode($products->product_detail);
        //print_r($product_details);die;
        $data = '';
          $Id =  $this->session->userdata('role_id');
          //print_r($Id);die;  
        foreach($product_details as $key=>$product){
          $condition = array('product_code'=>$product->product_code);
          $product_data = $this->Product_model->get_product($condition);
          //print_r($product_data);die;
          $remaining_qty = intVal($product->qty) - intVal($product_data->unit) ;
          if($remaining_qty<0){
            $remaining_qty = 0;
          }
          $sno = $key+1;
           $data.='<tr>';
           if($Id==4){
           $data.='<td>'.$sno.'</td>';
           }
           $data.='<td><input type="hidden" name="id" value="'.$products->id.'"><input type="hidden" name="project_id" value="'.$products->project_id.'"><input name="product_code[]" id="product_code_'.$product->product_code.'" class="no-border" value="'.$product->product_code.'" readonly></td>';
           $data.='<td><input name="part_name[]" class="no-border" value="'.$product_data->part_name.'" readonly></td>';
           $data.='<td><textarea name="make[]" readonly>'.$product_data->specification.'</textarea></td>';
            $data.='<td><input name="make[]" class="no-border" value="'.$product_data->make.'" readonly></td>';
           $data.='<td><input class="no-border" value="'.$product->qty.'" readonly></td>';
           $data.='<td><input class="no-border" value="'.$product_data->name.'" readonly></td>';
           $data.='<td><input class="no-border" value="'.$product_data->unit.'" readonly></td>';
           $data.='<td><input name="qty['.$product_data->product_code.']" id="qty_'.$product->product_code.'" class="no-border" value="'.$remaining_qty.'" readonly></td>';
           
           $data.='</tr>';
          
            $sno = $sno++;
         }
          
         
        echo $data;
      }
      }
      public function get_Assignedproducts()
      {
        $id =$this->input->post('ID');
        $vendorId = $this->input->post('vendorID');
        $products = $this->Bom_model->get_inquiry(array('id'=>$id,'assigned_to'=>$vendorId)); 
        $vendors = $this->Bom_model->get_user(array('id'=>$vendorId)); 
        //print_r($products);die;
        if(!empty($products->products)){
        $product_details= json_decode($products->products);
        //print_r($product_details);die;
        $data = '';
          $Id =  $this->session->userdata('role_id');
          //print_r($Id);die;  
          $sno =0;
        foreach($product_details as $products_data){
          foreach($products_data as $key=>$product){
            $condition = array('product_code'=>$key);
            $product_data = $this->Product_model->get_product($condition);
           //print_r($product_data);die;
            $sno = $sno + 1;
             $data.='<tr>';
            
             $data.='<td>'.$product_data->product_code.'</td>';
             $data.='<td>'.$product_data->part_name.'</td>';
             $data.='<td>'.$product_data->make.'</td>';
             $data.='<td>'.$product.'</td>';
            
             $data.='</tr>';
            
              $sno = $sno++;
          }
          
         }
          
         
        echo $data;
      }
      }
      public function get_products()
      {
        //ini_set('display_errors', 1);
        $id =$this->input->post('BomID');
        $products = $this->Bom_model->get_products(array('id'=>$id)); 
        $vendors = $this->Bom_model->get_vendors(); 
        $enquiries = $this->Bom_model->get_inquiries(array('bom_id'=>$id)); 
        $vendorsID = array();
         foreach($enquiries as $enqury){
          $user = $this->user_model->get_user(array('users.id'=>$enqury['assigned_to']));
          $produc = json_decode($enqury['products']);
          foreach($produc as $pros){
            foreach($pros as $key_pro=>$pro){

              $vendorsID[$key_pro.'@'.$user->name] = $user->name;
              //print_r($key_pro);
            }
            
          }
           
         }
        //print_r($vendorsID);
        $product_details= json_decode($products->product_detail);
        //print_r($product_details);die;
        $data = '';
          $Id =  $this->session->userdata('role_id');
          //print_r($Id);die;  
          
        foreach($product_details as $key=>$product){
          $condition = array('product_code'=>$product->product_code);
          $product_data = $this->Product_model->get_product($condition);
          //print_r($product_data);die;
          $remaining_qty = $product->qty - $product_data->unit ;
          if($remaining_qty<0){
            $remaining_qty = 0;
          }
          $sno = $key+1;
          ?>
<tr>
    <?php if($Id==7 || $Id==1){
              ?>
    <td>
        <div class="form-check checkbox checkbox-solid-info mb-0"><input type="checkbox" name="products[]"
                id="product_check_box_<?=$product->product_code?>" class="form-check-input"
                value="<?=$product->product_code?>"><label class="form-check-label"
                for="product_check_box_<?=$product->product_code?>"></label></div>
    </td>
    <td nowrap>
        <?php foreach($vendorsID as $ven_key=>$val){
    $data = explode('@',$ven_key);
    if($data[0]==$product->product_code){
      echo '<p>'.$data[1].'</p>';
    }
   // print_r($data);
   }
    ?>

    </td>
    <?php }
           if($Id==4){
            ?>
    <td><?=$sno?></td>
    <?php }?>
    <td><input type="hidden" name="id" value="<?=$products->id?>"><input type="hidden" name="project_id"
            value="<?=$products->project_id?>">
        <input name="product_code[]" id="product_code_<?$product->product_code?>" class="no-border"
            value="<?=$product->product_code?>" readonly>
    </td>
    <td><input name="part_name[]" class="no-border" value="<?=$product_data->part_name?>" readonly style="width:100%">
    </td>
    <td><textarea name="make[]" class="form-control" readonly><?=$product_data->specification?></textarea></td>
    <td><input name="make[]" class="form-control" value="<?=$product_data->make?>" readonly></td>
    <td><input class="form-control" value="<?=$product->qty?>" readonly></td>
    <td><input class="form-control" value="<?=$product_data->unit?>" readonly></td>
    <td><input name="qty[<?=$product_data->product_code?>]" id="qty_<?=$product->product_code?>" class="form-control"
            value="<?=$remaining_qty?>"></td>

</tr>
<?php
            $sno = $sno++;
         }
          
         if($Id==7 || $Id==1){
          ?>
<tr>
    <td colspan="2"><label><strong>Select Vendors :</strong></label></td>

    <?php
          $assiginTo = array();
          
          foreach($enquiries as $enquiry){
            $assiginTo[] = $enquiry['assigned_to'];
            
          }?>
    <td colspan="2"><select class="js-example-basic-multiple" name="vendors[]" multiple="multiple">
            <?php foreach($vendors as $key=>$vendor){?>
            <option value="<?=$vendor['id']?>"><?=$vendor['name']?></option>
            <?php
          }
          ?>
        </select></td>
</tr>
<tr>
    <td class="text-center" colspan="7"><input type="submit" class="btn btn-primary" value="Submit"></td>
</tr>
<?php
         }
         ?>
<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2({
        placeholder: "Select Vendor",
        allowClear: true
    });
});
</script>
<?php
      }

      public function requestBomProducts(){
        $id =$this->input->post('bomID');
        $products = $this->Bom_model->get_products(array('id'=>$id)); 
        
        //print_r($products);die;
        if(!empty($products->product_detail)){
        $product_details= json_decode($products->product_detail);
        $data = ''; 
        foreach($product_details as $key=>$product){
          $condition = array('product_code'=>$product->product_code);
          $product_data = $this->Product_model->get_product($condition);
          $sno = $key+1;
           $data.='<tr>';
           
           $data.='<td>'.$sno.'</td>';
           
      //      <td><input type="hidden" class="form-control digits" id="bomID" name="bom_id">
      //      <input type="text" class="form-control" id="product_code" name="product_code[]"
      //          placeholder="Enter Product Code">
      //  </td>
      //  <td><input type="number" class="form-control" id="qty" name="qty[]"
      //          placeholder="Enter Quantity"></td>

           $data.='<td><input type="hidden" name="bom_id" value="'.$products->id.'"><input name="product_code[]" class="no-border" value="'.$product->product_code.'" readonly></td>';
           $data.='<td><input class="no-border" value="'.$product_data->part_name.'" readonly></td>';
           $data.='<td><input class="no-border" value="'.$product_data->specification.'" readonly></td>';
           $cproductCode = "'".$product->product_code."'";
           $data.='<td><input type="hidden" id="bomqty_'.$product->product_code.'" value="'.$product->qty.'" ><input type="number" step="0.01" name="qty[]" class="form-control" id="qty_'.$product->product_code.'" value="'.$product->qty.'" onkeyup="checkbomqty(this.value,'.$cproductCode.')"></td>';
           
           $data.='</tr>';
          
            $sno = $sno++;
         }
          
         
        echo $data;
      }
      }


      public function get_inquiries()
      {
        $id =$this->input->post('EnID');
        $products = $this->Bom_model->get_inquiry(array('id'=>$id)); 
        $data = '';
        //print_r($products);die;
        $product= json_decode($products->products);
        //print_r($product);die;
        foreach($product as $key=>$row){
      
          foreach($row as $key=>$eqdata){
            //print_r($key);die;
            $product_code = $key;
            $qty = $eqdata;
            //echo $product_code;die;
            $pcondition = array('product_code'=>$product_code);
            $product_detail = $this->Bom_model->get_partName($pcondition);
            $productCode = "'$product_code'";
            //print_r($product_detail);die;
            $data.='<tr>';
            $data.='<td><input type="hidden" value="'.$products->id.'" name="id"><input type="hidden" value="'.$products->bom_id.'" name="bom_id"><input type="hidden" name="project_id" value="'.$products->project_id.'"><input type="hidden" value="'.$product_code.'" name="product_code[]">';
            
            $data.='<input name="part_name[]" class="no-border" value="'.$product_detail->part_name.'" readonly style="min-width: 160px;"></td>';
            $data.='<td><textarea class="form-control" readonly style="min-width: 160px;">'.$product_detail->specification.'</textarea></td>';
            $data.='<td><input class="form-control" value="'.$product_detail->make.'" readonly style="min-width: 160px;"></td>';
            
            $data.='<td><input name="qty['.$product_code.']" id="qty_'.$product_code.'" class="form-control" value="'.$qty.'" readonly></td>';
            $data.='<td><input type="number" name="price['.$product_code.']" step="0.01" class="form-control" onkeyup="get_tprice(this.value,'."'$product_code'".')"></td>';
            $data.='<td><input type="number" name="tax['.$product_code.']" step="0.01" class="form-control" placeholder="Enter Tax (%))"></td>';
            $data.='<td><input name="tprice['.$product_code.']" id="tprice_'.$product_code.'" class="form-control" readonly></td>';
        
        $data.='</tr>'; 
        

          }
          
      }
      $data.='<tr><td colspan="1"><label>Delivery Date :</label><input type="date" class="form-control" id="del_date" name="del_date" ></td>';
      $data.='<td colspan="1"><label>Fright Charge :</label><input type="number" step="0.01" class="form-control" id="fright" name="fright" placeholder="In Rupees"></td>';
      $data.='<td colspan="3"><label>Remark :</label><textarea class="form-control" id="qoute_remark" name="qoute_remark" placeholder="Enter Your Remark As Like Order Complete date etc.."></textarea></td></tr>';
      $data.='<tr><td class="text-center" colspan="4"><input type="submit" class="btn btn-primary" value="Submit"></td></tr>';
         
        echo $data;
      }

      public function requotation_form()
      {
        $id =$this->input->post('quID');
        $products = $this->Bom_model->get_quotations($id); 
        $data = '';
        //print_r($products);die;
        $product= json_decode($products->product_detail);
        
        foreach($product as $key=>$row){
          // echo "<pre>";
          // print_r($row);
            $product_code = $row->product_code;
            $qty = $row->qty;
            $price = $row->price;
            $tax = $row->tax;
            $tprice = $row->tprice;
            $pcondition = array('product_code'=>$product_code);
            $product_detail = $this->Bom_model->get_partName($pcondition);
            $productCode = "'$product_code'";
            //print_r($product_detail);die;
            $data.='<tr>';
            $data.='<td><input type="hidden" value="'.$products->id.'" name="id"><input type="hidden" value="'.$products->bom_id.'" name="bom_id"><input type="hidden" name="project_id" value="'.$products->project_id.'"><input type="hidden" value="'.$product_code.'" name="product_code[]">';
            
            $data.='<input name="part_name[]" class="no-border" value="'.$product_detail->part_name.'" readonly style="min-width: 160px;"></td>';
            $data.='<td><textarea class="form-control" readonly style="min-width: 160px;">'.$product_detail->specification.'</textarea></td>';
            $data.='<td><input class="form-control" value="'.$product_detail->make.'" readonly style="min-width: 160px;"></td>';
            
            $data.='<td><input name="qty['.$product_code.']" id="qty_'.$product_code.'" class="form-control" value="'.$qty.'" readonly></td>';
            $data.='<td><input type="number" name="price['.$product_code.']" step="0.01" class="form-control" onkeyup="get_tprice(this.value,'."'$product_code'".')" value="'.$price.'"></td>';
            $data.='<td><input type="number" name="tax['.$product_code.']" step="0.01" class="form-control" placeholder="Enter Tax (%))" value="'.$tax.'"></td>';
            $data.='<td><input name="tprice['.$product_code.']" id="tprice_'.$product_code.'" value="'.$tprice.'" class="form-control" readonly></td>';
        
        $data.='</tr>'; 
        

          
      }
      $data.='<tr><td colspan="1"><label>Delivery Date :</label><input type="date" class="form-control" id="del_date" value="'.$products->delivery_date.'" name="del_date" ></td>';
      $data.='<td colspan="1"><label>Fright Charge :</label><input type="number" step="0.01" class="form-control" id="fright" name="fright" value="'.$products->fright_charge.'" placeholder="In Rupees"></td>';
      $data.='<td colspan="3"><label>Remark :</label><textarea class="form-control" id="qoute_remark" name="qoute_remark" placeholder="Enter Your Remark As Like Order Complete date etc..">'.$products->qoute_remark.'</textarea></td></tr>';
      $data.='<tr><td class="text-center" colspan="4"><input type="submit" class="btn btn-primary" value="Submit"></td></tr>';
       $data.='<input type="hidden" id="quotID" name="quotID" value="'.$id.'">';  
        echo $data;
      }

      public function get_quotations()
      {
        $id =$this->input->post('ID');
        $pageName =$this->input->post('page_name');
        $products = $this->Bom_model->get_quotations($id); 
        $data = '';
        //print_r($products);die;
        $product= json_decode($products->product_detail);
        //print_r($product);die;
        foreach($product as $key=>$row){
            $product_code = $row->product_code;
            $qty = $row->qty;
            //echo $product_code;die;
            $pcondition = array('product_code'=>$product_code);
            $product_detail = $this->Bom_model->get_partName($pcondition);
            //print_r($product_detail);die;
            $data.='<tr>';
            $data.='<td>'.$product_detail->part_name.'</td>';
            $data.= '<td>'.$product_detail->specification.'</td>';
            $data.= '<td>'.$product_detail->make.'</td>';            
            $data.='<td>'.$qty.'</td>';
            $data.='<td>'.$row->price.'</td>';
            $data.='<td>'.$row->tprice.'</td>';
        
        $data.='</tr>'; 
          
      }
      if($pageName!='Quotations' && !empty($products->po_terms)){
        $data.='<tr><th colspan="6" class="text-danger border-bottom-primary">Terms & Conditions</th></tr>';
        $data.='<tr><td colspan="6">'.$products->po_terms.'</td></tr>';
      }
      
         
        echo $data;
      }

      public function get_po()
      {
        //ini_set('display_errors',1);
        $id =$this->input->post('id');
        $pageName =$this->input->post('page_name');
        $products = $this->Bom_model->get_po(array('po.id'=>$id));
        $vendorID =  $products->vendorID;
        $data = '';
        $product= json_decode($products->product_detail);

        foreach($product as $key=>$value){
          $row = json_decode($value);
          if($vendorID == $row->vendorID){
            $productID = 'productID@'.$vendorID;
            $product_code = $row->$productID;
            $qty_pro = 'qty@'.$vendorID;
            $qty = $row->$qty_pro;
            $price_pro = 'price@'.$vendorID;            
            $price = $row->$price_pro;
            $tax_pro = 'tax@'.$vendorID;
            $tax = $row->$tax_pro;           
            $totalPrice = intval($qty)* intval($price);
            $tax_amount = ($totalPrice*$tax)/100;
            $grand_total = $totalPrice + $tax_amount;
            $pcondition = array('product_code'=>$product_code);
            $product_detail = $this->Bom_model->get_partName($pcondition);
            ?>
<tr>
    <td><?=$product_detail->part_name?></td>
    <td><?=$product_detail->specification?></td>
    <td><?=$product_detail->make?></td>
    <td><?=$qty?></td>
    <td><?='₹ '.$price?></td>
    <td><?='₹ '.$totalPrice?></td>
    <td><?=!empty($tax)?$tax:0.?>%</td>
    <td><?='₹ '.$tax_amount?></td>
    <td><?='₹ '.$grand_total?></td>

</tr>
<?php  
      } }
      if($pageName!='Quotations' && !empty($products->po_terms)){
        ?>
<tr>
    <th colspan="6" class="text-danger border-bottom-primary">Terms & Conditions</th>
</tr>
<tr>
    <td colspan="6"><?=$products->po_terms?></td>
</tr>
<?php }
      
        
      }

      public function assignToVendor(){
       //print_r($_POST);die;
       $bom_id =$this->input->post('id');
       $project_id =$this->input->post('project_id');
       $products = $this->input->post('products') ? $this->input->post('products') : array() ;
       $vendors = $this->input->post('vendors') ? $this->input->post('vendors') : array();
       $userId = $this->session->userdata('id');
       $qty = $this->input->post('qty');
       $total_vendors = count($vendors);
       //print_r($qty);die;
       $total_toduct = count($products);
       if(empty($products)){
        echo json_encode(['status'=>403, 'message'=>'Please select atleast one product to assign Bom']);
        exit();
      }

      if($total_vendors<1){
        echo json_encode(['status'=>403, 'message'=>'Please select atleast one vendor to assign Bom']);
        exit();
      }
      $product_json = array();
       
    //   foreach($products as $product1){
    //       echo "<pre>";
    //       echo $product1;
    //       print_r($qty);
    //       print_r($qty[$product1]);
           
    //   }
       
      for($i=0; $i<$total_toduct; $i++){  
          $products[$i];
         $product_json[] = array($products[$i]=>$qty[$products[$i]]);
         //print_r($qty[$products[$i]]);die;
         if($qty[$products[$i]]<=0){
          echo json_encode(['status'=>403, 'message'=>'Required Quantity must be greater than zero..'.$products[$i]]);
          exit();
         }
      }
       

       $enquiries = $this->Bom_model->get_inquiries(array('bom_id'=>$bom_id)); 
       $vendorsID = array();
        foreach($enquiries as $enqury){
         $user = $this->user_model->get_user(array('users.id'=>$enqury['assigned_to']));
         $produc = json_decode($enqury['products']);
         foreach($produc as $pros){
           foreach($pros as $key_pro=>$pro){

             $vendorsID[$key_pro.'@'.$user->id] = $user->name;
             //print_r($key_pro);
           }
           
         }
          
        }
     $mesage = array();
      foreach($vendorsID as $ven_key=>$vendor){
        $data = explode('@',$ven_key);
        if(in_array($data[0],$products) && in_array($data[1],$vendors)){
         $mesage[] = $vendor .' all ready add this product code is '.$data[0].'<br>';
        }
      }

      if(!empty($mesage)){
        echo json_encode(['status'=>403, 'message'=>json_encode($mesage)]);
        exit();
      }
   
       //print_r($product_json);die;
       $add_quote = array();
       foreach($vendors as $key=>$vendor){
        $data = array(
          'bom_id' => $bom_id,
          'project_id' =>$project_id,
          'products' =>  json_encode($product_json),
          'assigned_to' => $vendors[$key],
          'assigned_by' => $userId,
          );
          $add_quote[] = $this->Bom_model->store_qoute($data); 
       } 
          if($add_quote){
           foreach($vendors as $send_vendor){
            $user_vendor =  $this->user_model->get_user(array('users.id'=>$send_vendor));
            $email = $user_vendor->email;
            $subject = 'Assign Quotation';
            $html = "Dear ".$user_vendor->name."<br> You Have Recieved New enquiry, please send quotestion hurry<br> Thank you";
            sendEmail($email,$subject,$html);

            $notification_data = array(
              'sendor_id' => $userId,
              'reciever_id' => $send_vendor,
              'msg' => 'Recieved New enquiry',
              'url' => 'enquiry',
            );
            $this->Bom_model->store_notification($notification_data); 
           }


          echo json_encode(['status'=>200, 'message'=>'Quotation Assigned successfully!']);
          }else{
          echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
        }
      }
      
      public function store_bom(){
          //ini_set('display_errors',1);
        $project_id = $this->input->post('project');
        $title = $this->input->post('title');
        $product_code = $this->input->post('product_code');
        $qty = $this->input->post('qty');
        //print_r($qty);die;
        $total_product = count($product_code);
        $product_json = array();
        if(empty($project_id)){
          echo json_encode(['status'=>403, 'message'=>'Please Select Project']);
          exit();
        }
        if(empty($title)){
          echo json_encode(['status'=>403, 'message'=>'Please Enter Bom Title']);
          exit();
        }
        for($i=0; $i<$total_product; $i++){  
          $product_json[] = array('product_code'=>$product_code[$i],'qty'=>$qty[$i]);
          $product_data = $this->Product_model->get_product(array('products.product_code'=>$product_code[$i]));
          if(empty($product_data)){
            echo json_encode(['status'=>403, 'message'=>'Please Enter Valid Product Code']);
            exit();
          }
          if(empty($qty[$i])){
            echo json_encode(['status'=>403, 'message'=>'Please Enter Qty']);
            exit();
          }
        }
        //$bom = $this->Bom_model->get_bom(array('id'=>$bom_id));
        $user_id = $this->session->userdata('id');
        $data = array(
          'project_id' => $project_id,
          'title' => $title,
          'product_detail' =>  json_encode($product_json),
          'added_by' => $user_id,
          );

       // print_r($data);die;
        $bom = $this->Bom_model->store_bom($data);
         if($bom){
            echo json_encode(['status'=>200, 'message'=>'Bom successfully Added']);
             }else{
              echo json_encode(['status'=>302, 'message'=>'Something went wrong']);   
             }
      }

      public function update_bom(){
        ///print_r($_POST);die;
        $bom_id = $this->input->post('bom_id');
        $project_id = $this->input->post('project_id');
        $title = $this->input->post('title');
        $product_code = $this->input->post('product_code');       
        $qty = $this->input->post('qty');
        //print_r($product_code);die;
        $total_product = count(array_filter($product_code));
        if(empty($total_product)){
          echo json_encode(['status'=>403, 'message'=>'Please Enter Alteast One Product Code & Qty']);
          exit();
        }
        
        //print_r($total_product);die;
        //$product_json = array();
        for($i=0; $i<$total_product; $i++){  
          $product_json[] = array('product_code'=>$product_code[$i],'qty'=>$qty[$i]);
          $product_data = $this->Product_model->get_product(array('products.product_code'=>$product_code[$i]));
          if(empty($product_data)){
            echo json_encode(['status'=>403, 'message'=>'Please Enter Valid Product Code']);
            exit();
          }
          if(empty($qty[$i])){
            echo json_encode(['status'=>403, 'message'=>'Please Enter Qty']);
            exit();
          }
        }
        $bom = $this->Bom_model->get_bom(array('id'=>$bom_id));
        $user_id = $this->session->userdata('id');
        $data = array(
          'bom_id' => $bom_id,
          'project_id' => $bom->project_id,
          'product_detail' =>  $bom->product_detail,
          'updated_products' => json_encode($product_json),
          'update_by' => $user_id,
          );

        //print_r($data);die;
        $this->Bom_model->insert_bom_history($data);
          $update_bom = $this->Bom_model->update_bom(array('title' => $title,'product_detail' =>  json_encode($product_json)),$bom_id);
          if($update_bom){
            echo json_encode(['status'=>200, 'message'=>'Bom successfully Updated']);
             }else{
              echo json_encode(['status'=>302, 'message'=>'Something went wrong']);   
             }
      }

      public function send_request(){        
        //print_r($_POST);die;
        $bom_id = $this->input->post('bom_id');
        $product_code = $this->input->post('product_code');       
        $qty = $this->input->post('qty');
        ///$req_comment = $this->input->post('req_comment');
        //print_r($qty);die;
        $total_product = count($product_code);
        //$product_json = array();
        for($i=0; $i<$total_product; $i++){  
          $product_json[] = array('product_code'=>$product_code[$i],'qty'=>$qty[$i]);
          $product_data = $this->Product_model->get_product(array('products.product_code'=>$product_code[$i]));
          if(empty($product_data)){
            echo json_encode(['status'=>403, 'message'=>'Please Enter Valid Product Code']);
            exit();
          }
          if(empty($product_code[$i])){
            echo json_encode(['status'=>403, 'message'=>'Please Enter Product Code']);
            exit();
          }
          if(empty($qty[$i])){
            echo json_encode(['status'=>403, 'message'=>'Please Enter Qty']);
            exit();
          }
        }

        $id = $this->session->userdata('id');
          $update_bom = $this->Bom_model->update_bom(array('requested_products' =>  json_encode($product_json)),$bom_id);
          
          if($update_bom){
            
              $user =  $this->user_model->get_user(array('users.id'=>$id));
              $admin =  $this->user_model->get_user(array('users.id'=>1));
              $bom = $this->Bom_model->get_bomsProjects(array('bom.id'=>$bom_id));
              $email = $admin->email;
              $subject = 'Request For Products';
              $html = "Dear ".$admin->name."<br>" .$user->name." send a request for products  Project - ".$bom->project_name."Bom Title ".$bom->title."<br>Please Take action For approval Thank you";
              sendEmail($email,$subject,$html);

              $notification_data = array(
                'sendor_id' => $id,
                'reciever_id' => $admin->id,
                'msg' => $user->name." send a request for Products,  Project - ".$bom->project_name."Bom - ".$bom->title,
                'url' => 'bom',
              );
              $this->Bom_model->store_notification($notification_data); 
             
            echo json_encode(['status'=>200, 'message'=>'Request Sent successfully..']);
             }else{
              echo json_encode(['status'=>302, 'message'=>'Something went wrong']);   
             }
      }

      public function delete_bom(){
        $bomID =  $this->input->post('bomId');
        $bom = $this->Bom_model->delete_bom(array('id' => $bomID));
        if($bom){
          echo json_encode(['status'=>200, 'message'=>'Bom successfully deleted']);
           }else{
            echo json_encode(['status'=>302, 'message'=>'Something went wrong']);   
           }
      }

      public function discard_bom(){
        //print_r($_POST);die;
        $bomID =  $this->input->post('bomId');
        $addedBy =  $this->input->post('addedBy');
        $reason = $this->input->post('reas');
        $projectId = $this->input->post('projectId');
        $id = $this->session->userdata('id');
        if(!empty($reason)){
        $data = array(
          'bom_id' => $bomID,
          'project_id' => $projectId,
          'reason' => $reason,
          'upload_by' => $addedBy,
          'discard_by' => $id,
        );
        
        $this->Bom_model->update_bom(array('status' => 0),$bomID);
        $bom = $this->Bom_model->store_discardBom($data);
      }
        if($bom){
            $user =  $this->user_model->get_user(array('users.id'=>$addedBy));       
            $admin =  $this->user_model->get_user(array('users.id'=>$id));
            $bom = $this->Bom_model->get_bom(array('id'=>$bomID));
            $email = $admin->email;
            $subject = 'Bom Discrded by '.$admin->name;
            $html = "Dear ".$user->name."<br>" .$admin->name." Discarded Bom Title ".$bom->title.", Project ".$bom->project_name."<br>Please Add Bom Again, Thank you";
            sendEmail($email,$subject,$html);
            
            $notification_data = array(
          'sendor_id' => $id,
          'reciever_id' => $addedBy,
          'msg' => "Bom Discarded Title ".$bom->title.", Project ".$bom->project_name,
          'url' => 'quotation',
        );
        $this->Bom_model->store_notification($notification_data); 
          echo json_encode(['status'=>200, 'message'=>'Bom successfully Discarded']);
           }else{
            echo json_encode(['status'=>302, 'message'=>'Something went wrong']);   
           }
      }

      public function ajaxEnquiries(){
        $condition = array('quotations_inquiry.status'=>1);
     // echo  $this->session->userdata('user_type');
        $enquotations = $this->Bom_model->make_Qoutedatatables($condition); // this will call modal function for fetching data
        $quotation = $this->session->userdata('user_type')=='Vendor' ? $this->Bom_model->get_allquotations(array('quote_by'=> $this->session->userdata('id'))) : $this->Bom_model->get_allquotations(array('status'=>1));
        //print_r($quotation);die;
        $enq_id = array(); 
        $quot_status =array();
        $quotetationID =array();
        foreach($quotation as $quote){
          $enq_id[]=$quote['enq_id'];
          $quot_status[$quote['enq_id']] = $quote['status'];
          $quotetationID[$quote['enq_id']] = $quote['id'];
        }
      //  echo "<pre>";
      //    print_r($quot_status);die;
            $data = array();
        foreach($enquotations as $key=>$quotation) // Loop over the data fetched and store them in array
        {
           
          $products = json_decode($quotation['products']);
          
                  $sub_array = array();
                  $button = '';
                  
                  $button .= !in_array($quotation['id'],$enq_id) ? '<button type="button" onclick="sendQuoteModal('.$quotation['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Send Qutation" class="btn  btn-sm  btn-danger"><i class="fa fa-eye"></i> Products / Send Qutation </button>': ($quot_status[$quotation['id']]== 4 ? '<a  href="javascript:void(0)" class="btn btn-danger" onclick="editqutation('.$quotetationID[$quotation['id']].')"><i class="fa fa-close"></i> Quotation Reject Please Requote</a>' :'<a  href="javascript:void(0)" class="btn btn-success"><i class="fa fa-check"></i> Quotation Sent</a>');
                  //$button .= '<a href="javascript:void(0)" onclick="delete_bom('.$quotation['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Bom" class="btn  btn-sm  text-danger"><i class="fa fa-trash-o"></i> </a>';
                  $sub_array[] = $key + 1;           
                  $sub_array[] = 'ENQUIRY '.($key+1);
                  //$sub_array[] = '<a href="javascript:void(0)" onclick="view_product('.$quotation['bom_id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Product" class="btn  btn-sm  text-danger"><i class="fa fa-eye"></i> Products</a>';
                  $sub_array[] = date('d-F-Y', strtotime($quotation['created_at']));  
                  $sub_array[] = $button;
                  $data[] = $sub_array;
               
                $product_qty = $data[0];
                
      }
      
        $output = array(
          //"draw"                    =>     intval($_POST["draw"]),
          "recordsTotal"            =>     $this->Bom_model->get_all_enq_data($condition),
          "recordsFiltered"         =>     $this->Bom_model->get_enq_filtered_data($condition),
          "data"                    =>     $data,
      );
    
      echo json_encode($output);
    
      }
      
      public function sendQuotation(){
        //print_r($_POST);die;
        $id = $this->input->post('id');
        $bom_id = $this->input->post('bom_id');
        $project_id = $this->input->post('project_id');
        $product_code = $this->input->post('product_code');
        $fright = $this->input->post('fright');
        $qty = $this->input->post('qty');
        $price = $this->input->post('price');
        $tprice = $this->input->post('tprice');
        $total_product = count($product_code);
        $del_date = $this->input->post('del_date');
        $remark = $this->input->post('qoute_remark');
        $tax = !empty($this->input->post('tax'))?$this->input->post('tax'):array();
        //print_r($tax);die;
        
      $product_json = array();
      for($i=0; $i<$total_product; $i++){  
        $taxs = !empty($tax[$product_code[$i]]) ? $tax[$product_code[$i]] : 0;
        $product_json[$product_code[$i]] = array('product_code'=>$product_code[$i],'qty'=>$qty[$product_code[$i]],'price'=>$price[$product_code[$i]],'tprice'=>$tprice[$product_code[$i]],'tax'=>$taxs);
       
        if($price[$product_code[$i]]==''){
          echo json_encode(['status'=>403, 'message'=>'Please Enter Price Per Qty']);
          exit();
        }
        
      }
      if(empty($del_date)){
        echo json_encode(['status'=>403, 'message'=>'Please Choose Delivery Date']);
        exit();
      }
      // if(empty($remark)){
      //   echo json_encode(['status'=>403, 'message'=>'Please Enter Remark']);
      //   exit();
      // }
      $user_id = $this->session->userdata('id');
      //$approval =  json_encode([$user_id,0,'Pending']);
      $data = array(
            'enq_id' => $id,
            'bom_id' => $bom_id,
            'project_id' => $project_id,
            'quote_by' => $user_id,
            'product_detail	' =>  json_encode($product_json),
            'delivery_date' => $del_date,
            'fright_charge' => $fright,
            'qoute_remark' => $remark,
            //'approval' => $approval,
      );
      //print_r($data);die;
      $send_quote = $this->Bom_model->send_qoute($data); 
      
      if($send_quote){
        
        $user =  $this->user_model->get_user(array('users.id'=>$user_id));       
        $admin =  $this->user_model->get_user(array('users.id'=>1));
        $bom = $this->Bom_model->get_bom(array('id'=>$bom_id));
        $email = $admin->email;
        $subject = 'Quotation Recived From '.$user->name;
        $html = "Dear ".$admin->name."<br>" .$user->name." send a Quotation for products  Of Bom Title ".$bom->title."<br>Please Take action For approval, Thank you";
        sendEmail($email,$subject,$html);

        $notification_data = array(
          'sendor_id' => $user_id,
          'reciever_id' => $admin->id,
          'msg' => 'Recieved New Quotation',
          'url' => 'quotation',
        );
        $this->Bom_model->store_notification($notification_data); 
             
        echo json_encode(['status'=>200, 'message'=>'Quotation Send successfully!']);
      }else{
        echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
      }

    }

    public function sendReQuotation(){
      //print_r($_POST);die;
      ini_set('display_errors',1);
      $user_id = $this->session->userdata('id');
      $quotID = $this->input->post('quotID');
      $id = $this->input->post('id');
      $bom_id = $this->input->post('bom_id');
      $project_id = $this->input->post('project_id');
      $product_code = $this->input->post('product_code');
      $fright = $this->input->post('fright');
      $qty = $this->input->post('qty');
      $price = $this->input->post('price');
      $tprice = $this->input->post('tprice');
      $total_product = count($product_code);
      $del_date = $this->input->post('del_date');
      $remark = $this->input->post('qoute_remark');
      $tax = !empty($this->input->post('tax'))?$this->input->post('tax'):array();
      //print_r($tax);die;
      $check_quot_exits = $this->Bom_model->check_quotation(array('bom_id'=>$bom_id,'project_id'=>$project_id,'quote_by'=>$user_id,'status'=>1));
      if(count($check_quot_exits) <= 0){
    $product_json = array();
    for($i=0; $i<$total_product; $i++){  
      $taxs = !empty($tax[$product_code[$i]]) ? $tax[$product_code[$i]] : 0;
      $product_json[$product_code[$i]] = array('product_code'=>$product_code[$i],'qty'=>$qty[$product_code[$i]],'price'=>$price[$product_code[$i]],'tprice'=>$tprice[$product_code[$i]],'tax'=>$taxs);
     
      if($price[$product_code[$i]]==''){
        echo json_encode(['status'=>403, 'message'=>'Please Enter Price Per Qty']);
        exit();
      }
      
    }
    if(empty($del_date)){
      echo json_encode(['status'=>403, 'message'=>'Please Choose Delivery Date']);
      exit();
    }

    $data = array(
  
          'product_detail	' =>  json_encode($product_json),
          'delivery_date' => $del_date,
          'fright_charge' => $fright,
          'qoute_remark' => $remark,
          'status' => 1,
    );
    //print_r($data);die;
    $send_quote = $this->Bom_model->update_quotation($data,$quotID); 
    
    if($send_quote){
      $history_data = array(
        'q_id' => $quotID,
        'enq_id' => $id,
        'project_id'=> $project_id,
        'bom_id'=> $bom_id,
        'vendor_id'=> $user_id,
        'approval_by'=> $user_id,
        'status'=> 1,
        'reason'=> $remark,
        'product_detail' =>json_encode($product_json)
  
      );
      //print_r($history_data);die;
      $this->Bom_model->store_qouteHistory($history_data);


      $user =  $this->user_model->get_user(array('users.id'=>$user_id));       
      $admin =  $this->user_model->get_user(array('users.id'=>1));
      $bom = $this->Bom_model->get_bom(array('id'=>$bom_id));
      $email = $admin->email;
      $subject = 'Quotation Recived From '.$user->name;
      $html = "Dear ".$admin->name."<br>" .$user->name." send a Re-Quotation for products  Of Bom Title ".$bom->title."<br>Please Take action For approval, Thank you";
      //sendEmail($email,$subject,$html);
           
      echo json_encode(['status'=>200, 'message'=>'Quotation Send successfully!']);
    }else{
      echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
    }
  }else{
    echo json_encode(['status'=>302, 'message'=>'Quotation already Quoted!']);
  }

  }

    public function ajaxQuotations(){
      //echo $this->session->userdata('id');
      $permissions = $this->permissions();
      $permis = array();
        foreach($permissions as $keys=>$permission){
        $permis[$keys] = $permission;
        }
        //print_r($permis);die;
      if($this->session->userdata('role_id')==4){
      $condition = array('quotation.quote_by'=>$this->session->userdata('id'));
      }else{
          $condition = array('quotation.status'=>1);
      }
      $page_name = $this->uri->segment(2);      
      $quotations = $this->Bom_model->make_Qoutationdatatables($condition,$page_name); // this will call modal function for fetching data
       //print_r($quotations); die;
          $data = array();
      foreach($quotations as $key=>$quotation) // Loop over the data fetched and store them in array
      {
          $po_condition = array('po.project_id'=>$quotation['project_id'],'po.bom_id'=>$quotation['bom_id']);
          $po = $this->Bom_model->get_pos($po_condition);
          //print_r($po);
        //  if($po->vendorID == $this->session->userdata('id')){
        //     // echo "hi<br>";
        //  }
        $products = json_decode($quotation['product_detail']);
        
                $sub_array = array();
                $button = '';
                $status = '';
               
                $approval = ($quotation['approval']==1)?'checked':'';
                $approveselect = ($quotation['approval']==1)?'selected':'';
                $disapproveselect = ($quotation['approval']==2)?'selected':'';
                if($this->session->userdata('role_id')==4){
                $button .= '<div style="display: flex;flex-wrap: nowrap;align-items: center;"><button type="button" onclick="viewQuoteModal('.$quotation['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Qutation" class="btn btn-sm  btn-warning"><i class="fa fa-eye"></i> Quotation </button>&nbsp;';
                //  $button .= '<select class="badge badge-success mr-2" onchange="onoffswitchspublish(this.value,'.$quotation['id'].')"><option selected disabled>Select</option><option vlaue="1" '.$approveselect.'>Approve</option><option vlaue="2"'.$disapproveselect.'>Disapprove</option></select>';
                //$button .= '<div class="icon-state mt-2"><label class="switch square-checked"><input type="checkbox" '.$approval.' id="primaryswitchStatus" onclick="onoffswitchspublish('.$quotation['id'].')"><span class="switch-state bg-success rounded-2" title="Approval"></span></label></div>&nbsp;';
               // $button .= '<button href="javascript:void(0)" onclick="delete_bom('.$quotation['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Bom" class="btn btn-sm btn-danger"><i class="fa fa-trash-o text-lg"></i> </button></div>';
                }
                else{
                  $title_condition = count($po) > 0 ? 'Po Generated' : 'All Quotation ';
                  $css_condition = count($po) > 0 ? 'btn btn-sm  btn-success' : 'btn btn-sm  btn-warning ';
                $button .= '<div style="display: flex;flex-wrap: nowrap;align-items: center;"><a href="'.base_url('view-quotations/').base64_encode($quotation['bom_id']).'" title="View Qutation" class="'.$css_condition.'"><i class="fa fa-eye"></i> '.$title_condition.'</a>&nbsp;';

                }
                $sub_array[] = $key + 1;           
                // echo "<pre>";
                // echo $po->vendorID; echo "</pre>";
                if($this->session->userdata('role_id')==4){
                    $status_btn = "";
                    
                    $status_btn = empty($po) ? '<span class="text-warning">Pending</span>' : ($po->vendorID == $this->session->userdata('id') ? '<span class="text-success">Po Generated</span>' : '<span class="text-danger">Rejected</span>') ;
                $sub_array[] = 'QUOTATION ' .($key+1);
                $sub_array[] = '<textarea readonly class="form-control">'.$quotation['qoute_remark'].'</textarea>';
                $sub_array[] = date('d F Y', strtotime($quotation['delivery_date'])); 
                $sub_array[] = date('d F Y', strtotime($quotation['created_at'])); 
                $sub_array[] = $status_btn;
                }
                if($this->session->userdata('role_id')!=4){ 
                $sub_array[] = $quotation['name'];
                $sub_array[] = $quotation['title'];
                //$sub_array[] = $quotation['vendor_name'];
                $sub_array[] = date('d/m/Y', strtotime($quotation['updated_at'])); 
                
                }
               
                $sub_array[] = $button;
                $data[] = $sub_array;
             
              $product_qty = $data[0];
              
    }
    
      $output = array(
         //"draw"                    =>     intval($_POST["draw"]),
         "recordsTotal"            =>     $this->Bom_model->get_all_quote_data($condition,$page_name),
         "recordsFiltered"         =>     $this->Bom_model->get_quote_filtered_data($condition,$page_name),
        "data"                    =>     $data,
    );
  
    echo json_encode($output);
  
    }

    public function ajaxBomQuotations(){
     //ini_set('display_errors',1);
      $bom_id = $this->uri->segment(3);
     $page_name = $this->uri->segment(2);
      $quotations = $this->Bom_model->make_Qoutationdatatables(array('quotation.bom_id'=>$bom_id),$page_name);
     // print_r($quotations);die;
      $data = '';
      $sno = 1;
      foreach($quotations as $quotation){
         $approval =  json_decode($quotation['approval']);
         //if(!empty($quotation['disapprove_reason']))
         //$approval_reason ='';
         $approval_reason= !empty($quotation['disapprove_reason'])?json_decode($quotation['disapprove_reason']):'';
        $approval_style = $approval[1] == 0 && $this->session->userdata('role_id')== 1 ?  'display:block' : ($approval[1] == 2 && $this->session->userdata('role_id')== 5 ?  'display:block' : ($approval[1] == 3 && $this->session->userdata('role_id')== 11 ?  'display:block' :  'display:none')) ;
        $button ='';
        $approveselect = ($quotation['approval']==1)?'selected':'';
                $disapproveselect = ($quotation['approval']==2)?'selected':'';
         $button= '<select class="btn btn-success mr-2" onchange="onoffswitchspublish(this.value,'.$quotation['id'].')" style="'.$approval_style.'"><option selected disabled>Select</option><option vlaue="1" '.$approveselect.'>Approve</option><option vlaue="2"'.$disapproveselect.'>Disapprove</option></select>';
        //   if($approval[2]=='Pending'){$status ='<span class="btn btn-warning btn-rounded">Approval Pending By Manager</span>';}elseif($approval[2]=='Disapprove'){$status ='<span class="btn btn-danger btn-rounded">Diss Approved</span>';}else{$status ='<span class="btn btn-success btn-rounded">Approved</span>';}
           $status ='';  
         if($approval[2]=='Pending'){
            $status ='<span class="btn btn-warning btn-rounded">Approval Pending By Manager</span>';
          }elseif($approval[2]=='Disapprove'){
            $status ='<span class="btn btn-danger btn-rounded">Diss Approved</span>';
          }elseif($approval[0]==1 && $approval[2]=='Approve'){
            $status ='<span class="btn btn-success btn-rounded mr-2 btn-sm">Approved By Manager</span><span class="btn btn-warning btn-rounded btn-sm">Pending By General Manager</span>';
          }elseif($approval[0]==11 || $approval[0]==10 && $approval[2]=='Approve'){
            $status ='<span class="btn btn-success btn-rounded mr-2 btn-sm">Approved By General Manager</span><span class="btn btn-warning btn-rounded btn-sm">Pending By Director</span>';
          }elseif($approval[0]==12 && $approval[2]=='Approve'){
            $status ='<span class="btn btn-success btn-rounded">Approved By Director</span>';
          }
           $viewreason="";
          // print_r($approval_reason);die;
            if(!empty($approval_reason)){$viewreason = '<a href="javascript:void(0)" class="ml-2 btn btn-primary" onclick="viewReason('.$quotation['id'].')"><i class="fa fa-eye"></i> Reason</a>';}
           $data .="<tr  class='border-bottom-primary bg-gray'>";
          $data .="<th class='text-light'>S. No.</th>";
          $data .="<th class='text-light'>Vendor Name</th>";
          $data .="<th class='text-light'>Status</th>";
          $data .="<th class='text-light' colspan='2'>Action</th>";
          $data .="</tr>";
          $data .="<tr>";
          $data .="<td>".$sno++."</td>";
          $data .="<td>".$quotation['vendor_name']."</td>";
          $data .="<td >".$status.$viewreason."</td>";
          $data .="<td colspan='2'>".$button."</td>";
          $data .="</tr>";
          $data .= "<tr class='bg-warning'><td colspan='2'><b>Delivery Date :</b>".date('d-m-Y', strtotime($quotation['delivery_date']))."</td><td colspan='2'><b>Remark :</b>".$quotation['qoute_remark']."</td></tr>";  
          $data .= "<tr>";
          $data .="<th nowrap>Part Name</th>";
          $data .="<th>Qty</th>";
          $data .="<th>Price/Qty</th>";
          $data .="<th>Total Price</th>";
          $data .= "</tr>";
          $total_price =0;
        foreach(json_decode($quotation['product_detail']) as $key=>$product){
          $pcondition = array('product_code'=>$product->product_code);
          $product_detail = $this->Bom_model->get_partName($pcondition);
          $data .= "<tr>";
          $data .="<td nowrap>".$product_detail->part_name."</td>";
          $data .="<td>".$product->qty."</td>";
          $data .="<td>".$product->price."</td>";
          $data .="<td>".$product->tprice."</td>";
          $data .= "</tr>";
       $total_price+= intVal($product->tprice);
        }  
        //print_r($total_price);die;  
        $data .= "<tr><td colspan='2'></td><td><b>Grand Total :</b></td><td>".$total_price."</td></tr><tr></tr>";  
      }
      echo $data;
    }

    public function ajaxdiscardBoms(){
      if($this->session->userdata('id')==1){
        $condition = array('bom_discard_history.status'=>1);
      }else{
        $condition = array('bom_discard_history.upload_by'=>$this->session->userdata('id'));
      }
      
      $history = $this->Bom_model->make_discardBomHistorytables($condition); // this will call modal function for fetching data
      //print_r($history); die;
          $data = array();
      foreach($history as $key=>$quotation) // Loop over the data fetched and store them in array
       {
      //   $products = json_decode($quotation['product_detail']);
        
                $sub_array = array();
                $button = '';
                $button .= '<div style="display: flex;flex-wrap: nowrap;align-items: center;"><button type="button" onclick="viewQuoteModal('.$quotation['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Qutation" class="btn btn-sm  btn-warning"><i class="fa fa-eye"></i> Products </button>&nbsp;';
                
                $sub_array[] = $key+1;           
                
                $sub_array[] = $quotation['name'];
                $sub_array[] = 'Bom By '.$quotation['u_name'];
                $sub_array[] = $quotation['user_name'];
                $sub_array[] = $quotation['reason'];
                
                $sub_array[] = date('d-F-Y', strtotime($quotation['created_at'])); 
                
                
                $data[] = $sub_array;
             
              $product_qty = $data[0];
              
    }
    
      $output = array(
        //"draw"                    =>     intval($_POST["draw"]),
        "recordsTotal"            =>     $this->Bom_model->get_all_Bom_discard_data($condition),
        "recordsFiltered"         =>     $this->Bom_model->get_bom_discard_data($condition),
        "data"                    =>     $data,
    );
  
    echo json_encode($output);
    }

    public function ajaxEditBomHistory(){
      
      $condition = array('bom_update_history.status'=>1);
      $history = $this->Bom_model->make_BomHistorytables($condition); // this will call modal function for fetching data
      //print_r($history); die;
          $data = array();
      foreach($history as $key=>$quotation) // Loop over the data fetched and store them in array
      {
        $products = json_decode($quotation['product_detail']);
        
                $sub_array = array();
                $button = '';
                $button .= '<div style="display: flex;flex-wrap: nowrap;align-items: center;"><button type="button" onclick="viewQuoteModal('.$quotation['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Qutation" class="btn btn-sm  btn-warning"><i class="fa fa-eye"></i> New Products </button>&nbsp;';
                $button .= '<div style="display: flex;flex-wrap: nowrap;align-items: center;"><button type="button" onclick="oldproductModal('.$quotation['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Qutation" class="btn btn-sm  btn-warning"><i class="fa fa-eye"></i> Old Products </button>&nbsp;';
                $sub_array[] = $key+1;           
                
                $sub_array[] = $quotation['name'];
                $sub_array[] = $quotation['title'];
                
                $sub_array[] = $button;
                $sub_array[] = $quotation['user_name'];
                $sub_array[] = date('d-F-Y', strtotime($quotation['updated_at'])); 
                
                
                $data[] = $sub_array;
             
              $product_qty = $data[0];
              
    }
    
      $output = array(
        //"draw"                    =>     intval($_POST["draw"]),
        "recordsTotal"            =>     $this->Bom_model->get_all_Bom_history_data($condition),
        "recordsFiltered"         =>     $this->Bom_model->get_bom_history_filtered_data($condition),
        "data"                    =>     $data,
    );
  
    echo json_encode($output);
    }


    public function ajaxPOQuotations(){
       // ini_set('display_errors',1);
      // if($this->session->userdata('role_id')==4){
      //   $condition = array('quotation.po'=>1);
      // }else{      
        $condition = array('po.status'=>1);
      //}
      
      $pos = $this->Bom_model->make_poDatatables($condition); // this will call modal function for fetching data
       //print_r($pos); die;
          $data = array();
          $sn= 1;
      foreach($pos as $key=>$po) // Loop over the data fetched and store them in array
      {
          //print_r($po);die;
        $products = json_decode($po['product_detail']);
        $approve = $po['created_by'];
        //print_r($approve);fdie;
        //if($approve[0]==12 && $approve[1]==1){
            if($approve==12){
              $sub_array = array();
              $button = '';
              //$approval = ($po['approval']==1)?'checked':'';
              $pobutton = ($this->session->userdata('role_id')!=4)?'Quotation':'Order';
              $button .= '<div style="display: flex;flex-wrap: nowrap;align-items: center;"><button type="button" onclick="viewPoModal('.$po['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Order" class="btn btn-sm  btn-warning"><i class="fa fa-eye"></i> '.$pobutton.' </button>&nbsp;';
              if($po['po_sent']==0 && $this->session->userdata('role_id')!=4){
                $button .= '<button href="javascript:void(0)" onclick="poModal('.$po['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Create PO" class="btn btn-sm btn-success">Create PO</button></div>';
              }
              if($po['po_sent']==1 && $this->session->userdata('role_id')!=4){
              $button .='<a href="'.base_url('Bom/download_po/'.base64_encode($po['id'])).'" target="_blank" title="Download PO" class="btn btn-sm btn-success"><i class="fa fa-download"></i> PO</a>';
              }
              if($po['order_completed']==0 && $this->session->userdata('role_id')==4){
                $button .= '<button href="javascript:void(0)" onclick="CompleteOrder('.$po['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Complete Order" class="btn btn-sm btn-success">Complete Order</button></div>';
              }
              $sub_array[] = $sn++;           
              if($this->session->userdata('role_id')==4){
                $sub_array[] = 'Order '.($sn++);
              }
              if($this->session->userdata('role_id')!=4){
              $sub_array[] = $po['name'];
              $sub_array[] = $po['vendor_name'];
              }
              $sub_array[] = date('d-F-Y', strtotime($po['created_at'])); 
              if($this->session->userdata('role_id')==4){ 
                $sub_array[] = ($po['order_completed']==1)?'<span class="text-success">Order Completed</span>':'<span class="text-danger">Order Pending</span>';
              }else{
              $sub_array[] = ($po['po_sent']==1)?'<span class="text-success">Closed</span>':'<span class="text-danger">PO Not Created</span>';
              
              }
              $sub_array[] = $button;
              $data[] = $sub_array;
           
            $product_qty = $data[0];
              //print_r($sub_array);die;
              }
    }
      $output = array(
         //"draw"                    =>     intval($_POST["draw"]),
         //"recordsTotal"            =>     $this->Bom_model->get_all_quote_data($condition),
        // "recordsFiltered"         =>     $this->Bom_model->get_quote_filtered_data($condition),
        "data"                    =>     $data,
    );
  
    echo json_encode($output);
  
    }


    public function quotation_approval(){
      //print_r($_POST);die;
      //ini_set('display_errors',1);
      $id = $this->input->post('id');
      $status = $this->input->post('status');
      $reason = ($this->input->post('reason')!='undefined')?$this->input->post('reason'):'';
      $data = array();
      //print_r($status);die;
      $approval = $this->session->userdata('role_id') == 11 ? json_encode([$this->session->userdata('id'),1,$status]) : ($this->session->userdata('role_id') == 5 ? json_encode([$this->session->userdata('id'),3,$status]) : json_encode([$this->session->userdata('id'),2,$status]));     
      //$approval = $this->session->userdata('role_id') == 1 ? json_encode([$this->session->userdata('id'),2,$status]) : ($this->session->userdata('role_id') == 5 ? json_encode([$this->session->userdata('id'),3,$status]) : json_encode([$this->session->userdata('id'),1,$status]));     
      $quotation = $this->Bom_model->get_quotations($id);
      $user_id = $this->session->userdata('id');
      if(!empty($reason)){
      $old_reason = json_decode($quotation->disapprove_reason);
      //print_r($old_reason);
      $old_res = array();
      if(!empty($old_reason)){
      foreach($old_reason as $key=>$res){
        $old_res[] = array($key=>$res);
      }
    }
      $reason1[] = array($user_id=>$reason);
       if(!empty($old_res)){
        $new_reason = array_merge($old_res[0],$reason1);
       }else{
        $new_reason = $reason1;
       }
      
      
      $latest_reson = json_encode($new_reason);

      $data = array(
          'approval'=>$approval,
          'disapprove_reason' => $latest_reson,
        );
 
      }else{
          $data = array(
          'approval'=>$approval,
        );
      }
    //print_r($data);die;
     $history_data = array(
      'q_id' => $quotation->id,
      'enq_id' => $quotation->enq_id,
      'project_id'=> $quotation->project_id,
      'bom_id'=> $quotation->bom_id,
      'vendor_id'=> $quotation->quote_by,
      'approval_by'=> $user_id,
      'status'=> $status,
      'reason'=> $reason,

    );
    //print_r($history_data);die;
    $this->Bom_model->store_qouteHistory($history_data);
      $update_approval = $this->Bom_model->update_quotation($data,$id);
      if($update_approval && $status=='Disapprove'){
        echo json_encode(['status'=>200, 'message'=>'Qoutation DisApproved Successfully!']);
      }else{
        echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
      }
    }

    public function update_po(){
      //print_r($_POST);die;
      $id = $this->input->post('qID');
      $po = $this->input->post('po');
      $terms = $this->input->post('terms');

      if(empty($terms)){
        echo json_encode(['status'=>403, 'message'=>'Please Enter Terms Conditions !']);  
        exit();
      }

      $data = array(
        'po_sent'=>$po,
        'terms_conditions' => $terms,
      );
      
      $qoute = $this->Bom_model->get_po(array('id'=>$id));
      
      $bom = $this->Bom_model->get_bom(array('id'=>$qoute->bom_id));
      $vendor = $this->Bom_model->get_user(array('id'=>$qoute->vendorID));
      
      $quote_products = json_decode($qoute->product_detail);
      //print_r($quote_products);die;
      $vendorID =  $qoute->vendorID;
      $quote_totalPrice = 0;
      $project_id = $qoute->project_id;
      $perticular = 'Expense For PO Of '.$bom->title.' Bom Send To '.$vendor->name;
      $quote_id =array();
      foreach($quote_products as $key=>$value){
        $row = json_decode($value);
        if($vendorID == $row->vendorID){
        $qty_pro = 'qty@'.$vendorID;
        $qty = $row->$qty_pro;   
        $price_pro = 'price@'.$vendorID;            
        $price = $row->$price_pro;
        $tax_pro = 'tax@'.$vendorID;
        $tax = $row->$tax_pro;     
        $quot_pro = 'quotID@'.$vendorID;
        $quote_id[] = $row->$quot_pro; 
        $totalPrice = intval($qty)* intval($price);
        $tax_amount = ($totalPrice*$tax)/100;
        $quote_totalPrice+= $totalPrice + $tax_amount;
        }
      }
      $expence_data = array(
        'project' => $project_id,
        'date' => date("Y-m-d"),
        'particular' => $perticular,
        'expence' => $quote_totalPrice,
        'added_by' => $this->session->userdata('id'),
      );
      //print_r($expence_data);die;
      //$add_expence = $this->project_model->store_expence($expence_data);
      //print_r($add_expence);die;
      $this->Bom_model->update_quotation(array('status'=>2),$quote_id[0]);
      $update_po = $this->Bom_model->update_po($data,array('id'=>$id));
      if($update_po){
        $email = $vendor->email;
        $subject = 'Order Recieved  From Hindiko Solutions Pvt Ltd';
        $html = "Dear ".$vendor->name."<br> You Have Recieved An Order, Please Complete Your Order As Soon As Possible,<br> Thank you";
        sendEmail($email,$subject,$html);

        $notification_data = array(
          'sendor_id' => $this->session->userdata('id'),
          'reciever_id' => $vendor->id,
          'msg' => 'Order Recieved  From Hindiko Solutions Pvt Ltd',
          'url' => 'orders',
        );
        $this->Bom_model->store_notification($notification_data); 

        echo json_encode(['status'=>200, 'message'=>'PO Send Successfully!']);
      }else{
        echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);         
      }
    }

    public function update_order(){
      //print_r($_POST);die;
      $id = $this->input->post('id');
      $order_status = $this->input->post('order_status');
      $data = array(
        'order_completed' => $order_status,
      );
      //$update_order = $this->Bom_model->update_quotation($data,$id);
      $update_order = $this->Bom_model->update_po($data,array('id'=>$id));
      if($update_order){
        $po = $this->Bom_model->get_po(array('id'=>$id));
        $vendor = $this->Bom_model->get_user(array('id'=>$po->vendorID));
        $store =  $this->Bom_model->get_user(array('user_type'=>3));
        $bom = $this->Bom_model->get_bomsProjects(array('bom.id'=>$po->bom_id));
        $email = $store->email;
        $subject = 'Order Completed By '.$vendor->name;
        $html = "Dear ".$store->name."<br> ".$vendor->name." Completed Order For Project".$bom->project_name.",Bom -".$bom->title." Please In material ,<br> Thank you";
        sendEmail($email,$subject,$html);

        $notification_data = array(
          'sendor_id' => $this->session->userdata('id'),
          'reciever_id' => $store->id,
          'msg' => 'Order Completed By '.$vendor->name.',For Project'.$bom->project_name.'('.$bom->title.')',
          'url' => 'manage-store',
        );
        $this->Bom_model->store_notification($notification_data); 

        echo json_encode(['status'=>200, 'message'=>'PO Send Successfully!']);
      }else{
        echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);         
      }
    }

     public function stock_in_form(){
      //ini_set('display_errors',1);
      //print_r($_POST);die;
        $po_id = $this->input->post('po_id');
        $bom_id = $this->input->post('bom_id');
        $project_id = $this->input->post('project');
        $product_code = $this->input->post('product_code');
        $part_name = $this->input->post('part_name');
        $qty = $this->input->post('qty');
        $rcqty = $this->input->post('rcqty');
        $rjqty = $this->input->post('rjqty');
        $nrqty = $this->input->post('nrqty');
        //$finalqty = $this->input->post('finalqty');
        //print_r($part_name);die;
        $total_product = count(array_filter($product_code));
        $product_json = array();
        $po = $this->Bom_model->get_po(array('id'=>$po_id));
        
        $vendorID = $po->vendorID;
        $price = array();
        foreach(json_decode($po->product_detail) as $key=>$value){
          $row = json_decode($value);
          //print_r($row);die;
          if($vendorID == $row->vendorID){
            $product_pro = 'productID@'.$vendorID; 
            $price_pro = 'price@'.$vendorID;      
               
            $price[$row->$product_pro] = $row->$price_pro;
            //print_r($price);  die;
         
          }
        }
        for($i=0; $i<$total_product; $i++){  
          $product_json[$product_code[$i]] = array('product_code'=>$product_code[$i],'qty'=>$qty[$product_code[$i]],'rcqty'=>$rcqty[$product_code[$i]],'rjqty'=>$rjqty[$product_code[$i]],'nrqty'=>$nrqty[$product_code[$i]]);
          $recieve_qty = $rcqty[$product_code[$i]];
          $reject_qty = $rjqty[$product_code[$i]];
          $totalQty = intval($recieve_qty) + intval($reject_qty);
          $product_price = $price[$product_code[$i]];
          if($recieve_qty==''){
            echo json_encode(['status'=>403, 'message'=>'Please Enter Recieve Qty..']);
            exit();
          }
          if(($qty[$product_code[$i]])<$totalQty){
            echo json_encode(['status'=>403, 'message'=>'Quantity Missmatch please enter correct value..']);
            exit();
          }
          $product_data = $this->Product_model->get_product(array('products.product_code'=>$product_code[$i]));
         $total_qty = floatVal($product_data->unit) + floatVal($recieve_qty);
          //echo "<br>".$product_data->id;
          $update_data = array('unit'=>$total_qty,'price'=>$product_price);
         // print_r($update_data);
          $update_qty = $this->Product_model->update_product($update_data,$product_data->id);
                
        }
      //print_r($product_json);die;
      $user_id = $this->session->userdata('id');
      $storedata = array(
        'project_id' => $project_id,
        'bom_id' => $bom_id,
        'po_id' => $po_id,
        'material_detail' => json_encode($product_json),
        'added_by' => $user_id,
      );
       // print_r($data);die;
      //  $this->Bom_model->update_po(array('status'=>2),array('id'=>$po_id));
      $add_stock = $this->Bom_model->store_material($storedata);
//print_r($add_stock);die;
      if($add_stock){
        echo json_encode(['status'=>200, 'message'=>'In Material Updated Succefully!', 'id'=>base64_encode($add_stock)]);
      }else{
        echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
      }
      
    }

    public function download_po(){
      $id = base64_decode($this->uri->segment(3));
      $products = $this->Bom_model->get_po(array('id'=>$id)); 
      $data['project']=$this->project_model->get_project(array('id'=>$products->project_id));
      //print_r($products);die;
      $data['vendors'] = $this->user_model->get_vendor(array('vendor_id'=>$products->vendorID)); 
      $pdata = '';
      //print_r($data);die;
      $product= json_decode($products->product_detail);
     //print_r($products);die;
      $sno = 1;
      $vendorID = $products->vendorID;
      $grand_total = 0;
      $quote_id =array();
      foreach($product as $key=>$value){
        $row = json_decode($value);
        //print_r($row);die;
        if($vendorID == $row->vendorID){
          $productID = 'productID@'.$vendorID;
          $product_code = $row->$productID;
          $qty_pro = 'qty@'.$vendorID;
          $qty = $row->$qty_pro;   
          $price_pro = 'price@'.$vendorID;            
          $price = $row->$price_pro;
          $tax_pro = 'tax@'.$vendorID;
          $tax = $row->$tax_pro; 
          $quot_pro = 'quotID@'.$vendorID;
          $quote_id[] = $row->$quot_pro; 
          $totalPrice = intval($qty)* intval($price);
          $tax_amount = ($totalPrice*$tax)/100;
          $sub_total = $totalPrice + $tax_amount; 
          $grand_total+= $totalPrice + $tax_amount;       
          $pcondition = array('products.product_code'=>$product_code);
          $product_detail = $this->Bom_model->get_partName($pcondition);
          //print_r($product_detail);die;
          $pdata.='<tr>';
          $pdata.='<td style="padding:5px; border: 1px solid #000; text-align:center; width:5%!important;">'.$sno.'</td>';
          $pdata.='<td valign="top" align="left" style="padding:5px; border: 1px solid #000; text-align:center;">'.$product_detail->part_name.'</td>';
          $pdata.='<td valign="top" align="left" style="padding:5px; border: 1px solid #000; text-align:center">'.$key.'</td>';
          $pdata.='<td valign="top" align="right" style="padding:5px; border: 1px solid #000; text-align:center">'.$qty.'</td>';
          $pdata.='<td valign="top" align="right" style="padding:5px; border: 1px solid #000; text-align:center">'.$product_detail->unit_name.'</td>';
          $pdata.='<td valign="top" align="right" style="padding:5px; border: 1px solid #000; text-align:center">₹ '.$price.'</td>';
          $pdata.='<td valign="top" align="right" style="padding:5px; border: 1px solid #000; text-align:center">₹ '.$totalPrice.'</td>';
          $pdata.='<td style="padding:5px; border: 1px solid #000; text-align:center">₹ '.$tax_amount.'('.$tax.'%)</td>';
          $pdata.='<td style="padding:5px; border: 1px solid #000; text-align:center">₹ '.$sub_total.'</td>';
          $totalprice += $row->tprice;
          $sno++; 
      $pdata.='</tr>'; 
        
    }}
      $quotation = $this->Bom_model->get_quotations($quote_id[0]);
      
      $data['total'] = $grand_total;
      $data['po_id'] = $products->poID;
      $data['date'] = date('d/m/Y', strtotime($products->created_at));  
      $data['terms'] = $products->terms_conditions;
       $data['remark'] = $quotation->qoute_remark;
      $data['products'] = $pdata;
      $data['totalprice'] = $totalprice;
      $data['title'] = 'Download PO';
      $data['siteinfo'] = $this->siteinfo();
      $this->load->library('pdf');
      $html = $this->load->view('bom/po_pdf',$data, true);
      $this->pdf->createPDF($html, 'Purchase-Order', false);
    }

    public function in_stock_pdf(){
      $id = base64_decode($this->uri->segment(3));
      $data['title'] = 'Print PDF';
      $h_stock_data = $this->Bom_model->get_in_material(array('id'=> $id));
      $products = array();
      foreach(json_decode($h_stock_data->material_detail) as $key=>$product){
        $condition = array('product_code'=>$product->product_code);
        $product_data = $this->Product_model->get_product($condition);
        //print_r($product_data->product_code);
        $products[$product_data->product_code] = $product;
      }
//print_r($products);die;
      $data['products'] = $products;
      $this->load->library('pdf');
      $html = $this->load->view('bom/in_stock_pdf',$data, true);
      $this->pdf->createPDF($html, 'mypdf', false);
    }

    public function stock_out_form(){
      //print_r($_POST);die;
      $bom_id = $this->input->post('bom_users2');
      $bom = $this->Bom_model->get_bom(array('id'=>$bom_id));
      if($bom->material_status==1){
        echo json_encode(['status'=>403, 'message'=>'Material Already Issued..']);
                  exit();
      }
      $id = $bom->added_by;
      
      $project_id = $this->input->post('project_id');
      $product_code = $this->input->post('product_code');
      $part_name = $this->input->post('part_name');
      $qty = $this->input->post('qty');
      $iqty = $this->input->post('iqty');
      $bqty = $this->input->post('bqty');
      $totalqty = $this->input->post('totalqty');
      $total_product = count($product_code);
      $user_id = $this->session->userdata('id');
      // returnable products
      if($totalqty<$iqty){
        echo json_encode(['status'=>403, 'message'=>'Out of Stock..']);
                  exit();
      }
      if($this->input->post('returnableProducts')== 'on'){
        $product_name = $this->input->post('product_name');
        $person = $this->input->post('person_name');
        $product_qty = $this->input->post('product_qty');
        $return_date = $this->input->post('return_date');
        $total_returnable = count($product_name);
        //print_r($total_returnable);die;

        $return_products = array();
        for($i=0; $i<$total_returnable; $i++){       

              if(empty($product_name[$i])){
                echo json_encode(['status'=>403, 'message'=>'Please Enter Product Name..']);
                  exit();
              }

              if(empty($product_qty[$i])){
                echo json_encode(['status'=>403, 'message'=>'Please Enter Qty..']);
                  exit();
              }
      
              if(empty($person[$i])){
                echo json_encode(['status'=>403, 'message'=>'Please Enter Person Name..']);
                  exit();
              }
      
              if(empty($return_date[$i])){
                echo json_encode(['status'=>403, 'message'=>'Please Select Return Date..']);
                  exit();
              }
      

              $return_products = array(
                'project' => $project_id,
                'products_name'=>$product_name[$i],
                'qty'=>$product_qty[$i],
                'person'=>$person[$i],
                'return_date'=>$return_date[$i],
                'issue_by' => $user_id,
                );
               
          $store_return = $this->Product_model->store_return($return_products);
        }
        
      }
      
    $product_json = array();
    for($i=0; $i<$total_product; $i++){  
      $product_json[$product_code[$i]] = array('product_code'=>$product_code[$i],'qty'=>$qty[$product_code[$i]],'iqty'=>$iqty[$product_code[$i]],'bqty'=>$bqty[$product_code[$i]],'totalqty'=>$totalqty[$product_code[$i]]);
      $product_data = $this->Product_model->get_product(array('products.product_code'=>$product_code[$i]));
      //$total_qty = $product_data->unit + $finalqty[$product_code[$i]];
         
          if(empty($iqty[$product_code[$i]])){
            echo json_encode(['status'=>403, 'message'=>'Please Enter Qty To Issue..']);
            exit();
          }
          if((intval($iqty[$product_code[$i]])) > (intval($qty[$product_code[$i]]))){
            echo json_encode(['status'=>403, 'message'=>'You Can Not Issue Quantity Which Is More Than Stock Qty..']);
            exit();
          }
      $update_qty = $this->Product_model->update_product(array('unit'=>$bqty[$product_code[$i]]),$product_data->id);
    }
    
    $data = array(
      'project_id' => $project_id,
      'bom_id' => $bom->id,
      'material_detail' => json_encode($product_json),
      'issue_to' => $id,
      'added_by' => $user_id,
    );
      //print_r($data);die;
      $this->Bom_model->update_bom(array('material_status'=>1),$bom_id);
    $stock = $this->Bom_model->out_material($data);
    if($stock){
      $user_detail = $this->Bom_model->get_user(array('id'=>$id));
      $bom_detail = $this->Bom_model->get_bomProject(array('bom.id'=>$bom_id));
      $email = $user_detail->email;
      $subject = 'Material Project-'.$bom_detail->project_name.', Bom -'.$bom_detail->title;
      $html = "Dear ".$user_detail->name."<br> Material Issued For Project".$bom_detail->project_name."  Of Bom Title ".$bom_detail->title."<br>Please Take action For approval, <br>Thank you";
      //sendEmail($email,$subject,$html);
      $notification_data = array(
        'sendor_id' => $user_id,
        'reciever_id' => $id,
        'msg' => "Material Issued For Project".$bom_detail->project_name.", Bom Title ".$bom_detail->title,
        'url' => 'bom',
      );

      $this->Bom_model->store_notification($notification_data); 
      echo json_encode(['status'=>200, 'message'=>'Out Material Updated Succefully!' ,'id'=>base64_encode($stock)]);
    }else{
      echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
    }
  }

  public function out_stock_pdf(){
    $id = base64_decode($this->uri->segment(3));
    //print_r($id);die;
    $data['title'] = 'Print PDF';
    $h_stock_data = $this->Bom_model->get_out_material(array('id'=> $id));
    //print_r($h_stock_data->issue_to);die;
    $products = array();
    foreach(json_decode($h_stock_data->material_detail) as $key=>$product){
      $condition = array('product_code'=>$product->product_code);
      $product_data = $this->Product_model->get_product($condition);
      //print_r($product);die;
      $products[$product_data->product_code] = $product;
    }
    $user_detail = $this->Bom_model->get_user(array('id' => $h_stock_data->issue_to));
    $Issue_by = $this->Bom_model->get_user(array('id' => $h_stock_data->added_by));
    //print_r($user_detail);
    $data['siteinfo'] = $this->siteinfo();
    $data['project']=$this->project_model->get_project(array('id'=>$h_stock_data->project_id));
    $data['User'] = $user_detail->name;
    $data['date'] = $h_stock_data->created_at;
    $data['Issue_by'] = $Issue_by->name;
    $data['products'] = $products;
    //print_r($data);die;
    $this->load->library('pdf');
    $html = $this->load->view('bom/out_stock_pdf',$data, true);
    $this->pdf->createPDF($html, 'mypdf', false);
  }
  
    public function delivery_challan_pdf(){
    $id = base64_decode($this->uri->segment(3));
    //print_r($id);die;
    $data['title'] = 'Delivery Challan';
    $h_stock_data = $this->Bom_model->get_in_material(array('id'=> $id));
    //print_r($h_stock_data->issue_to);die;
    $products = array();
    foreach(json_decode($h_stock_data->material_detail) as $key=>$product){
      $condition = array('product_code'=>$product->product_code);
      $product_data = $this->Product_model->get_product($condition);
      //print_r($product);die;
      $products[$product_data->product_code] = $product;
    }
    //$user_detail = $this->Bom_model->get_user(array('id' => $h_stock_data->issue_to));
    $user_detail = $this->Bom_model->get_detail_in_material(array('in_material.id' => $id));
    $Issue_by = $this->Bom_model->get_user(array('id' => $h_stock_data->added_by));
    //print_r($user_detail);
    $data['siteinfo'] = $this->siteinfo();
    $data['project']=$this->project_model->get_project(array('id'=>$h_stock_data->project_id));
    $data['user'] = $user_detail;
    $data['date'] = $h_stock_data->created_at;
    $data['Issue_by'] = $Issue_by->name;
    $data['products'] = $products;
    $data['challan'] = $challan_detail;
    //print_r($data);die;
    $this->load->library('pdf');
    $html = $this->load->view('bom/delivery_challan',$data, true);
    //print_r($html);
    $this->pdf->createPDF($html, 'mypdf', false);
  }

  public function compare_quotations(){
    //ini_set('display_errors',1);
    $project_id = $this->input->post('project_id');
    $bom_id = $this->input->post('bom_id');
    $comapres = !empty($this->input->post('comapre'))?$this->input->post('comapre'):array();
    //print_r($comapres);die;
   $compare = $this->Bom_model->get_compare_quatations(array('compare_quotation.project_id'=>$project_id,'compare_quotation.bom_id'=>$bom_id));
   
   $compare_quatation = $compare->row();
   $product_detail = array();
   
    foreach($comapres as $key=>$row){
     $comapre_data =  base64_decode($row);
     $product_detail[$key] = $comapre_data;
    }

    $productDetail = $compare->num_rows() > 0 && $compare_quatation->status != 0 ? $compare_quatation->product_detail : json_encode($product_detail) ;
    //print_r(json_decode($productDetail));
    if(empty(json_decode($productDetail))){
      echo json_encode(['status'=>302, 'message'=>'Please select atleast one product price']);   
      exit();
    }
    $vendorID = array();
    foreach(json_decode($productDetail) as $row){
      $decode_row = json_decode($row);
      $vendorID[$decode_row->vendorID] = $decode_row->vendorID;
    }
    $user_id = $this->session->userdata('id');
    
    $role_id = $this->session->userdata('role_id') == 1 ? 5 : ($this->session->userdata('role_id')== 5 ? 11 : ($this->session->userdata('role_id')==11 ? 13 : 0));
    // print_r($role_id);die;
    $approval_by = $this->session->userdata('role_id') == 11 ? json_encode(['id'=>$this->session->userdata('id'),'role_id'=>$this->session->userdata('role_id'),'statusint'=>1,'status'=>'Approve']) : ($this->session->userdata('role_id') == 5 ? json_encode(['id'=>$this->session->userdata('id'),'role_id'=>$this->session->userdata('role_id'),'statusint'=>3,'status'=>'Approve']) : json_encode(['id'=>$this->session->userdata('id'),'role_id'=>$this->session->userdata('role_id'),'statusint'=>2,'status'=>'Approve']));     
    if(empty($compare_quatation)){
      $data = array( 
        'project_id' => $project_id,
        'bom_id' => $bom_id,
        'product_detail' => $productDetail,
        'approval_by' => $approval_by,
       );
       $store = $this->Bom_model->store_compare_quatation($data);
       
       if($store){
        $user =  $this->user_model->get_user(array('users.id'=>$user_id));   
        $project = $this->project_model->get_project(array('id'=>$project_id));  
        $admins =  $this->Bom_model->get_user_location_wise($project->location,array('users.user_type'=>$role_id));
        $bom = $this->Bom_model->get_bom(array('id'=>$bom_id));
        //print_r($admins);die;
        foreach($admins as $admin){
          $email = $admin['email'];
          $subject = 'Quotation Compared From Manager For Project-'.$project->name.', Bom -'.$bom->title;
          $html = "Dear ".$admin['name']."<br>" .$user->name." Compare a Quotation To Create PO  Of Bom Title ".$bom->title."<br>Please Take action For approval, <br>Thank you";
          //sendEmail($email,$subject,$html);
          $notification_data = array(
            'sendor_id' => $user_id,
            'reciever_id' => $admin['id'],
            'msg' => $user->name." Compare a Quotation To Create PO Of Bom Title ".$bom->title,
            'url' => 'quotation',
          );
          $this->Bom_model->store_notification($notification_data); 
        }
       }
    } else{
      $data = array( 
        'product_detail' => $productDetail,
        'approval_by' => $approval_by,
        'status' => 1
       );
       $update = $this->Bom_model->update_compare_quatation($data,array('id'=>$compare_quatation->id));
       if($update){
        $user =  $this->user_model->get_user(array('users.id'=>$user_id));   
        $project = $this->project_model->get_project(array('id'=>$project_id));  
        $admins =  $this->Bom_model->get_user_location_wise($project->location,array('users.user_type'=>$role_id));
        if(empty($admins)){
          $admins = $this->Bom_model->get_user_location_wise($project->location,array('users.user_type'=>1));
          $bom = $this->Bom_model->get_bom(array('id'=>$bom_id));
          foreach($admins as $admin){
            $email = $admin['email'];
            $subject = 'Quotation Approved By Director For Project-'.$project->name.', Bom -'.$bom->title;
            $html = "Dear ".$admin['name']."<br>" .$user->name." Approved Quotation To Create PO  Of Bom Title ".$bom->title."<br>Please Send PO To Vendor,<br> Thank you";
            //sendEmail($email,$subject,$html);
            $notification_data = array(
              'sendor_id' => $user_id,
              'reciever_id' => $admin['id'],
              'msg' => 'Quotation Approved By Director For Project-'.$project->name.', Bom -'.$bom->title,
              'url' => 'PO',
            );
            $this->Bom_model->store_notification($notification_data); 
          }
        }else{
          $bom = $this->Bom_model->get_bom(array('id'=>$bom_id));
        foreach($admins as $admin){
          $email = $admin['email'];
          $subject = 'Quotation Compared From Manager For Project-'.$project->name.', Bom -'.$bom->title;
          $html = "Dear ".$admin['name']."<br>" .$user->name." Compare a Quotation To Create PO  Of Bom Title ".$bom->title."<br>Please Take action For approval,<br> Thank you";
          //sendEmail($email,$subject,$html);
          $notification_data = array(
            'sendor_id' => $user_id,
            'reciever_id' => $admin['id'],
            'msg' => 'Quotation Approved By Director For Project-'.$project->name.', Bom -'.$bom->title,
            'url' => 'quotation',
          );
          $this->Bom_model->store_notification($notification_data); 
        }
        }
        //print_r($admins);die;
        
        
       }
       $store = $compare_quatation->id;
    }

    if($store){
       if($this->session->userdata('role_id')==11){
       
       
         foreach($vendorID as $vendor){
          $po = $this->Bom_model->get_po(array('po.status' =>1));
          if($po){
            $poID = date('Ymd').$po->id+1;
           }else{
            $poID = date('Ymd').'1';
           }
          $poData = array(
            'comapre_id' => $store,
            'project_id' => $project_id,
            'bom_id' => $bom_id,
            'vendorID' =>$vendor,
            'created_by' => $this->session->userdata('id'),
            'poID' => $poID,
            'product_detail' => $productDetail,
          );
         $storePO =  $this->Bom_model->store_po($poData);
         }
       }
      $history = array( 
        'comapre_id' => $store,
        'project_id' => $project_id,
        'bom_id' => $bom_id,
        'product_detail' => $productDetail,
        'approval_by' => $this->session->userdata('id'),
        'approve_disapprove_status' => "Approve",
       );
       $insert = $this->Bom_model->store_compare_history($history);
       echo json_encode(['status'=>200, 'message'=>'Comapre Quatation send successfully']);
      }else{
        echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
      }
   
  }

  public function send_recompare(){
    
    $project_id = $this->input->post('project_id');
    $bom_id = $this->input->post('bom_id');
    $recompare = $this->input->post('recompare');
    $compare_quatation = $this->Bom_model->get_compare_quatations(array('compare_quotation.project_id'=>$project_id,'compare_quotation.bom_id'=>$bom_id));
    if(empty($recompare)){
      echo json_encode(['status'=>302, 'message'=>'Please enter recomapre reason']); 
      exit();  
    }

    if($compare_quatation->num_rows() >0){

      $disapprove_reason = json_decode($compare_quatation->row()->disapprove_reason);
      $recompare_reson[] = array($this->session->userdata('id')=>$recompare);
      if(!empty($disapprove_reason)){
        foreach($disapprove_reason as $key=>$res){
          $old_res[] = array($key=>$res);
        }
        $new_reason = array_merge($old_res[0],$recompare_reson);
      }else{
        $new_reason = $recompare_reson;
      }
      
      $data = array( 
        'disapprove_reason' => json_encode($new_reason),
        'approval_by' => '',
        'status' => 0
      );

      $update = $this->Bom_model->update_compare_quatation($data,array('id'=>$compare_quatation->row()->id));
      if($update){
        $history = array( 
          'comapre_id' => $compare_quatation->row()->id,
          'project_id' => $project_id,
          'bom_id' => $bom_id,
          'product_detail' => $compare_quatation->row()->product_detail,
          'approval_by' => $this->session->userdata('id'),
          'disapprove_reason' => $recompare,
          'approve_disapprove_status' => "Disapprove",
         );
         $insert = $this->Bom_model->store_compare_history($history);
        echo json_encode(['status'=>200, 'message'=>'Recompare send successfully']);
      }else{
        echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
      }
    }

  }


  public function send_requotetation(){
    
    $project_id = $this->input->post('requot_projectID');
    $bom_id = $this->input->post('requot_bomID');
    $vendorID = $this->input->post('requot_vendorID');
    $quotetationID = $this->input->post('requot_quotetationID');
    $requotetation = $this->input->post('requotetation');
    
    if(empty($requotetation)){
      echo json_encode(['status'=>302, 'message'=>'Please enter requotetation reason']); 
      exit();  
    }

      $quotation = $this->Bom_model->get_quotations($quotetationID);
      $user_id = $this->session->userdata('id');
      if(!empty($reason)){
      $old_reason = json_decode($quotation->disapprove_reason);
      //print_r($old_reason);
      $old_res = array();
      if(!empty($old_reason)){
      foreach($old_reason as $key=>$res){
        $old_res[] = array($key=>$res);
      }
    }
      $reason1[] = array($user_id=>$requotetation);
       if(!empty($old_res)){
        $new_reason = array_merge($old_res[0],$reason1);
       }else{
        $new_reason = $reason1;
       }
      
      
      $latest_reson = json_encode($new_reason);

      $data = array(
          'approval'=>$approval,
          'disapprove_reason' => $latest_reson,
          'status' =>4
        );
 
      }else{
          $data = array(
          'approval'=>$approval,
          'status' =>4
        );
      }

      $update = $this->Bom_model->update_quotation($data,$quotetationID);
      if($update){

        $history_data = array(
          'q_id' => $quotetationID,
          'enq_id' => $quotation->enq_id,
          'project_id'=> $project_id,
          'bom_id'=> $quotation->bom_id,
          'vendor_id'=> $quotation->quote_by,
          'approval_by'=> $user_id,
          'status'=> 4,
          'reason'=> $requotetation,
        );
        //print_r($history_data);die;
        $this->Bom_model->store_qouteHistory($history_data);
        
        echo json_encode(['status'=>200, 'message'=>'Recompare send successfully']);
      }else{
        echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
      }
    }

    public function get_delivery_challan_form(){
      $id = $this->input->post('id');
      $data = $this->Bom_model->get_detail_in_material(array('in_material.id'=>$id));
      //print_r($data);
      ?>

    <div class="row">
    <div class="form-group col-md-6">
        <label for="">Bill To</label>
        <input type="text" class="form-control" value="<?=$data->vendorName?>" readonly>
    </div>


    <div class="form-group col-md-6">
        <label for="">Email</label>
        <input type="text" class="form-control" value="<?=$data->vendorEmail?>" readonly>
    </div>

    <div class="form-group col-md-6">
        <label for="">Phone</label>
        <input type="text" class="form-control" value="<?=$data->vendorPhone?>" readonly>
    </div>

    <div class="form-group col-md-6">
        <label for="">Address</label>
        <input type="text" class="form-control" value="<?=$data->vendorAddress?>" readonly>
    </div>

    <div class="form-group col-md-6">
        <label for="">State</label>
        <input type="text" class="form-control" value="<?=$data->vendorState?>" readonly>
    </div>

    <div class="form-group col-md-6">
        <label for="">GST No</label>
        <input type="text" class="form-control" value="<?=$data->vendorGst?>" readonly>
    </div>
    <div class="form-group col-md-6">
        <label for="">PO No</label>
        <input type="text" class="form-control" value="<?=$data->poID?>" name="po_no" id="po_no" readonly>
    </div>
    <div class="form-group col-md-6">
        <label for="">Delivery Challan No.</label>
        <input type="text" class="form-control" name="challan_no" id="challan_no">
    </div>

    <div class="form-group col-md-6">
        <label for="">DC Date and Time</label>
        <input type="datetime-local" class="form-control" name="challan_date_time" id="challan_date_time">
    </div>

    <div class="form-group col-md-6">
        <label for="">Mode of Delivery</label>
        <input type="text" class="form-control" name="mode_of_delivery" id="mode_of_delivery">
    </div>

    <div class="form-group col-md-6">
        <label for="">Vehicle Number</label>
        <input type="text" class="form-control" name="vehicle_no" id="vehicle_no">
    </div>

    <div class="form-group col-md-6">
        <label for="">Place of Supply</label>
        <input type="text" class="form-control" name="place_of_supply" id="place_of_supply">
    </div>

    <div class="form-group col-md-6">
        <label for="">FAX</label>
        <input type="text" class="form-control" name="fax" id="fax">
    </div>
    <input type="hidden" class="form-control" name="vendorID" id="vendorID" value="<?=$data->issue_to?>">
    <input type="hidden" class="form-control" name="outMaterialID" id="outMaterialID" value="<?=$id?>">
    <?php
      }

    public function store_delivery_challan(){
      $po_no = $this->input->post('po_no');
      $challan_no = $this->input->post('challan_no');
      $challan_date_time = $this->input->post('challan_date_time');
      $mode_of_delivery = $this->input->post('mode_of_delivery');
      $vehicle_no = $this->input->post('vehicle_no');
      $place_of_supply = $this->input->post('place_of_supply');
      $fax = $this->input->post('fax');
      $vendorID = $this->input->post('vendorID');
      $outMaterialID = $this->input->post('outMaterialID');
     
      if(empty($po_no)){
        echo json_encode(['status'=>302, 'message'=>'Please enter PO No.']); 
        exit();  
      }

      if(empty($challan_no)){
        echo json_encode(['status'=>302, 'message'=>'Please enter challan no.']); 
        exit();  
      }

      if(empty($challan_date_time)){
        echo json_encode(['status'=>302, 'message'=>'Please enter dc date time']); 
        exit();  
      }

      if(empty($mode_of_delivery)){
        echo json_encode(['status'=>302, 'message'=>'Please enter mode of delivery']); 
        exit();  
      }

      if(empty($vehicle_no)){
        echo json_encode(['status'=>302, 'message'=>'Please enter vehicle number']); 
        exit();  
      }

      if(empty($place_of_supply)){
        echo json_encode(['status'=>302, 'message'=>'Please enter place of supplier']); 
        exit();  
      }

    

      $data = array(
        'in_material_id'   => $outMaterialID,
        'vendorID'          => $vendorID,
        'challan_no'        => $challan_no,
        'challan_date_time' => $challan_date_time,
        'mode_of_delivery'  => $mode_of_delivery,
        'po_no'             => $po_no,
        'vehicle_no'        => $vehicle_no,
        'place_of_supply'   => $place_of_supply,
        'fax'               => $fax,
      );

      $store = $this->Bom_model->store_challan_delivery($data);
      if($store){
      echo json_encode(['status'=>200, 'message'=>'Delivery challan added successfully']);
      exit();
      }else{
        echo json_encode(['status'=>302, 'message'=>'Somthing went wrong!']);   
        exit();
      }
    }

}

?>