<?php 
date_default_timezone_set('Asia/Kolkata');
class Dashboard extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
		$this->load->model('auth_model');
		$this->load->model('dashboard_model');
		$this->load->model('Project_model');
		$this->load->model('bom_model');
	}

	public function index()
	{	 
		$data['title'] = 'Dashboard';
		$data['total_vendor'] = $this->dashboard_model->vendors_count(array('user_type'=>4));
		$data['total_expense'] = $this->dashboard_model->total_expense(array('status'=>1));
		$data['total_product'] = $this->dashboard_model->total_product(array('status'=>1));
		$data['total_project'] = $this->dashboard_model->total_project(array('status<>'=>0));
		$locations =  $this->Common_model->get_create_locations(array('location.status'=>1));
		$location_users = '';
		$location_projects = '';
		$bom_projects = '';
		foreach($locations as $location){
			$users = $this->dashboard_model->total_users($location->id);
			$location_users.= '["'.$location->location.'",'.$users.'],';
			$projects = count($this->Project_model->get_projects(array('projects.location'=>$location->id)));
			$location_projects.= '["'.$location->location.'",'.$projects.'],';
		}
		$all_projects = $this->Project_model->get_projects(array('projects.status'=>1));
		//print_r($all_projects);die;
		foreach($all_projects as $project){
			$boms = count($this->bom_model->get_boms(array('project_id'=>$project['id'])));
			$bom_projects.= '["'.$project['name'].'",'.$boms.'],';
		}
		$data['location_users']= $location_users;
		$data['location_projects']= $location_projects;
		$data['bom_projects'] = $bom_projects;
		
		//print_r($bom_projects);die;
		$data['total_enquiry'] = $this->dashboard_model->total_enquiry(array('assigned_to'=>$_SESSION['id']));
		$data['total_orders'] = $this->dashboard_model->total_order(array('quote_by'=>$_SESSION['id']));
		// $condition = $this->session->userdata('user_type') == 'Admin' ? array('customers.status'=>1) :  array('customers.created_by'=>$this->session->userdata('unique_id'),'customers.status'=>1);
		// if($this->session->userdata('booked_date')){
		// }else{
		// 	$data['customers'] = array();
		// }
		//print_r($data);die;
	  $this->template('dashboard',$data);
		
	}


  public function customer_bar_chart(){
   $months = array('01'=>'Jan', '02'=>'Feb', '03'=>'Mar', '04'=>'Apr', '05'=>'May', '06'=>'Jun', '07'=>'Jul', '08'=>'Aug', '09'=>'Sep', '10'=>'Oct', '11'=>'Nov', '12'=>'Dec');
   

  }


}