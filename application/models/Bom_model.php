<?php 

class Bom_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}
  function make_BomHistoryQuery($condition)
  {
    $this->db->select('bom_update_history.*,bom.title,projects.name,users.name as user_name');
    $this->db->from('bom_update_history'); 
    $this->db->join('projects','bom_update_history.project_id = projects.id','left');    
    $this->db->join('users','bom_update_history.update_by = users.id','left');  
    $this->db->join('bom','bom_update_history.bom_id = bom.id','left'); 
    $this->db->where($condition);
    if($this->session->userdata('id') && $this->session->userdata('role_id')== 4){
      $this->db->like('quote_by',$this->session->userdata('id'));
    }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    // $this->db->like('part_name', $_POST["search"]["value"]);
    // $this->db->or_like('product_code', $_POST["search"]["value"]);
    // $this->db->or_like('qty', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
  
  }

  function make_discardBomHistoryQuery($condition)
  {
    $this->db->select('bom_discard_history.*,projects.name,users.name as user_name,u.name as u_name');
    $this->db->from('bom_discard_history'); 
    $this->db->join('projects','bom_discard_history.project_id = projects.id','left');    
    $this->db->join('users as u','bom_discard_history.upload_by = u.id','left');    
    $this->db->join('users','bom_discard_history.discard_by = users.id','left');    
    $this->db->where($condition);
   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    // $this->db->like('part_name', $_POST["search"]["value"]);
    // $this->db->or_like('product_code', $_POST["search"]["value"]);
    // $this->db->or_like('qty', $_POST["search"]["value"]);
   }
   $this->db->order_by('bom_discard_history.id','desc');
  
  }

  function get_bom_discard_data($condition){
	  $this->make_discardBomHistoryQuery($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }

  function get_bom_history_filtered_data($condition){
	  $this->make_BomHistoryQuery($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }

  function make_discardBomHistorytables($condition){
    $this->make_discardBomHistoryQuery($condition,);
    if($_POST["length"] != -1)
    {
      $this->db->limit($_POST['length'], $_POST['start']);
    }
    $query = $this->db->get();
    return $query->result_array();
  }
    function make_BomHistorytables($condition){
      $this->make_BomHistoryQuery($condition,);
      if($_POST["length"] != -1)
      {
        $this->db->limit($_POST['length'], $_POST['start']);
      }
      $query = $this->db->get();
      return $query->result_array();
       //echo $this->db->last_query();die;
    }

    function get_all_Bom_history_data($condition)
  {
    $this->db->select('bom_update_history.*,projects.name,users.name as user_name,u.name as u_name');
    $this->db->from('bom_update_history'); 
    $this->db->join('projects','bom_update_history.project_id = projects.id','left');  
    $this->db->join('users','bom_update_history.update_by = users.id','left');    
    $this->db->where($condition);
   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    // $this->db->like('part_name', $_POST["search"]["value"]);
    // $this->db->or_like('product_code', $_POST["search"]["value"]);
    // $this->db->or_like('qty', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
	   return $this->db->count_all_results();
    //  / echo $this->db->last_query();die;
  }

  function get_all_Bom_discard_data($condition)
  {
    $this->db->select('bom_discard_history.*,projects.name,users.name as user_name,u.name as u_name');
    $this->db->from('bom_discard_history'); 
    $this->db->join('projects','bom_discard_history.project_id = projects.id','left');  
    $this->db->join('users','bom_discard_history.discard_by = users.id','left');    
    $this->db->where($condition);
   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    // $this->db->like('part_name', $_POST["search"]["value"]);
    // $this->db->or_like('product_code', $_POST["search"]["value"]);
    // $this->db->or_like('qty', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
	   return $this->db->count_all_results();
    //  / echo $this->db->last_query();die;
  }
    function get_vendors(){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('user_type',4);
        $query =  $this->db->get();
        return $query->result_array();
    }
    function get_products($condition){
        $this->db->select('*');
        $this->db->from('bom');
        $this->db->where($condition);
        $query =  $this->db->get()->row();
            return $query;
    }
    
    function get_editBomproducts($condition){
      $this->db->select('*');
      $this->db->from('bom_update_history');
      $this->db->where($condition);
      $query =  $this->db->get()->row();
          return $query;
    }
    
    function get_inquiry($condition){
      $this->db->select('*');
      $this->db->from('quotations_inquiry');
      $this->db->where($condition);
      $query =  $this->db->get()->row();
          return $query;
  }
  function get_inquiries($condition){
    $this->db->select('*');
    $this->db->from('quotations_inquiry');
    $this->db->where($condition);
    $query =  $this->db->get();
        return $query->result_array();
}

function get_assigneddata($condition){
  $this->db->select('quotations_inquiry.*,users.name as vendor,u.name assign_by');
  $this->db->from('quotations_inquiry');
  $this->db->join('users as u','quotations_inquiry.assigned_by = u.id','left');
  $this->db->join('users','quotations_inquiry.assigned_to = users.id','left');
  $this->db->where($condition);
  $query =  $this->db->get();
      return $query->result_array();
}
  function get_quotations($id){
    $this->db->select('*');
    $this->db->from('quotation');
    $this->db->where('id',$id);
    $query =  $this->db->get()->row();
        return $query;
}

function get_allquotations($condition){
  $this->db->select('*');
  $this->db->from('quotation');
  $this->db->where($condition);
  $query =  $this->db->get();
  return $query->result_array();
}

function get_pending_qoutes($condition){
  $this->db->select('*');
  $this->db->from('quotation');
  $this->db->where_in('status',$condition);
  //$this->db->or_where($condition2);
  $query =  $this->db->get();
  return $query->result_array();
}

function check_quotation($condition){
  $this->db->select('*');
  $this->db->from('quotation');
  $this->db->where($condition);
  $query =  $this->db->get();
  return $query->result_array();
}

  function get_bom($condition){
    $this->db->select('*');
    $this->db->from('bom');
    $this->db->where($condition);
    $query =  $this->db->get()->row();
		return $query;
  }

  function get_boms($condition){
    $this->db->select('*');
    $this->db->from('bom');
    $this->db->where($condition);
    $query =  $this->db->get()->result();
		return $query;
  }

  function get_boms_enquiry($condition){
    $this->db->where($condition);
    $query =  $this->db->get('bom')->row_array();
    $this->db->select('quotation.*,users.name as userName,users.id as vendorID');
    $this->db->join('users', 'users.id=quotation.quote_by','left');
    $this->db->where('bom_id',$query['id']);
    $this->db->where_in('quotation.status',array(1,2));
    //$this->db->or_where('quotation.status<>',4);
    $query['object'] =  $this->db->get('quotation')->result_array(); 
    //echo $this->db->last_query();die;
    return $query;
  }
  
  function get_in_material($condition){
    $this->db->select('*');
    $this->db->from('in_material');
    $this->db->where($condition);
    $query =  $this->db->get()->row();
   //echo $this->db->last_query();die;
		 return $query;
  }

  function get_in_materials($condition){
    $this->db->select('in_material.*,users.name,projects.name as project_name,bom.title as bom_title');
    $this->db->from('in_material');
    $this->db->join('users','in_material.added_by = users.id','left'); 
    $this->db->join('projects','in_material.project_id = projects.id','left'); 
    $this->db->join('bom','in_material.bom_id = bom.id','left'); 
    $this->db->where($condition);
    $this->db->order_by('in_material.id','DESC');
    $query =  $this->db->get();
		 return $query->result_array();
  }

  function get_out_materials($condition){
    $this->db->select('out_material.*,users.name,u.name as issue_name,projects.name as project_name,bom.title as bom_title');
    $this->db->from('out_material');
    $this->db->join('users','out_material.added_by = users.id','left'); 
    $this->db->join('users as u','out_material.issue_to = u.id','left'); 
    $this->db->join('projects','out_material.project_id = projects.id','left'); 
    $this->db->join('bom','out_material.bom_id = bom.id','left'); 
    $this->db->where($condition);
    $this->db->order_by('id','desc');
    $query =  $this->db->get();
		 return $query->result_array();
  }

  function get_out_material($condition){
    $this->db->select('*');
    $this->db->from('out_material');
    $this->db->where($condition);
    $query =  $this->db->get()->row();
		 return $query;
  }

  function get_bomsUsers($condition){
    $this->db->select('bom.*,users.name,users.id as user_id');
    $this->db->from('bom');
    $this->db->join('users','bom.added_by = users.id','left'); 
    $this->db->where($condition);
    $query =  $this->db->get();
		return $query->result_array();
  }

  function get_bomsProjects($condition){
    $this->db->select('bom.*,projects.name as project_name');
    $this->db->from('bom');
    $this->db->join('projects','bom.project_id = projects.id','left'); 
    $this->db->where($condition);
    $query =  $this->db->get();
		return $query->row();
  }

  function get_bomsQoutes($condition){
    $this->db->select('quotation.*,quotation.id as quote_id,bom.*,users.name,u.name as vendor_name');
    $this->db->from('quotation');
    $this->db->join('bom','quotation.bom_id = bom.id','left'); 
    $this->db->join('users','bom.added_by = users.id','left'); 
    $this->db->join('users as u','quotation.quote_by = u.id','left'); 
    $this->db->where($condition);
    $query =  $this->db->get();
    //echo $this->db->last_query();die;
		return $query->result_array();
  }

  function get_bomProject($condition){
    $this->db->select('bom.*,projects.name');
    $this->db->from('bom');
    $this->db->join('projects','bom.project_id = projects.id','left'); 
    $this->db->join('location', 'location.id=projects.location','left');
    $this->db->group_by('name');
    $this->db->where($condition);
    $this->db->where_in('projects.location',explode(',',$this->session->userdata('location'))); 
    $query = $this->db->get();
    return $query->result_array();
  }

	function make_query($condition)
  {
    $this->db->select('bom.*,projects.name,projects.status as project_status,users.name as user_name');
    $this->db->from('bom');
    $this->db->join('projects','bom.project_id = projects.id','left');  
    $this->db->join('users','bom.added_by = users.id','left');  
    //$this->db->join('quotations_inquiry','quotations_inquiry.bom_id = bom.id','left');  
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('bom.title', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    //$this->db->or_like('qty', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
  
  }

  function make_quoteQuery($condition)
  {
    $this->db->select('quotations_inquiry.*');
    $this->db->from('quotations_inquiry'); 
    $this->db->where($condition);
    if($this->session->userdata('id') && $this->session->userdata('role_id')== 4){
      $this->db->like('assigned_to',$this->session->userdata('id'));
    }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    // $this->db->like('part_name', $_POST["search"]["value"]);
    // $this->db->or_like('product_code', $_POST["search"]["value"]);
    // $this->db->or_like('qty', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
  
  }
  function get_user($ucondition){
    $this->db->select('*');
     $this->db->from('users');
     $this->db->where($ucondition);
     $query =  $this->db->get()->row();
     return $query;
 }

 function get_user_location_wise($location,$condition){
    $this->db->select('users.*');
    $this->db->from('users');
    $this->db->where($condition);
    $this->db->where("location LIKE '%$location%'");
    $query =  $this->db->get()->result_array();
    //echo $this->db->last_query();die;
    return $query;
 }
  function make_quotationQuery($condition,$page_name)
  {
   // echo $page_name;die;
    $this->db->select('quotation.*,projects.name,location.location,users.name as vendor_name,bom.title');
    $this->db->from('quotation'); 
    $this->db->join('projects','quotation.project_id = projects.id','left');    
    $this->db->join('location','projects.location = location.id','left');    
    $this->db->join('users','quotation.quote_by = users.id','left');    
    $this->db->join('bom','quotation.bom_id = bom.id','left'); 
    
    if($this->session->userdata('role_id')!=4 && $page_name =="ajaxQuotations"){
        $this->db->where_in('projects.location',explode(',',$this->session->userdata('location')));  
      $this->db->group_by('quotation.bom_id'); 
      $this->db->order_by('quotation.updated_at','desc');
    } 
    if($this->session->userdata('quoteLocationID')){
      $this->db->where('projects.location',$this->session->userdata('quoteLocationID'));
    }
    $this->db->where($condition);
    if($this->session->userdata('id') && $this->session->userdata('role_id')== 4){
      $this->db->like('quotation.quote_by',$this->session->userdata('id'));
    }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    // $this->db->like('part_name', $_POST["search"]["value"]);
    // $this->db->or_like('product_code', $_POST["search"]["value"]);
    // $this->db->or_like('qty', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
  
  }
    function make_datatables($condition){
	  $this->make_query($condition,);
    if(isset($_POST["length"])){
      if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }}
	  $query = $this->db->get();
	  return $query->result_array();
  }

  function get_partName($pcondition){
    $this->db->select('products.*,unit.name as unit_name');
    $this->db->from('products');
    $this->db->join('unit','products.unit_type = unit.id','left');    
    $this->db->where($pcondition);
    $query =  $this->db->get()->row();
		return $query;
  }
  function make_Qoutedatatables($condition){
    $this->make_quoteQuery($condition,);
    if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array();
  }

  function make_Qoutationdatatables($condition,$page_name=""){
    
    $this->make_quotationQuery($condition,$page_name);
   // if(isset($_POST["length"])){
    if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }//}
	  $query = $this->db->get();
	  //echo $this->db->last_query();die;
	  return $query->result_array();
  }
  function make_poQuery($condition,$page_name)
  {
   // echo $page_name;die;
    $this->db->select('po.*,projects.name,users.name as vendor_name,bom.title,location.location');
    $this->db->from('po'); 
    $this->db->join('projects','po.project_id = projects.id','left');    
    $this->db->join('users','po.vendorID = users.id','left');    
    $this->db->join('bom','po.bom_id = bom.id','left');   
    $this->db->join('location','location.id = projects.location','left');
    if($this->session->userdata('role_id')!=4 && $page_name =="ajaxQuotations"){
      $this->db->group_by('po.bom_id'); 
      $this->db->order_by('po.updated_at','desc');
    } 

    if($this->session->userdata('poLocationID')){
      $this->db->where('projects.location',$this->session->userdata('poLocationID'));
    }
    $this->db->where($condition);
    $this->db->where_in('projects.location',explode(',',$this->session->userdata('location')));
    if($this->session->userdata('id') && $this->session->userdata('role_id')== 4){
      $this->db->like('po.vendorID',$this->session->userdata('id'));
    }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    // $this->db->like('part_name', $_POST["search"]["value"]);
    // $this->db->or_like('product_code', $_POST["search"]["value"]);
    // $this->db->or_like('qty', $_POST["search"]["value"]);
   }
   $this->db->order_by('po.id','desc');
  
  }
  function make_poDatatables($condition,$page_name=""){
    
    $this->make_poQuery($condition,$page_name);
   // if(isset($_POST["length"])){
    if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }//}
	  $query = $this->db->get();
	  return $query->result_array();
  }
  function get_filtered_data($condition){
	  $this->make_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data($condition)
  {
    $this->db->select('bom.*');
    $this->db->from('bom');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    //$this->db->like('part_name', $_POST["search"]["value"]);
    $this->db->or_like('bom.title', $_POST["search"]["value"]);
    //$this->db->or_like('qty', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
	   return $this->db->count_all_results();
  }
  function store_bom($data){
   return $this->db->insert('bom', $data);
  }

  function store_discardBom($data){
    return $this->db->insert('bom_discard_history', $data);
  }

  function delete_bom($condition){
    $this->db->where($condition);
    return $this->db->delete('bom');
   }

  function store_material($data){
     $this->db->insert('in_material', $data);
     return $this->db->insert_id();
  }

  function out_material($data){
    $this->db->insert('out_material', $data);
    return $this->db->insert_id();
  }
  function update_quotation($data,$id){
    //print_r($data);die;
    $this->db->where('id',$id);
    return $this->db->update('quotation', $data);
  }
  
  function store_qouteHistory($data){
    $this->db->insert('quotation_history', $data);
    return $this->db->insert_id();
  }

  function update_bom($data,$id){
    $this->db->where('id',$id);
    return $this->db->update('bom', $data);
  }

  function store_product_request_history($data){
    $this->db->insert('product_request_history', $data);
    return $this->db->insert_id();
  }

  function get_product_request_history($condition){
    $this->db->select('*');
     $this->db->from('product_request_history');
     $this->db->where($condition);
     $this->db->order_by('id','DESC');
     $query =  $this->db->get()->row();
     return $query;
 }

 function get_productrequest_history($condition){
  $this->db->select('*');
   $this->db->from('product_request_history');
   $this->db->where($condition);
   //$this->db->group_by('bom_id');
   $query =  $this->db->get()->result_array();
   //echo $this->db->last_query();die;
   return $query;
}

  function update_product_request_history($data,$id){
    $this->db->where('id',$id);
    return $this->db->update('product_request_history', $data);
  }

  function insert_bom_history($data){
    return $this->db->insert('bom_update_history', $data);
  }
   function update_product($data,$productId){
    $this->db->where('id',$productId);
    return $this->db->update('products', $data);
   }

   function delete_product($condiotion){
    $this->db->where($condiotion);
    return $this->db->delete('products');
   }

   function store_qoute($data){
    return $this->db->insert('quotations_inquiry',$data);
   }

   function send_qoute($data){
    return $this->db->insert('quotation',$data);
   }

   function get_enq_filtered_data($condition){
	  $this->make_quoteQuery($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }

  function get_quote_filtered_data($condition,$page_name=''){
	  $this->make_quotationQuery($condition,$page_name);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }

   function get_all_enq_data($condition)
  {
    $this->db->select('quotations_inquiry.*');
    $this->db->from('quotations_inquiry');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('part_name', $_POST["search"]["value"]);
    $this->db->or_like('product_code', $_POST["search"]["value"]);
    $this->db->or_like('qty', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
	   return $this->db->count_all_results();
  }

  function get_all_quote_data($condition)
  {
    $this->db->select('quotation.*');
    $this->db->from('quotation');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    // $this->db->like('part_name', $_POST["search"]["value"]);
    // $this->db->or_like('product_code', $_POST["search"]["value"]);
    // $this->db->or_like('qty', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
	   return $this->db->count_all_results();
  }

  public function get_compare_quatation($conditions){
    $this->db->select('compare_quotation.*');
    $this->db->from('compare_quotation');
    $this->db->where($conditions);
    return $this->db->get()->row();
  }

  public function get_compare_quatations($conditions){
    $this->db->select('compare_quotation.*');
    $this->db->from('compare_quotation');
    $this->db->where($conditions);
    return $this->db->get();
  }

  public function store_compare_quatation($data){
    $this->db->insert('compare_quotation',$data);
    return $this->db->insert_id();
  }

  public function store_compare_history($data){
   return $this->db->insert('compare_history',$data);
  }

  public function update_compare_quatation($data,$condition){
    $this->db->where($condition);
    return $this->db->update('compare_quotation',$data);
  }

  public function get_po($condition){
    $this->db->select('po.*');
    $this->db->from('po');
    $this->db->where($condition);
    $this->db->order_by('po.id','desc');
    return $this->db->get()->row();
  }
  
  public function get_pos($condition){
    $this->db->select('po.*,users.name as vendor_name,bom.title');
    $this->db->from('po');
    $this->db->where($condition);
    $this->db->join('users','po.vendorID = users.id','left'); 
    $this->db->join('bom','po.bom_id = bom.id','left'); 
    $this->db->order_by('po.id','desc');
    return $this->db->get()->result_array();
  }
  // $this->db->select('quotation.*,quotation.id as quote_id,bom.*,users.name,u.name as vendor_name');
  //   $this->db->from('quotation');
  //   $this->db->join('bom','quotation.bom_id = bom.id','left'); 
  //   $this->db->join('users','bom.added_by = users.id','left'); 
  //   $this->db->join('users as u','quotation.quote_by = u.id','left'); 
  

  public function store_po($data){
    return $this->db->insert('po',$data);
  }

  public function update_po($data,$condition){
    $this->db->where($condition);
    return $this->db->update('po',$data);
  }

  public function get_dc($condition){
    $this->db->select('dc.*,projects.name');
    $this->db->from('dc');
    $this->db->where($condition);
    $this->db->join('projects','dc.project = projects.id','left'); 
    $this->db->order_by('dc.id','DESC');
    return $this->db->get()->result_array();
  }

  public function get_dc_row($condition){
    $this->db->select('dc.*,projects.name,location.location,users.email as vendor_email,users.name as vendor,users.phone as vendor_mobile,users.address as vendor_address,vendor.gst');
    $this->db->join('projects','dc.project = projects.id','left'); 
    $this->db->join('users','dc.bill_to = users.id','left');  
    $this->db->join('vendor','dc.bill_to = vendor.vendor_id','left');  
    $this->db->join('location','projects.location = location.id','left'); 
    $this->db->order_by('dc.id','DESC');
    $this->db->where($condition);
    return $this->db->get('dc')->row();
  }

  public function store_dc_data($data){
    $this->db->insert('dc',$data);
    return $this->db->insert_id();
  }

  public function update_dc($data,$condition){
    $this->db->where($condition);
    return $this->db->update('dc',$data);
  }

  function make_dcquery($condition)
  {
    $this->db->select('dc.*,projects.name,location.location,users.name as user_name,u.name as vendor,u.phone as mobile');
    $this->db->from('dc');
    $this->db->join('projects','dc.project = projects.id','left');  
    $this->db->join('location','projects.location = location.id','left');  
    $this->db->join('users','dc.added_by = users.id','left');  
    $this->db->join('users as u','dc.bill_to = u.id','left');  
    //$this->db->join('quotations_inquiry','quotations_inquiry.bom_id = bom.id','left');  
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('dc.challan_no', $_POST["search"]["value"]);
    $this->db->or_like('projects.name', $_POST["search"]["value"]);
    //$this->db->or_like('qty', $_POST["search"]["value"]);
   }
   $this->db->order_by('dc.id','desc');
  
  }

  function make_dcdatatables($condition){
	  $this->make_dcquery($condition,);
    if(isset($_POST["length"])){
      if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }}
	  $query = $this->db->get();
	  return $query->result_array();
  }

  function get_all_dcdata($condition)
  {
    $this->db->select('dc.*');
    $this->db->from('dc');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('dc.po_number', $_POST["search"]["value"]);
    $this->db->or_like('dc.challan_no', $_POST["search"]["value"]);
    //$this->db->or_like('projects.name', $_POST["search"]["value"]);
   }
   $this->db->order_by('dc.id','desc');
	   return $this->db->count_all_results();
  }

  function get_filtered_dcdata($condition){
	  $this->make_dcquery($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }

  public function get_delivery_challan($condition){
    $this->db->where($condition);
    return $this->db->get('delivery_challan')->row();
  }

  public function get_detail_in_material($condition){
    $this->db->select('in_material.*,po.*,users.name as vendorName,users.email as vendorEmail,users.phone as vendorPhone,users.address as vendorAddress,states.name as vendorState,vendor.gst as vendorGst,delivery_challan.challan_no,delivery_challan.challan_date_time,delivery_challan.mode_of_delivery,delivery_challan.po_no,delivery_challan.vehicle_no,delivery_challan.place_of_supply,delivery_challan.fax');
    $this->db->from('in_material');
    $this->db->join('po','in_material.po_id = po.id','left'); 
    $this->db->join('users','po.vendorID = users.id','left'); 
    $this->db->join('vendor','vendor.vendor_id = users.id','left'); 
    $this->db->join('states','states.id = users.state','left'); 
    $this->db->join('delivery_challan','delivery_challan.in_material_id = in_material.id','left'); 
    $this->db->where($condition);
   return $query =  $this->db->get()->row();
  }

  public function get_detail_out_material($condition){
    $this->db->select('out_material.*,users.name as vendorName,users.email as vendorEmail,users.phone as vendorPhone,users.address as vendorAddress,states.name as vendorState,vendor.gst as vendorGst,delivery_challan.challan_no,delivery_challan.challan_date_time,delivery_challan.mode_of_delivery,delivery_challan.po_no,delivery_challan.vehicle_no,delivery_challan.place_of_supply,delivery_challan.fax');
    $this->db->from('out_material');
    $this->db->join('users','out_material.issue_to = users.id','left'); 
    $this->db->join('vendor','vendor.vendor_id = users.id','left'); 
    $this->db->join('states','states.id = users.state','left'); 
    $this->db->join('delivery_challan','delivery_challan.out_material_id = out_material.id','left'); 
    $this->db->where($condition);
   return $query =  $this->db->get()->row();
  }

  public function store_challan_delivery($data){
    return $this->db->insert('delivery_challan',$data);
  }


  function store_notification($data){
    return $this->db->insert('notifications',$data);
   }
}