<?php 
class Common_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

  public function get_menues($condition){
    $this->db->where($condition);
    return $this->db->get('menues')->result_array();
  }

  public function get_menu($condition){
    $this->db->where($condition);
    return $this->db->get('menues')->row();
  } 
  public function get_site_info(){
    //$adminID = $this->session->userdata('user_type')=='Teacher' ? $this->session->userdata('id') : 1;
    $this->db->where('adminID', 1);
    return $this->db->get('site_info')->row();
  }
  // $this->db->select('location.*');
  // $this->db->from('location');
  // $this->db->join('users','users.location=location.id','left');
  // $this->db->where($condition);
  // $this->db->where_in('users.location',explode(',',$this->session->userdata('location')));
  // return $this->db->get()->result();
  public function get_notifications($condition){
    $this->db->select('notifications.*,users.name as sendor_name,reciever.name as reciever_name');
    $this->db->from('notifications');
    $this->db->join('users','users.id=notifications.sendor_id','left');
    $this->db->join('users as reciever','reciever.id=notifications.reciever_id','left');
    $this->db->where($condition);
    $this->db->order_by('notifications.id', 'DESC');
    return $this->db->get()->result_array();
    //echo $this->db->last_query();die;
    
  }

  public function update_notification($data,$id){
    $this->db->where('id',$id);
  return $this->db->update('notifications',$data);
  }


  public function get_about(){
   return $this->db->get('about_us')->row();
  }

  public function get_country()
  {
    return $this->db->get('countries')->result();
  }

  public function get_country_by_id($condition)
  {
    $this->db->where($condition);
    return $this->db->get('countries')->row();
  }

  public function get_state($condition)
  {
      $this->db->where($condition);
      $this->db->order_by('name', 'ASC');
      return $this->db->get('states')->result();
  }

  public function get_city($condition)
  {
      $this->db->where($condition);
      $this->db->order_by('name', 'ASC');
      return $this->db->get('cities')->result();
  }

  public function get_state_id($condition)
  {
      $this->db->where($condition);
      return $this->db->get('states')->row();
  }

  public function get_city_id($condition)
  {
      $this->db->where($condition);
      return $this->db->get('cities')->row();
  }
  public function get_country_id($condition){
    $this->db->where($condition);
      return $this->db->get('countries')->row();
  }

  public function store_city($city_data){
  $this->db->insert('cities', $city_data);
  return $this->db->insert_id();
}

public function store_state($state_data){
  $this->db->insert('states', $state_data);
  return $this->db->insert_id();
}

public function get_permission($condition){
  $this->db->where($condition);
  return $this->db->get('permission')->row();
}

public function get_unit(){
  return $this->db->get('unit')->result();
}

public function get_locations($condition){
  $this->db->select('location.*');
  $this->db->from('location');
  $this->db->join('users','users.location=location.id','left');
  $this->db->where($condition);
  $this->db->where_in('users.location',explode(',',$this->session->userdata('location')));
  return $this->db->get()->result();
   //echo $this->db->last_query();die;
}

public function get_create_locations($condition){
  $this->db->select('location.*');
  $this->db->from('location');
  $this->db->where($condition);
  return $this->db->get()->result();
   //echo $this->db->last_query();die;
}

public function get_location($condition){
  $this->db->where($condition);
  return $this->db->get('location')->row();
  // echo $this->db->last_query();
}

public function store_location($data){
  return $this->db->insert('location',$data);
}

public function update_location($data,$id){
  $this->db->where('id',$id);
  return $this->db->update('location',$data);
}

}