<?php 
class Auth_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('encryption');
	}

    public function login($email, $password) {
		if($email && $password) {
      $encryptPass=base64_encode($password);
        $query = $this->db->query("SELECT users.*,roles.role FROM users LEFT JOIN roles ON roles.id=users.user_type WHERE (users.email = '$email' OR users.phone = '$email') AND users.password = '$encryptPass'");
			//$query = $this->db->query($sql, array($email,$email));
			if($query->num_rows() == 1) {
				$result = $query->row_array();

				if(1==$result['status'])
				{
          $session = array(
            'id'          => $result['id'],
            'name'        => $result['name'],
            'email'       => $result['email'],
            'phone'       => $result['phone'],
            'address'     => $result['address'],
            'city'        => $result['city'],
            'state'       => $result['state'],
			       'role_id'     =>$result['user_type'],
            'user_type'   => $result['role'],
			      'permission'  => $result['permission'],
			      'profile_pic' => $result['profile_pic'],
						'location'    => $result['location'],
            'status'      => $result['status'],
            'logged_in'   => TRUE
          );
        $this->session->set_userdata($session);
				return $result;	
			}
			else {
				return 302;
			}
			}else {
				return 403;
			}
		}
	}

  public function register($data) {
	   $this->db->insert('users', $data);
     return $this->db->insert_id();
   }
   

	 public function vendor_info($data){
    $this->db->insert('vendor', $data);
     return $this->db->insert_id(); 
   }

}
