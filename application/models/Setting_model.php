<?php 
class Setting_model extends CI_Model 
{

  public function __construct()
  {
      parent::__construct();

  }

  public function update_siteInfo($data){
     $adminID =  $this->session->userdata('id');
    $this->db->where('adminID', $adminID);
    return $this->db->update('site_info',$data);
  }

  public function store_siteInfo($data){
    return $this->db->insert('site_info',$data);
  }

  public function add_unit($data){
    return $this->db->insert('unit',$data);
  }

	function make_query($condition)
  {
	  $this->db->select('unit.*');
    $this->db->from('unit');
    $this->db->where($condition);
    if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
    {
     $this->db->like('name', $_POST["search"]["value"]);
    }
   $this->db->order_by('id','desc');
     
  }
  
  public function make_datatables($condition){
    //print_r($condition); die;
	  $this->make_query($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return  $query->result_array();
  }

  function get_all_data($condition)
  {
	$this->db->select('unit.*');
    $this->db->from('unit');
    $this->db->where($condition);
    if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('name', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
	   return $this->db->count_all_results();
  }

  function get_filtered_data($condition){
	  $this->make_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  
  public function get_unit($condition){
    $this->db->select('*');
    $this->db->from('unit');
    $this->db->where($condition);
    $query =  $this->db->get()->row();
		return $query;
  }

  public function update_unit($data, $unitID){
    $this->db->where('id',$unitID);
    return $this->db->update('unit', $data);
  }

  public function delete_unit($condition){
    $this->db->where($condition);
    return $query = $this->db->delete('unit');
  }
}