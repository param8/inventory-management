<?php 

class Product_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}

  function get_product($condition){
    $this->db->select('products.*,unit.name');
    $this->db->from('products');
    $this->db->join('unit','unit.id = products.unit_type','left'); 
    $this->db->where($condition);
    $this->db->where_in('products.location',explode(',',$this->session->userdata('location')));
    $query =  $this->db->get()->row();
		return $query;
  }

  function get_checkProduct($condition){
    $this->db->select('products.*,unit.name');
    $this->db->from('products');
    $this->db->join('unit','unit.id = products.unit_type','left'); 
    $this->db->where($condition);
    $query =  $this->db->get()->row();
		return $query;
  }

  function get_products($condition){
    $this->db->select('products.*,unit.name as unit_name');
    $this->db->from('products');
    $this->db->join('unit','unit.id = products.unit_type','left'); 
    $this->db->where($condition);
    $this->db->where_in('products.location',explode(',',$this->session->userdata('location')));
    $query =  $this->db->get();
		return $query->result_array();
  }
  function get_returnable($conditon){
    $this->db->select('retutnable_products.*,projects.name,users.name as issue_name');
    $this->db->from('retutnable_products');
    $this->db->join('projects','projects.id = retutnable_products.project','left'); 
    $this->db->join('users','users.id = retutnable_products.issue_by','left'); 
    $this->db->where($conditon);
    $query =  $this->db->get();
		return $query->row();
  }

  function get_returnables($conditon){
    $this->db->select('retutnable_products.*,projects.name,users.name as issue_name');
    $this->db->from('retutnable_products');
    $this->db->join('projects','projects.id = retutnable_products.project','left'); 
    $this->db->join('users','users.id = retutnable_products.issue_by','left'); 
    $this->db->where($conditon);
    $this->db->order_by('id','DESC');
    $query =  $this->db->get();
		return $query->result_array();
  }

  function updateReturnable($data,$id){
    $this->db->where('id',$id);
    return $this->db->update('retutnable_products', $data);
  }

  function get_unit($condition){
    $this->db->select('*');
    $this->db->from('unit');
    $this->db->where($condition);
    $query =  $this->db->get()->row();
		return $query;
  }

  function get_units(){
    $this->db->select('*');
    $this->db->from('unit');
    $this->db->where('status',1);
    $query =  $this->db->get();
		return $query->result_array();
     //echo $this->db->last_query();die;
  }

	function make_query($condition)
  {
	  $this->db->select('products.*,unit.name, users.name as user_name,location.location as productLocation');
    $this->db->from('products');
    $this->db->join('unit','unit.id = products.unit_type','left');    
    $this->db->join('users','users.id = products.added_by','left'); 
    $this->db->join('location','location.id = products.location','left');
    $this->db->where($condition);
    if($this->session->userdata('locationID')){
      $this->db->where('products.location',$this->session->userdata('locationID'));
    }else{
      $this->db->where_in('products.location',explode(',',$this->session->userdata('location'))); 
    }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('part_name', $_POST["search"]["value"]);
    $this->db->or_like('products.product_code', $_POST["search"]["value"]);
    $this->db->or_like('unit.name', $_POST["search"]["value"]);
    $this->db->or_like('products.specification', $_POST["search"]["value"]);
    $this->db->or_like('products.make', $_POST["search"]["value"]);
    $this->db->or_like('products.model', $_POST["search"]["value"]);
    $this->db->or_like('location.location', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
  
  }
    function make_datatables($condition){
	  $this->make_query($condition,);
    if(isset($_POST['length']) && !empty($_POST['length']))
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array();
  }
  function get_filtered_data($condition){
	  $this->make_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data($condition)
  {
    $this->db->select('products.*,unit.name, users.name as user_name,location.location as productLocation');
    $this->db->from('products');
    $this->db->join('unit','unit.id = products.unit_type','left');    
    $this->db->join('users','users.id = products.added_by','left'); 
    $this->db->join('location','location.id = products.location','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('part_name', $_POST["search"]["value"]);
    $this->db->or_like('products.product_code', $_POST["search"]["value"]);
    $this->db->or_like('unit.name', $_POST["search"]["value"]);
    $this->db->or_like('products.specification', $_POST["search"]["value"]);
    $this->db->or_like('products.make', $_POST["search"]["value"]);
    $this->db->or_like('products.model', $_POST["search"]["value"]);
    $this->db->or_like('location.location', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
	   return $this->db->count_all_results();
  }
  function store_product($data){
   return $this->db->insert('products', $data);
  }

  function store_return($data){
    return $this->db->insert('retutnable_products', $data);
  }

   function update_product($data,$productId){
    $this->db->where('id',$productId);
    return $this->db->update('products', $data);
   }

   function delete_product($condiotion){
    $this->db->where($condiotion);
    return $this->db->delete('products');
   }

}