<?php 

class Project_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}

    public function store_project($data){
        //echo "hi";die;
        return $this->db->insert('projects', $data);
    }

    public function store_expence($data){
      return $this->db->insert('expences', $data);
    }

    function make_query($condition)
  {
	  $this->db->select('projects.*,users.name as userName,location.location as projectLocation');
    $this->db->from('projects');
    $this->db->join('users', 'users.id=projects.added_by','left');
    $this->db->join('location', 'location.id=projects.location','left');
    $this->db->where($condition);
    if($this->session->userdata('ProjectlocationID')){
      $this->db->where('projects.location',$this->session->userdata('ProjectlocationID'));
    }else{
      $this->db->where_in('projects.location',explode(',',$this->session->userdata('location'))); 
    }
    //$this->db->where_in('projects.location',explode(',',$this->session->userdata('location')));
   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('projects.name', $_POST["search"]["value"]);
    $this->db->or_like('projects.client', $_POST["search"]["value"]);
    $this->db->or_like('projects.budget', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('location.location', $_POST["search"]["value"]);
   }
   $this->db->order_by('projects.id','desc');
  
  }

  function make_expquery($condition)
  {
	  $this->db->select('expences.*,projects.name');
    $this->db->from('expences');
    $this->db->join('projects','expences.project = projects.id','left');
    $this->db->where($condition);
    $this->db->where_in('projects.location',explode(',',$this->session->userdata('location'))); 
    if($this->session->userdata('projectID')){
      $this->db->where('project',$this->session->userdata('projectID'));
    }
    if($this->session->userdata('ProjectlocationID')){
      $this->db->where('projects.location',$this->session->userdata('ProjectlocationID'));
    }
    if($this->session->userdata('from_date')){
      $first_date = $this->session->userdata('from_date');
      $this->db->where("DATE_FORMAT(date,'%Y-%m-%d') >='$first_date'");
   }

    if($this->session->userdata('to_date')){
      $second_date = $this->session->userdata('to_date');
      $this->db->where("DATE_FORMAT(date,'%Y-%m-%d') <='$second_date'");
    }
    

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('particular', $_POST["search"]["value"]);
    $this->db->or_like('expence', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
  
  }
  
    function make_datatables($condition){
        $this->make_query($condition,);
      if($_POST["length"] != -1)
        {
            $this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function make_expdatatables($condition){
      $this->make_expquery($condition,);
    if($_POST["length"] != -1)
      {
          $this->db->limit($_POST['length'], $_POST['start']);
      }
      $query = $this->db->get();
      return $query->result_array();
       //echo $this->db->last_query();die;
  }

    function get_filtered_data($condition){
        $this->make_query($condition);
        $query = $this->db->get();
        return $query->num_rows();
        //echo $this->db->last_query();die;
    }

    function get_expfiltered_data($condition){
      $this->make_expquery($condition);
      $query = $this->db->get();
      return $query->num_rows();
      //echo $this->db->last_query();die;
  }

    function get_project($condition){
        $this->db->select('*');
        $this->db->from('projects');
        $this->db->where($condition);
        $query =  $this->db->get();
         return $query->row();
      }
      
      function get_projects($condition){
        $this->db->select('projects.*,users.name as userName,location.location as projectLocation');
        $this->db->from('projects');
        $this->db->join('users', 'users.id=projects.added_by','left');
        $this->db->join('location', 'location.id=projects.location','left');
        $this->db->where($condition);
        $this->db->where_in('projects.location',explode(',',$this->session->userdata('location')));
        $query =  $this->db->get();
         return $query->result_array();
      }

      function get_excel_projects($condition){
        $this->db->select('projects.*,users.name as userName,location.location as projectLocation');
        $this->db->from('projects');
        $this->db->join('users', 'users.id=projects.added_by','left');
        $this->db->join('location', 'location.id=projects.location','left');
        $this->db->where($condition);
        if($this->session->userdata('ProjectlocationID')){
          $this->db->where('projects.location',$this->session->userdata('ProjectlocationID'));
        }else{
          $this->db->where_in('projects.location',explode(',',$this->session->userdata('location'))); 
        }
        //$this->db->where_in('projects.location',explode(',',$this->session->userdata('location')));
        $query =  $this->db->get();
         return $query->result_array();
      }

      function get_expences($condition){
        $this->db->select('expences.*,projects.name');
        $this->db->from('expences');
        $this->db->join('projects','expences.project = projects.id','left');
        $this->db->where($condition);
        $query =  $this->db->get();
         return $query->result_array();
      }
    function get_all_data($condition)
    {
      $this->db->select('projects.*,users.name as userName,location.location as projectLocation');
      $this->db->from('projects');
      $this->db->join('users', 'users.id=projects.added_by','left');
      $this->db->join('location', 'location.id=projects.location','left');
      $this->db->where($condition);
      $this->db->where_in('projects.location',explode(',',$this->session->userdata('location')));
     if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
     {
      $this->db->like('projects.name', $_POST["search"]["value"]);
      $this->db->or_like('projects.client', $_POST["search"]["value"]);
      $this->db->or_like('projects.budget', $_POST["search"]["value"]);
      $this->db->or_like('users.name', $_POST["search"]["value"]);
      $this->db->or_like('location.location', $_POST["search"]["value"]);
     }
       $this->db->order_by('projects.id','desc');
        return $this->db->count_all_results();
         //echo $this->db->last_query();die;
    }

    function get_all_expdata($condition)
    {
        $this->db->select('expences.*');
        $this->db->from('expences');
        $this->db->where($condition);
    
       if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
       {
        // $this->db->like('name', $_POST["search"]["value"]);
        // $this->db->or_like('client', $_POST["search"]["value"]);
        // $this->db->or_like('budget', $_POST["search"]["value"]);
        // $this->db->or_like('remark', $_POST["search"]["value"]);
       }
       $this->db->order_by('id','desc');
        return $this->db->count_all_results();
         //echo $this->db->last_query();die;
    }

    function update_project($data,$projectId){
        //print_r($projectId);die;
        $this->db->where('id',$projectId);
        return $this->db->update('projects', $data);
       }
    
       function delete_project($condiotion){
        $this->db->where($condiotion);
        return $this->db->delete('projects');
       }
       

       function delete_expence($condition){
        $this->db->where($condition);
        return $this->db->delete('expences');
       }

}