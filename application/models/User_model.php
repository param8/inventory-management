<?php 

class User_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}
  function get_role($condition){
    $this->db->select('*');
    $this->db->from('roles');
    $this->db->where($condition);
    $query =  $this->db->get()->row();
		return $query;
  }
  function get_roles($condition){
    $this->db->select('*');
    $this->db->from('roles');
    $this->db->where($condition);
    $query =  $this->db->get();
    return  $query->result_array();
  }
  function get_condition_roles(){
    $this->db->select('*');
    $this->db->from('roles');
    $this->db->where('id <>', 1);
    $this->db->where('id <>', 4);
    $query =  $this->db->get();
      return $query->result_array();
      //echo $this->db->last_query();die;
  }
  
  function get_user($condition){
    $this->db->select('*');
    $this->db->from('users');
    $this->db->where($condition);
    $query =  $this->db->get()->row();
		return $query;
  }

  function get_users($condition){
    $this->db->select('*');
    $this->db->from('users');
    $this->db->where($condition);
    $query =  $this->db->get();
		return $query->result();
  }

  function get_vendor($condition){
    $this->db->select('users.*,vendor.*');
    $this->db->from('users');
    $this->db->join('vendor','vendor.vendor_id = users.id','left');
    $this->db->where($condition);
    $query =  $this->db->get()->row();
		return $query;
  }

  // function get_vendor($condition){
  //   $this->db->select('*');
  //   $this->db->from('vendor');
  //   $this->db->where($condition);
  //   $query =  $this->db->get()->row();
	// 	return $query;
  // }

	

  function make_role_query($condition)
  {
	  $this->db->select('*');
    $this->db->from('roles');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('role', $_POST["search"]["value"]);
   }
   $this->db->order_by('role','asc');
     
  }
   function make_role_datatables($condition){
    $this->make_role_query($condition,);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	 return  $query->result_array();
      //$this->db->last_query();
   }
   function get_role_filtered_data($condition){
	  $this->make_role_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_role_data($condition)
  {
    $this->db->select('*');
    $this->db->from('roles');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('role', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
	   return $this->db->count_all_results();
  }

   function make_query($condition)
   {
     $this->db->select('users.*,cities.name as cityName,states.name as stateName,roles.role as role_name');
     $this->db->from('users');
     $this->db->join('cities','cities.id = users.city','left');
     $this->db->join('states','states.id = users.state','left');
     $this->db->join('roles','roles.id = users.user_type','left');
     $this->db->where($condition);
 
    if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
    {
     $this->db->like('users.name', $_POST["search"]["value"]);
     $this->db->or_like('users.email', $_POST["search"]["value"]);
     $this->db->or_like('users.phone', $_POST["search"]["value"]);
     $this->db->or_like('cities.name', $_POST["search"]["value"]);
     $this->db->or_like('states.name', $_POST["search"]["value"]);
    }
    $this->db->order_by('users.id','desc');
      
   }

    function make_datatables($condition){
	  $this->make_query($condition,);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	 return  $query->result_array();
      //$this->db->last_query();
  }

  function get_filtered_data($condition){
	  $this->make_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data($condition)
  {
	$this->db->select('users.*,cities.name as cityName,states.name as stateName,roles.role as role_name');
    $this->db->from('users');
    $this->db->join('cities','cities.id = users.city','left');
    $this->db->join('states','states.id = users.state','left');
    $this->db->join('roles','roles.id = users.user_type','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('users.email', $_POST["search"]["value"]);
    $this->db->or_like('users.phone', $_POST["search"]["value"]);
    $this->db->or_like('cities.name', $_POST["search"]["value"]);
    $this->db->or_like('states.name', $_POST["search"]["value"]);
   }
   $this->db->order_by('users.id','desc');
	   return $this->db->count_all_results();
  }

  public function store_role($data){
    return $this->db->insert('roles',$data);
    // echo $this->db->last_query();
  }
  public function update_role($data,$roleID){
    $this->db->where('id',$roleID);
    return $this->db->update('roles',$data);
  }

  public function delete_role($condition){
    $this->db->where($condition);
    return $query = $this->db->delete('roles');
  }
  public function store_user($data){
    return $this->db->insert('users',$data);
  }

  public function update_user($data,$userID){
    $this->db->where('id',$userID);
    return $this->db->update('users',$data);
  }

  public function delete_user($condition){
    $this->db->where($condition);
    return $query = $this->db->delete('users');
  }
  public function insert_vendor($data){
    return $this->db->insert('vendor',$data);
  }

  public function update_vendor($data,$userID){
    $this->db->where('vendor_id',$userID);
    return $this->db->update('vendor',$data);
    // echo $this->db->last_query();die;
  }

  
  // public function store_permission($data){
  //   return $this->db->insert('permission',$data);
  // }

  // public function update_permission($data,$staffID){
  //   $this->db->where('userID',$staffID);
  //   return $this->db->update('permission',$data);
  // }


}